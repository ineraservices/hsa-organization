-- noinspection SqlNoDataSourceInspectionForFile

truncate table organization_edge;
truncate table organization_node;

-- insert top root node
insert into organization_node (internal_entry_uuid, node_type, node_name,  json_object) values
('11111111-1111-1111-1111-111111111111', 'Top', 'Top',  '{
  "node_id": "11111111-1111-1111-1111-111111111111",
  "node_name": "Top",
  "node_type": "Top"}');

-- insert Sverige Country node
insert into organization_node (internal_entry_uuid, node_type, node_name,  json_object) values
('3ebda4ad-79fc-4c43-ac0c-089d3ba93501', 'Country', 'Sverige',  '{
  "uuid": "3ebda4ad-79fc-4c43-ac0c-089d3ba93501",
  "createTimestamp": "2018-09-20T11:12:02.507Z[UTC]",
  "creatorUUID": "994d4026-9c57-4228-b4b7-e7efd81a6806",
  "data": {
    "type": "Country",
    "name": "Sverige",
    "isoCode": "SE"
  }
}');

-- connect root with country
insert into organization_edge ($from_id, $to_id) values (
                                                            (select $node_id from organization_node where node_name = 'Top'),
                                                            (select $node_id from organization_node where node_name = 'Sverige'));



-- add some counties
insert into organization_node (internal_entry_uuid, node_type, node_name,  json_object) values
('71367294-f2fd-4544-873e-6fc591e1bd36', 'County', 'Stockholms län', '{"uuid":"71367294-f2fd-4544-873e-6fc591e1bd36","createTimestamp":"2018-09-20T11:12:02.508Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Stockholms län"}}'),
('30582572-8dc3-44c8-9721-90a9177033d4', 'County', 'Uppsala län', '{"uuid":"30582572-8dc3-44c8-9721-90a9177033d4","createTimestamp":"2018-09-20T11:12:02.509Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Uppsala län"}}'),
('7b7ac165-7854-4c4c-9fd3-addc6fccddaf', 'County', 'Sörmlands län', '{"uuid":"7b7ac165-7854-4c4c-9fd3-addc6fccddaf","createTimestamp":"2018-09-20T11:12:02.510Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Sörmlands län"}}'),
('6307e277-1d04-4c6b-8afb-f32836267203', 'County', 'Östergötlands län', '{"uuid":"6307e277-1d04-4c6b-8afb-f32836267203","createTimestamp":"2018-09-20T11:12:02.511Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Östergötlands län"}}'),
('742f9f5a-283c-438b-b8ec-6a5cbd29ac73', 'County', 'Jönköpings län', '{"uuid":"742f9f5a-283c-438b-b8ec-6a5cbd29ac73","createTimestamp":"2018-09-20T11:12:02.512Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Jönköpings län"}}'),
('1b367aa0-9add-4b25-a8db-751d22db11c8', 'County', 'Kronobergs län', '{"uuid":"1b367aa0-9add-4b25-a8db-751d22db11c8","createTimestamp":"2018-09-20T11:12:02.513Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Kronobergs län"}}'),
('5e9a2ce2-2fe7-4fe8-afd5-fe3753c9c5fb', 'County', 'Kalmar län', '{"uuid":"5e9a2ce2-2fe7-4fe8-afd5-fe3753c9c5fb","createTimestamp":"2018-09-20T11:12:02.514Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Kalmar län"}}'),
('4edc7e0a-b137-4775-b2b9-7d136cb31b2a', 'County', 'Gotlands län', '{"uuid":"4edc7e0a-b137-4775-b2b9-7d136cb31b2a","createTimestamp":"2018-09-20T11:12:02.515Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Gotlands län"}}'),
('a0723bde-7929-4ed3-aff4-d162bdfa4dfd', 'County', 'Blekinge län', '{"uuid":"a0723bde-7929-4ed3-aff4-d162bdfa4dfd","createTimestamp":"2018-09-20T11:12:02.516Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Blekinge län"}}'),
('29d9b0e2-40e5-4239-bd0b-ee3c3b21d099', 'County', 'Skåne län', '{"uuid":"29d9b0e2-40e5-4239-bd0b-ee3c3b21d099","createTimestamp":"2018-09-20T11:12:02.517Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Skåne län"}}'),
('c95ef655-f53d-4604-8f6f-6cdb72deb3a5', 'County', 'Hallands län', '{"uuid":"c95ef655-f53d-4604-8f6f-6cdb72deb3a5","createTimestamp":"2018-09-20T11:12:02.518Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Hallands län"}}'),
('24ab6fc6-23ae-450e-b416-29233c033c1c', 'County', 'Västra Götalands län', '{"uuid":"24ab6fc6-23ae-450e-b416-29233c033c1c","createTimestamp":"2018-09-20T11:12:02.519Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Västra Götalands län"}}'),
('35249932-a24e-48b7-a11a-0080b18dad01', 'County', 'Värmlands län', '{"uuid":"35249932-a24e-48b7-a11a-0080b18dad01","createTimestamp":"2018-09-20T11:12:02.520Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Värmlands län"}}'),
('09fee888-b4c2-42bd-98c1-587ee13b1021', 'County', 'Örebro län', '{"uuid":"09fee888-b4c2-42bd-98c1-587ee13b1021","createTimestamp":"2018-09-20T11:12:02.521Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Örebro län"}}'),
('87e6b64f-ec35-49ce-9c4b-5ce0dd600884', 'County', 'Västmanlands län', '{"uuid":"87e6b64f-ec35-49ce-9c4b-5ce0dd600884","createTimestamp":"2018-09-20T11:12:02.522Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Västmanlands län"}}'),
('448862e2-230d-4db1-9dd3-fd98ff27fe33', 'County', 'Dalarnas län', '{"uuid":"448862e2-230d-4db1-9dd3-fd98ff27fe33","createTimestamp":"2018-09-20T11:12:02.523Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Dalarnas län"}}'),
('1bb184fc-2ad6-4be5-9c54-f04018d6c547', 'County', 'Gävleborgs län', '{"uuid":"1bb184fc-2ad6-4be5-9c54-f04018d6c547","createTimestamp":"2018-09-20T11:12:02.524Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Gävleborgs län"}}'),
('14091e24-a30d-4bc0-b43e-c331839f5acb', 'County', 'Västernorrlands län', '{"uuid":"14091e24-a30d-4bc0-b43e-c331839f5acb","createTimestamp":"2018-09-20T11:12:02.525Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Västernorrlands län"}}'),
('027ef783-a300-4ad3-807a-aa28ba876174', 'County', 'Jämtlands län', '{"uuid":"027ef783-a300-4ad3-807a-aa28ba876174","createTimestamp":"2018-09-20T11:12:02.526Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Jämtlands län"}}'),
('c874e7e5-090b-4287-8987-782ea1a9a564', 'County', 'Västerbottens län', '{"uuid":"c874e7e5-090b-4287-8987-782ea1a9a564","createTimestamp":"2018-09-20T11:12:02.527Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Västerbottens län"}}'),
('d50a9c5c-880d-451d-b7db-2e0864332cee', 'County', 'Norrbottens län', '{"uuid":"d50a9c5c-880d-451d-b7db-2e0864332cee","createTimestamp":"2018-09-20T11:12:02.528Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Norrbottens län"}}'),
('2bf47d16-f3cf-4edd-bec0-efa113b5650c', 'County', 'Öster län', '{"uuid":"2bf47d16-f3cf-4edd-bec0-efa113b5650c","createTimestamp":"2018-09-20T11:12:02.529Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"County","name":"Öster län"}}');

-- connect country with the counties
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Stockholms län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Uppsala län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Sörmlands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Östergötlands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Jönköpings län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Kronobergs län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Kalmar län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Gotlands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Blekinge län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Skåne län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Hallands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Västra Götalands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Värmlands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Örebro län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Västmanlands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Dalarnas län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Gävleborgs län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Västernorrlands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Jämtlands län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Västerbottens län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Norrbottens län'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Öster län'));

-- add organizations
insert into organization_node (internal_entry_uuid, node_type, node_name,  json_object) values
('3aaf2fdd-3951-455e-afca-d26fca790207', 'Organization', 'Tussilagon', '{"uuid":"3aaf2fdd-3951-455e-afca-d26fca790207","createTimestamp":"2018-09-20T11:12:02.530Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"SE-25-01","name":"Tussilagon","orgNo":"56565656-0101","hsaHealthCareProvider":false,"o":"Tussilagon","postalAddress":{"addressLine":["Blomstergatan 1","972 31","Luleå"]},"mail":"hello@tussilago.se","hsaHpt":"HPT Producent HSA Admin;Luleå;v 2.0;Mall 4.0","telephoneNumber":["+46900001"]}}'),
('1bdc0925-33c0-489c-877e-74e0ad39ea59', 'Organization', 'Snödroppen', '{"uuid":"1bdc0925-33c0-489c-877e-74e0ad39ea59","createTimestamp":"2018-09-20T11:12:02.530Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"SE-25-02","name":"Snödroppen","orgNo":"56565656-0202","hsaHealthCareProvider":false,"o":"Snödroppen","postalAddress":{"addressLine":["Blomstergatan 2","972 32","Luleå"]},"mail":"hello@snowdrop.se","hsaHpt":"HPT Producent HSA Admin;Luleå;v 2.0;Mall 4.0","telephoneNumber":["+46900002"]}}'),
('5ce91947-94ec-481c-bdd8-bda67414d65e', 'Organization', 'Stockholms läns landsting', '{"uuid":"5ce91947-94ec-481c-bdd8-bda67414d65e","createTimestamp":"2018-09-20T11:12:02.530Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"SE-01-01","name":"Stockholms läns landsting","orgNo":"01010101-0101","hsaHealthCareProvider":false,"o":"Stockholms läns landsting","postalAddress":{"addressLine":["Mynttorget 1","111 11","Stockholm"]},"mail":"aaa@test.se","hsaHpt":"HPT Producent HSA Admin;Stockholm;v 2.0;Mall 4.0","telephoneNumber":["+46800000"]}}'),
('2c2ca7fd-26db-4fe9-a12c-9170436ba5f2', 'Organization', 'Landstinget Dalarna', '{"uuid":"2c2ca7fd-26db-4fe9-a12c-9170436ba5f2","createTimestamp":"2018-09-20T11:12:02.531Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"SE-20-01","name":"Landstinget Dalarna","orgNo":"20202020-0101","hsaHealthCareProvider":false,"o":"Landstinget Dalarna","postalAddress":{"addressLine":["Myntgatan 45","791 30","Falun"]},"mail":"bbb@test.se","hsaHpt":"HPT Producent HSA Admin;Dalarna;v 2.0;Mall 4.0","telephoneNumber":["+4624300000"]}}'),
('8eab6074-2c06-43ab-b096-817bde3b185c', 'Organization', 'Landstinget i Värmland', '{"uuid":"8eab6074-2c06-43ab-b096-817bde3b185c","createTimestamp":"2018-09-20T11:12:02.532Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"SE-17-01","name":"Landstinget i Värmland","orgNo":"17171717-0101","hsaHealthCareProvider":false,"o":"Landstinget i Värmland","postalAddress":{"addressLine":["Bryggareg 12","653 40","Karlstad"]},"mail":"ccc@test.se","hsaHpt":"HPT Producent HSA Admin;Värmland;v 2.0;Mall 4.0","telephoneNumber":["+46563000000"]}}'),
('5ad14783-4361-4619-85f1-a0bc27fb9938', 'Organization', 'Region Gotland', '{"uuid":"5ad14783-4361-4619-85f1-a0bc27fb9938","createTimestamp":"2019-05-16T11:12:02.532Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"GOT1234LAND","name":"Region Gotland","orgNo":"19171719-0101","hsaHealthCareProvider":false,"o":"Region Gotland","postalAddress":{"addressLine":["Gotlandsvägen 12","121 55","Visby"]},"mail":"regionen@regiongotland.se","hsaHpt":"HPT Producent HSA Admin;Gotland;v 2.0;Mall 4.0","telephoneNumber":["+46078000000"]}}'),
('1df57352-8c03-4e38-adc0-c3e2a3ff0e32', 'Organization', 'Nordic MedTest', '{"uuid":"1df57352-8c03-4e38-adc0-c3e2a3ff0e32","createTimestamp":"2018-09-20T11:12:02.533Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"TSTNMT2321000156-0001","name":"Nordic MedTest","countyCode":"17","municipalityCode":"1780","orgNo":"8888888-8888","postalAddress":{"addressLine":["Bryggareg 11","653 40","Karlstad"]},"telephoneNumber":["+46555123456"],"hsaHealthCareProvider":false,"o":"Värmlands län","mail":"ddd@test.se","hsaHpt":"HPT Producent HSA Admin;Värmland;v 2.0;Mall 4.0"}}'),
('877f0b8b-9848-4f69-a667-31cb4436eda3', 'Organization', 'Region Blekinge Synk', '{"uuid":"877f0b8b-9848-4f69-a667-31cb4436eda3","createTimestamp":"2019-03-20T11:12:02.533Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"TSTBLT2321000156-0001","name":"Region Blekinge Synk","countyCode":"17","municipalityCode":"1781","orgNo":"8888371-4242","postalAddress":{"addressLine":["Hästövägen 11","371 42","Karlskrona"]},"telephoneNumber":["+46450123456"],"hsaHealthCareProvider":false,"o":"Blekinge län","mail":"ddd@blekinge.se","hsaHpt":"HPT Producent HSA Admin;Värmland;v 2.0;Mall 4.0"}}'),
('877f0b8b-9848-4f69-a667-31cb444edre3', 'Organization', 'Region HSA Test', '{"uuid":"877f0b8b-9848-4f69-a667-31cb444edre3","createTimestamp":"2019-03-20T11:12:02.533Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"TSTBLT2321000156-00011","name":"Region HSA Test","countyCode":"17","municipalityCode":"1781","orgNo":"8888371-4242","postalAddress":{"addressLine":["Hästövägen 11","371 42","Karlskrona"]},"telephoneNumber":["+46450123456"],"hsaHealthCareProvider":false,"o":"test län","mail":"ddd@blekinge.se","hsaHpt":"HPT Producent HSA Admin;Värmland;v 2.0;Mall 4.0"}}'),
('68eb5e0a-c850-45c0-8122-2db13e4cd7c5', 'Organization', 'Skövde kommun', '{"uuid":"68eb5e0a-c850-45c0-8122-2db13e4cd7c5","createTimestamp":"2019-05-10T11:12:02.533Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"PL2120001710-0001","name":"Skövde kommun","countyCode":"14","municipalityCode":"1796","orgNo":"212000-1710","postalAddress":{"addressLine":["Skövde kommun","Fredsgatan 4","541 83","SKÖVDE"]},"telephoneNumber":["+464503407430"],"hsaHealthCareProvider":true,"o":"Västra Götalands län","mail":"skovde.kommun@skovde.kommun.test","hsaHpt":"HPT Producent;Skövde kommun;v 5.0;Mall 4.0.1"}}'),
('24994727-6443-4f54-8415-9dca8a3ea462', 'Organization', 'Region Halland', '{"uuid":"24994727-6443-4f54-8415-9dca8a3ea462","createTimestamp":"2019-09-16T11:12:02.533Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"SE2321000115-O00001","name":"Region Halland","countyCode":"13","orgNo":"232100-0115","hsaHealthCareProvider":true,"o":"Region Halland","hsaDirectoryContact":"INFO.Hallandskatalogen@lthalland.se","hsaIdPrefix":"SE2321000115-T","labeledURI":"www.lthalland.se","hsaIdCounter":"27001"}}'),
('dd699457-9a18-409b-8252-5ab0428485de', 'Organization', 'Svensk Organisation', '{"uuid":"dd699457-9a18-409b-8252-5ab0428485de","createTimestamp":"2018-12-03T13:38:02.530Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Organization","hsaIdentity":"TSTNMT2321003456-0001","name":"Svensk Organisation","orgNo":"8888888-9999","hsaHealthCareProvider":false,"o":"Svensk Organisation","postalAddress":{"addressLine":["Bryggareg 13","653 40","Karlstad"]},"mail":"eee@test.se","hsaHpt":"HPT Producent HSA Admin;Värmland;v 2.0;Mall 4.0","telephoneNumber":["+4656300000"]}}'),
('8cbeb8cc-9ef4-42d3-9223-b8201f022b23', 'Organization', 'Nordic Organization Maximum', '{"uuid":"8cbeb8cc-9ef4-42d3-9223-b8201f022b23","createTimestamp":"2019-02-12T14:37:02.999Z[UTC]","creatorUUID":"1df57352-8c03-4e38-adc0-c3e2a3ff0e32","data":{"name":"Nordic Organization Maximum","type":"Organization","hsaIdentity":"TSTNCO1919191919-0001","countyCode":"17","hsaHealthCareProvider":false,"municipalityCode":"1780","orgNo":"123412-1234","postalAddress":{"addressLine":["Bryggareg 11","653 40","Karlstad"]},"telephoneNumber":["+4654150200"],"c":"Sverige","description":"Beskrivningstext","displayOption":"1#2","endDate":"20200212225900Z","geographicalCoordinates":{"coordinate":{"type":"XY","x":"6581164","y":"1630250"}},"hsaAdminComment":"Hsa-adminkommentar","hsaAltText":"Hund","hsaAuthenticationMethod":"UNKNOWN_VALUE","hsaDestinationIndicator":"04","hsaDirectoryContact":"contact@test.se","hsaHealthCareArea":"Karlstad","hsaHealthCareUnitManager":"Mattias","hsaHpt":"yes Box","hsaIdPrefix":"666","hsaIdCounter":"6","hsaJpegLogotype":"77+977+977+977+9ABBKRklGAAEBAAABAAEAAO+/ve+/vQBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQHvv73vv70AQwEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB77+977+9ABEIAAEAAQMBIgACEQEDEQHvv73vv70AHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL77+977+9AO+/vRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDLvv73vv73vv70II0Lvv73vv70VUu+/ve+/vSQzYnLvv70JChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl677+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+9AB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC++/ve+/vQDvv70RAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjLvv70IFELvv73vv73vv73vv70JIzNS77+9FWJy77+9ChYkNO+/vSXvv70XGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eu+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/vQAMAwEAAhEDEQA/AO+/ve+/vSjvv73vv70A77+977+9","hsaResponsibleHealthCareProvider":"VC Gripen","hsaSwitchboardNumber":"+4654110022","hsaVisitingRuleReferral":"yes","hsaVisitingRules":"När som","hsaVpwInformation1":"Text till hsaVpwInformation1","hsaVpwInformation2":"Text till hsaVpwInformation2","hsaVpwInformation3":"Text till hsaVpwInformation3","hsaVpwInformation4":"Text till hsaVpwInformation4","hsaVpwWebPage":"http://www.oktyr.se","jpegPhoto":"77+977+977+977+9ABBKRklGAAEBAAABAAEAAO+/ve+/vQBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQHvv73vv70AQwEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB77+977+9ABEIAAEAAQMBIgACEQEDEQHvv73vv70AHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL77+977+9AO+/vRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDLvv73vv73vv70II0Lvv73vv70VUu+/ve+/vSQzYnLvv70JChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl677+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+977+9AB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC++/ve+/vQDvv70RAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjLvv70IFELvv73vv73vv73vv70JIzNS77+9FWJy77+9ChYkNO+/vSXvv70XGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eu+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/ve+/vQAMAwEAAhEDEQA/AO+/ve+/vSjvv73vv70A77+977+9","l":"Tyrskogen","labeledURI":"http://google.se","mail":"org.test@nmt.test","postalCode":"12345","route":"Andra höger","smsTelephoneNumber":"+46701212121","startDate":"20190212","street":"Tyrstugevägen 1","businessClassificationCode":["1214"],"careType":["01"],"dropInHours":[{"timeSpan":{"fromDay":"Söndag","toDay":"Måndag","fromTime2":"01:00","toTime2":"23:59","comment":"Tystnad","fromDate":"20190212","toDate":"20190213"}}],"facsimileTelephoneNumber":["+4654100100"],"financingOrganization":["232100-0181"],"hsaBusinessType":["04"],"hsaHealthCareUnitMember":["Jaja men"],"hsaSyncId":["S-ID"],"hsaTelephoneNumber":["+4654222333"],"hsaTextTelephoneNumber":["+4654222444"],"hsaVisitingRuleAge":["13#Underåret"],"hsaVpwNeighbouringObject":["TESTHSAADMIN-106S"],"management":["Privat"],"mobile":["+46701122334"],"ouShort":["organisationen"],"seeAlso":["Harry Potter"],"surgeryHours":[{"timeSpan":{"fromDay":"Måndag","toDay":"Fredag","fromTime2":"01:00","toTime2":"23:59","comment":"Laga","fromDate":"20190212","toDate":"20190213"}}],"telephoneHours":[{"timeSpan":{"fromDay":"Söndag","toDay":"Måndag","fromTime2":"01:00","toTime2":"23:59","comment":"Pratafonen","fromDate":"20190212","toDate":"20190213"}}],"unitPrescriptionCode":["01110016521"],"visitingHours":[{"timeSpan":{"fromDay":"Söndag","toDay":"Måndag","fromTime2":"01:00","toTime2":"23:59","comment":"Pratafonen","fromDate":"20190212","toDate":"20190213"}}]}}')

-- connect organization with the counties/countries
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Sverige'),
        (select $node_id from organization_node where node_name = 'Svensk Organisation'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Blekinge län'),
        (select $node_id from organization_node where node_name = 'Region Blekinge Synk'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Dalarnas län'),
        (select $node_id from organization_node where node_name = 'Landstinget Dalarna'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Gotlands län'),
        (select $node_id from organization_node where node_name = 'Region Gotland'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Hallands län'),
        (select $node_id from organization_node where node_name = 'Region Halland'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Norrbottens län'),
        (select $node_id from organization_node where node_name = 'Snödroppen'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Norrbottens län'),
        (select $node_id from organization_node where node_name = 'Tussilagon'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Stockholms län'),
        (select $node_id from organization_node where node_name = 'Stockholms läns landsting'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Värmlands län'),
        (select $node_id from organization_node where node_name = 'Landstinget i Värmland'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Värmlands län'),
        (select $node_id from organization_node where node_name = 'Nordic MedTest'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Värmlands län'),
        (select $node_id from organization_node where node_name = 'Nordic Organization Maximum'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Västra Götalands län'),
        (select $node_id from organization_node where node_name = 'Skövde kommun'));


-- Add units
insert into organization_node (internal_entry_uuid, node_type, node_name,  json_object) values
('92b1f660-de03-40b1-afcc-74b59f75efc2', 'Unit', 'Nordic Unit', '{"uuid":"92b1f660-de03-40b1-afcc-74b59f75efc2","createTimestamp":"2018-09-20T11:12:02.534Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"hsaHealthCareProvider":false,"hsaHealthCareUnit":false,"type":"Unit","hsaIdentity":"TSTNMT2321000156-0002","name":"Nordic Unit","telephoneNumber":["+46555123456"],"hsaSyncId":["HSASYNCID1234567890"]}}'),
('7a73d615-2ffa-45f4-9092-af7c88b73c5d', 'Unit', 'enhet utan underordnade objekt', '{"uuid":"7a73d615-2ffa-45f4-9092-af7c88b73c5d","createTimestamp":"2018-09-20T11:12:02.535Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"hsaHealthCareProvider":false,"hsaHealthCareUnit":false,"type":"Unit","hsaIdentity":"TSTNMT2324252627-0001","name":"enhet utan underordnade objekt","telephoneNumber":["+46555123456"]}}'),
('70a7346a-f331-49cc-825c-52e2f337a387', 'Unit', 'enhet med underordnade objekt', '{"uuid":"70a7346a-f331-49cc-825c-52e2f337a387","createTimestamp":"2018-09-20T11:12:02.536Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"hsaHealthCareProvider":false,"hsaHealthCareUnit":false, "type":"Unit","hsaIdentity":"TEST999999994-0001","name":"enhet med underordnade objekt","telephoneNumber":["+46555123454"]}}'),
('6a396087-e2cc-4953-b2b1-fffa42ba0f2e', 'Unit', 'Nordic Unit4', '{"uuid":"6a396087-e2cc-4953-b2b1-fffa42ba0f2e","createTimestamp":"2019-01-22T11:03:02.999Z[UTC]","creatorUUID":"1df57352-8c03-4e38-adc0-c3e2a3ff0e32","data":{"type":"Unit","hsaIdentity":"TSTNMT232100156-0001","name":"Nordic Unit4","postalAddress":{"addressLine":["Hemvägen 4 65591 Karlstad"]},"postalCode":"65591","countyCode":"17","telephoneNumber":["+4654100000"],"mail":"testNordicUnit4@nordicunit4.se","labeledURI":"http://www.test.se","hsaHealthCareUnit":true,"hsaResponsibleHealthCareProvider":"TSTNMT2321000156-1012"}}'),
('95d0c924-8062-4ec2-b470-09bbc07f9e1e', 'Unit', 'underenhet', '{"uuid":"95d0c924-8062-4ec2-b470-09bbc07f9e1e","createTimestamp":"2018-12-04T11:12:02.536Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"hsaHealthCareProvider":false,"hsaHealthCareUnit":false, "type":"Unit","hsaIdentity":"TEST999999994-0011","name":"underenhet","telephoneNumber":["+46555123466"]}}'),
('82153d75-719a-40a3-bd1b-3818b99b2c01', 'Unit', 'Svensk enhet', '{"uuid":"82153d75-719a-40a3-bd1b-3818b99b2c01","createTimestamp":"2018-12-03T14:50:02.536Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"hsaHealthCareProvider":false,"hsaHealthCareUnit":false, "type":"Unit","hsaIdentity":"TSTNMT2321003456-0002","name":"Svensk enhet","telephoneNumber":["+4654545454"]}}')

-- Connect Units
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Nordic MedTest'),
        (select $node_id from organization_node where node_name = 'Nordic Unit'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Nordic MedTest'),
        (select $node_id from organization_node where node_name = 'enhet utan underordnade objekt'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Nordic MedTest'),
        (select $node_id from organization_node where node_name = 'enhet med underordnade objekt'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Nordic MedTest'),
        (select $node_id from organization_node where node_name = 'Nordic Unit4'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'enhet med underordnade objekt'),
        (select $node_id from organization_node where node_name = 'underenhet'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Svensk Organisation'),
        (select $node_id from organization_node where node_name = 'Svensk enhet'));

-- Add Functions
insert into organization_node (internal_entry_uuid, node_type, node_name,  json_object) values
('2a674d48-2579-4592-9b36-48a4ced6cde4', 'Function', 'Svensk funktion', '{"uuid":"2a674d48-2579-4592-9b36-48a4ced6cde4","createTimestamp":"2018-12-04T14:35:02.536Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Function","hsaIdentity":"TSTNMT2321003456-1000","name":"Svensk funktion","street":"Svenskavägen 42","telephoneNumber":["+4688888888"]}}'),
('9dd1a2eb-d4cb-44de-92ce-ef9723241ca6', 'Function', 'underenhet funktion', '{"uuid":"9dd1a2eb-d4cb-44de-92ce-ef9723241ca6","createTimestamp":"2018-12-04T15:26:02.536Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Function","hsaIdentity":"TSTNMT2321003456-1002","name":"underenhet funktion","countyCode":"01","businessClassificationCode":["1035"]}}'),
('afad03a9-e7f0-44b4-9913-33036751ab44', 'Function', 'Nordic Unit Function', '{"uuid":"afad03a9-e7f0-44b4-9913-33036751ab44","createTimestamp":"2018-12-04T14:49:02.536Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Function","hsaIdentity":"TSTNMT2321003456-1001","name":"Nordic Unit Function","orgNo":"123456-1234"}}'),
('0492e34a-1cb3-4a76-8082-fc98a5e71658', 'Function', 'Nordic Function', '{"uuid":"0492e34a-1cb3-4a76-8082-fc98a5e71658","createTimestamp":"2018-12-05T11:20:02.536Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Function","hsaIdentity":"TSTNMT2321003456-1003","name":"Nordic Function","telephoneNumber":["+4633441144"],"mobile":["+46709999999"],"route":"Andra höger bakom hörnet ovanför nedre trappan innanför dörren till vänster"}}')

--Connect functions
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Nordic MedTest'),
        (select $node_id from organization_node where node_name = 'Nordic Function'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Nordic Unit'),
        (select $node_id from organization_node where node_name = 'Nordic Unit Function'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'underenhet'),
        (select $node_id from organization_node where node_name = 'underenhet funktion'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Svensk enhet'),
        (select $node_id from organization_node where node_name = 'Svensk funktion'));

-- Add Employees
insert into organization_node (internal_entry_uuid, node_type, node_name,  json_object) values
('d4b1a084-0b30-429b-acb9-8feaee4f93db', 'Employee', 'Anna Fia Inera', '{"uuid":"d4b1a084-0b30-429b-acb9-8feaee4f93db","createTimestamp":"2019-05-10T14:23:35.908Z[UTC]","creatorUUID":"d4b1a084-0b30-429b-acb9-8feaee4f93db","data":{"type":"Employee","hsaIdentity":"EMP-1000000001","name":"Anna Fia Inera","fullName":"Anna Fia Inera","givenName":"Anna","middleName":"Fia","personalIdentityNumber":"198101010011","sn":"Inera"}}'),
('d4b1a084-0b30-429b-acb9-8feaee4f93da', 'Employee', 'Censur X Sekretessblom', '{"uuid":"d4b1a084-0b30-429b-acb9-8feaee4f93da","createTimestamp":"2019-05-10T14:23:35.908Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Employee","hsaIdentity":"EMP-1000000002","name":"Censur X Sekretessblom","fullName":"Censur X Sekretessblom","givenName":"Censur","middleName":"X","personalIdentityNumber":"198101010012","sn":"Sekretessblom","description":"Är en hemlig hen","mobile":["+4677777777"]}}'),
('58b3ddbf-db81-4ea5-b678-0933eca47701', 'Employee', 'Sven O Person', '{"uuid":"58b3ddbf-db81-4ea5-b678-0933eca47701","createTimestamp":"2019-08-08T15:15:35.908Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Employee","hsaIdentity":"EMP-2000000002","name":"Sven O Person","fullName":"Sven O Person","givenName":"Sven","middleName":"O","personalIdentityNumber":"198202020022","sn":"Person","title":"Organisatör","hsaTelephoneNumber":["+46555123456"]}}'),
('d21eb603-af32-46b5-a3b3-bceb4ce2755e', 'Employee', 'Sven Ou Person', '{"uuid":"d21eb603-af32-46b5-a3b3-bceb4ce2755e","createTimestamp":"2019-08-08T15:15:45.928Z[UTC]","creatorUUID":"994d4026-9c57-4228-b4b7-e7efd81a6806","data":{"type":"Employee","hsaIdentity":"EMP-3000000003","name":"Sven Ou Person","fullName":"Sven Ou Person","givenName":"Sven","middleName":"Ou","personalIdentityNumber":"198303030033","sn":"Person"}}')

--Connect employees
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'underenhet'),
        (select $node_id from organization_node where node_name = 'Censur X Sekretessblom'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Nordic Organization Maximum'),
        (select $node_id from organization_node where node_name = 'Anna Fia Inera'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Svensk Organisation'),
        (select $node_id from organization_node where node_name = 'Sven O Person'));
insert into organization_edge ($from_id, $to_id)
values ((select $node_id from organization_node where node_name = 'Svensk enhet'),
        (select $node_id from organization_node where node_name = 'Sven Ou Person'));

-- select * from organization_node order by node_type
-- select * from organization_edge