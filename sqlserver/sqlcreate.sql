-- create
use master;
drop database if exists dhsa;
go
create database dhsa;
go
use dhsa;

drop table if exists organization_node;
create table organization_node (
    internal_entry_uuid nvarchar(100) not null primary key,
    node_type nvarchar(100) not null,
    node_name nvarchar(100) not null,
    json_object nvarchar(max)
        constraint [json_object record should be formatted as json] check (isjson(json_object)=1)
) as node;

drop table if exists organization_edge;
create table organization_edge as edge;
