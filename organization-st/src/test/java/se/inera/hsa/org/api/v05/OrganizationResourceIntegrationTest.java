package se.inera.hsa.org.api.v05;

import io.restassured.http.ContentType;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import se.inera.hsa.jsonb.JsonbUtil;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.model.version_0_5.Organization;
import se.inera.hsa.org.validation.version_0_3.ModificationTypes;
import se.inera.hsa.org.validation.version_0_3.OrganizationModification;
import se.inera.hsa.org.validation.version_0_3.PatchModification;
import se.inera.hsa.org.validation.version_0_3.PatchModificationList;

import javax.ws.rs.core.UriBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static se.inera.hsa.org.validation.version_0_3.PatchModification.ADD;
import static se.inera.hsa.org.validation.version_0_3.PatchModification.DELETE;

class OrganizationResourceIntegrationTest extends ResourceIT {


    @Test
    @DisplayName("Move request on organization should fail")
    @Disabled("Move not implemented yet")
    void assertChangePathFailOnRequestWithExistingPath() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        // get org
        EntityResponse<Organization> organizationRespone = given().when().get(builder.build()).as(organiztionType);

        // modification request
        OrganizationModification modification = new OrganizationModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = organizationRespone.getMeta().path;

        given().contentType(ContentType.JSON).body(modification).when().put(organizationRespone.getMeta().path)
                .then().statusCode(403);
    }


    @Test
    @DisplayName("Changing name on organization should fail")
    void assertPatchFailOnRequestWithNameChange() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        PatchModificationList mods = new PatchModificationList();
        List<PatchModification> list = new ArrayList<>();
        list.add(new PatchModification(DELETE, "name"));
        list.add(new PatchModification(ADD, "name", "NEW_NAME_" + Math.abs(new Random().nextInt())));
        mods.setModifications(list);
        // modification request

        given().contentType(ContentType.JSON).body(mods).when().patch(builder.build())
                .then().statusCode(400);
    }

    @Test
    @DisplayName("Removing postalAddress on organization should fail")
    void assertMergeFailOnRequestWithNoPostalAddress() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        // get org
        PatchModificationList mods = new PatchModificationList();
        List<PatchModification> list = new ArrayList<>();
        list.add(new PatchModification(DELETE, "postalAddress"));
        mods.setModifications(list);

        given()
            .contentType(ContentType.JSON)
            .body(mods)
        .when()
            .patch(builder.build())
        .then()
            .statusCode(400);
    }

    @Test
    @DisplayName("Removing mail on organization should fail")
    void assertPatchFailOnRequestWithNoMail() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        PatchModificationList mods = new PatchModificationList();
        List<PatchModification> list = new ArrayList<>();
        list.add(new PatchModification(DELETE, "mail"));
        mods.setModifications(list);

        given()
            .contentType(ContentType.JSON)
            .body(mods)
        .when()
            .patch(builder.build())
        .then()
            .statusCode(400);
    }

    @Test
    @DisplayName("Removing telephonenumber on organization should fail")
    void assertPatchFailOnRequestWithNoTelephoneNumber() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        PatchModificationList mods = new PatchModificationList();
        List<PatchModification> list = new ArrayList<>();
        list.add(new PatchModification(DELETE, "telephoneNumber"));
        mods.setModifications(list);

        given()
            .contentType(ContentType.JSON)
            .body(mods)
        .when()
            .patch(builder.build())
        .then()
            .statusCode(400);
    }


    @Test
    @DisplayName("Removing orgNo should fail")
    void assertRemovingOrgNoFail() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        PatchModificationList mods = new PatchModificationList();
        List<PatchModification> list = new ArrayList<>();
        list.add(new PatchModification(DELETE, "orgNo"));
        mods.setModifications(list);

        given()
            .contentType(ContentType.JSON)
            .body(mods)
        .when()
            .patch(builder.build())
        .then()
            .statusCode(400);
    }

    @Test
    @DisplayName("Get non existing Organization")
    void assertGetNonExistingOrganizationReturns404() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(NON_EXISTING_ORGANIZATION);

        // get org
        given().when().get(builder.build()).then().statusCode(404);
    }
}
