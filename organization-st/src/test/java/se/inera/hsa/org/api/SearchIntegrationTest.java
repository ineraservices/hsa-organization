package se.inera.hsa.org.api;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.Random;

import javax.ws.rs.core.UriBuilder;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import se.inera.hsa.org.model.unions.Unit;
import se.inera.hsa.org.model.version_0_3.Function;
import se.inera.hsa.org.model.version_0_3.SearchResult;

class SearchIntegrationTest extends ResourceIT {

    @Test
    @DisplayName("Should be able to find the newly created Unit in the free-text search result. \n " +
            "The result should not include units which don't have the free-text in their searchable attributes.")
    void assertResultSuccessWhenFreeTextSearchingAfterAddingUnit() {

        // first unit
        Unit unit = newUnit();

        String searchString = String.format("@hsaIdentity:%s", unit.hsaIdentity);

        // path to parent
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post unit
        given().body(unit).when().post(builder.build()).then().statusCode(201);

        // do free text search and check if unit is listed in search result
        SearchResult searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // assert search result not empty
        assertEquals(1, searchResult.units.size());

        // assert unit is available in search result.
        assertEquals(unit.hsaIdentity, searchResult.units.get(0).hsaIdentity);

        given().when().delete(searchResult.units.get(0).metaData.path).then().statusCode(204);

        // re-fetch after remove, should return empty unit list
        searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // assert search result not empty
        assertTrue(searchResult.units.isEmpty());

    }

    @Test
    @DisplayName("Should be able to find the newly created Unit with syncId in the free-text search result. \n " +
            "The result should not include units which don't have matching syncId.")
    void assertResultSuccessWhenFreeTextSearchingAfterAddingUnitWithSyncId() {

        // first unit
        Unit unit = newUnit();

        // change name to something unique to retrieve through search
        unit.hsaSyncId = Collections.singletonList("SYNC_ID_" + +Math.abs(new Random().nextInt()));

        String searchString = String.format("@hsaSyncId:(%s)", String.join(" ", unit.hsaSyncId));

        // path to parent
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post unit
        given().body(unit).when().post(builder.build().getPath());

        //do free text search and check if unit is listed in search result
        SearchResult searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // assert search result not empty
        assertEquals(0, searchResult.countries.size(), "countries list should be empty");
        assertEquals(0, searchResult.counties.size(), "counties list should be empty");
        assertEquals(0, searchResult.organizations.size(), "organizations list should be empty");
        assertEquals(0, searchResult.functions.size(), "function list should be empty");
        assertEquals(1, searchResult.units.size(), "unit list should contain exactly one unit");

        // assert unit is available in the search result.
        assertEquals(unit.hsaSyncId, searchResult.units.get(0).hsaSyncId, "syncId should match");

        given().when().delete(searchResult.units.get(0).metaData.path).then().statusCode(204);

        // re-fetch after remove, should return empty unit list
        searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // assert search result not empty
        assertTrue(searchResult.units.isEmpty());

    }

    @Test
    @DisplayName("Should be able to find the newly created Function with syncId in the free-text search result. \n " +
            "The result should not include objects which don't have matching syncId.")
    void assertResultSuccessWhenFreeTextSearchingAfterAddingFunctionWithSyncId() {

        // first unit
        Function function = newFunction();

        // change name to something unique to retrieve through search
        function.hsaSyncId = Collections.singletonList("SYNC_ID_" + +Math.abs(new Random().nextInt()));

        String searchString = String.format("@hsaSyncId:(%s)", String.join(" ", function.hsaSyncId));

        // path to parent
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        // post function
        given().body(function).when().post(builder.build().getPath()).then().statusCode(201);

        // do free text search and check if function is listed in search result
        SearchResult searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // assert search result not empty
        assertEquals(0, searchResult.countries.size(), "countries list should be empty");
        assertEquals(0, searchResult.counties.size(), "counties list should be empty");
        assertEquals(0, searchResult.organizations.size(), "organizations list should be empty");
        assertEquals(0, searchResult.units.size(), "units list should be empty");
        assertEquals(1, searchResult.functions.size(), "function list should contain exactly one function");

        // assert function is available in the search result.
        assertEquals(function.hsaSyncId, searchResult.functions.get(0).hsaSyncId, "syncId should match");

        given().when().delete(searchResult.functions.get(0).metaData.path).then().statusCode(204);

        // re-fetch after remove, should return empty function list
        searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // assert search result not empty
        assertTrue(searchResult.functions.isEmpty());

    }

    @Test
    @DisplayName("Should be able to find a single object by complete name (exact match)")
    void assertResultSuccessWhenFreeTextSearchingOnName() {

        SearchResult searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(EXISTING_COUNTY).build().getPath()).as(SearchResult.class);

        assertEquals(1, searchResult.counties.size(), "counties list should contain exactly one county");

        assertEquals(EXISTING_COUNTY, searchResult.counties.get(0).name, "Name should match");

    }

    @Test
    @DisplayName("Should be able to find a single object by slightly mispelled name (phonetic match)")
    void assertResultSuccessWhenFreeTextSearchingOnPhoneticName() {

        String searchString = "Dalarnas+laen";

        SearchResult searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        assertEquals(1, searchResult.counties.size(), "counties list should contain exactly one county");

        assertEquals(EXISTING_COUNTY, searchResult.counties.get(0).name, "Name should match");

        searchString = "Dalarnas+lan";

        searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        assertEquals(1, searchResult.counties.size(), "counties list should contain exactly one county");

        assertEquals(EXISTING_COUNTY, searchResult.counties.get(0).name, "Name should match");
    }

    @Test
    @DisplayName("Should be able to find by hsaSyncId")
    void assertResultSuccessWhenFreeTextSearchingOnHsaSyncId() {

        String searchString = "@hsaSyncId:HSASYNCID*";

        // do free text search and check if function is listed in search result
        SearchResult searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // assert search result  one not empty
        assertTrue(searchResult.units.size() > 0, "units list should not be empty");

        searchResult.units.forEach(unit -> assertNotNull(unit.hsaSyncId));

    }
}
