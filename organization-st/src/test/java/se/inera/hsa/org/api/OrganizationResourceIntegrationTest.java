package se.inera.hsa.org.api;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import se.inera.hsa.org.model.version_0_3.Organization;
import se.inera.hsa.org.model.version_0_3.OrganizationResponse;
import se.inera.hsa.org.validation.version_0_3.ModificationTypes;
import se.inera.hsa.org.validation.version_0_3.OrganizationModification;

import javax.ws.rs.core.UriBuilder;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class OrganizationResourceIntegrationTest extends ResourceIT {


    @Test
    @DisplayName("Move request on organization should fail")
    void assertChangePathFailOnRequestWithExistingPath() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        // get org
        OrganizationResponse organizationRespone = given().when().get(builder.build()).as(OrganizationResponse.class);

        // modification request
        OrganizationModification modification = new OrganizationModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = organizationRespone.metaData.path;

        given().contentType(ContentType.JSON).body(modification).when().put(organizationRespone.metaData.path)
                .then().statusCode(403);
    }
    

    @Test
    @DisplayName("Changing name on organization should fail")
    void assertMergeFailOnRequestWithNameChange() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        // get org
        OrganizationResponse organizationResponse = given().when().get(builder.build()).as(OrganizationResponse.class);

        Organization organization = new Organization();
        organization.hsaIdentity = organizationResponse.hsaIdentity;
        organization.type = organizationResponse.type;
        organization.hsaHpt = organizationResponse.hsaHpt;
        organization.mail = organizationResponse.mail;
        organization.name = organizationResponse.name;
        organization.orgNo = organizationResponse.orgNo;
        organization.telephoneNumber = organizationResponse.telephoneNumber;
        
        organization.name = "NEW_NAME_" + Math.abs(new Random().nextInt());

        // modification request
        OrganizationModification modification = new OrganizationModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.organization = organization;

        given().contentType(ContentType.JSON).body(modification).when().put(organizationResponse.metaData.path)
                .then().statusCode(400);
    }
    
    @Test
    @DisplayName("Removing postalAddress on organization should fail")
    void assertMergeFailOnRequestWithNoPostalAddress() {
        
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);
        
        // get org
        OrganizationResponse organizationResponse = given().when().get(builder.build()).as(OrganizationResponse.class);
        
        Organization organization = new Organization();
        organization.hsaIdentity = organizationResponse.hsaIdentity;
        organization.type = organizationResponse.type;
        organization.hsaHpt = organizationResponse.hsaHpt;
        organization.mail = organizationResponse.mail;
        organization.name = organizationResponse.name;
        organization.orgNo = organizationResponse.orgNo;
        organization.telephoneNumber = organizationResponse.telephoneNumber;
        organization.postalAddress = null;
        
        // modification request
        OrganizationModification modification = new OrganizationModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.organization = organization;
        
        given()
            .contentType(ContentType.JSON)
            .body(modification)
        .when()
            .put(organizationResponse.metaData.path)
        .then()
            .statusCode(400);
    }
    
    @Test
    @DisplayName("Removing mail on organization should fail")
    void assertMergeFailOnRequestWithNoMail() {
        
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);
        
        // get org
        OrganizationResponse organizationResponse = given().when().get(builder.build()).as(OrganizationResponse.class);
        
        Organization organization = new Organization();
        organization.hsaIdentity = organizationResponse.hsaIdentity;
        organization.type = organizationResponse.type;
        organization.hsaHpt = organizationResponse.hsaHpt;
        organization.mail = null;
        organization.name = organizationResponse.name;
        organization.orgNo = organizationResponse.orgNo;
        organization.telephoneNumber = organizationResponse.telephoneNumber;
        organization.postalAddress = organizationResponse.postalAddress;
        
        // modification request
        OrganizationModification modification = new OrganizationModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.organization = organization;
        
        given()
            .contentType(ContentType.JSON)
            .body(modification)
        .when()
            .put(organizationResponse.metaData.path)
        .then()
            .statusCode(400);
    }
    
    @Test
    @DisplayName("Removing telephonenumber on organization should fail")
    void assertMergeFailOnRequestWithNoTelephoneNumber() {
        
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);
        
        // get org
        OrganizationResponse organizationResponse = given().when().get(builder.build()).as(OrganizationResponse.class);
        
        Organization organization = new Organization();
        organization.hsaIdentity = organizationResponse.hsaIdentity;
        organization.type = organizationResponse.type;
        organization.hsaHpt = organizationResponse.hsaHpt;
        organization.mail = organizationResponse.mail;
        organization.name = organizationResponse.name;
        organization.orgNo = organizationResponse.orgNo;
        organization.telephoneNumber = null;
        organization.postalAddress = organizationResponse.postalAddress;
        
        // modification request
        OrganizationModification modification = new OrganizationModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.organization = organization;
        
        given()
            .contentType(ContentType.JSON)
            .body(modification)
        .when()
            .put(organizationResponse.metaData.path)
        .then()
            .statusCode(400);
    }


    @Test
    @DisplayName("Removing orgNo should fail")
    void assertRemovingOrgNoFail() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION);

        // get org
        OrganizationResponse organizationResponse = given().when().get(builder.build()).as(OrganizationResponse.class);

        assertNotNull(organizationResponse.orgNo);

        Organization organization = new Organization();
        organization.hsaIdentity = organizationResponse.hsaIdentity;
        organization.type = organizationResponse.type;
        organization.hsaHpt = organizationResponse.hsaHpt;
        organization.mail = organizationResponse.mail;
        organization.name = organizationResponse.name;
        organization.telephoneNumber = organizationResponse.telephoneNumber;
        organization.orgNo = null;

        // modification request
        OrganizationModification modification = new OrganizationModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.organization = organization;

        given().contentType(ContentType.JSON).body(modification).when().put(organizationResponse.metaData.path)
                .then().statusCode(400);
    }

    @Test
    @DisplayName("Get non existing Organization")
    void assertGetNonExistingOrganizationReturns404() {

        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(NON_EXISTING_ORGANIZATION);

        // get org
        given().when().get(builder.build()).then().statusCode(404);
    }
}
