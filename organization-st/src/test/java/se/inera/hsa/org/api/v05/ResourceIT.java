package se.inera.hsa.org.api.v05;

import java.lang.reflect.Type;
import java.util.Random;

import org.junit.jupiter.api.extension.ExtendWith;

import com.google.gson.reflect.TypeToken;

import se.inera.hsa.org.model.version_0_5.Employee;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.model.version_0_5.Function;
import se.inera.hsa.org.model.version_0_5.Organization;
import se.inera.hsa.org.model.version_0_5.Unit;
import se.inera.junit5.extension.RestAssuredExtensionV05;

@ExtendWith({RestAssuredExtensionV05.class})
abstract class ResourceIT {

    static final String COUNTRIES_PATH = "countries";
    static final String COUNTIES_PATH = "counties";
    static final String ORGANIZATIONS_PATH = "organizations";
    static final String UNITS_PATH = "units";
    static final String FUNCTIONS_PATH = "functions";

    static final String EXISTING_COUNTRY = "Sverige";
    static final String EXISTING_COUNTY = "Dalarnas län";
    static final String EXISTING_ORGANIZATION = "Landstinget Dalarna";
    static final String NON_EXISTING_ORGANIZATION = "Landstinget Nangalia";

    static final String SEARCH_PATH = "search";
    static final String FREE_TEXT_PATH = "free-text";
    static final String PARAMS_SEARCH_PATH = "by-params";

    static final String PARENT_PATH = "parent";

    static final String EXISTING_COUNTRY_ID = "3ebda4ad-79fc-4c43-ac0c-089d3ba93501";

    static final String EXISTING_COUNTY_ID = "448862e2-230d-4db1-9dd3-fd98ff27fe33";

    static final String EXISTING_ORGANIZATION_ID = "2c2ca7fd-26db-4fe9-a12c-9170436ba5f2";

    static final String NEW_HSA_ID_1 = "ST_TEST_01";

    static final String NEW_HSA_ID_2 = "ST_TEST_02";

    static final String NEW_HSA_ID_3 = "ST_TEST_03";

    static final String NEW_HSA_ID_4 = "ST_TEST_04";

    static final String NEW_HSA_ID_5 = "ST_TEST_05";

    static final Type unitType = new TypeToken<EntityResponse<Unit>>() {}.getType();
    static final Type functionType = new TypeToken<EntityResponse<Function>>() {}.getType();
    static final Type employeeType = new TypeToken<EntityResponse<Employee>>() {}.getType();
    static final Type organiztionType = new TypeToken<EntityResponse<Organization>>() {}.getType();

    // mock data
    Unit newUnit() {
        Unit unit = new Unit();
        unit.type = "Unit";
        unit.name = "Unit_" + Math.abs(new Random().nextInt());
        unit.hsaIdentity = "HSA_ID_" + Math.abs(new Random().nextInt());
        return unit;
    }

    Function newFunction() {
        Function function = new Function();
        function.type = "Function";
        function.name = "Function_" + Math.abs(new Random().nextInt());
        function.hsaIdentity = "HSA_ID_" + Math.abs(new Random().nextInt());
        return function;
    }
}


