package se.inera.hsa.org.api;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;
import java.util.Random;

import javax.ws.rs.core.UriBuilder;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import se.inera.hsa.org.model.unions.Unit;
import se.inera.hsa.org.model.unions.UnitResponse;
import se.inera.hsa.org.model.version_0_3.Function;
import se.inera.hsa.org.model.version_0_3.FunctionResponse;
import se.inera.hsa.org.validation.version_0_3.FunctionModification;
import se.inera.hsa.org.validation.version_0_3.ModificationTypes;

class FunctionResourceIntegrationTest extends ResourceIT {

    @Test
    @DisplayName("Http response code should be 201 created when adding Function to existing Organization, and data \ngenerated on " +
            "get should be present, on correct format and contain correct information")
    void assertResultSuccessWhenAddingFunctionToExistingOrganization() {

        Function function = newFunction();

        ZonedDateTime postingTime2 = ZonedDateTime.now();

        // post function
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        UriBuilder firstFunctionPathBuilder = UriBuilder.fromPath(given().body(function).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        FunctionResponse functionResponse = RestAssured.given().contentType(ContentType.JSON).when().get(firstFunctionPathBuilder.build()).as(FunctionResponse.class);

        assertNotNull(functionResponse);

        assertEquals(functionResponse.name, function.name);
        assertEquals(functionResponse.hsaIdentity, function.hsaIdentity);

        // remove function
        given().when().delete(functionResponse.metaData.path).then().statusCode(204);


        // createTimeStamp not null and after posting TimeStamp
        assertTrue(functionResponse.metaData.createTimestamp.isAfter(postingTime2));

        // createdBy not null nor empty
        assertFalse(functionResponse.metaData.creatorUUID.isEmpty());

        // createdByPath not null nor empty
        assertFalse(functionResponse.metaData.creatorPath.isEmpty());

        // node count correct, i.e number of descendants is 0 for function
        assertEquals(0, functionResponse.metaData.numSubordinates);
        assertEquals(0, functionResponse.metaData.numAllSubordinates);
    }

    @Test
    @DisplayName("The same object should be retrieved by id vs by path from object retrieved by id")
    void assertFunctionPathIsEqualToRelativeUrl() {

        Function function = newFunction();

        // post function
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);
        UriBuilder functionPathBuilder = UriBuilder.fromPath(given().body(function).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get function
        FunctionResponse functionResponse1 = RestAssured.given().contentType(ContentType.JSON).when()
                .get(functionPathBuilder.build()).as(FunctionResponse.class);

        // get the same function via metaData path
        FunctionResponse functionResponse2 = given().when().get(functionResponse1.metaData.path)
                .as(FunctionResponse.class);

        // remove function
        given().when().delete(functionResponse2.metaData.path).then().statusCode(204);

        // should be the same
        assertEquals(functionResponse1.metaData.createTimestamp, functionResponse2.metaData.createTimestamp);

    }

    @Test
    @DisplayName("Changing name using merge should fail")
    void assertMergeFailOnRequestWithNameChange() {

        Function function = newFunction();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        // post and get unit
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(function).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get function
        FunctionResponse functionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(FunctionResponse.class);
        function.name = "NEW_NAME_" + Math.abs(new Random().nextInt());
        FunctionModification modification = new FunctionModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.function = function;

        given().contentType(ContentType.JSON).body(modification).when().put(functionResponse.metaData.path)
                .then().statusCode(400);

        // remove
        given().when().delete(functionResponse.metaData.path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing hsa identity using merge should fail")
    void assertMergeSucceedOnAllowedAttributeChange() {

        Function function = newFunction();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        // post and get function path
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(function).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get function
        FunctionResponse functionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(FunctionResponse.class);
        function.hsaIdentity = "HSA_ID_" + Math.abs(new Random().nextInt());
        FunctionModification modification = new FunctionModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.function = function;

        // update
        given().contentType(ContentType.JSON).body(modification).when().put(functionResponse.metaData.path)
                .then().statusCode(400);

        // remove
        given().when().delete(functionResponse.metaData.path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing path on existing object should fail on request with new path equal to another existing path")
    void assertChangePathFailOnRequestWithExistingPath() {

        Function function = newFunction();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        // post and get function
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(function).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get function
        FunctionResponse functionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(FunctionResponse.class);

        FunctionModification modification = new FunctionModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = functionResponse.metaData.path;

        // changing path to already existing path is a bad request
        given().contentType(ContentType.JSON).body(modification).when().put(functionResponse.metaData.path)
                .then().statusCode(400);

        // remove
        given().when().delete(functionResponse.metaData.path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing path on existing object should fail on request with new parent not of allowed type")
    void assertChangePathFailOnRequestWithParentNotOfAllowedType() {

        Function function = newFunction();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        // post and get function
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(function).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get function
        FunctionResponse functionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(FunctionResponse.class);

        FunctionModification modification = new FunctionModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = UriBuilder.fromPath(COUNTRIES_PATH)
                .path(EXISTING_COUNTRY).path(COUNTIES_PATH).path(EXISTING_COUNTY).path(FUNCTIONS_PATH)
                .path(functionResponse.name).build().getPath();

        // changing path to already existing path is a bad request
        given().contentType(ContentType.JSON).body(modification).when().put(functionResponse.metaData.path)
                .then().statusCode(400);

        // remove
        given().when().delete(functionResponse.metaData.path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing name using move should succeed")
    void assertMoveSucceedOnRequestWithNameChange() {

        Function function = newFunction();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        // post and get function
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(function).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get function
        String newName = "NEW_NAME_" + Math.abs(new Random().nextInt());
        String newPath = builder.path(newName).build().getPath();
        FunctionResponse functionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(FunctionResponse.class);
        FunctionModification modification = new FunctionModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = newPath;

        // changing path to already existing path is a bad request
        UriBuilder putUriBuilder = UriBuilder.fromPath(given().contentType(ContentType.JSON).body(modification).when().put(functionResponse.metaData.path)
                .thenReturn().getHeader("Location"));

        assertNotEquals(uriBuilder.build().getPath(), putUriBuilder.build().getPath(), "paths should not match after successful move request");

        FunctionResponse updatedFunctionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(putUriBuilder.build()).as(FunctionResponse.class);

        assertEquals(functionResponse.uuid, updatedFunctionResponse.uuid, "uuid should match");
        assertEquals(functionResponse.hsaIdentity, updatedFunctionResponse.hsaIdentity, "hsaIdentity should match");
        assertEquals(newName, updatedFunctionResponse.name);

        assertEquals(newPath, updatedFunctionResponse.metaData.path);

        // remove
        given().when().delete(updatedFunctionResponse.metaData.path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing parent using move should succeed")
    void assertMoveSucceedOnRequestWithParentChange() {

        // first function
        Unit unit1 = newUnit();

        // build unit parent path
        UriBuilder unitBuilder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get function
        UriBuilder postBuilder1 = UriBuilder.fromPath(given().body(unit1).when().post(unitBuilder.build().getPath())
                .thenReturn().getHeader("Location"));

        UnitResponse unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(postBuilder1.build()).as(UnitResponse.class);

        // second function
        Function function = newFunction();

        // build function parent path
        UriBuilder functionBuilder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        // post and get function
        UriBuilder postBuilder2 = UriBuilder.fromPath(given().body(function).when().post(functionBuilder.build().getPath())
                .thenReturn().getHeader("Location"));

        FunctionResponse functionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(postBuilder2.build()).as(FunctionResponse.class);


        // get function
        String newPath = unitBuilder.path(unitResponse.name).path(FUNCTIONS_PATH).path(functionResponse.name).build().getPath();

        FunctionModification modification = new FunctionModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = newPath;

        // changing path to already existing path is a bad request
        UriBuilder putUriBuilder = UriBuilder.fromPath(given().contentType(ContentType.JSON).body(modification).when().put(functionResponse.metaData.path)
                .thenReturn().getHeader("Location"));

        FunctionResponse updatedFunctionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(putUriBuilder.build()).as(FunctionResponse.class);

        assertEquals(functionResponse.uuid, updatedFunctionResponse.uuid, "uuid should match");
        assertEquals(functionResponse.hsaIdentity, updatedFunctionResponse.hsaIdentity, "hsaIdentity should match");
        assertEquals(newPath, updatedFunctionResponse.metaData.path, "requested new path should match path to updated function");

        // remove
        given().when().delete(updatedFunctionResponse.metaData.path).then().statusCode(204);
        given().when().delete(unitResponse.metaData.path).then().statusCode(204);
    }

    @Test
    @DisplayName("Adding and removing attributes using merge should succeed")
    void assertAddingAndRemovingAttributesUsingMergeSucceed() {

        Function function = newFunction();
        function.mail = "kalle@hobbe.se";

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(FUNCTIONS_PATH);

        // post and get unit
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(function).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get function
        FunctionResponse functionResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(FunctionResponse.class);

        assertEquals(function.mail, functionResponse.mail, "mail should be present");

        function.mail = null;
        FunctionModification modification = new FunctionModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.function = function;

        given().contentType(ContentType.JSON).body(modification).when().put(functionResponse.metaData.path)
                .then().statusCode(204);

        // get again
        FunctionResponse functionResponseRemovedMail = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(FunctionResponse.class);

        assertNull(functionResponseRemovedMail.mail, "mail should be absent");

        function.mail = "pelle@hobbe.se";
        modification.function = function;

        given().contentType(ContentType.JSON).body(modification).when().put(functionResponseRemovedMail.metaData.path)
                .then().statusCode(204);

        // get again
        FunctionResponse functionResponseAddedMail = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(FunctionResponse.class);

        assertEquals(function.mail, functionResponseAddedMail.mail, "mail should be present");

        // remove
        given().when().delete(functionResponseAddedMail.metaData.path).then().statusCode(204);
    }
}
