package se.inera.hsa.org.api;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.UUID;

import javax.ws.rs.core.UriBuilder;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import io.restassured.http.ContentType;
import se.inera.hsa.org.model.unions.Unit;
import se.inera.hsa.org.model.version_0_3.SearchResult;
import se.inera.hsa.org.validation.version_0_3.ModificationTypes;
import se.inera.hsa.org.validation.version_0_3.UnitModification;

class SearchIndexVerificationTest extends ResourceIT {

    @Test
    @DisplayName("An index document should be created when creating an object, and updated when updating that same object")
    void assertIndexDocumentCreatedAndUpdated() {

        Unit unit = newUnit();

        // post unit
        given().body(unit).when().post(
                UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                        .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION)
                        .path(UNITS_PATH).build().getPath()).then().statusCode(201);

        // search for unit
        String searchString = String.format("@hsaIdentity:%s", unit.hsaIdentity);

        SearchResult searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // should find unit by hsaIdentity (index)
        assertTrue(searchResult.countries.isEmpty());
        assertTrue(searchResult.counties.isEmpty());
        assertTrue(searchResult.organizations.isEmpty());
        assertTrue(searchResult.functions.isEmpty());
        assertEquals(1, searchResult.units.size(), "search result shall contain one unit");
        assertEquals(searchResult.units.get(0).hsaIdentity, unit.hsaIdentity);

        String path = searchResult.units.get(0).metaData.path;

        unit.hsaSyncId = Collections.singletonList(UUID.randomUUID().toString());

        // search by sync id
        searchString = String.format("@hsaSyncId:%s", unit.hsaSyncId.get(0).replace("-", "*"));

        // should not find by syncId
        searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        assertTrue(searchResult.countries.isEmpty());
        assertTrue(searchResult.counties.isEmpty());
        assertTrue(searchResult.organizations.isEmpty());
        assertTrue(searchResult.functions.isEmpty());
        assertTrue(searchResult.units.isEmpty());

        // modify (add syncId)
        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        modification.unit = unit;

        // update
        given().contentType(ContentType.JSON).body(modification).when().put(path).then().statusCode(204);

        // should find by syncId
        searchString = String.format("@hsaSyncId:%s", unit.hsaSyncId.get(0).replace("-", "*"));

        searchResult = given().when().get(UriBuilder.fromPath(SEARCH_PATH).path(FREE_TEXT_PATH)
                .path(searchString).build()).as(SearchResult.class);

        // remove unit
        given().when().delete(path).then().statusCode(204);

        assertTrue(searchResult.countries.isEmpty());
        assertTrue(searchResult.counties.isEmpty());
        assertTrue(searchResult.organizations.isEmpty());
        assertTrue(searchResult.functions.isEmpty());
        assertEquals(1, searchResult.units.size(), "search result shall contain one unit");
        assertEquals(searchResult.units.get(0).hsaIdentity, unit.hsaIdentity);
        assertEquals(searchResult.units.get(0).hsaSyncId, unit.hsaSyncId);


    }

}
