package se.inera.hsa.org.api.v05;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static se.inera.hsa.org.validation.version_0_3.PatchModification.ADD;
import static se.inera.hsa.org.validation.version_0_3.PatchModification.DELETE;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ws.rs.core.UriBuilder;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import se.inera.hsa.jsonb.JsonbUtil;
import se.inera.hsa.org.model.version_0_5.EntityPersistence;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.model.version_0_5.Unit;
import se.inera.hsa.org.validation.version_0_3.ModificationTypes;
import se.inera.hsa.org.validation.version_0_3.PatchModification;
import se.inera.hsa.org.validation.version_0_3.PatchModificationList;
import se.inera.hsa.org.validation.version_0_3.UnitModification;

class UnitResourceIntegrationTest extends ResourceIT {

    @Test
    @DisplayName("Http response code should be 201 created when adding Unit to existing Unit, and data \ngenerated on " +
            "get should be present, on correct format and contain correct information")
    void assertResultSuccessWhenAddingUnitToExistingUnit() {
        Unit unit1 = newUnit();
        Unit unit2 = newUnit();

        ZonedDateTime postingTime2 = ZonedDateTime.now();

        // post first unit
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        UriBuilder firstUnitPathBuilder = UriBuilder.fromPath(given().body(unit1).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        EntityResponse<Unit> unitResponse1 = RestAssured.given().contentType(ContentType.JSON).when().get(firstUnitPathBuilder.build()).as(unitType);

        assertNotNull(unitResponse1);

        assertEquals(unitResponse1.getData().name, unit1.name);
        assertEquals(unitResponse1.getData().hsaIdentity, unit1.hsaIdentity);

        // post second unit
        UriBuilder secondUnitPathBuilder = UriBuilder.fromPath(given().body(unit2).when().post(builder.path(unit1.name)
                .path(UNITS_PATH).build().getPath()).thenReturn().getHeader("Location"));

        // get second unit
        EntityResponse<Unit> unitResponse2 = RestAssured.given().contentType(ContentType.JSON).when().get(secondUnitPathBuilder.build()).as(unitType);

        assertNotNull(unitResponse2);

        // refetch first unit to get numSubordinates and numAllSubordinates
        unitResponse1 = given().when().get(unitResponse1.getMeta().path).as(unitType);

        assertNotNull(unitResponse1);

        // remove units
        given().when().delete(unitResponse2.getMeta().path).then().statusCode(204);
        given().when().delete(unitResponse1.getMeta().path).then().statusCode(204);


        // createTimeStamp not null and after posting TimeStamp
        assertTrue(unitResponse1.getMeta().createTimestamp.isAfter(postingTime2));
        assertTrue(unitResponse2.getMeta().createTimestamp.isAfter(postingTime2));

        // createdBy not null nor empty
        assertFalse(unitResponse1.getMeta().creatorUUID.isEmpty());
        assertFalse(unitResponse2.getMeta().creatorUUID.isEmpty());

        // createdByPath not null nor empty
        assertFalse(unitResponse1.getMeta().creatorPath.isEmpty());
        assertFalse(unitResponse2.getMeta().creatorPath.isEmpty());

        // node count correct, i.e number of descendants is 1 for unit1 and 0 unit2
        assertEquals(1, unitResponse1.getMeta().numSubordinates);
        assertEquals(1, unitResponse1.getMeta().numAllSubordinates);
        assertEquals(0, unitResponse2.getMeta().numSubordinates);
        assertEquals(0, unitResponse2.getMeta().numAllSubordinates);

    }

    @Test
    @DisplayName("The same object should be retrieved by id vs by path from object retrieved by id")
    void assertUnitPathIsEqualToRelativeUrl() {

        Unit unit = newUnit();

        // post unit
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);
        UriBuilder firstUnitPathBuilder = UriBuilder.fromPath(given().body(unit).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get unit
        EntityResponse<Unit>unitResponse1 = RestAssured.given().contentType(ContentType.JSON).when().get(firstUnitPathBuilder.build()).as(unitType);

        // get the same unit via getMeta() path
        EntityResponse<Unit>unitResponse2 = given().when().get(unitResponse1.getMeta().path).as(unitType);

        // remove unit
        given().when().delete(unitResponse2.getMeta().path).then().statusCode(204);

        // should be the same
        assertEquals(unitResponse1.getMeta().createTimestamp, unitResponse2.getMeta().createTimestamp);

    }

    @Test
    @DisplayName("Changing path on existing object should fail on request with new path equal to another existing path")
    @Disabled("Move not yet implemented")
    void assertChangePathFailOnRequestWithExistingPath() {

        Unit unit = newUnit();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(unit).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get unit
        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);

        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = unitResponse.getMeta().path;

        // changing path to already existing path is a bad request
        given().contentType(ContentType.JSON).body(modification).when().put(unitResponse.getMeta().path)
                .then().statusCode(400);

        // remove
        given().when().delete(unitResponse.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing path on existing object should fail on request with new parent not of allowed type")
    @Disabled("Move not yet implemented")
    void assertChangePathFailOnRequestWithParentNotOfAllowedType() {

        Unit unit = newUnit();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(unit).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get unit
        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);

        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = UriBuilder.fromPath(COUNTRIES_PATH)
                .path(EXISTING_COUNTRY).path(COUNTIES_PATH).path(EXISTING_COUNTY).path(UNITS_PATH)
                .path(unitResponse.getData().name).build().getPath();

        // changing path to already existing path is a bad request
        given().contentType(ContentType.JSON).body(modification).when().put(unitResponse.getMeta().path)
                .then().statusCode(400);

        // remove
        given().when().delete(unitResponse.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing name using merge should fail")
    void assertPatchFailOnRequestWithNameChange() {

        Unit unit = newUnit();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        String path = builder.build().getPath();
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(unit).when().post(path)
                .thenReturn().getHeader("Location"));

        PatchModificationList mods = new PatchModificationList();
        List<PatchModification> list = new ArrayList<>();
        list.add(new PatchModification(DELETE, "name"));
        list.add(new PatchModification(ADD, "name", "NEW_NAME_" + Math.abs(new Random().nextInt())));
        mods.setModifications(list);

        // modification request
        RestAssured.given()
            .contentType(ContentType.JSON)
            .body(mods)
        .when()
            .patch(uriBuilder.build())
        .then().statusCode(400);
//
//        // get unit
        EntityResponse<Unit> unitResponse = RestAssured.given()
                                                           .contentType(ContentType.JSON)
                                                       .when()
                                                           .get(uriBuilder.build()).as(unitType);

        // remove
        given().when().delete(unitResponse.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Flagging a unit as healthCareProvider without supplying orgNo should fail")
    void assertPatchFailOnHealthCareProviderWithoutOrgNo() {

        Unit unit = newUnit();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        String path = builder.build().getPath();
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(unit).when().post(path)
                .thenReturn().getHeader("Location"));
System.out.println(path);
        PatchModificationList mods = new PatchModificationList();
        List<PatchModification> list = new ArrayList<>();
        list.add(new PatchModification(ADD, "hsaHealthCareProvider", true));
        mods.setModifications(list);

     // modification request
        RestAssured.given()
            .contentType(ContentType.JSON)
            .body(mods)
        .when()
            .patch(uriBuilder.build())
        .then().statusCode(400);

        // get unit
        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);
        // remove
        given().when().delete(unitResponse.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Flagging a unit as healthCareProvider and supplying orgNo should succeed")
    @Disabled("PUT IS NOT TO BE USED")
    void assertMergeSuccessOnHealthCareProviderWithOrgNo() {

        Unit unit = newUnit();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(unit).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        PatchModificationList mods = new PatchModificationList();
        List<PatchModification> list = new ArrayList<>();
        list.add(new PatchModification(ADD, "hsaHealthCareProvider", true));
        mods.setModifications(list);

        // get unit
        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);
        unit.hsaHealthCareProvider = true;
        unit.orgNo = "anything";
        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MERGE.name();
//        modification.unit = unit;

        given().contentType(ContentType.JSON).body(modification).when().put(unitResponse.getMeta().path)
                .then().statusCode(204);

        unit.hsaHealthCareProvider = false;
        modification.modificationType = ModificationTypes.MERGE.name();
//        modification.unit = unit;

        given().contentType(ContentType.JSON).body(modification).when().put(unitResponse.getMeta().path)
                .then().statusCode(204);

        // remove
        given().when().delete(unitResponse.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Adding and removing attributes using merge should succeed")
    @Disabled("PUT IS NOT TO BE USED")
    void assertAddingAndRemovingAttributesUsingMergeSucceed() {

        Unit unit = newUnit();
        unit.mail = "kalle@hobbe.se";

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(unit).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get unit
        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);

        assertEquals(unit.mail, unitResponse.getData().mail, "mail should be present");

        unit.mail = null;
        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MERGE.name();
//        modification.unit = unit;

        given().contentType(ContentType.JSON).body(modification).when().put(unitResponse.getMeta().path)
                .then().statusCode(204);

        // get again
        EntityResponse<Unit>unitResponseRemovedMail = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);

        assertNull(unitResponseRemovedMail.getData().mail, "mail should be absent");

        unit.mail = "pelle@hobbe.se";
//        modification.unit = unit;

        given().contentType(ContentType.JSON).body(modification).when().put(unitResponseRemovedMail.getMeta().path)
                .then().statusCode(204);

        // get again
        EntityResponse<Unit>unitResponseAddedMail = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);

        assertEquals(unit.mail, unitResponseAddedMail.getData().mail, "mail should be present");

        // remove
        given().when().delete(unitResponseAddedMail.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing hsa identity using merge should fail")
    @Disabled("PUT IS NOT TO BE USED")
    void assertMergeFailOnForbiddendAttributeChange() {

        Unit unit = newUnit();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit path
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(unit).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get unit
        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);
        unit.hsaIdentity = "HSA_ID_" + Math.abs(new Random().nextInt());
        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MERGE.name();
//        modification.unit = unit;

        // update
        given().contentType(ContentType.JSON).body(modification).when().put(unitResponse.getMeta().path)
                .then().statusCode(400);

        // remove
        given().when().delete(unitResponse.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing name using move should succeed")
    @Disabled("PUT IS NOT TO BE USED")
    void assertMoveSucceedOnRequestWithNameChange() {

        Unit unit = newUnit();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        UriBuilder uriBuilder = UriBuilder.fromPath(given().body(unit).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get unit
        String newName = "NEW_NAME_" + Math.abs(new Random().nextInt());
        String newPath = builder.path(newName).build().getPath();
        EntityResponse<Unit> unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);
        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = newPath;

        // changing path to already existing path is a bad request
        UriBuilder putUriBuilder = UriBuilder.fromPath(given().contentType(ContentType.JSON).body(modification).when().put(unitResponse.getMeta().path)
                .thenReturn().getHeader("Location"));

        assertNotEquals(uriBuilder.build().getPath(), putUriBuilder.build().getPath(), "paths should not match after successful move request");

        EntityResponse<Unit> updatedUnitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(putUriBuilder.build()).as(unitType);

        assertEquals(unitResponse.getUuid(), updatedUnitResponse.getUuid(), "uuid should match");
        assertEquals(unitResponse.getData().hsaIdentity, updatedUnitResponse.getData().hsaIdentity, "hsaIdentity should match");
        assertEquals(newName, updatedUnitResponse.getData().name);

        assertEquals(newPath, updatedUnitResponse.getMeta().path);

        // remove
        given().when().delete(updatedUnitResponse.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Changing parent using move should succeed")
    @Disabled("PUT IS NOT TO BE USED")
    void assertMoveSucceedOnRequestWithParentChange() {

        // first unit
        Unit unit1 = newUnit();

        // build parent path
        UriBuilder orgUriBuilder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        UriBuilder postBuilder1 = UriBuilder.fromPath(given().body(unit1).when().post(orgUriBuilder.build().getPath())
                .thenReturn().getHeader("Location"));

        EntityResponse<Unit>unitResponse1 = RestAssured.given().contentType(ContentType.JSON).when()
                .get(postBuilder1.build()).as(unitType);

        // second unit
        Unit unit2 = newUnit();

        // post and get unit
        UriBuilder postBuilder2 = UriBuilder.fromPath(given().body(unit2).when().post(orgUriBuilder.build().getPath())
                .thenReturn().getHeader("Location"));

        EntityResponse<Unit>unitResponse2 = RestAssured.given().contentType(ContentType.JSON).when()
                .get(postBuilder2.build()).as(unitType);


        // get unit
        String newPath = orgUriBuilder.path(unitResponse1.getData().name).path(UNITS_PATH).path(unitResponse2.getData().name).build().getPath();

        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MOVE.name();
        modification.newPath = newPath;

        // changing path to already existing path is a bad request
        UriBuilder putUriBuilder = UriBuilder.fromPath(given().contentType(ContentType.JSON).body(modification).when().put(unitResponse2.getMeta().path)
                .thenReturn().getHeader("Location"));

        EntityResponse<Unit>updatedUnitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(putUriBuilder.build()).as(unitType);

        assertEquals(unitResponse2.getUuid(), updatedUnitResponse.getUuid(), "uuid should match");
        assertEquals(unitResponse2.getData().hsaIdentity, updatedUnitResponse.getData().hsaIdentity, "hsaIdentity should match");
        assertEquals(newPath, updatedUnitResponse.getMeta().path);

        // remove
        given().when().delete(updatedUnitResponse.getMeta().path).then().statusCode(204);
        given().when().delete(unitResponse1.getMeta().path).then().statusCode(204);
    }

    @Test
    @DisplayName("Request containing invalid non mandatory attribute should result in silent removal of that same attribute")
    void assertNonRequiredFaultyAttributesRemovedSoftly() {

        Unit unit = newUnit();

        unit.mail = "X";

        // post unit
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);
        UriBuilder responseBuilder = UriBuilder.fromPath(given().body(unit).when().post(builder.build().getPath())
                .thenReturn().getHeader("Location"));

        // get unit
        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when().get(responseBuilder.build()).as(unitType);

        // remove unit
        given().when().delete(unitResponse.getMeta().path).then().statusCode(204);

        // should be the same
        assertNull(unitResponse.getData().mail, "mail with value X should have been removed");

    }

    @Test
    @DisplayName("Malformed incoming json should result in bad request")
    void assertEmptyJsonObjectCauses400() {

        // post unit shall not work
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);
        given().body("{}").when().post(builder.build().getPath()).then().statusCode(400);

    }

    @Test
    @DisplayName("Malformed incoming json with type other than \"Unit\" should result in bad request")
    void assertJsonObjectWithoutUnitTypeCauses400() {

        Unit unit = newUnit();
        unit.type = "Oonit";
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);
        given().body(unit).when().post(builder.build().getPath()).then().statusCode(400);

    }


    @Test
    @DisplayName("Modification request containing invalid non mandatory attribute should result in silent removal of that same attribute")
    @Disabled("PUT IS NOT TO BE USED")
    void assertNonRequiredFaultyAttributesRemovedSoftlyInModificationRequest() {

        Unit unit = newUnit();

        // build parent path
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH);

        // post and get unit
        Response response1 = given().body(unit).when().post(builder.build().getPath())
                .thenReturn();
        UriBuilder uriBuilder = UriBuilder.fromPath(response1.getHeader("Location"));

        // get unit
        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(uriBuilder.build()).as(unitType);

        // set invalid value on non-mandatory attribute
        UnitModification modification = new UnitModification();
        modification.modificationType = ModificationTypes.MERGE.name();
        Unit retrievedUnit = JsonbUtil.jsonb().fromJson(JsonbUtil.jsonb().toJson(unitResponse), Unit.class);
        retrievedUnit.mail = "X";
//        modification.unit = retrievedUnit;

        String path = unitResponse.getMeta().path;


        Response thenReturn = given().contentType(ContentType.JSON).body(modification).when().put(path)
                .thenReturn();
        String header = thenReturn.getHeader("Location");
        UriBuilder putUriBuilder = UriBuilder.fromPath(header);

        EntityResponse<Unit>updatedUnitResponse = RestAssured.given().contentType(ContentType.JSON).when()
                .get(putUriBuilder.build()).as(unitType);

        assertNull(updatedUnitResponse.getData().mail, "mail with value X should have been removed");

        // remove
        given().when().delete(updatedUnitResponse.getMeta().path).then().statusCode(204);
    }

//    @Disabled
//    @Test
//    @DisplayName("Get request containing invalid non mandatory attribute should contain erroneous data")
//    void assertFaultyAttributesRemainInGetRequest() {
//
//        // build parent path
//        UriBuilder builder = UriBuilder.fromPath(COUNTRIES_PATH).path(EXISTING_COUNTRY).path(COUNTIES_PATH)
//                .path(EXISTING_COUNTY).path(ORGANIZATIONS_PATH).path(EXISTING_ORGANIZATION).path(UNITS_PATH).path("Felaktig Dala-enhet");
//
//        // get unit
//        EntityResponse<Unit>unitResponse = RestAssured.given().contentType(ContentType.JSON).when()
//                .get(builder.build()).as(unitType);
//
//        assertEquals("X", unitResponse.getData().mail, "expected invalid value X for mail attribute");
//
//       Set<ConstraintViolation<UnitResponse>> constraintViolationSet =
//               Validation.buildDefaultValidatorFactory().getValidator().validate(unitResponse);
//
//       assertTrue(!constraintViolationSet.isEmpty());
//
//    }
}
