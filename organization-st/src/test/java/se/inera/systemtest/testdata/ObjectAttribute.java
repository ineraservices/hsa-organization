package se.inera.systemtest.testdata;

import java.util.ArrayList;
import java.util.List;

public class ObjectAttribute {
    private String type;
    private List<String> mandatory = new ArrayList<>();
    private List<String> optional = new ArrayList<>();

    public String getType() {
        return type;
    }

    public List<String> getMandatory() {
        return mandatory;
    }

    public List<String> getOptional() {
        return optional;
    }

    public List<String> getAllAttribute() {
        List<String> allAttributes = new ArrayList<>();
        allAttributes.addAll(mandatory);
        allAttributes.addAll(optional);

        return allAttributes;
    }
}
