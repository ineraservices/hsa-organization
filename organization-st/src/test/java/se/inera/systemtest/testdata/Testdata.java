package se.inera.systemtest.testdata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.io.File;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

public class Testdata implements ParameterResolver {
    private Map<String, ObjectAttribute> objectAttribute;

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface ObjectAttr {
        String value();
    }

    public Testdata() {
        loadTestData();
    }

    private void loadTestData() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            File attributeFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/objectAttributes.yml");
            objectAttribute = Arrays.stream(mapper.readValue(attributeFile, ObjectAttribute[].class))
                    .collect(toMap(ObjectAttribute::getType, u -> u));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        Parameter parameter = parameterContext.getParameter();
        Class<?> type = parameter.getType();
        if (ObjectAttribute.class.equals(type)) {
            return true;
        }
        return false;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.findAnnotation(ObjectAttr.class).map(objAttr -> objectAttribute.get(objAttr.value())).orElse(null);
    }
}
