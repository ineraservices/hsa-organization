package se.inera.systemtest;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import se.inera.hsa.org.model.version_0_5.Unit;
import se.inera.junit5.extension.RestAssuredExtension;
import se.inera.systemtest.testdata.ObjectAttribute;
import se.inera.systemtest.testdata.Testdata;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(RestAssuredExtension.class)
@ExtendWith(Testdata.class)
class UnitEndPointTest {
    private Map<String, Unit> units;

    UnitEndPointTest() {
        loadTestUnits();
    }

    private void loadTestUnits() {
        ObjectMapper mapper = new ObjectMapper(new JsonFactory());
        try {
            File unitFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/units.json");
            units = Arrays.stream(mapper.readValue(unitFile, Unit[].class))
                    .collect(toMap(u -> u.name, u -> u));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("GET unit: under country/organization")
    @Tag("unit")
    @Tag("GET")
    void unitGETendPointAtCountry() {
        String unitName = "Svensk enhet";
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("name", equalTo(unitName))
                .body("type", equalTo("Unit"))
                .body("metaData.path", equalTo("countries/Sverige/organizations/Svensk Organisation/units/" + unitName));
    }

    @Test
    @DisplayName("GET unit:  under county/organization")
    @Tag("unit")
    @Tag("GET")
    void unitGETendPointAtCounty() {
        String unitName = "Nordic Unit";
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("name", equalTo(unitName))
                .body("type", equalTo("Unit"))
                .body("metaData.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName));
    }

    @Test
    @DisplayName("GET unit: under organization/unit")
    @Tag("unit")
    @Tag("GET")
    void unitGETendPointAtUnit() {
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet")
                .then()
                .statusCode(200)
                .and()
                .body("name", equalTo("underenhet"))
                .body("type", equalTo("Unit"))
                .body("metaData.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet"));
    }

    @Test
    @DisplayName("GET unit: none-existing")
    @Tag("unit")
    @Tag("GET")
    void unitGETendPointNotExisting() {
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Not existing")
                .then()
                .statusCode(404); //404 = Not Found
    }

    @Test
    @DisplayName("POST and DELETE unit: under /country/organization/ VIND-017")
    @Tag("unit")
    @Tag("POST")
    @Tag("DELETE")
    void unitPOSTandDELETEendPointAtCountry() {
        // POST new unit with basic content
        String unitName = "enhet enbart med obligatoriska attribut och objektklasser";
        given()
                .body(units.get(unitName))
                .when()
                .post("/countries/Sverige/organizations/Svensk Organisation/units")
                .then()
                .statusCode(201); //201 = Created

        //Verify unit is available in system
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("name", equalTo(units.get(unitName).name))
                .body("type", equalTo(units.get(unitName).type))
                .body("hsaIdentity", equalTo(units.get(unitName).hsaIdentity))
                .body(containsString("creatorUUID"))
                .body(containsString("createTimestamp"))
                .body(containsString("creatorPath"))
                .body(containsString("numSubordinates"))
                .body(containsString("numAllSubordinates"))
                .body(containsString("path"))
                .body(containsString("uuid"));

        // DELETE unit again
        when()
                .delete("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName)
                .then()
                .statusCode(anyOf(is(200), is(204))); // 200=OK or 204=No Content

        //Verify unit is deleted
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName)
                .then()
                .statusCode(404); //404 = Not Found
    }

    @Test
    @DisplayName("POST and DELETE unit: under /county/organization/ VIND-017")
    @Tag("unit")
    @Tag("POST")
    @Tag("DELETE")
    void unitPOSTandDELETEendPointAtCounty() {
        String unitName = "Nordic Unit2";
        given()
                .body(units.get(unitName))
                .when()
                .post("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units")
                .then()
                .statusCode(201); //201 = Created

        //Verify unit is available in system
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("name", equalTo(units.get(unitName).name))
                .body("type", equalTo(units.get(unitName).type))
                .body("hsaIdentity", equalTo(units.get(unitName).hsaIdentity));

        // DELETE unit again
        when()
                .delete("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(anyOf(is(200), is(204))); // 200=OK or 204=No Content

        //Verify unit is available in system
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(404); //404 = Not Found
    }

    @Test
    @DisplayName("POST and DELETE unit: under /country/organization/unit/unit/ VIND-017")
    @Tag("unit")
    @Tag("POST")
    @Tag("DELETE")
    void unitPOSTandDELETEendPointAtCountrySubUnit() {
        String unitName = "Svensk enhet2";
        given()
                .body(units.get(unitName))
                .when()
                .post("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/units")
                .then()
                .statusCode(201); //201 = Created

        //Verify unit is available in system
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("name", equalTo(units.get(unitName).name))
                .body("type", equalTo(units.get(unitName).type))
                .body("hsaIdentity", equalTo(units.get(unitName).hsaIdentity));

        // DELETE unit again
        when()
                .delete("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/units/" + unitName)
                .then()
                .statusCode(anyOf(is(200), is(204))); // 200=OK or 204=No Content

        //Verify unit is available in system
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/units/" + unitName)
                .then()
                .statusCode(404); //404 = Not Found
    }

    @Test
    @DisplayName("POST and DELETE unit: under /county/organization/unit/unit/ VIND-017")
    @Tag("unit")
    @Tag("POST")
    @Tag("DELETE")
    void unitPOSTandDELETEendPointAtCountySubUnit() {
        String unitName = "Nordic Unit3";
        given()
                .body(units.get(unitName))
                .when()
                .post("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/units")
                .then()
                .statusCode(201); //201 = Created

        //Verify unit is available in system
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("name", equalTo(units.get(unitName).name))
                .body("type", equalTo(units.get(unitName).type))
                .body("hsaIdentity", equalTo(units.get(unitName).hsaIdentity));

        // DELETE unit again
        when()
                .delete("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/units/" + unitName)
                .then()
                .statusCode(anyOf(is(200), is(204))); // 200=OK or 204=No Content

        //Verify unit is available in system
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit2/units/" + unitName)
                .then()
                .statusCode(404); //404 = Not Found
    }

    @Test
    @DisplayName("POST and DELETE unit: none-existing VIND-017")
    @Tag("unit")
    @Tag("POST")
    @Tag("DELETE")
    void unitPOSTandDELETEendPointNotExisting() {
        given()
                .body(units.get("Nordic Unit3"))
                .when()
                .post("/countries/Sverige/counties/Värmlands län/organizations/Not existing org/units/")
                .then()
                .statusCode(404); //404 = Not found (parent)

        when()
                .delete("/countries/Sverige/counties/Värmlands län/organizations/Not existing org/units/Not existing unit")
                .then()
                .statusCode(404); //404 = Not found
    }

    @Test
    @DisplayName("POST unit: object already exist")
    @Tag("unit")
    @Tag("POST")
    void unitPOSTendPointAlreadyExist() {
        given()
                .body(units.get("Svensk enhet"))
                .when()
                .post("/countries/Sverige/organizations/Svensk Organisation/units/")
                .then()
                .statusCode(400); //400 = Bad Request (already exist)
    }

    @Test
    @DisplayName("POST unit: parent invalid object type VIND-017")
    @Tag("unit")
    @Tag("POST")
    void unitPOSTendPointWrongParentType() {
        // Unit not allowed under Country and endpoint of this type should not be available
        given()
             .body(units.get("Svensk enhet2"))
        .when()
             .post("/countries/Sverige/units/")
        .then()
             .statusCode(404); //404 = Endpoint not existing

        // Unit not allowed under County and endpoint of this type should not be available
        given()
             .body(units.get("Svensk enhet2"))
        .when()
             .post("/countries/Sverige/counties/Värmlands län/units/")
        .then()
             .statusCode(404); //404 = Endpoint not existing

        // Unit not allowed under function and endpoint of this type should not be available
        given()
             .body(units.get("Svensk enhet2"))
        .when()
             .post("/countries/Sverige/counties/Värmlands län/organization/Nordic MedTest/functions/Nordic Function/units/")
        .then()
             .statusCode(404); //404 = Endpoint not existing

        // Unit not allowed under person and endpoint of this type should not be available
        given()
             .body(units.get("Svensk enhet2"))
        .when()
             .post("/countries/Sverige/organization/Svensk Organisation/employees/Sven O Person/units/")
        .then()
             .statusCode(404); //404 = Endpoint not existing
    }

    @Test
    @DisplayName("DELETE unit: non-leaf object")
    @Tag("unit")
    @Tag("DELETE")
    void unitDELETEendPointNonLeafUnit() {
        when()
                .delete("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit")
                .then()
                .statusCode(anyOf(is(400), is(403)));

        //Verify unit is still available in system
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit")
                .then()
                .statusCode(200); //200 = OK
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT to update postalCode: Endpoint /county/organization/unit/unit/")
    @Tag("unit")
    @Tag("GET")
    @Tag("PUT")
    void unitPUTValidAttributeValue() throws ParseException {
        String unitName = "Nordic Unit4";
        String testPostalCode = "12345";

        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);


        JSONParser parser = new JSONParser();
        JSONObject unitJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        unitJson.remove("metaData"); //Strip the GET response free from metaData and uuid
        unitJson.remove("uuid");
        String orgPostalCode = unitJson.get("postalCode").toString();
        unitJson.put("postalCode", testPostalCode);

        JSONObject unitPutJson = new JSONObject();
        unitPutJson.put("unit", unitJson);
        unitPutJson.put("modificationType", "MERGE");

        request.header("Content-Type", "application/json");
        request.body(unitPutJson.toJSONString());

        Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is available in system and holds updated postalCode
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("postalCode", equalTo(testPostalCode));

        //Clean-up by change back to original value again
        unitJson.put("postalCode", orgPostalCode);
        unitPutJson.put("unit", unitJson);

        PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PUTresponse.then()
                .statusCode(204);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT remove mail and put back again: Endpoint /county/organization/unit/unit/")
    @Tag("unit")
    @Tag("GET")
    @Tag("PUT")
    void unitPUTtoRemoveAttribute() throws ParseException {
        String unitName = "Nordic Unit4";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);


        JSONParser parser = new JSONParser();
        JSONObject unitJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        unitJson.remove("metaData"); //Strip the GET response free from metaData and uuid
        unitJson.remove("uuid");
        String orgmail = unitJson.get("mail").toString();
        unitJson.remove("mail");

        JSONObject unitPutJson = new JSONObject();
        unitPutJson.put("unit", unitJson);
        unitPutJson.put("modificationType", "merge");

        request.header("Content-Type", "application/json");
        request.body(unitPutJson.toJSONString());
        System.out.println(unitPutJson.toJSONString());
        Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PUTresponse.body().prettyPrint();
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is available in system and mail is removed from object
        GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

        unitJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        assertFalse(unitJson.containsKey("mail"));

        //Clean-up by put back mail in the objekt again
        unitJson.put("mail", orgmail);
        unitPutJson.put("unit", unitJson);
        request.body(unitPutJson.toJSONString());

        PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PUTresponse.then()
                .statusCode(204);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT HSAID is not modifiable: Endpoint /county/organization/unit/unit/")
    @Tag("unit")
    @Tag("GET")
    @Tag("PUT")
    void unitPUTAttributeValueNonModifiable() throws ParseException {
        String unitName = "Nordic Unit4";
        String testHsaIdentity = "TST123456-0001";

        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);


        JSONParser parser = new JSONParser();
        JSONObject unitJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        unitJson.remove("metaData"); //Strip the GET response free from metaData and uuid
        unitJson.remove("uuid");
        String orgHsaIdentity = unitJson.get("hsaIdentity").toString();
        unitJson.put("hsaIdentity", testHsaIdentity);

        JSONObject unitPutJson = new JSONObject();
        unitPutJson.put("unit", unitJson);
        unitPutJson.put("modificationType", "merge");

        request.header("Content-Type", "application/json");
        request.body(unitPutJson.toJSONString());
        //Not allowed to edit HSAID
        Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PUTresponse.then()
                .statusCode(400);

        //Verify unit is available in system and holds hsaID from beginning
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("hsaIdentity", equalTo(orgHsaIdentity));

    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT to wrong path: Endpoint /county/organization/unit/unit/")
    @Tag("unit")
    @Tag("GET")
    @Tag("PUT")
    void unitPUTtoWrongPath() throws ParseException {
        String unitName = "Nordic Unit4";
        String testPostalCode = "12345";

        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);


        JSONParser parser = new JSONParser();
        JSONObject unitJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        unitJson.remove("metaData"); //Strip the GET response free from metaData and uuid
        unitJson.remove("uuid");
        String orgPostalCode = unitJson.get("postalCode").toString();
        unitJson.put("postalCode", testPostalCode);

        JSONObject unitPutJson = new JSONObject();
        unitPutJson.put("unit", unitJson);
        unitPutJson.put("modificationType", "merge");

        request.header("Content-Type", "application/json");
        request.body(unitPutJson.toJSONString());

        Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/organizations/" + unitName);
        PUTresponse.then()
                .statusCode(404);

        //Verify unit is available in system and holds the same postalCode
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body("postalCode", equalTo(orgPostalCode));

    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT move unit: Endpoint /county/organization/unit")
    @Tag("unit")
    @Tag("GET")
    @Tag("PUT")
    void unitPUTmove() {
        String unitName = "Nordic Unit4";
        String originalParent = "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/";
        String testParent = "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/units/";
        RequestSpecification request = RestAssured.given();

        //Verify unit exist as a start
        when()
                .get(originalParent + unitName)
                .then()
                .statusCode(200);

        //Prepare body of PUT
        JSONObject unitPutJson = new JSONObject();
        unitPutJson.put("modificationType", "move");
        unitPutJson.put("newPath", testParent + unitName);
        request.header("Content-Type", "application/json");
        request.body(unitPutJson.toJSONString());

        /* PUT (move)
         *****************************************/
        Response PUTresponse = request.put(originalParent + unitName);
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is moved
        when()
                .get(testParent + unitName)
                .then()
                .statusCode(200);

        //Verify unit is gone in original position
        when()
                .get(originalParent + unitName)
                .then()
                .statusCode(404);

        /* PUT (move) back
         *****************************************/
        unitPutJson.put("newPath", originalParent + unitName);
        request.body(unitPutJson.toJSONString());

        PUTresponse = request.put(testParent + unitName);
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is moved back
        when()
                .get(originalParent + unitName)
                .then()
                .statusCode(200);

        //Verify unit is gone in test position
        when()
                .get(testParent + unitName)
                .then()
                .statusCode(404);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT move unit with child: Endpoint /county/organization/unit")
    @Tag("unit")
    @Tag("GET")
    @Tag("PUT")
    void unitWithChildPUTmove() {
        String unitName = "enhet med underordnade objekt";
        String childUnitName = "/units/underenhet";
        String childFunctionName = "/functions/underenhet funktion";
        String originalParent = "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/";
        String testParent = "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/units/";
        RequestSpecification request = RestAssured.given();

        //Verify unit exist as a start
        when()
                .get(originalParent + unitName)
                .then()
                .statusCode(200);

        //Prepare body of PUT
        JSONObject unitPutJson = new JSONObject();
        unitPutJson.put("modificationType", "move");
        unitPutJson.put("newPath", testParent + unitName);
        request.header("Content-Type", "application/json");
        request.body(unitPutJson.toJSONString());

        /* PUT (move)
         *****************************************/
        Response PUTresponse = request.put(originalParent + unitName);
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is moved
        when()
                .get(testParent + unitName)
                .then()
                .statusCode(200);

        //Verify unit is gone in original position
        when()
                .get(originalParent + unitName)
                .then()
                .statusCode(404);
        //Verify function and childunit is moved
        when()
                .get(testParent + unitName + childUnitName)
                .then()
                .statusCode(200);
        when()
                .get(testParent + unitName + childUnitName + childFunctionName)
                .then()
                .statusCode(200);

        /* PUT (move) back
         *****************************************/
        unitPutJson.put("newPath", originalParent + unitName);
        request.body(unitPutJson.toJSONString());

        PUTresponse = request.put(testParent + unitName);
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is moved back
        when()
                .get(originalParent + unitName)
                .then()
                .statusCode(200);

        //Verify childfunction is gone in test position
        when()
                .get(testParent + unitName + childUnitName + childFunctionName)
                .then()
                .statusCode(404);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT unit: Try remove mandatory attribute")
    @Tag("unit")
    @Tag("PUT")
    void unitPUTendPointRemoveMandatory(@Testdata.ObjectAttr("Unit") ObjectAttribute objAttr) throws ParseException {
        String unitName = "Nordic Unit";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

        JSONParser parser = new JSONParser();

        // Verifiera att inga obligatoriska attribute går att ta bort genom att
        // försöka ta bort alla "mandatory" attribute i objectAttributes.yml
        for (int i = 0; i < objAttr.getMandatory().size(); i++) {
            JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
            orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
            orgJson.remove("uuid");
            orgJson.remove(objAttr.getMandatory().get(i));
            JSONObject orgPutJson = new JSONObject();
            orgPutJson.put("unit", orgJson);
            orgPutJson.put("modificationType", "merge");

            request.header("Content-Type", "application/json");
            request.body(orgPutJson.toJSONString());

            Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
            PUTresponse.then()
                    .statusCode(400); //Forbidden expected!!
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH unit: Update multivalue attribute with one more value and remove only the test value again")
    @Tag("unit")
    @Tag("PATCH")
    void unitPATCHMultivalueAddoneMore() throws ParseException {
        String unitName = "Nordic Unit";
        String testNumber = "+460000000001";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());

        //modification JSON
        JSONObject modificationsJson = new JSONObject();
        modificationsJson.put("modificationType", "add");
        modificationsJson.put("attributeName", "telephoneNumber");

        //telephoneNumberArray
        JSONArray telephoneNumberJsonArray = new JSONArray();
        telephoneNumberJsonArray.add(testNumber);

        //invoke telephoneNumber in modificationJSON
        modificationsJson.put("stringValues", telephoneNumberJsonArray);

        //modificationsArray including modifications JSON
        JSONArray orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson);

        //Finally create JSON top level for PATCH based on modificationsArray
        JSONObject orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Add telephoneNumber with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify new telephoneNUmber is present as well as pre-existing number
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body(containsString("telephoneNumber"))
                .body(containsString(testNumber))
                .body(containsString(orgJson.get("telephoneNumber").toString().substring(1, orgJson.get("telephoneNumber").toString().length() - 1)));

        //Remove the testnumber again
        //modification JSON
        modificationsJson = new JSONObject();
        modificationsJson.put("modificationType", "delete");
        modificationsJson.put("attributeName", "telephoneNumber");

        //telephoneNumberArray
        telephoneNumberJsonArray = new JSONArray();
        telephoneNumberJsonArray.add(testNumber);

        //invoke telephoneNumber in modificationJSON
        modificationsJson.put("stringValues", telephoneNumberJsonArray);

        //modificationsArray including modifications JSON
        orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson);

        //Finally create JSON top level for PATCH based on modificationsArray
        orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Delete telephoneNumber with PATCH
        PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify new telephoneNUmb is NOT present any more
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body(containsString("telephoneNumber"))
                .body(not(containsString(testNumber)))  //Not present any more
                .body(containsString(orgJson.get("telephoneNumber").toString().substring(1, orgJson.get("telephoneNumber").toString().length() - 1)));
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH unit: Replace multivalue attribute with new value")
    @Tag("unit")
    @Tag("PATCH")
    void unitPATCHMultivalueAttributeReplace() throws ParseException {
        String unitName = "Svensk enhet";
        String testNumber = "+460000000002";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());

        //modification JSON
        JSONObject modificationsJson1 = new JSONObject();
        modificationsJson1.put("modificationType", "add");
        modificationsJson1.put("attributeName", "telephoneNumber");
        //telephoneNumberArray
        JSONArray telephoneNumberJsonArray1 = new JSONArray();
        telephoneNumberJsonArray1.add(testNumber);
        //invoke telephoneNumber in modificationJSON
        modificationsJson1.put("stringValues", telephoneNumberJsonArray1);

        JSONObject modificationsJson2 = new JSONObject();
        modificationsJson2.put("modificationType", "delete");
        modificationsJson2.put("attributeName", "telephoneNumber");
        //telephoneNumberArray
        JSONArray telephoneNumberJsonArray2 = new JSONArray();
        telephoneNumberJsonArray2.add(orgJson.get("telephoneNumber").toString().substring(2, orgJson.get("telephoneNumber").toString().length() - 2));
        //invoke telephoneNumber in modificationJSON
        modificationsJson2.put("stringValues", telephoneNumberJsonArray2);

        //modificationsArray including modifications JSON
        JSONArray orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson1);
        orgPatchJsonArray.add(modificationsJson2);

        //Finally create JSON top level for PATCH based on modificationsArray
        JSONObject orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Replace telephoneNumber with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify telephoneNUmber is replaced
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body(containsString(testNumber))
                .body(not(containsString(orgJson.get("telephoneNumber").toString().substring(1, orgJson.get("telephoneNumber").toString().length() - 1))));

        //*****REVERT UPDATE******************
        modificationsJson1.replace("modificationType", "delete");
        modificationsJson1.replace("attributeName", "telephoneNumber");
        //invoke telephoneNumber in modificationJSON
        modificationsJson1.replace("stringValues", telephoneNumberJsonArray1);

        modificationsJson2.replace("modificationType", "add");
        modificationsJson2.replace("attributeName", "telephoneNumber");
        //invoke telephoneNumber in modificationJSON
        modificationsJson2.put("stringValues", telephoneNumberJsonArray2);

        //modificationsArray including modifications JSON
        orgPatchJsonArray.clear();
        orgPatchJsonArray.add(modificationsJson1);
        orgPatchJsonArray.add(modificationsJson2);

        //Finally create JSON top level for PATCH based on modificationsArray
        orgPatchJson.replace("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Replace back original telephoneNumber with PATCH
        PATCHresponse = request.patch("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify telephoneNUmber is replaced
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body(not(containsString(testNumber)))
                .body(containsString(orgJson.get("telephoneNumber").toString().substring(1, orgJson.get("telephoneNumber").toString().length() - 1)));
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH unit: Delete single attribute and add old value back again")
    @Tag("unit")
    @Tag("PATCH")
    void unitPATCHSinglevalueAttribute() throws ParseException {
        String unitName = "Nordic Unit4";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());

        //modification JSON
        JSONObject modificationsJson = new JSONObject();
        modificationsJson.put("modificationType", "delete");
        modificationsJson.put("attributeName", "postalCode");

        //modificationsArray including modifications JSON
        JSONArray orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson);

        //Finally create JSON top level for PATCH based on modificationsArray
        JSONObject orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Delete postalCode with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify postalCode is not present
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body(not(containsString("postalCode")));

        //Add original postalCode again
        //modification JSON
        modificationsJson = new JSONObject();
        modificationsJson.put("modificationType", "add");
        modificationsJson.put("attributeName", "postalCode");
        modificationsJson.put("stringValue", orgJson.get("postalCode").toString());

        //modificationsArray including modifications JSON
        orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson);

        //Finally create JSON top level for PATCH based on modificationsArray
        orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Add org postalCode with PATCH again
        PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify orginal postalCode is present again
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body(containsString("postalCode"))
                .body(containsString(orgJson.get("postalCode").toString()));
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH unit: Add value to already existing attribute and expect error response")
    @Tag("unit")
    @Tag("PATCH")
    void unitPATCHAddValueToAlreadyExisting() throws ParseException {
        String unitName = "Nordic Unit4";
        String testcode = "65461";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());

        //modification JSON
        JSONObject modificationsJson = new JSONObject();
        modificationsJson.put("modificationType", "add");
        modificationsJson.put("attributeName", "postalCode");
        modificationsJson.put("stringValue", testcode);

        //modificationsArray including modifications JSON
        JSONArray orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson);

        //Finally create JSON top level for PATCH based on modificationsArray
        JSONObject orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Delete postalCode with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);
        PATCHresponse.then()
                .statusCode(400); //NOK?

        //Verify orginal postalCode is present again
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName)
                .then()
                .statusCode(200)
                .and()
                .body(containsString("postalCode"))
                .body(not(containsString(testcode)))
                .body(containsString(orgJson.get("postalCode").toString()));
    }
}