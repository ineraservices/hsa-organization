package se.inera.systemtest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(RestAssuredExtension.class)
public class CountryEndPointTest {

    @Test
    @DisplayName("GET countries: only Sverige")
    @Tag("country")
    @Tag("GET")
    public void countryGETendPoint() {
        when()
            .get("/countries/Sverige")
        .then()
            .statusCode(200)
            .and()
            .body("isoCode", equalTo("SE"))
            .body("name", equalTo("Sverige"))
            .body("type", equalTo("Country"));
    }

    @Test
    @DisplayName("GET countries: none-existing country")
    @Tag("country")
    @Tag("GET")
    public void countryGETendPointNotExisting() {
        when()
            .get("/countries/Norge") // As we all know, Norway does not really exist
        .then()
            .statusCode(404);  //404 = "Not found"
    }

}
