package se.inera.systemtest;

import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.hsa.org.model.version_0_3.SearchResult;
import se.inera.junit5.extension.RestAssuredExtension;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(RestAssuredExtension.class)
class SearchEndPointTest {

    @Test
    @DisplayName("GET search: one level /countries")
    @Tag("search")
    @Tag("GET")
    void searchGETendPointOneLevelCountry() {
        when()
            .get("/search/one/parent-path/countries/Sverige/")
        .then()
            .statusCode(200)
            .and()
            .body("units", empty())
            .body("countries", empty())
            .body("counties", not(empty()))
            .body("counties.name", hasItem("Värmlands län"))
            .body("functions", empty())
            .body("organizations[0].name", equalTo("Svensk Organisation"));
    }

    @Test
    @DisplayName("GET search: one level /countries/organization/")
    @Tag("search")
    @Tag("GET")
    void searchGETendPointOneLevelOrganization() {
        when()
            .get("/search/one/parent-path/countries/Sverige/organizations/Svensk Organisation")
        .then()
            .statusCode(200)
            .and()
            .body("units[0].name", equalTo("Svensk enhet"))
            .body("units[0].metaData.path", equalTo("countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet"))
            .body("countries", empty())
            .body("counties", empty())
            .body("functions", empty())
            .body("organizations", empty());
    }

    @Test
    @DisplayName("GET search: one level /countries/county/")
    @Tag("search")
    @Tag("GET")
    void searchGETendPointOneLevelCounty() {
        when()
            .get("/search/one/parent-path/countries/Sverige/counties/Värmlands län")
        .then()
            .statusCode(200)
            .and()
            .body("organizations[1].name", equalTo("Nordic MedTest"))
            .body("organizations[1].metaData.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest"))
            .body("countries", empty())
            .body("counties", empty())
            .body("functions", empty())
            .body("units", empty());
    }

    @Test
    @DisplayName("GET search: one level /organization/function/")
    @Tag("search")
    @Tag("GET")
    void searchGETendPointOneLevelFunction() {
        when()
           .get("/search/one/parent-path/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest")
       .then()
           .statusCode(200)
           .and()
           .body("organizations", empty())
           .body("countries", empty())
           .body("counties", empty())
           .body("functions[0].name", equalTo("Nordic Function"))
           .body("functions[0].metaData.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/Nordic Function"))
           .body("units", not(empty()));
    }

    @Test
    @DisplayName("GET search: one level /organization/unit/")
    @Tag("search")
    @Tag("GET")
    void searchGETendPointOneLevelUnit() {
        when()
           .get("/search/one/parent-path/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt")
       .then()
           .statusCode(200)
           .and()
           .body("organizations", empty())
           .body("countries", empty())
           .body("counties", empty())
           .body("units[0].name", equalTo("underenhet"))
           .body("units[0].metaData.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet"))
           .body("functions", empty());
    }

    @Test
    @DisplayName("GET search: subtree")
    @Tag("search")
    @Tag("GET")
    void searchGETendPointSubTree() {
        when()
           .get("/search/sub/parent-path/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest")
       .then()
           .statusCode(200)
           .and()
           .body("organizations", not(empty()))
           .body("countries", empty())
           .body("counties", empty())
           .body("units", not(empty()))
           .body("functions", not(empty()));
    }

    @Test
    @DisplayName("GET search: free-text")
    @Tag("search")
    @Tag("GET")
    void searchFreeTextGETendPoint() {
        Response response = when()
           .get("/search/free-text/Öster");

        response.then()
           .statusCode(200)
           .and()
           .body("organizations", empty())
           .body("countries", empty())
           .body("counties", not(empty()))
           .body("units", empty())
           .body("functions", empty());

        SearchResult result = response.body().as(SearchResult.class);

        result.counties.forEach(countyResponse ->  assertTrue(
                countyResponse.name.contains("Öster") && countyResponse.metaData.path.contains("Öster")));
    }

    @Test
    @DisplayName("GET search: free-text not case sensitive")
    @Tag("search")
    @Tag("GET")
    void searchFreeTextCaseSensGETendPoint() {
        Response response =
        when()
           .get("/search/free-text/nORDIC uNIT");
        response.then()
           .statusCode(200)
           .and()
           .body("organizations", empty())
           .body("countries", empty())
           .body("units", not(empty()))
           .body("counties", empty())
           .body("functions", not(empty()));

        SearchResult result = response.body().as(SearchResult.class);

        result.units.forEach(unitResponse ->  assertTrue(
                unitResponse.name.contains("Nordic Unit") && unitResponse.metaData.path.contains("Nordic Unit")));
        result.functions.forEach(functionResponse ->  assertTrue(
                functionResponse.name.contains("Nordic Unit") && functionResponse.metaData.path.contains("Nordic Unit")));

    }

    @Test
    @DisplayName("GET search: free-text not case sensitive Ö")
    @Tag("search")
    @Tag("GET")
    void searchFreeTextCaseSensOGETendPoint() {
        Response response = when()
           .get("/search/free-text/öster");

        response.then()
           .statusCode(200)
           .and()
           .body("organizations", empty())
           .body("countries", empty())
           .body("counties", not(empty()))
           .body("units", empty())
           .body("functions", empty());

        SearchResult result = response.body().as(SearchResult.class);

        result.counties.forEach(countyResponse ->  assertTrue(
                countyResponse.name.contains("Öster") && countyResponse.metaData.path.contains("Öster")));
    }
}
