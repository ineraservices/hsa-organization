package se.inera.systemtest;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.hsa.org.model.version_0_3.*;
import se.inera.junit5.extension.RestAssuredExtension;

import static io.restassured.RestAssured.given;

/**
 * Useful tests while migrating from annotionbased validation to xml-configured
 */
@Disabled("Waiting for implementation")
@ExtendWith(RestAssuredExtension.class)
class IndataValidationShallowTest {
    @Disabled("Waiting for implementation Post does not exist for Country")
    @Test
    @DisplayName("Test if Country.java has any validation at all")
    @Tag("country")
    @Tag("POST")
    void countryPOSTendPointToTestValidation() {
        given()
                .body(new Country())
                .when()
                .post("/countries")
                .then()
                .body(Matchers.containsString("must not be blank"))
                .statusCode(400);
    }

    @Disabled("Waiting for implementation Post does not exist for County")
    @Test
    @DisplayName("Test if County.java has any validation at all")
    @Tag("county")
    @Tag("POST")
    void countyPOSTendPointToTestValidation() {
        given()
                .body(new County())
                .when()
                .post("/countries/Sverige/counties")
                .then()
                .body(Matchers.containsString("must not be blank"))
                .statusCode(400);
    }

    @Disabled("Waiting for implementation Post does not exist for Organization")
    @Test
    @DisplayName("Test if Organization.java has any validation at all")
    @Tag("organization")
    @Tag("POST")
    void organizationPOSTendPointToTestValidation() {
        given()
                .body(new Organization())
                .when()
                .post("/countries/Sverige/counties/Värmlands län/organizations/")
                .then()
                .body(Matchers.containsString("must not be blank"))
                .statusCode(400);
    }

    @Test
    @DisplayName("Test if Function.java has any validation at all")
    @Tag("function")
    @Tag("POST")
    void functionPOSTendPointToTestValidation() {
        given()
                .body(new Function())
                .when()
                .post("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/functions")
                .then()
                .body(Matchers.containsString("must not be blank"))
                .statusCode(400);
    }

    @Test
    @DisplayName("Test if Unit.java has any validation at all")
    @Tag("unit")
    @Tag("POST")
    void unitPOSTendPointToTestValidation() {
        given()
                .body(new Unit())
                .when()
                .post("/countries/Sverige/organizations/Svensk Organisation/units")
                .then()
                .body(Matchers.containsString("must not be blank"))
                .statusCode(400);
    }
}

