package se.inera.systemtest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(RestAssuredExtension.class)
class CountyEndPointTest {

    @Test
    @DisplayName("GET counties: only Värmlands län")
    @Tag("county")
    @Tag("GET")
    void countyGETendPoint() {
        given().when().get("/countries/Sverige/counties/Värmlands län").then()
                .statusCode(200)
                .and()
                .body("name", equalTo("Värmlands län"))
                .body("type", equalTo("County"))
                .body("metaData.path", equalTo("countries/Sverige/counties/Värmlands län"));
    }

    @Test
    @DisplayName("GET counties: none-existing")
    @Tag("county")
    @Tag("GET")
    void countyGETendPointNotExisting() {
        when()
            .get("/countries/Sverige/counties/Väster län")
        .then()
            .statusCode(404); //404 = Not Found
    }
}
