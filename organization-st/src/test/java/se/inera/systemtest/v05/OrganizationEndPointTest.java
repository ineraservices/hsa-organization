package se.inera.systemtest.v05;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;
import se.inera.junit5.extension.RestAssuredExtensionV05;
import se.inera.systemtest.testdata.ObjectAttribute;
import se.inera.systemtest.testdata.Testdata;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;

@ExtendWith(RestAssuredExtensionV05.class)
@ExtendWith(Testdata.class)
class OrganizationEndPointTest {

    @Test
    @DisplayName("GET organization: under country/organization")
    @Tag("organization")
    @Tag("GET")
    void organizationGETendPointAtCountry() {
        when()
            .get("/countries/Sverige/organizations/Svensk Organisation")
        .then()
            .statusCode(200)
            .and()
            .body("data.name", equalTo("Svensk Organisation"))
            .body("data.type", equalTo("Organization"))
            .body("meta.path", equalTo("countries/Sverige/organizations/Svensk Organisation"));
    }

    @Test
    @DisplayName("GET organization: under county/organization")
    @Tag("organization")
    @Tag("GET")
    void organizationGETendPointAtCounty(){
        when()
            .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest")
        .then()
            .statusCode(200)
            .and()
            .body("data.name", equalTo("Nordic MedTest"))
            .body("data.type", equalTo("Organization"))
            .body("meta.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest"));
    }

    @Test
    @DisplayName("GET organization: none-existing")
    @Tag("organization")
    @Tag("GET")
    void organizationGETendPointNotExisting(){
        when()
            .get("/countries/Sverige/counties/Värmlands län/organizations/Not existing org")
        .then()
            .statusCode(404);  //404 = Not Found
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT organization: Update the value of existing attribute")
    @Tag("organization")
    @Tag("PUT")
    @Disabled("PUT IS NOT TO BE USED")
    void organizationPUTendPointUpdateExistingAtt() throws ParseException {
        String orgName = "Nordic Organization Maximum";
        String testhsaDestinationIndicator = "55";
        String testhsaVisitingRules = "Nu till snart";

        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());

        orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
        orgJson.remove("uuid");

        String orghsaDestinationIndicator = orgJson.get("hsaDestinationIndicator").toString();
        String orghsaVisitingRules = orgJson.get("hsaVisitingRules").toString();
        orgJson.put("hsaDestinationIndicator",testhsaDestinationIndicator);
        orgJson.put("hsaVisitingRules",testhsaVisitingRules);

        JSONObject orgPutJson = new JSONObject();
        orgPutJson.put("organization",orgJson);
        orgPutJson.put("modificationType","MERGE");

        request.header("Content-Type", "application/json");
        request.body(orgPutJson.toJSONString());

        Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is available in system and holds updated postalCode
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName)
                .then()
                .statusCode(200)
                .and()
                .body("hsaDestinationIndicator", equalTo(testhsaDestinationIndicator))
                .body("hsaVisitingRules", equalTo(testhsaVisitingRules));

        //Clean-up by change back to original value again
        orgJson.put("hsaDestinationIndicator",orghsaDestinationIndicator);
        orgJson.put("hsaVisitingRules",orghsaVisitingRules);
        orgPutJson.put("organization",orgJson);

        PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);
        PUTresponse.then()
                .statusCode(204);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT organization: Remove attribute")
    @Tag("organization")
    @Tag("PUT")
    @Disabled("PUT IS NOT TO BE USED")
    void organizationPUTendPointRemoveAttribute() throws ParseException {
        String orgName = "Nordic MedTest";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
        orgJson.remove("uuid");

        String orgmunicipalityCode = orgJson.get("municipalityCode").toString();
        orgJson.remove("municipalityCode"); //Remove municipalityCode from Nordic MedTest

        JSONObject orgPutJson = new JSONObject();
        orgPutJson.put("organization",orgJson);
        orgPutJson.put("modificationType","merge");

        request.header("Content-Type", "application/json");
        request.body(orgPutJson.toJSONString());

        Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is available in system and holds updated postalCode
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName)
                .then()
                .statusCode(200)
                .and()
                .body(not(containsString("municipalityCode")));

        //Clean-up by change back to original value again
        orgJson.put("municipalityCode",orgmunicipalityCode);
        orgPutJson.put("organization",orgJson);
        request.body(orgPutJson.toJSONString());

        PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);
        PUTresponse.then()
                .statusCode(204);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT organization: Update non-existing attribrute")
    @Tag("organization")
    @Tag("PUT")
    @Disabled("PUT IS NOT TO BE USED")
    void organizationPUTendPointUpdateNonExistingAttribute() throws ParseException {
        String orgName = "Nordic MedTest";
        String testString = "Självporträtt";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
        orgJson.remove("uuid");
        orgJson.put("hsaAltText", testString); //Add hsaAltText to Nordic MedTest blobb

        JSONObject orgPutJson = new JSONObject();
        orgPutJson.put("organization",orgJson);
        orgPutJson.put("modificationType","merge");

        request.header("Content-Type", "application/json");
        request.body(orgPutJson.toJSONString());

        Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);
        PUTresponse.then()
                .statusCode(204);

        //Verify unit is available in system and holds updated postalCode
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName)
                .then()
                .statusCode(200)
                .and()
                .body("hsaAltText", equalTo(testString));

        //Clean-up by change back to original value again
        orgJson.remove("hsaAltText");
        orgPutJson.put("organization",orgJson);

        PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);
        PUTresponse.then()
                .statusCode(204);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT organization: Try remove mandatory attribute")
    @Tag("organization")
    @Tag("PUT")
    @Disabled("PUT IS NOT TO BE USED")
    void organizationPUTendPointRemoveMandatory(@Testdata.ObjectAttr("Organization") ObjectAttribute objAttr) throws ParseException {
        String orgName = "Nordic MedTest";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);

        JSONParser parser = new JSONParser();

        // Verifiera att inga obligatoriska attribute går att ta bort genom att
        // försöka ta bort alla "mandatory" attribute i objectAttributes.yml
        for (int i = 0; i < objAttr.getMandatory().size(); i++) {
            JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
            orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
            orgJson.remove("uuid");
            orgJson.remove(objAttr.getMandatory().get(i));

            JSONObject orgPutJson = new JSONObject();
            orgPutJson.put("organization",orgJson);
            orgPutJson.put("modificationType","merge");

            request.header("Content-Type", "application/json");
            request.body(orgPutJson.toJSONString());

            Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);
            PUTresponse.then()
                    .statusCode(400); //Forbidden expected!!
        }
    }
}

