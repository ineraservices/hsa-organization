package se.inera.systemtest.v05;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static se.inera.systemtest.v05.JSONUtils.getFirstEntryAsString;
import static se.inera.systemtest.v05.JSONUtils.getString;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import se.inera.hsa.org.model.version_0_3.Employee;
import se.inera.junit5.extension.RestAssuredExtension;
import se.inera.junit5.extension.RestAssuredExtensionV05;
import se.inera.systemtest.testdata.ObjectAttribute;
import se.inera.systemtest.testdata.Testdata;

@ExtendWith(RestAssuredExtensionV05.class)
@ExtendWith(Testdata.class)
class EmployeeEndPointTest {

    private Map<String, Employee> employees;

    EmployeeEndPointTest() {
        loadTestPersons();
    }

    private void loadTestPersons() {
        ObjectMapper mapper = new ObjectMapper(new JsonFactory());
        try {
            File employeeFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/employees.json");
            employees = Arrays.stream(mapper.readValue(employeeFile, Employee[].class))
                    .collect(toMap(u -> u.name, u -> u));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("GET employee: under /unit/unit/employees/")
    @Tag("employee")
    @Tag("GET")
    void employeeGETendPointAtUnit() {
        String employeeName = "Censur X Sekretessblom";
        String path = "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/Censur X Sekretessblom";
        when()
                .get(path)
                .then()
                .statusCode(200)
                .and()
                .body("data.name", equalTo(employeeName))
                .body("data.type", equalTo("Employee"))
                .body("meta.path", equalTo(path));
    }


    @Test
    @DisplayName("GET employee: under countries/counties/organizations/employees/")
    @Tag("employee")
    @Tag("GET")
    void employeeGETendPointAtOrg() {
        String employeeName = "Anna Fia Inera";
        String path = "countries/Sverige/counties/Värmlands län/organizations/Nordic Organization Maximum/employees/" + employeeName;
        when()
                .get(path)
                .then()
                .statusCode(200)
                .and()
                .body("data.name", equalTo(employeeName))
                .body("data.type", equalTo("Employee"))
                .body("meta.path", equalTo(path));
    }

    @Test
    @DisplayName("GET employee: under countries/organizations/employees/")
    @Tag("employee")
    @Tag("GET")
    void employeeGETendPointAtOrgUnderCountry() {
        String employeeName = "Sven O Person";
        String path = "countries/Sverige/organizations/Svensk Organisation/employees/" + employeeName;
        when()
                .get(path)
                .then()
                .statusCode(200)
                .and()
                .body("data.name", equalTo(employeeName))
                .body("data.type", equalTo("Employee"))
                .body("meta.path", equalTo(path));
    }

    @Test
    @DisplayName("GET employee: under countries/organizations/units/employees/")
    @Tag("employee")
    @Tag("GET")
    void employeeGETendPointAtUnitUnderCountry() {
        String employeeName = "Sven Ou Person";
        String path = "countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/employees/" + employeeName;
        when()
                .get(path)
                .then()
                .statusCode(200)
                .and()
                .body("data.name", equalTo(employeeName))
                .body("data.type", equalTo("Employee"))
                .body("meta.path", equalTo(path));
    }


    @Test
    @DisplayName("GET employee: returns 404 Not Found")
    @Tag("employee")
    @Tag("GET")
    void employeeGET404() {
        String employeeName = "" + Math.random();
        String path = "countries/Sverige/counties/Värmlands län/organizations/Nordic Organization Maximum/employees/" + employeeName;
        when()
                .get(path)
                .then()
                .statusCode(404);
    }

    @DisplayName("POST and DELETE employee: under olika endpoints VIND-017: ")
    @Tag("POST")
    @Tag("DELETE")
    @ParameterizedTest
    @ValueSource(strings = {"/countries/Sverige/organizations/Svensk Organisation",
                            "/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet",
                            "/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest",
                            "/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit"})
    void employeePOSTandDELETEendPoint(String inputPath) {
        String employeeName = "minimal person1";
        System.out.println("Path to employee Endpoint:"+inputPath);
        given()
            .body(employees.get(employeeName))
        .when()
            .post(inputPath+"/employees")
        .then()
            .statusCode(201); //201 = Created

        //Verify employee is available in system
        when()
            .get(inputPath+"/employees/" + employeeName)
        .then()
            .statusCode(200)
        .and()
            .body("data.name", equalTo(employees.get(employeeName).name))
            .body("data.type", equalTo(employees.get(employeeName).type))
            .body("data.sn", equalTo(employees.get(employeeName).sn))
            .body("data.fullName", equalTo(employees.get(employeeName).fullName))
            .body("data.hsaIdentity", equalTo(employees.get(employeeName).hsaIdentity));

        // DELETE employee again
        when()
            .delete(inputPath+"/employees/" + employeeName)
        .then()
            .statusCode(anyOf(is(200), is(204))); // 200=OK or 204=No Content

        //Verify employee is removed
        when()
            .get(inputPath+"/employees/"+employeeName)
        .then()
            .statusCode(404); //404 = Not Found
    }

    @Test
    @DisplayName("POST and DELETE employee: none-existing VIND-017")
    @Tag("employee")
    @Tag("POST")
    @Tag("DELETE")
    void employeePOSTandDELETEendPointNotExisting() {
        given()
            .body(employees.get("minimal person5"))
        .when()
            .post("/countries/Sverige/counties/Värmlands län/organizations/Not existing org/employees/")
        .then()
            .statusCode(404); //404 = Not found (parent)

        when()
            .delete("/countries/Sverige/counties/Värmlands län/organizations/Not existing org/employees/Not existing employee")
        .then()
            .statusCode(404); //404 = Not found
    }

    @Test
    @DisplayName("POST employee: parent invalid object type VIND-017")
    @Tag("employee")
    @Tag("POST")
    void employeePOSTendPointWrongParentType() {
        // Person not allowed under Country and endpoint of this type should not be available
        given()
            .body(employees.get("minimal person1"))
        .when()
            .post("/countries/Sverige/employees/")
        .then()
            .statusCode(404); //404 = Endpoint not existing

        // Person not allowed under County and endpoint of this type should not be available
        given()
            .body(employees.get("minimal person1"))
        .when()
            .post("/countries/Sverige/counties/Värmlands län/employees/")
        .then()
            .statusCode(404); //404 = Endpoint not existing

        // Person not allowed under function and endpoint of this type should not be available
        given()
            .body(employees.get("minimal person1"))
        .when()
            .post("/countries/Sverige/counties/Värmlands län/organization/Nordic MedTest/functions/Nordic Function/employees/")
        .then()
            .statusCode(404); //404 = Endpoint not existing

        // Person not allowed under person and endpoint of this type should not be available
        given()
            .body(employees.get("minimal person1"))
        .when()
            .post("/countries/Sverige/organization/Svensk Organisation/employees/Sven O Person/employees/")
        .then()
            .statusCode(404); //404 = Endpoint not existing
    }

    @Test
    @DisplayName("POST employee: already exist")
    @Tag("employee")
    @Tag("POST")
    void employeePOSTendPointAlreadyExist() {
        // TODO: Verify error message
        given()
            .body(employees.get("Anna Fia Inera"))
        .when()
            .post("/countries/Sverige/counties/Värmlands län/organizations/Nordic Organization Maximum/employees/")
        .then()
            .statusCode(400); //400 = Bad request (already exist)
    }

    @SuppressWarnings("unchecked")
    @DisplayName("PATCH attribute test employee: Endpoint /county/organization/employee")
    @Test
    @Tag("employee")
    @Tag("GET")
    @Tag("PATCH")
    void employeePATCHAddUpdateDeleteAttribute() throws ParseException {
        String employeeName = "Anna Fia Inera";
        RequestSpecification request = RestAssured.given();

        /* Prepare some testdata to play with.*/
        JSONObject postalAddressJson = new JSONObject();
        JSONArray addressLine = new JSONArray();
        addressLine.add(0,"Testvägen 555");
        addressLine.add(1,"777 77 SjuKöping");
        postalAddressJson.put("addressLine",addressLine);

        // GET the already existing employee object we use for test
        String nordicOrganizationEmployees = "/countries/Sverige/counties/Värmlands län/organizations/Nordic Organization Maximum/employees/";
        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get(nordicOrganizationEmployees + employeeName);


        JSONParser parser = new JSONParser();
        JSONObject employeeJson = (JSONObject) parser.parse(GETresponse.getBody().asString());

        JSONObject modificationList  = new ModificationBuilder()
                                                    .addValue("postalAddress", postalAddressJson).build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        /* ADD postalAddress to employee object
        *****************************************/
        Response PATCHresponse = request.patch(nordicOrganizationEmployees + employeeName);
        PATCHresponse.then()
                .statusCode(204);

        //Verify employee is available in system and holds updated postalAddress
        GETresponse = expect()
                .statusCode(200)
                .given()
                .get(nordicOrganizationEmployees + employeeName);

        employeeJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        JSONObject data = (JSONObject) employeeJson.get("data");
        assertEquals("{\"addressLine\":[\"Testvägen 555\",\"777 77 SjuKöping\"]}", data.get("postalAddress").toString());

        /* UPDATE postalAddress of employee object
         *****************************************/
        JSONObject newPostalAddressJson = new JSONObject();
        JSONArray newAddressLine = new JSONArray();
        newAddressLine.add(0,"Testvägen 666");
        newAddressLine.add(1,"777 77 SjuKöping");
        newPostalAddressJson.put("addressLine",newAddressLine);

        modificationList  = new ModificationBuilder()
                .deleteValue("postalAddress", postalAddressJson)
                .addValue("postalAddress", newPostalAddressJson)
                .build();

        request.body(modificationList.toJSONString());

        PATCHresponse = request.patch(nordicOrganizationEmployees + employeeName);
        PATCHresponse.then()
                .statusCode(204);

        //Verify employee is available in system and holds updated postalAddress
        GETresponse = expect()
                .statusCode(200)
                .given()
                .get(nordicOrganizationEmployees + employeeName);

        employeeJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        data = (JSONObject) employeeJson.get("data");
        assertEquals("{\"addressLine\":[\"Testvägen 666\",\"777 77 SjuKöping\"]}", data.get("postalAddress").toString());

        /* DELETE postalAddress of employee object
         *****************************************/
        modificationList  = new ModificationBuilder()
                .deleteValue("postalAddress").build();
        request.body(modificationList.toJSONString());

        PATCHresponse = request.patch(nordicOrganizationEmployees + employeeName);
        PATCHresponse.then()
                .statusCode(204);

        //Verify employee is available in system and holds updated postalAddress
        GETresponse = expect()
                .statusCode(200)
                .given()
                .get(nordicOrganizationEmployees + employeeName);

        employeeJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        assertFalse(employeeJson.containsKey("postalAddress"));
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT move employee: Endpoint /county/organization/employee")
    @Tag("employee")
    @Tag("GET")
    @Tag("PUT")
    @Disabled("PUT SHOULD NOT BE USED")
    void employeePUTmove() {
        String employeeName = "Anna Fia Inera";
        String originalParent = "countries/Sverige/counties/Värmlands län/organizations/Nordic Organization Maximum/employees/";
        String testParent = "countries/Sverige/organizations/Svensk Organisation/employees/";
        RequestSpecification request = RestAssured.given();

        //Verify employee exist as a start
        when()
           .get(originalParent + employeeName)
        .then()
           .statusCode(200);

        //Prepare body of PUT
        JSONObject employeePutJson = new JSONObject();
        employeePutJson.put("modificationType","move");
        employeePutJson.put("newPath",testParent+employeeName);
        request.header("Content-Type", "application/json");
        request.body(employeePutJson.toJSONString());

        /* PUT (move)
         *****************************************/
        Response PUTresponse = request.put(originalParent + employeeName);
        PUTresponse.then()
                .statusCode(204);

        //Verify employee is moved
        when()
           .get(testParent + employeeName)
        .then()
           .statusCode(200);

        //Verify employee is gone in original position
        when()
           .get(originalParent + employeeName)
        .then()
           .statusCode(404);

        /* PUT (move) back
         *****************************************/
        employeePutJson.put("newPath",originalParent+employeeName);
        request.body(employeePutJson.toJSONString());

        PUTresponse = request.put(testParent + employeeName);
        PUTresponse.then()
                .statusCode(204);

        //Verify employee is moved back
        when()
           .get(originalParent + employeeName)
        .then()
           .statusCode(200);

        //Verify employee is gone in test position
        when()
           .get(testParent + employeeName)
        .then()
           .statusCode(404);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH employee: Try remove mandatory attribute")
    @Tag("employee")
    @Tag("PATCH")
    void employeePATCHendPointRemoveMandatory(@Testdata.ObjectAttr("Employee") ObjectAttribute objAttr) {
        String employeeName = "Anna Fia Inera";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic Organization Maximum/employees/" + employeeName);

        // Verifiera att inga obligatoriska attribute går att ta bort genom att
        // försöka ta bort alla "mandatory" attribute i objectAttributes.yml
        for (int i = 0; i < objAttr.getMandatory().size(); i++) {
          //modification JSON
            JSONObject modificationsJson = new JSONObject();
            modificationsJson.put("modificationType", "delete");
            modificationsJson.put("attributeName", objAttr.getMandatory().get(i));

          //modificationsArray including modifications JSON
            JSONArray orgPatchJsonArray = new JSONArray();
            orgPatchJsonArray.add(modificationsJson);
            //Finally create JSON top level for PATCH based on modificationsArray
            JSONObject orgPatchJson = new JSONObject();
            orgPatchJson.put("modifications", orgPatchJsonArray);

            request.header("Content-Type", "application/json");
            request.body(orgPatchJson.toJSONString());

            Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic Organization Maximum/employees/" + employeeName);
            PATCHresponse.then()
                    .statusCode(400); //Forbidden expected!!
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH employee: Update multivalue attribute with one more value and remove only the test value again")
    @Tag("employee")
    @Tag("PATCH")
    void employeePATCHMultivalueAddoneMore() throws ParseException {
        String name = "Sven O Person";
        String testNumber = "+460000000001";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/organizations/Svensk Organisation/employees/" + name);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        JSONObject data = (JSONObject) orgJson.get("data");

        //modification JSON
        JSONObject modificationsJson = new JSONObject();
        modificationsJson.put("modificationType", "add");
        modificationsJson.put("attributeName", "hsaTelephoneNumber");

        //telephoneNumberArray
        JSONArray telephoneNumberJsonArray = new JSONArray();
        telephoneNumberJsonArray.add(testNumber);

        //invoke hsaTelephoneNumber in modificationJSON
        modificationsJson.put("attributeValue", telephoneNumberJsonArray);

        //modificationsArray including modifications JSON
        JSONArray orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson);

        //Finally create JSON top level for PATCH based on modificationsArray
        JSONObject orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Add hsaTelephoneNumber with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/organizations/Svensk Organisation/employees/" + name);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify new hsaTelephoneNumber is present as well as pre-existing number
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/employees/" + name)
                .then()
                .statusCode(200)
                .and()
                .body(containsString("hsaTelephoneNumber"))
                .body(containsString(testNumber))
                .body(containsString(data.get("hsaTelephoneNumber").toString().substring(1, data.get("hsaTelephoneNumber").toString().length() - 1)));

        //Remove the testnumber again
        //modification JSON
        modificationsJson = new JSONObject();
        modificationsJson.put("modificationType", "delete");
        modificationsJson.put("attributeName", "hsaTelephoneNumber");

        //telephoneNumberArray
        telephoneNumberJsonArray = new JSONArray();
        telephoneNumberJsonArray.add(testNumber);

        //invoke telephoneNumber in modificationJSON
        modificationsJson.put("attributeValue", telephoneNumberJsonArray);

        //modificationsArray including modifications JSON
        orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson);

        //Finally create JSON top level for PATCH based on modificationsArray
        orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Delete hsaTelephoneNumber with PATCH
        PATCHresponse = request.patch("/countries/Sverige/organizations/Svensk Organisation/employees/" + name);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify new hsaTelephoneNumber is NOT present any more
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/employees/" + name)
                .then()
                .statusCode(200)
                .and()
                .body(containsString("hsaTelephoneNumber"))
                .body(not(containsString(testNumber)))  //Not present any more
                .body(containsString(data.get("hsaTelephoneNumber").toString().substring(1, data.get("hsaTelephoneNumber").toString().length() - 1)));
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH employee: Replace multivalue attribute with new value")
    @Tag("employee")
    @Tag("PATCH")
    void employeePATCHMultivalueAttributeReplace() throws ParseException {
        String name = "Censur X Sekretessblom";
        String testNumber = "+4622222222";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        JSONObject data = (JSONObject) orgJson.get("data");

        //modification JSON
        JSONObject modificationsJson1 = new JSONObject();
        modificationsJson1.put("modificationType", "add");
        modificationsJson1.put("attributeName", "mobile");
        //telephoneNumberArray
        JSONArray telephoneNumberJsonArray1 = new JSONArray();
        telephoneNumberJsonArray1.add(testNumber);
        //invoke mobile in modificationJSON
        modificationsJson1.put("attributeValue", telephoneNumberJsonArray1);

        JSONObject modificationsJson2 = new JSONObject();
        modificationsJson2.put("modificationType", "delete");
        modificationsJson2.put("attributeName", "mobile");
        //telephoneNumberArray
        JSONArray telephoneNumberJsonArray2 = new JSONArray();
        telephoneNumberJsonArray2.add(data.get("mobile").toString().substring(2, data.get("mobile").toString().length() - 2));
        //invoke mobile in modificationJSON
        modificationsJson2.put("attributeValue", telephoneNumberJsonArray2);

        //modificationsArray including modifications JSON
        JSONArray orgPatchJsonArray = new JSONArray();
        orgPatchJsonArray.add(modificationsJson1);
        orgPatchJsonArray.add(modificationsJson2);

        //Finally create JSON top level for PATCH based on modificationsArray
        JSONObject orgPatchJson = new JSONObject();
        orgPatchJson.put("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Replace mobile with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify mobile is replaced
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name)
                .then()
                .statusCode(200)
                .and()
                .body(containsString(testNumber))
                .body(not(containsString(data.get("mobile").toString().substring(1, data.get("mobile").toString().length() - 1))));

        //*****REVERT UPDATE******************
        modificationsJson1.replace("modificationType", "delete");
        modificationsJson1.replace("attributeName", "mobile");
        //invoke mobile in modificationJSON
        modificationsJson1.replace("attributeValue", telephoneNumberJsonArray1);

        modificationsJson2.replace("modificationType", "add");
        modificationsJson2.replace("attributeName", "mobile");
        //invoke mobile in modificationJSON
        modificationsJson2.put("attributeValue", telephoneNumberJsonArray2);

        //modificationsArray including modifications JSON
        orgPatchJsonArray.clear();
        orgPatchJsonArray.add(modificationsJson1);
        orgPatchJsonArray.add(modificationsJson2);

        //Finally create JSON top level for PATCH based on modificationsArray
        orgPatchJson.replace("modifications", orgPatchJsonArray);

        request.header("Content-Type", "application/json");
        request.body(orgPatchJson.toJSONString());

        //Replace back original telephoneNumber with PATCH
        PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify telephoneNUmber is replaced
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name)
                .then()
                .statusCode(200)
                .and()
                .body(not(containsString(testNumber)))
                .body(containsString(data.get("mobile").toString().substring(1, data.get("mobile").toString().length() - 1)));
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH employee: Delete single attribute and add original value again")
    @Tag("employee")
    @Tag("PATCH")
    void employeePATCHSinglevalueAttribute() throws ParseException {
        String name = "Censur X Sekretessblom";

        RequestSpecification request = RestAssured.given();
        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        JSONObject data = (JSONObject) orgJson.get("data");

        JSONObject modificationList = new ModificationBuilder().deleteValue("description").build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());
        //Delete "description" with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify "description" is not present
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name)
                .then()
                .statusCode(200)
                .and()
                .body(not(containsString("description")));

        //Add original "description" again
        modificationList = new ModificationBuilder().addValue("description", getString(data, "description")).build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Add org "description" with PATCH again
        PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify original "description" is present again
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/employees/" + name)
                .then()
                .statusCode(200)
                .and()
                .body(containsString("description"))
                .body(containsString(data.get("description").toString()));
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH employee: Replace mandatory objectclass with new value")
    @Tag("employee")
    @Tag("PATCH")
    void employeePATCHReplaceMandatoryAttribute() throws ParseException {
        String name = "Sven Ou Person";
        String personId = "197411146231";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/employees/" + name);

        JSONParser parser = new JSONParser();
        JSONObject orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        JSONObject data = (JSONObject) orgJson.get("data");

        JSONObject modificationList = new ModificationBuilder()
                            .deleteValue("personalIdentityNumber", getString(data, "personalIdentityNumber"))
                            .addValue("personalIdentityNumber", personId)
                            .build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Replace personId with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/employees/" + name);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify personId is replaced
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/employees/" + name)
                .then()
                .statusCode(200)
                .and()
                .body(containsString(personId))
                .body(not(containsString(data.get("personalIdentityNumber").toString())));

        //*****REVERT UPDATE******************
        modificationList = new ModificationBuilder()
                .deleteValue("personalIdentityNumber", personId)
                .addValue("personalIdentityNumber", getString(data, "personalIdentityNumber"))
                .build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Replace back original personId with PATCH
        PATCHresponse = request.patch("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/employees/" + name);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify personId is replaced
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/employees/" + name)
                .then()
                .statusCode(200)
                .and()
                .body(not(containsString(personId)))
                .body(containsString(data.get("personalIdentityNumber").toString()));
    }
}
