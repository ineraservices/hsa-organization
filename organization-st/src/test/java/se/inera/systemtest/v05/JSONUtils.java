package se.inera.systemtest.v05;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JSONUtils {
    private JSONUtils() {
        // empty for now
    }

    static String getString(JSONObject json, String field) {
        return (String) json.get(field);
    }

    static String getFirstEntryAsString(JSONObject json, String field) {
        JSONArray tmp = (JSONArray) json.get(field);
        return (String) tmp.get(0);
    }
}
