package se.inera.systemtest.v05;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;
import se.inera.junit5.extension.RestAssuredExtensionV05;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(RestAssuredExtensionV05.class)
public class CountryEndPointTest {

    @Test
    @DisplayName("GET countries: only Sverige")
    @Tag("country")
    @Tag("GET")
    public void countryGETendPoint() {
        when()
            .get("/countries/Sverige")
        .then()
            .statusCode(200)
            .and()
            .body("data.isoCode", equalTo("SE"))
            .body("data.name", equalTo("Sverige"))
            .body("data.type", equalTo("Country"));
    }

    @Test
    @DisplayName("GET countries: none-existing country")
    @Tag("country")
    @Tag("GET")
    public void countryGETendPointNotExisting() {
        when()
            .get("/countries/Norge") // As we all know, Norway does not really exist
        .then()
            .statusCode(404);  //404 = "Not found"
    }

}
