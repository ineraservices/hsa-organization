package se.inera.systemtest.v05;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static java.util.stream.Collectors.toMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static se.inera.systemtest.v05.JSONUtils.getFirstEntryAsString;
import static se.inera.systemtest.v05.JSONUtils.getString;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.model.version_0_5.Function;
import se.inera.junit5.extension.RestAssuredExtension;
import se.inera.junit5.extension.RestAssuredExtensionV05;
import se.inera.systemtest.testdata.ObjectAttribute;
import se.inera.systemtest.testdata.Testdata;


@ExtendWith(RestAssuredExtensionV05.class)
@ExtendWith(Testdata.class)
class FunctionEndPointTest {
    private Map<String, Function> functions;

    FunctionEndPointTest() {
        loadTestUnits();
    }

    private void loadTestUnits() {
        ObjectMapper mapper = new ObjectMapper(new JsonFactory());
        try {
            File functionFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/functions.json");
            functions = Arrays.stream(mapper.readValue(functionFile, Function[].class))
                    .collect(toMap(u -> u.name, u -> u));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("GET functions: under country/organization/unit/")
    @Tag("function")
    @Tag("GET")
    void functionGETendPointAtCountry() {
        String functionName = "Svensk funktion";
        when()
                .get("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/functions/" + functionName)
                .then()
                .statusCode(200)
                .and()
                .body("data.name", equalTo(functionName))
                .body("data.type", equalTo("Function"))
                .body("meta.path", equalTo("countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/functions/" + functionName));
    }

    @Test
    @DisplayName("GET function: under county/organization/unit/")
    @Tag("function")
    @Tag("GET")
    void functionGETendPointAtCounty() {
        String functionName = "Nordic Unit Function";
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/" + functionName)
                .then()
                .statusCode(200)
                .and()
                .body("data.name", equalTo(functionName))
                .body("data.type", equalTo("Function"))
                .body("meta.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/" + functionName));
    }

    @Test
    @DisplayName("GET function:  under county/organization/")
    @Tag("function")
    @Tag("GET")
    void functionGETendPointAtOrganization() {
        String functionName = "Nordic Function";
        when()
            .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName)
        .then()
            .statusCode(200)
            .and()
            .body("data.name", equalTo(functionName))
            .body("data.type", equalTo("Function"))
            .body("meta.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName));
    }

    @Test
    @DisplayName("GET function: under /unit/unit/")
    @Tag("function")
    @Tag("GET")
    void functionGETendPointAtUnit() {
        String functionName = "underenhet funktion";
        when()
            .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + functionName)
        .then()
            .statusCode(200)
            .and()
            .body("data.name", equalTo(functionName))
            .body("data.type", equalTo("Function"))
            .body("meta.path", equalTo("countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + functionName));
    }

    @Test
    @DisplayName("GET function: none-existing")
    @Tag("function")
    @Tag("GET")
    void functionGETendPointNotExisting() {
        when()
            .get("/countries/Sverige/Svensk Organisation/units/Svensk enhet/functions/Not existing")
        .then()
            .statusCode(404); //404= Not Found
    }

    @DisplayName("POST and DELETE function: under /country/organization/ VIND-017: ")
    @Tag("POST")
    @Tag("DELETE")
    @ParameterizedTest
    @ValueSource(strings = {"/countries/Sverige/organizations/Svensk Organisation",
                            "/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet",
                            "/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest",
                            "/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit"})
    void functionPOSTandDELETEendPoint(String inputPath) {
        String functionName = "test funktion";
        System.out.println("Path to function Endpoint:"+inputPath);
        given()
            .body(functions.get(functionName))
        .when()
            .post(inputPath+"/functions")
        .then()
            .statusCode(201); //201 = Created

        //Verify function is available in system
        when()
            .get(inputPath+"/functions/" + functionName)
        .then()
            .statusCode(200)
        .and()
            .body("data.name", equalTo(functions.get(functionName).name))
            .body("data.type", equalTo(functions.get(functionName).type))
            .body("data.hsaIdentity", equalTo(functions.get(functionName).hsaIdentity));

        // DELETE function again
        when()
            .delete(inputPath+"/functions/" + functionName)
        .then()
            .statusCode(anyOf(is(200), is(204))); // 200=OK or 204=No Content

        //Verify function is removed
        when()
            .get(inputPath+"/functions/"+functionName)
        .then()
            .statusCode(404); //404 = Not Found
    }

    @Test
    @DisplayName("POST and DELETE function: none-existing")
    @Tag("function")
    @Tag("POST")
    @Tag("DELETE")
    void functionPOSTandDELETEendPointNotExisting() {

        given()
            .body(functions.get("Nordic Function3"))
        .when()
            .post("/countries/Sverige/counties/Värmlands län/organizations/Not existing org/functions/")
        .then()
            .statusCode(404); //404 = Not found (parent)

        when()
            .delete("/countries/Sverige/counties/Värmlands län/organizations/Not existing org/functions/Not existing function")
        .then()
            .statusCode(404); //404 = Not found
    }

    @Test
    @DisplayName("POST function: already exist")
    @Tag("function")
    @Tag("POST")
    void functionPOSTendPointAlreadyExist() {
        given()
            .body(functions.get("Svensk funktion"))
        .when()
            .post("/countries/Sverige/organizations/Svensk Organisation/units/Svensk enhet/functions/")
        .then()
            .statusCode(400); //400 = Bad request (already exist)
    }

    @Test
    @DisplayName("POST function: parent invalid object type VIND-017")
    @Tag("function")
    @Tag("POST")
    void functionPOSTendPointWrongParentType() {
        //Function under Country not allowed and endpoint of this type should not be available
        given()
            .body(functions.get("Svensk funktion2"))
        .when()
            .post("/countries/Sverige/functions/")
        .then()
            .statusCode(404); //404 = Endpoint not existing

        //Function under County not allowed and endpoint of this type should not be available
        given()
            .body(functions.get("Svensk funktion2"))
        .when()
            .post("/countries/Sverige/counties/Värmlands län/functions/")
        .then()
            .statusCode(404); //404 = Endpoint not existing

        //Function under Employee not allowed and endpoint of this type should not be available
        given()
            .body(functions.get("Svensk funktion2"))
        .when()
            .post("/countries/Sverige/Svensk Organisation/employees/Sven O Person/functions/")
        .then()
            .statusCode(404); //404 = Endpoint not existing

        //Function under function not allowed and endpoint of this type should not be available
        given()
            .body(functions.get("Svensk funktion2"))
        .when()
            .post("/countries/Sverige/Svensk Organisation/units/Svensk enhet/functions/Svensk funktion/functions/")
        .then()
            .statusCode(404); //404 = Endpoint not existing
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT attribute test function: Endpoint /county/organization/unit/function")
    @Tag("function")
    @Tag("GET")
    @Tag("PUT")
    @Disabled
    void FunctionPUTAddUpdateDeleteAttribute() throws ParseException {
        String functionName = "Nordic Function";
        RequestSpecification request = RestAssured.given();

        /* Prepare some testdata to play with.*/
        JSONObject postalAddressJson = new JSONObject();
        JSONArray addressLine = new JSONArray();
        addressLine.add(0,"Testvägen 555");
        addressLine.add(1,"777 77 SjuKöping");
        postalAddressJson.put("addressLine",addressLine);

        // GET the already existing function object we use for test
        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);

        JSONParser parser = new JSONParser();
        JSONObject functionJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        //Strip the GET response free from metaData and uuid
        functionJson.remove("metaData");
        functionJson.remove("uuid");
        //Add the prepared postalAddress
        functionJson.put("postalAddress",postalAddressJson);

        //Store the Json to the request body of PUT
        JSONObject functionPutJson = new JSONObject();
        functionPutJson.put("function",functionJson);
        functionPutJson.put("modificationType","merge");

        request.header("Content-Type", "application/json");
        request.body(functionPutJson.toJSONString());

        /* ADD postalAddress to function object
        *****************************************/
        Response PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);
        PUTresponse.then()
                .statusCode(204);

        //Verify function is available in system and holds updated postalAddress
        GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);

        functionJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        assertEquals(functionJson.get("postalAddress").toString(),"{\"addressLine\":[\"Testvägen 555\",\"777 77 SjuKöping\"]}");

        /* UPDATE postalAddress of function object
         *****************************************/
        addressLine.set(0,"Testvägen 666");
        postalAddressJson.put("addressLine",addressLine);
        functionJson.put("postalAddress",postalAddressJson);
        functionPutJson.put("function",functionJson);
        request.body(functionPutJson.toJSONString());

        PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);
        PUTresponse.then()
                .statusCode(204);

        //Verify function is available in system and holds updated postalAddress
        GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);

        functionJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        assertEquals(functionJson.get("postalAddress").toString(),"{\"addressLine\":[\"Testvägen 666\",\"777 77 SjuKöping\"]}");

        /* DELETE postalAddress of function object
         *****************************************/
        functionJson.remove("postalAddress");
        functionPutJson.put("function",functionJson);
        request.body(functionPutJson.toJSONString());

        PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);
        PUTresponse.then()
                .statusCode(204);

        //Verify function is available in system and holds updated postalAddress
        GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);

        functionJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        assertFalse(functionJson.containsKey("postalAddress"));
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PUT move function: Endpoint /county/organization/unit/function")
    @Tag("function")
    @Tag("GET")
    @Tag("PUT")
    @Disabled
    void FunctionPUTmove() {
        String functionName = "Nordic Unit Function";
        String originalParent = "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/";
        String testParent = "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit4/functions/";
        RequestSpecification request = RestAssured.given();

        //Verify function exist as a start
        when()
           .get(originalParent + functionName)
        .then()
           .statusCode(200);

        //Prepare body of PUT
        JSONObject functionPutJson = new JSONObject();
        functionPutJson.put("modificationType","move");
        functionPutJson.put("newPath",testParent+functionName);
        request.header("Content-Type", "application/json");
        request.body(functionPutJson.toJSONString());

        /* PUT (move)
         *****************************************/
        Response PUTresponse = request.put(originalParent + functionName);
        PUTresponse.then()
                .statusCode(204);

        //Verify function is moved
        when()
           .get(testParent + functionName)
        .then()
           .statusCode(200);

        //Verify function is gone in original position
        when()
           .get(originalParent + functionName)
        .then()
           .statusCode(404);

        /* PUT (move) back
         *****************************************/
        functionPutJson.put("newPath",originalParent+functionName);
        request.body(functionPutJson.toJSONString());

        PUTresponse = request.put(testParent + functionName);
        PUTresponse.then()
                .statusCode(204);

        //Verify function is moved back
        when()
           .get(originalParent + functionName)
        .then()
           .statusCode(200);

        //Verify function is gone in test position
        when()
           .get(testParent + functionName)
        .then()
           .statusCode(404);
    }

    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH function: Try remove mandatory attribute")
    @Tag("function")
    @Tag("PATCH")
    void functionPATCHendPointRemoveMandatory(@Testdata.ObjectAttr("Function") ObjectAttribute objAttr) {
        String funcName = "Nordic Function";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + funcName);


        // Verifiera att inga obligatoriska attribute går att ta bort genom att
        // försöka ta bort alla "mandatory" attribute i objectAttributes.yml
        for (int i = 0; i < objAttr.getMandatory().size(); i++) {
            JSONObject modificationList  = new ModificationBuilder().deleteValue(objAttr.getMandatory().get(i)).build();

            request.header("Content-Type", "application/json");
            request.body(modificationList.toJSONString());

            Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + funcName);
            PATCHresponse.then()
                    .statusCode(400); //Forbidden expected!!
        }
    }
    @Test
    @DisplayName("PATCH function: Delete single attribute and add old value back again")
    @Tag("function")
    @Tag("PATCH")
    void functionPATCHSingleValueAttribute() throws ParseException {
        String functionName = "Nordic Unit Function";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/" + functionName);

        JSONParser parser = new JSONParser();
        JSONObject funcJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        JSONObject data = (JSONObject) funcJson.get("data");

        JSONObject modificationList  = new ModificationBuilder().deleteValue("orgNo").build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //use PATCH to delete organisation Number
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/" + functionName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify organisation number is not present
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/" + functionName)
                .then()
                .statusCode(200)
                .and()
                .body(not(containsString("orgNo")));

        //Add original organisationNumber again
        modificationList  = new ModificationBuilder().addValue("orgNo", data.get("orgNo").toString()).build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Add organisation number with PATCH again
        PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/" + functionName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify that organisation number from beginning  is present again
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit/functions/" + functionName)
                .then()
                .statusCode(200)
                .and()
                .body(containsString("orgNo"))
                .body(containsString(data.get("orgNo").toString()));
    }
    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH function: Replace multivalue attribute with new value")
    @Tag("function")
    @Tag("PATCH")
    void funcPATCHMultiValueAttributeReplace() throws ParseException {
        String funcName = "Nordic Function";
        String testNumber = "+460000000002";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + funcName);

        JSONParser parser = new JSONParser();
        JSONObject funcJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        JSONObject data = (JSONObject) funcJson.get("data");

        JSONObject modificationList  = new ModificationBuilder().addValues("telephoneNumber", testNumber)
                                                                .deleteValues("telephoneNumber", getFirstEntryAsString(data, "telephoneNumber")).build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Replace telephoneNumber with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + funcName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify telephoneNUmber is replaced
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + funcName)
                .then()
                .statusCode(200)
                .and()
                .body(containsString(testNumber))  //Not present any more
                .body(not(containsString(getFirstEntryAsString(data, "telephoneNumber"))));

        //*****REVERT UPDATE******************
        modificationList  = new ModificationBuilder()
                                .deleteValues("telephoneNumber", testNumber)
                                .addValues("telephoneNumber", getFirstEntryAsString(data, "telephoneNumber"))
                                .build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Replace back original telephoneNumber with PATCH
        PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + funcName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify telephoneNUmber is replaced
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + funcName)
           .then()
                .statusCode(200)
           .and()
                .body(not(containsString(testNumber)))
                .body(containsString(getFirstEntryAsString(data, "telephoneNumber")));  //Not present any more
    }
    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH function: Add value to already existing attribute and expect error response")
    @Tag("function")
    @Tag("PATCH")
    void functionPATCHAddValueToAlreadyExisting() {
        String functionName = "underenhet funktion";
        String countyCode = "17";
        RequestSpecification request = RestAssured.given();

        JSONObject modificationList = new ModificationBuilder().addValue("countyCode", countyCode).build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Delete countyCode with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + functionName);
        PATCHresponse.then()
                .statusCode(400); //NOK?

        //Verify orginal countyCode is still present
       when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + functionName)
                .then()
                .statusCode(200)
                .and()
                .body("data.countyCode", containsString("01"))
                .body("data.countyCode", not(containsString(countyCode)));
    }


    @SuppressWarnings("unchecked")
    @Test
    @DisplayName("PATCH function: Update multivalue attribute with one more value and remove only the test value again")
    @Tag("function")
    @Tag("PATCH")
    void functionPATCHMultivalueAddoneMore() throws ParseException {
        String funcName = "underenhet funktion";
        String testCode = "2017";
        RequestSpecification request = RestAssured.given();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + funcName);

        JSONParser parser = new JSONParser();
        JSONObject funcJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
        JSONObject data = (JSONObject) funcJson.get("data");

        String originalCode = getFirstEntryAsString(data, "businessClassificationCode");

        JSONObject modificationList = new ModificationBuilder().addValues("businessClassificationCode", testCode).build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Add businessClassification with PATCH
        Response PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + funcName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify that the added code and already existing code is present
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + funcName)
                .then()
                .statusCode(200)
                .and()
                .body("data.businessClassificationCode", contains(originalCode,testCode));
//                .body("data.businessClassificationCode", containsString(testCode));
//                .extract().response().as(FunctionResponse.class);
//        assertThat(funcResponse.businessClassificationCode).contains(originalCode, testCode);

        //Remove the testCode again
        modificationList = new ModificationBuilder().deleteValues("businessClassificationCode", testCode).build();

        request.header("Content-Type", "application/json");
        request.body(modificationList.toJSONString());

        //Delete telephoneNumber with PATCH
        PATCHresponse = request.patch("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + funcName);
        PATCHresponse.then()
                .statusCode(204); //OK?

        //Verify that added testCode is NOT present
        when()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/enhet med underordnade objekt/units/underenhet/functions/" + funcName)
                .then()
                .statusCode(200)
                .and()
                .body("data.businessClassificationCode", contains(originalCode))
                .body("data.businessClassificationCode", not(contains(testCode)));
    }


}