package se.inera.systemtest.v05;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ModificationBuilder {
        private static final String ATTRIBUTE_VALUE = "attributeValue";
        private static final String ATTRIBUTE_NAME = "attributeName";
        private static final String ADD = "add";
        private static final String MODIFICATION_TYPE = "modificationType";
        List<JSONObject> modificationList;
        public ModificationBuilder() {
            modificationList = new ArrayList<>();
        }

        @SuppressWarnings("unchecked")
        public ModificationBuilder addValue(String attrName, Object value) {
          //modification JSON
            JSONObject modificationsJson = new JSONObject();
            modificationsJson.put(MODIFICATION_TYPE,ADD);
            modificationsJson.put(ATTRIBUTE_NAME, attrName);
            if ( value != null ) {
                modificationsJson.put(ATTRIBUTE_VALUE, value);
            }
            modificationList.add(modificationsJson);
            return this;
        }
        @SuppressWarnings("unchecked")
        public ModificationBuilder deleteValue(String attrName, Object value) {
            //modification JSON
            JSONObject modificationsJson = new JSONObject();
            modificationsJson.put(MODIFICATION_TYPE,"delete");
            modificationsJson.put(ATTRIBUTE_NAME, attrName);
            if ( value != null ) {
                modificationsJson.put(ATTRIBUTE_VALUE, value);
            }

            modificationList.add(modificationsJson);
            return this;
        }
        @SuppressWarnings("unchecked")
        public ModificationBuilder deleteValue(String attrName) {
            return deleteValue(attrName, null);
        }

        @SuppressWarnings("unchecked")
        public ModificationBuilder addValues(String attrName, Object... value) {
            //modification JSON
            JSONObject modificationsJson = new JSONObject();
            modificationsJson.put(MODIFICATION_TYPE,ADD);
            modificationsJson.put(ATTRIBUTE_NAME, attrName);
            if ( value != null ) {
                JSONArray array = new JSONArray();
                for (Object object : value) {
                    array.add(object);
                }
                modificationsJson.put(ATTRIBUTE_VALUE, array);
            }

            modificationList.add(modificationsJson);
            return this;
        }
        @SuppressWarnings("unchecked")
        public ModificationBuilder deleteValues(String attrName, Object... value) {
            //modification JSON
            JSONObject modificationsJson = new JSONObject();
            modificationsJson.put(MODIFICATION_TYPE,"delete");
            modificationsJson.put(ATTRIBUTE_NAME, attrName);
            if ( value != null ) {
                JSONArray array = new JSONArray();
                for (Object object : value) {
                    array.add(object);
                }
                modificationsJson.put(ATTRIBUTE_VALUE, array);
            }

            modificationList.add(modificationsJson);
            return this;
        }
        public ModificationBuilder deleteValues(String attrName) {
            return deleteValues(attrName, (Object[])null);
        }

        @SuppressWarnings("unchecked")
        public JSONObject build() {
          //modificationsArray including modifications JSON
            JSONArray orgPatchJsonArray = new JSONArray();
            for (JSONObject aMod : modificationList) {
                orgPatchJsonArray.add(aMod);
            }
            //Finally create JSON top level for PATCH based on modificationsArray
            JSONObject orgPatchJson = new JSONObject();
            orgPatchJson.put("modifications", orgPatchJsonArray);
            return orgPatchJson;
        }
}
