package se.inera.systemtest.v05;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;
import se.inera.junit5.extension.RestAssuredExtensionV05;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(RestAssuredExtensionV05.class)
class CountyEndPointTest {

    @Test
    @DisplayName("GET counties: only Värmlands län")
    @Tag("county")
    @Tag("GET")
    void countyGETendPoint() {
        given().when().get("/countries/Sverige/counties/Värmlands län").then()
                .statusCode(200)
                .and()
                .body("data.name", equalTo("Värmlands län"))
                .body("data.type", equalTo("County"))
                .body("meta.path", equalTo("countries/Sverige/counties/Värmlands län"));
    }

    @Test
    @DisplayName("GET counties: none-existing")
    @Tag("county")
    @Tag("GET")
    void countyGETendPointNotExisting() {
        when()
            .get("/countries/Sverige/counties/Väster län")
        .then()
            .statusCode(404); //404 = Not Found
    }
}
