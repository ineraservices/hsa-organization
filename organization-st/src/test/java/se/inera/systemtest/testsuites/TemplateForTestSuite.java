package se.inera.systemtest.testsuites;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import se.inera.systemtest.CountryEndPointTest;

@RunWith(JUnitPlatform.class)
@SelectClasses(CountryEndPointTest.class)
//@SelectPackages("se.inera.systemtest")

public class TemplateForTestSuite {

}
