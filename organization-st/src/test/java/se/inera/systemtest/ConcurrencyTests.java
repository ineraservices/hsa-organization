package se.inera.systemtest;

import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.hsa.org.model.version_0_5.Unit;
import se.inera.junit5.extension.RestAssuredExtension;
import se.inera.systemtest.testdata.Testdata;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(RestAssuredExtension.class)
@ExtendWith(Testdata.class)
public class ConcurrencyTests {
    private ExecutorService executor
            = Executors.newFixedThreadPool(10);

    private Integer itterationCount = 20;

    @Test
    @DisplayName("Concurrency test for POST : under /country/organization/")
    @Tag("unit")
    @Tag("POST")
    @Tag("Concurrency")
    void concurrencyCreate() throws ExecutionException, InterruptedException {

        Unit unit = getUnit();

        List<Future<Boolean>> futures = new ArrayList<>();
        for (int counter=0; counter < itterationCount; counter++) {
            Future<Boolean> submited = executor.submit(new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    // POST new unit with basic content
                    Response response = given()
                            .body(unit)
                            .when()
                            .post("/countries/Sverige/organizations/Svensk Organisation/units");
                    return response.statusCode() == 201;
                }
            });
            futures.add(submited);

        }
        int successCount = 0;
        for (Future<Boolean> future : futures) {
            if (future.get()) {
                successCount++;
            }
        }
        when().delete("/countries/Sverige/organizations/Svensk Organisation/units/" + unit.name).then().statusCode(204);
        assertEquals(1, successCount);
    }

    @Test
    @DisplayName("Concurrency test for DELETE : under /country/organization/")
    @Tag("unit")
    @Tag("DELETE")
    @Tag("Concurrency")
    void concurrencyDelete() throws ExecutionException, InterruptedException {
        // Create something to remove
        Unit unit = getUnit();
        String parentPath = "/countries/Sverige/organizations/Svensk Organisation/units";
        given().body(unit)
                .when()
                .post(parentPath)
                .then()
                .statusCode(201); //201 = Created;

        List<Future<Boolean>> futures = new ArrayList<>();
        for (int counter=0; counter < itterationCount; counter++) {
            Future<Boolean> submited = executor.submit(new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    // POST new unit with basic content
                    Response response = when().delete(parentPath + "/" + unit.name);
                    return response.statusCode() == 204;
                }
            });
            futures.add(submited);

        }
        int successCount = 0;
        for (Future<Boolean> future : futures) {
            if (future.get()) {
                successCount++;
            }
        }
        assertEquals(1, successCount);

    }
    //Detta är problematiskt. Det borde vara determintistiskt så att krockar pga samma random nummer garanterat undviks.
    private static Unit getUnit() {
        Unit unit = new Unit();
        unit.name = "enhet skapad fr att testa samtidighet " + Math.abs(new Random().nextInt());
        unit.type = "Unit";
        unit.hsaIdentity = "HSA_ID_" + Math.abs(new Random().nextInt());
        return unit;
    }
}