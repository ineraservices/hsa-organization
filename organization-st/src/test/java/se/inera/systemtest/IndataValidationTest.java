package se.inera.systemtest;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;
import se.inera.systemtest.testdata.ObjectAttribute;
import se.inera.systemtest.testdata.Testdata;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.expect;
import static org.junit.Assert.fail;

@Disabled("Waiting for implementation")
@ExtendWith(RestAssuredExtension.class)
@ExtendWith(Testdata.class)
class IndataValidationTest {
    private Map<String, List> invalidIndataMap;
    private Map<String, List> validIndataMap;

    IndataValidationTest() {
        loadTestdata();
    }

    private void loadTestdata() {
        ObjectMapper mapper = new ObjectMapper(new JsonFactory());
        try {
            File InvalidIndataFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/invalidIndata.json");
            invalidIndataMap = mapper.readValue(InvalidIndataFile, new TypeReference<Map<String, List>>() {});
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            File ValidIndataFile = new File(System.getProperty("user.dir") + "/src/test/resources/testdata/validIndata.json");
            validIndataMap = mapper.readValue(ValidIndataFile, new TypeReference<Map<String, List>>() {});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("PUT organization: Invalid indata format")
    @Tag("indatavalidation")
    @Tag("PUT")
    void verifyInvalidAttributeFormatOrganization(@Testdata.ObjectAttr("Organization") ObjectAttribute objAttr) throws ParseException {
        String orgName = "Nordic MedTest";
        RequestSpecification request = RestAssured.given();
        Response PUTresponse;
        JSONParser parser = new JSONParser();
        JSONObject orgJson;
        List<String> errorList = new ArrayList<>();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);

        List<String> attributeList = objAttr.getAllAttribute();
        for (String anAttributeList : attributeList) {
            for (int i = 0; i < invalidIndataMap.get(anAttributeList).size(); i++) {

                orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
                orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
                orgJson.remove("uuid");

                //If the JSONObject is an JSONArray
                if (orgJson.get(anAttributeList) instanceof JSONArray) {
                    JSONArray testDataList = new JSONArray();
                    testDataList.add(invalidIndataMap.get(anAttributeList).get(i));
                    orgJson.replace(anAttributeList, testDataList);
                } else {//Expect the JSONObject to be a string
                    orgJson.replace(anAttributeList, invalidIndataMap.get(anAttributeList).get(i));
                }

                JSONObject orgPutJson = new JSONObject();
                orgPutJson.put("organization", orgJson);
                orgPutJson.put("modificationType", "merge");

                request.header("Content-Type", "application/json");
                request.body(orgPutJson.toJSONString());

                PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);

                if (PUTresponse.statusCode() != 400) {
                    errorList.add("Error when PUT value \"" + invalidIndataMap.get(anAttributeList).get(i) +
                            "\" to Attribute \"" + anAttributeList + "\". Faulty response code (" + PUTresponse.statusCode() +
                            PUTresponse.body().asString() +  ") expected (400)");
                }
            }
        }
        if (errorList.size() > 0) {
            errorList.forEach(System.out::println);
            fail("Testcase verifyInvalidAttributeFormatOrganization ended with warnings according to printed list");
        }
    }

    @Test
    @DisplayName("PUT unit: Invalid indata format")
    @Tag("indatavalidation")
    @Tag("PUT")
    void verifyInvalidAttributeFormatUnit(@Testdata.ObjectAttr("Unit") ObjectAttribute objAttr) throws ParseException {
        String unitName = "Nordic Unit";
        RequestSpecification request = RestAssured.given();
        Response PUTresponse;
        JSONParser parser = new JSONParser();
        JSONObject orgJson;
        List<String> errorList = new ArrayList<>();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

        List<String> attributeList = objAttr.getAllAttribute();
        for (String anAttributeList : attributeList) {
            for (int i = 0; i < invalidIndataMap.get(anAttributeList).size(); i++) {

                orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
                orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
                orgJson.remove("uuid");

                //If the JSONObject is an JSONArray
                if (orgJson.get(anAttributeList) instanceof JSONArray) {
                    JSONArray testDataList = new JSONArray();
                    testDataList.add(invalidIndataMap.get(anAttributeList).get(i));
                    orgJson.replace(anAttributeList, testDataList);
                } else {//Expect the JSONObject to be a string
                    orgJson.replace(anAttributeList, invalidIndataMap.get(anAttributeList).get(i));
                }

                JSONObject orgPutJson = new JSONObject();
                orgPutJson.put("unit", orgJson);
                orgPutJson.put("modificationType", "merge");

                request.header("Content-Type", "application/json");
                request.body(orgPutJson.toJSONString());

                PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

                if (PUTresponse.statusCode() != 400) {
                    errorList.add("Error when PUT value \"" + invalidIndataMap.get(anAttributeList).get(i) +
                            "\" to Attribute \"" + anAttributeList + "\". Faulty response code (" + PUTresponse.statusCode() +
                            PUTresponse.body().asString() + ") expected (400)");
                }
            }
        }
        if (errorList.size() > 0) {
            errorList.forEach(System.out::println);
            fail("Testcase verifyInvalidAttributeFormatUnit ended with warnings according to printed list");
        }
    }

    @Test
    @DisplayName("PUT function: Invalid indata format")
    @Tag("indatavalidation")
    @Tag("PUT")
    void verifyInvalidAttributeFormatFunction(@Testdata.ObjectAttr("Function") ObjectAttribute objAttr) throws ParseException {
        String functionName = "Nordic Function";
        RequestSpecification request = RestAssured.given();
        Response PUTresponse;
        JSONParser parser = new JSONParser();
        JSONObject orgJson;
        List<String> errorList = new ArrayList<>();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);

        List<String> attributeList = objAttr.getAllAttribute();
        for (String anAttributeList : attributeList) {
            for (int i = 0; i < invalidIndataMap.get(anAttributeList).size(); i++) {

                orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
                orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
                orgJson.remove("uuid");

                //If the JSONObject is an JSONArray
                if (orgJson.get(anAttributeList) instanceof JSONArray) {
                    JSONArray testDataList = new JSONArray();
                    testDataList.add(invalidIndataMap.get(anAttributeList).get(i));
                    orgJson.replace(anAttributeList, testDataList);
                } else {//Expect the JSONObject to be a string
                    orgJson.replace(anAttributeList, invalidIndataMap.get(anAttributeList).get(i));
                }

                JSONObject orgPutJson = new JSONObject();
                orgPutJson.put("function", orgJson);
                orgPutJson.put("modificationType", "merge");

                request.header("Content-Type", "application/json");
                request.body(orgPutJson.toJSONString());

                PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);

                if (PUTresponse.statusCode() != 400) {
                    errorList.add("Error when PUT value \"" + invalidIndataMap.get(anAttributeList).get(i) +
                            "\" to Attribute \"" + anAttributeList + "\". Faulty response code (" + PUTresponse.statusCode() +
                            PUTresponse.body().asString() + ") expected (400)");
                }
            }
        }
        if (errorList.size() > 0) {
            errorList.forEach(System.out::println);
            fail("Testcase verifyInvalidAttributeFormatFunction ended with warnings according to printed list");
        }
    }
    @Test
    @DisplayName("PUT organization: Valid indata format")
    @Tag("validIndata")
    @Tag("PUT")
    void verifyValidAttributeFormatOrganization(@Testdata.ObjectAttr("Organization") ObjectAttribute objAttr) throws ParseException {
        String orgName = "Nordic MedTest";
        RequestSpecification request = RestAssured.given();
        Response PUTresponse;
        JSONParser parser = new JSONParser();
        JSONObject orgJson;
        List<String> errorList = new ArrayList<>();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);

        List<String> attributeList = objAttr.getAllAttribute();
        for (String anAttributeList : attributeList) {
            for (int i = 0; i < validIndataMap.get(anAttributeList).size(); i++) {

                if (!(anAttributeList.equals("name")||anAttributeList.equals("type")) ){

                    orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
                    orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
                    orgJson.remove("uuid");

                    //If the JSONObject is an JSONArray
                    if (orgJson.get(anAttributeList) instanceof JSONArray) {
                        JSONArray testDataList = new JSONArray();
                        testDataList.add(validIndataMap.get(anAttributeList).get(i));
                        orgJson.replace(anAttributeList, testDataList);
                    } else {//Expect the JSONObject to be a string
                        orgJson.replace(anAttributeList, validIndataMap.get(anAttributeList).get(i));
                    }

                    JSONObject orgPutJson = new JSONObject();
                    orgPutJson.put("organization", orgJson);
                    orgPutJson.put("modificationType", "merge");

                    request.header("Content-Type", "application/json");
                    request.body(orgPutJson.toJSONString());

                    PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/" + orgName);

                    if (PUTresponse.statusCode() != 200) {
                        errorList.add("Error when PUT value \"" + validIndataMap.get(anAttributeList).get(i) +
                                "\" to Attribute \"" + anAttributeList + "\". Faulty response code (" + PUTresponse.statusCode() +
                                PUTresponse.body().asString() + ") expected (200)");
                    }
                }
            }
        }
        if (errorList.size() > 0) {
            errorList.forEach(System.out::println);
            fail("Testcase verifyInvalidAttributeFormatOrganization ended with warnings according to printed list");
        }
    }
    @Test
    @DisplayName("PUT unit: Valid indata format")
    @Tag("indata")
    @Tag("PUT")
    void verifyValidAttributeFormatUnit(@Testdata.ObjectAttr("Unit") ObjectAttribute objAttr) throws ParseException {
        String unitName = "Nordic Unit";
        RequestSpecification request = RestAssured.given();
        Response PUTresponse;
        JSONParser parser = new JSONParser();
        JSONObject orgJson;
        List<String> errorList = new ArrayList<>();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

        List<String> attributeList = objAttr.getAllAttribute();
        for (String anAttributeList : attributeList) {
            for (int i = 0; i < validIndataMap.get(anAttributeList).size(); i++) {
                if (!(anAttributeList.equals("name") || anAttributeList.equals("type"))) {
                    orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
                    orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
                    orgJson.remove("uuid");

                    //If the JSONObject is an JSONArray
                    if (orgJson.get(anAttributeList) instanceof JSONArray) {
                        JSONArray testDataList = new JSONArray();
                        testDataList.add(validIndataMap.get(anAttributeList).get(i));
                        orgJson.replace(anAttributeList, testDataList);
                    } else {//Expect the JSONObject to be a string
                        orgJson.replace(anAttributeList, validIndataMap.get(anAttributeList).get(i));
                    }

                    JSONObject orgPutJson = new JSONObject();
                    orgPutJson.put("unit", orgJson);
                    orgPutJson.put("modificationType", "merge");

                    request.header("Content-Type", "application/json");
                    request.body(orgPutJson.toJSONString());
                    PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/" + unitName);

                    if (!(PUTresponse.statusCode() == 200 || PUTresponse.statusCode() == 204)) {
                        errorList.add("Error when PUT value \"" + validIndataMap.get(anAttributeList).get(i) +
                                "\" to Attribute \"" + anAttributeList + "\". Faulty response code (" + PUTresponse.statusCode() +
                                PUTresponse.body().asString() + ") expected (200 or 204)");
                    }
                }
            }
        }
        if (errorList.size() > 0) {
            errorList.forEach(System.out::println);
            fail("Testcase verifyInvalidAttributeFormatUnit ended with warnings according to printed list");
        }
    }
    @Test
    @DisplayName("PUT function: Valid indata format")
    @Tag("validIndata")
    @Tag("PUT")
    void verifyValidAttributeFormatFunction(@Testdata.ObjectAttr("Function") ObjectAttribute objAttr) throws ParseException {
        String functionName = "Nordic Function";
        RequestSpecification request = RestAssured.given();
        Response PUTresponse;
        JSONParser parser = new JSONParser();
        JSONObject orgJson;
        List<String> errorList = new ArrayList<>();

        Response GETresponse = expect()
                .statusCode(200)
                .given()
                .get("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);

        List<String> attributeList = objAttr.getAllAttribute();
        for (String anAttributeList : attributeList) {
            for (int i = 0; i < validIndataMap.get(anAttributeList).size(); i++) {

                orgJson = (JSONObject) parser.parse(GETresponse.getBody().asString());
                orgJson.remove("metaData"); //Strip the GET response free from metaData and uuid
                orgJson.remove("uuid");

                //If the JSONObject is an JSONArray
                if (orgJson.get(anAttributeList) instanceof JSONArray) {
                    JSONArray testDataList = new JSONArray();
                    testDataList.add(validIndataMap.get(anAttributeList).get(i));
                    orgJson.replace(anAttributeList, testDataList);
                } else {//Expect the JSONObject to be a string
                    orgJson.replace(anAttributeList, validIndataMap.get(anAttributeList).get(i));
                }

                JSONObject orgPutJson = new JSONObject();
                orgPutJson.put("function", orgJson);
                orgPutJson.put("modificationType", "merge");

                request.header("Content-Type", "application/json");
                request.body(orgPutJson.toJSONString());

                PUTresponse = request.put("/countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/functions/" + functionName);

                if (!(PUTresponse.statusCode() == 200 || PUTresponse.statusCode() == 204)) {
                    errorList.add("Error when PUT value \"" + validIndataMap.get(anAttributeList).get(i) +
                            "\" to Attribute \"" + anAttributeList + "\". Faulty response code (" + PUTresponse.statusCode() +
                            PUTresponse.body().asString() +") expected (200 or 204)");
                }
            }
        }
        if (errorList.size() > 0) {
            errorList.forEach(System.out::println);
            fail("Testcase verifyInvalidAttributeFormatFunction ended with warnings according to printed list");
        }
    }
}
