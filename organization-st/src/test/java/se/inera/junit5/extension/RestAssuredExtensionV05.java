package se.inera.junit5.extension;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.Properties;
import java.util.logging.Logger;

public class RestAssuredExtensionV05 implements BeforeAllCallback, AfterAllCallback {
    private Properties props = new Properties();
    private static final Logger LOG = Logger.getLogger(RestAssuredExtensionV05.class.getName());

    public RestAssuredExtensionV05() {
        try {
            props.load(RestAssuredExtensionV05.class.getResourceAsStream("testv05.properties"));

        } catch (Exception e) {
            LOG.warning("Unable to find testv05.properties, will try with default values.");
        }
    }

    @Override
    public void afterAll(ExtensionContext context) {
        RestAssured.reset();
    }

    @Override
    public void beforeAll(ExtensionContext context) {
        int port = Integer.parseInt(getProperty("port"));
        String baseUri = getProperty("baseUri");
        String rootPath = getProperty("basePath");

        LOG.info("Running tests on " + baseUri + ":" + port + rootPath);

        RestAssured.port = port;
        RestAssured.baseURI = baseUri;
        RestAssured.basePath = rootPath+"v0.5/";

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
        RestAssured.defaultParser = Parser.JSON;
    }

    private String getProperty(String propertyName) {
        return System.getProperty(propertyName, props.getProperty(propertyName));
    }
}
