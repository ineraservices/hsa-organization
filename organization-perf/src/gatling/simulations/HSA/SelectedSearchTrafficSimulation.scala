package HSA

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class SelectedSearchTrafficSimulation extends Simulation {

	val httpProtocol = http
		.baseUrl("https://organization-dhsa.app-test1.ind-ocp.sth.basefarm.net")
		.inferHtmlResources()
		.acceptHeader("application/json, application/javascript, text/javascript, text/json")
		.acceptEncodingHeader("gzip,deflate")
		.contentTypeHeader("application/json; charset=UTF-8")
		.userAgentHeader("Apache-HttpClient/4.5.3 (Java/11.0.4)")

	val headers_0 = Map("Proxy-Connection" -> "Keep-Alive")

	val searchGETendPointOneLevelCountry = exec(http("request_0")
			.get("/organization/resources/v0.3/search/one/parent-path/countries/Sverige/")
			.headers(headers_0)
			.check(status.is(200)))
			
	val searchGETendPointOneLevelOrganization = exec(http("request_1")
			.get("/organization/resources/v0.3/search/one/parent-path/countries/Sverige/organizations/Svensk%20Organisation")
			.headers(headers_0)
			.check(status.is(200)))
			
	val searchGETendPointOneLevelCounty = exec(http("request_2")
			.get("/organization/resources/v0.3/search/one/parent-path/countries/Sverige/counties/V%C3%A4rmlands%20l%C3%A4n")
			.headers(headers_0)
			.check(status.is(200)))
			
	val	searchGETendPointOneLevelFunction = exec(http("request_3")
			.get("/organization/resources/v0.3/search/one/parent-path/countries/Sverige/counties/V%C3%A4rmlands%20l%C3%A4n/organizations/Nordic%20MedTest")
			.headers(headers_0)
			.check(status.is(200)))
			
	val searchGETendPointOneLevelUnit = exec(http("request_4")
			.get("/organization/resources/v0.3/search/one/parent-path/countries/Sverige/counties/V%C3%A4rmlands%20l%C3%A4n/organizations/Nordic%20MedTest/units/enhet%20med%20underordnade%20objekt")
			.headers(headers_0)
			.check(status.is(200)))
			
	val	searchGETendPointSubTree = exec(http("request_5")
			.get("/organization/resources/v0.3/search/sub/parent-path/countries/Sverige/counties/V%C3%A4rmlands%20l%C3%A4n/organizations/Nordic%20MedTest")
			.headers(headers_0)
			.check(status.is(200)))
			
	val searchFreeTextGETendPoint = exec(http("request_6")
			.get("/organization/resources/v0.3/search/free-text/%C3%96ster")
			.headers(headers_0)
			.check(status.is(200)))
			
    val	searchFreeTextCaseSensGETendPoint = exec(http("request_7")
			.get("/organization/resources/v0.3/search/free-text/nORDIC%20uNIT")
			.headers(headers_0)
			.check(status.is(200)))
			
    val	searchFreeTextCaseSensOGETendPoint = exec(http("request_8")
			.get("/organization/resources/v0.3/search/free-text/%C3%B6ster")
			.headers(headers_0)
			.check(status.is(200)))

	val searchLoadSenario = scenario("SearchLoadSenario")
			.exec(searchGETendPointOneLevelCountry)
			.exec(searchGETendPointOneLevelOrganization)
			.exec(searchGETendPointOneLevelCounty)
			.exec(searchGETendPointOneLevelFunction)
			.exec(searchGETendPointOneLevelUnit)
			.exec(searchGETendPointSubTree)
			.exec(searchFreeTextGETendPoint)
			.exec(searchFreeTextCaseSensGETendPoint)
			.exec(searchFreeTextCaseSensOGETendPoint)

	//Setup the total load flow
	setUp(searchLoadSenario.inject(constantUsersPerSec(3) during(10 seconds))).protocols(httpProtocol)
}