# 4. Use redis as backend

Date: 2018-04-18

## Status

Accepted

## Context

We need an in-memory-backend store

## Decision

Redis has a proven and stable track record with several interesting modules that fits our purposes, like graph, search and json.

## Consequences

Complete vendor lock-in, moving to another json-store would require refactoring of all backend controllers.
