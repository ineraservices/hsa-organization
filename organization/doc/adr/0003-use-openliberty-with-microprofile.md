# 3. Use openliberty with microprofile

Date: 2018-04-18

## Status

Accepted

## Context

We need a Java EE 8 (mainly jaxrs, jsonb, cdi) container.

## Decision

Use Openliberty (IBM's opensource version of Websphere), easy configuration, small footprint, quick startup, quick redeploy.

## Consequences

We do not intend to use any specific Openliberty features, any Java EE 8 container should work.
