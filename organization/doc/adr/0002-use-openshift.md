# 2. Use openshift

Date: 2018-04-18

## Status

Accepted

## Context

We need a package structure for delivering the service to operations.

## Decision

Use Openshift as the "infrastructure as code" framework for packaging the application.

## Consequences

Small lockin to the openshift.yml, but the rest of the components are more or less de facto standards, like docker, kubernetes, jenkins.
Moving away from openshift to docker-compose or kubernetes would be a small thing to do.
