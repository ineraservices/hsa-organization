package se.inera.hsa.org.rules;

import se.inera.hsa.org.model.version_0_5.Employee;
import se.inera.hsa.org.model.version_0_5.Function;
import se.inera.hsa.org.model.version_0_5.Organization;
import se.inera.hsa.org.model.version_0_5.Unit;
import se.inera.hsa.org.drools.DroolsValidationResult;
import se.inera.hsa.org.drools.ValidationCode;
import se.inera.hsa.org.drools.RegexConstants;
import se.inera.hsa.clients.CodeSystemClient;

// "fel/varning och info"-samlare, så att beslut kan tas i javakoden
global DroolsValidationResult droolsValidationResult;

// en extern tjänst
global CodeSystemClient codeSystemClient;

// -------- Unit Rules

rule "Mandatory unit attribute - name"
    when
        unit : Unit( name == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "name", "Enheten har inte ett värde på attributet name");
end

rule "Mandatory unit attribute - hsaIdentity"
    when
        unit : Unit( hsaIdentity == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "hsaIdentity", "Enheten har inte ett värde på attributet hsaIdentity");
end

rule "Mandatory unit attribute - type"
    when
        unit : Unit( type == null || type != "Unit" )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "type", "Enheten har inte värdet Unit på attributet type");
end


rule "Unit hsaHealthCareProvider Mandatory attribute - orgNo"
    when
        unit : Unit( hsaHealthCareProvider && orgNo == null)
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "orgNo", "orgNo måste vara satt ifall enheten är en hsaHealthCareProvider");
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "hsaHealthCareProvider", "orgNo måste vara satt ifall enheten är en hsaHealthCareProvider");
end

rule "Unit mail syntax"
    when
        unit : Unit( mail != null && mail not matches RegexConstants.VALID_MAIL )
    then
    	//TODO returnera vilket som var det felaktiga tecknet
        droolsValidationResult.addWarning(ValidationCode.ILLEGAL_SYNTAX, "mail", "Felaktig syntax");
end

// -------- Organization Rules

rule "Mandatory org attribute - name"
    when
        org : Organization( name == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "name", "Organisationen har inte ett värde på attributet name");
end

rule "Mandatory org attribute - hsaHpt"
    when
        org : Organization( hsaHpt == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "hsaHpt", "Organisationen har inte ett värde på attributet hsaHpt");
end

rule "Mandatory org attribute - mail"
    when
        org : Organization( mail == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "mail", "Organisationen har inte ett värde på attributet mail");
end

rule "Mandatory org attribute - orgNo"
    when
        org : Organization( orgNo == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "orgNo", "Organisationen har inte ett värde på attributet orgNo");
end

rule "Mandatory org attribute - postalAddress"
    when
        org : Organization( postalAddress == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "postalAddress", "Organisationen har inte ett värde på attributet postalAddress");
end

rule "Mandatory org attribute - telephoneNumber"
    when
        org : Organization( telephoneNumber == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "telephoneNumber", "Organisationen har inte ett värde på attributet telephoneNumber");
end

// -------- Function Rules


rule "Mandatory function attribute - name"
    when
        func : Function( name == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "name", "Funktionen har inte ett värde på attributet name");
end

rule "Mandatory function attribute - hsaIdentity"
    when
        func : Function( hsaIdentity == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "hsaIdentity", "Funktionen har inte ett värde på attributet hsaIdentity");
end

rule "Mandatory function attribute - type"
    when
        func : Function( type == null || type != "Function" )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "type", "Funktionen har inte värdet Function på attributet type");
end

// -------- Employee Rules

rule "Mandatory employee attribute - name"
    when
        employee : Employee( name == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "name", "Personen har inte ett värde på attributet name");
end

rule "Mandatory employee attribute - hsaIdentity"
    when
        employee : Employee( hsaIdentity == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "hsaIdentity", "Personen har inte ett värde på attributet hsaIdentity");
end

rule "Mandatory employee attribute - type"
    when
        employee : Employee( type == null || type != "Function" )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "type", "Personen har inte värdet Employee på attributet type");
end

rule "Mandatory employee attribute - fullName"
    when
        employee : Employee( fullName == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "fullName", "Personen har inte värde ett värde på attributet fullName");
end

rule "Mandatory employee attribute - sn"
    when
        employee : Employee( sn == null )
    then
        droolsValidationResult.addError(ValidationCode.MANDATORY_ATTRIBUTE_NOT_SET, "sn", "Personen har inte värde ett värde på attributet sn");
end
