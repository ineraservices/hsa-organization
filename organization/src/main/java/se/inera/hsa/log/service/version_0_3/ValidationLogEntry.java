package se.inera.hsa.log.service.version_0_3;

import java.util.Map;

public class ValidationLogEntry {

    public String adminHsaIdentity;

    public String eventType;

    public String requestPath;

    public Map<String, String> warnings;


    public ValidationLogEntry(String adminHsaIdentity, String eventType, String requestPath, Map<String, String> warnings) {
        this.adminHsaIdentity = adminHsaIdentity;
        this.eventType = eventType;
        this.requestPath = requestPath;
        this.warnings = warnings;
    }

}