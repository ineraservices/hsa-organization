package se.inera.hsa.log.service.version_0_3;

import java.time.ZonedDateTime;

public class AuditLogEntry {

    private String adminUUID;

    private ZonedDateTime timeStamp;

    private String eventType;

    private String requestPath;

    private AuditLogEntry(){}

    public AuditLogEntry(String adminUUID, ZonedDateTime timeStamp, String eventType, String requestPath) {
        this.adminUUID = adminUUID;
        this.timeStamp = timeStamp;
        this.eventType = eventType;
        this.requestPath = requestPath;
    }

    public String getAdminUUID() {
        return adminUUID;
    }

    public ZonedDateTime getTimeStamp() {
        return timeStamp;
    }

    public String getEventType() {
        return eventType;
    }

    public String getRequestPath() {
        return requestPath;
    }

}