package se.inera.hsa.log.service.version_0_3;

import se.inera.hsa.jsonb.JsonbUtil;

import java.util.Map;

public class HsaValidationLog {

    public ValidationLogEntry validationLogEntry;

    public HsaValidationLog(String adminUUID, String eventType, String resourcePath, Map<String, String> warnings) {
        this.validationLogEntry = new ValidationLogEntry(adminUUID, eventType, resourcePath, warnings);
    }

    String toJson() {
        return JsonbUtil.jsonb().toJson(this);
    }
}
