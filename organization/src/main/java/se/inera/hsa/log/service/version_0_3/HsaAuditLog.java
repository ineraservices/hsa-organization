package se.inera.hsa.log.service.version_0_3;

import se.inera.hsa.jsonb.JsonbUtil;

import java.time.ZonedDateTime;

public class HsaAuditLog {

    private AuditLogEntry hsaAuditLogEntry;

    private HsaAuditLog() {
    }

    public HsaAuditLog(String adminUUID, ZonedDateTime timeStamp, String eventType, String resourceUUID) {
        this.hsaAuditLogEntry = new AuditLogEntry(adminUUID, timeStamp, eventType, resourceUUID);
    }

    public AuditLogEntry getHsaAuditLogEntry() {
        return hsaAuditLogEntry;
    }

    String toJson() {
        return JsonbUtil.jsonb().toJson(this);
    }
}
