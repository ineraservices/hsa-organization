package se.inera.hsa.log.service;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class TechnicalLogger {

    private static final String TAG = "technical";

    private final List<LogLevel> logLevels;

    public TechnicalLogger(String logLevelsString) {
        if (logLevelsString == null) {
            String errorMessage = "You have to add a log level in your config file! format: technical.log.levels = ERROR,INFO";
            throw new IllegalArgumentException(errorMessage);
        }

        logLevels = new ArrayList<>();

        String[] split = logLevelsString
                .replace(" ", "")
                .split(",");
        for (String loglevel : split) {
            logLevels.add(LogLevel.get(loglevel));
        }

    }

    private boolean log(String tag, String name, String messageAsJson) {
        System.out.println("tag: " + tag + " level: " + name + " message: " + messageAsJson);
        return true;
    }

    public boolean info(String message) {
        if (dontLog(LogLevel.INFO)) {
            return false;
        }
        return log(TAG, LogLevel.INFO.name(), infoMessageAsJson(message));
    }

    public boolean error(String message) {
        if (dontLog(LogLevel.ERROR)) {
            return false;
        }
        return log(TAG, LogLevel.ERROR.name(), errorMessageAsJson(message));
    }

    public boolean error(String message, Exception exception) {
        if (dontLog(LogLevel.ERROR)) {
            return false;
        }
        return log(TAG, LogLevel.ERROR.name(), errorMessageAsJson(message, exception));
    }

    private static String exceptionToString(Exception e) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        e.printStackTrace(ps);
        ps.close();
        return baos.toString();
    }

    private String infoMessageAsJson(String message) {
        JsonObjectBuilder infoMessageJson = Json.createObjectBuilder();

        infoMessageJson.add("message", message);

        return infoMessageJson.build().toString();
    }

    private String errorMessageAsJson(String message, Exception exception) {
        JsonObjectBuilder errorMessageJson = Json.createObjectBuilder();

        errorMessageJson.add("message", message);
        errorMessageJson.add("exception", exceptionToString(exception));

        return errorMessageJson.build().toString();
    }

    private String errorMessageAsJson(String message) {
        JsonObjectBuilder errorMessageJson = Json.createObjectBuilder();

        errorMessageJson.add("message", message);
        errorMessageJson.add("exception", "N/A");

        return errorMessageJson.build().toString();
    }

    private boolean dontLog(LogLevel logLevel) {
        return !logLevels.contains(logLevel);
    }

    enum LogLevel {
        NONE,
        INFO,
        ERROR;

        public static LogLevel get(String string) {
            for (LogLevel logLevel : LogLevel.values()) {
                if (string.toUpperCase().equals(logLevel.name().toUpperCase())) {
                    return logLevel;
                }
            }
            return LogLevel.NONE;
        }
    }

}
