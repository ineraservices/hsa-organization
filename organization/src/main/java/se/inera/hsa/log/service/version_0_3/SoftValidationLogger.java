package se.inera.hsa.log.service.version_0_3;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.constraints.NotNull;
import java.util.Map;

@ApplicationScoped
public class SoftValidationLogger {

    private Logger LOGGER = LogManager.getLogger(SoftValidationLogger.class);

    public void log(@NotNull Level level,
                    @NotNull String hsaIdentity,
                    @NotNull AuditLogger.eventTypes eventType,
                    @NotNull String requestPath,
                    @NotNull Map<String, String> warnings) {

        LOGGER.log(level, new HsaValidationLog(hsaIdentity, eventType.name(), requestPath, warnings).toJson());
    }
}
