package se.inera.hsa.log.service.version_0_3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@ApplicationScoped
public class AuditLogger {

    private Logger LOGGER = LogManager.getLogger(AuditLogger.class);

    public enum eventTypes {
        GET_COUNTRY,
        ADD_COUNTRY,
        DELETE_COUNTRY,
        MODIFY_COUNTRY,
        MOVE_COUNTRY,
        GET_COUNTY,
        ADD_COUNTY,
        DELETE_COUNTY,
        MODIFY_COUNTY,
        MOVE_COUNTY,
        GET_ORGANIZATION,
        ADD_ORGANIZATION,
        DELETE_ORGANIZATION,
        MODIFY_ORGANIZATION,
        MOVE_ORGANIZATION,
        GET_UNIT,
        ADD_UNIT,
        DELETE_UNIT,
        MODIFY_UNIT,
        MOVE_UNIT,
        GET_FUNCTION,
        ADD_FUNCTION,
        DELETE_FUNCTION,
        MODIFY_FUNCTION,
        MOVE_FUNCTION,
        SEARCH_BY_PARAMS,
        SUB_SEARCH,
        ONE_SEARCH,
        FREE_TEXT_SEARCH,
        GET_EMPLOYEE,
        ADD_EMPLOYEE,
        MODIFY_EMPLOYEE,
        DELETE_EMPLOYEE, 
        GET_PATH
    }


    public void log(@NotNull String adminHsaIdentity,
                    @NotNull ZonedDateTime timestamp,
                    @NotNull eventTypes eventType,
                    @NotNull String requestPath) {
        System.out.println(new HsaAuditLog(adminHsaIdentity, timestamp, eventType.name(), requestPath).toJson());
    }

    public void logWithLogger(@NotNull String adminHsaIdentity,
                              @NotNull ZonedDateTime timestamp,
                                @NotNull eventTypes eventType,
                                @NotNull String requestPath) {
        LOGGER.info(new HsaAuditLog(adminHsaIdentity, timestamp, eventType.name(), requestPath).toJson());
    }
}
