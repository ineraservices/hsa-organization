package se.inera.hsa.log.service;


import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;


public class TechnicalLogExposer {
    @Inject
    @ConfigProperty(name="technical.log.levels", defaultValue="ERROR,INFO")
    String logLevels;

    @Produces
    public TechnicalLogger expose() {
        return new TechnicalLogger(logLevels);
    }
}
