package se.inera.hsa.lettuce.commands;

import io.lettuce.core.TransactionResult;
import io.lettuce.core.api.sync.RedisCommands;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class RedisHSATransactionCommands {
    private final RedisCommands<String, String> redisCommandsDelegate;

    public RedisHSATransactionCommands(RedisCommands<String, String> redisCommands) {
        redisCommandsDelegate = redisCommands;
    }

    public void watch(String key) throws IllegalStateException {
        failOrContinue(redisCommandsDelegate.watch(key));
    }

    public void unwatch() throws IllegalStateException {
        failOrContinue(redisCommandsDelegate.unwatch());
    }


    public void multi() throws IllegalStateException, ConcurrentModificationException {
        failOrContinue(redisCommandsDelegate.multi());
    }

    public void exec() throws IllegalStateException {
        List<String> results = new ArrayList<>();
        boolean isErrorInTransaction = false;
        TransactionResult exec = redisCommandsDelegate.exec();
        for (Object transactionResult : exec) {
            String result = convert(transactionResult);
            results.add(result);
            if (!"OK".equalsIgnoreCase(result) && !"[]".equalsIgnoreCase(result) && !"1".equalsIgnoreCase(result)) {
                isErrorInTransaction = true;
            }
        }

        if (exec.wasDiscarded()) {
            throw new ConcurrentModificationException("Transaction failed, because it clashed with another write/update/move or delete operation on the same path.");
        }

        if (isErrorInTransaction) {
            throw new IllegalStateException("This is not allowed to happen! There is a bug in the code! Parts of a redis transaction failed. Transaction result is: (" + String.join(", ", results) + ")");
        }
    }

    private String convert(Object charSequenceObject) {
        return String.valueOf(charSequenceObject);
    }

    private void failOrContinue(String reply) throws IllegalStateException {
        System.out.println("reply: " + reply);
        if (!"OK".equals(reply)) {
            throw new IllegalStateException("This is not allowed to happen! There is a bug in the code!?");
        }
    }
}
