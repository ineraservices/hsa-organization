package se.inera.hsa.lettuce.redisgraph.model;

import java.util.ArrayList;
import java.util.List;

public class GraphQueryResult {

    /**
     *  Example:
     *   if Cypher query = "MATCH (parent)-[:'aRelation']->(child:'aType' {'anID_KEY':'id'}) RETURN parent.id, parent.type, child.type, child.name, parent.rel"
     *   the quer result will be:
     *     Record:
     *       header : [parent.id, parent.type, child.type, child.name, parent.rel]
     *       values: [2c2ca7fd-26db-4fe9-a12c-9170436ba5f2, Organization, Unit, Unit_1087099644, HAS_ORGANIZATIONS]
     *     statistics : [Query internal execution time: 0.599600 milliseconds]
     */

    private List<Record> records;
    private List<String> statistics;

    public GraphQueryResult(List<Object> response) {
        this.records = new ArrayList<>();
        this.statistics = new ArrayList<>();

        if (response != null && !response.isEmpty()) {
            try {
                this.statistics = (List<String>) response.get(1);
                List<List<String>> results = (List<List<String>>) response.get(0);
                if (results != null && !results.isEmpty()) {
                    List<String> headers = results.remove(0);
                    for (List<String> values : results) {
                        Record record = new Record(headers, values);
                        records.add(record);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<String> getStatistics() {
        return this.statistics;
    }

    public List<Record> getRecords() {
        return records;
    }

    /**
     * Use this if you are sure you are supposed to get ONE UNIQUE record back
     *
     * @return one record, if the query was successful but didn't find anything, the record will contain a header with an empty value
     * @exception NotUniqueResultException throws an exception if this is not a one record reply 
     */
    public Record getUniqueRecord() {
        if (records.size() != 1) {
            throw new NotUniqueResultException("You are expecting a unique record, but you are receiving " + records.size() + " records.");
        }
        return records.get(0);
    }
}
