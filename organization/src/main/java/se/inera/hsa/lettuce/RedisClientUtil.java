package se.inera.hsa.lettuce;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;

public class RedisClientUtil {
    private static RedisClient redisClient = null;

    public static synchronized RedisClient getClient(String host) {
        if (redisClient == null) {
            System.out.println("Create new RedisClient...");
            redisClient =  RedisClient.create(RedisURI.Builder.redis(host, 6379).build());
        }
        return redisClient;
    }

    public static RedisClient getClient() {
        return getClient("hsa-redis");
    }
}
