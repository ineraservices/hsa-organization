package se.inera.hsa.lettuce.redisgraph.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Edge {
    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = (Map)Optional.ofNullable(properties).orElse(new HashMap());
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    private String relation;
    private Map<String, String> properties = new HashMap<>();

    private String quoteString(String val) {
        if (val.charAt(0) != '\'') {
            val = '\'' + val;
        }

        if (val.charAt(val.length() -1 ) != '\'') {
            val = val + '\'';
        }
        return val;
    }

    public String toString() {
        String res = "-[:" + relation;
        if( ! properties.isEmpty() ) {
            String p = this.properties.entrySet().stream()
                    .map(entry -> entry.getKey() + ":" + quoteString(entry.getValue()))
                    .collect(Collectors.joining(","));
            res += "{" + p + "}";
        }
        res += "]->";
        return res;
    }

    public static Builder start() {
        return new Builder();
    }

    public static class Builder {
        private Edge edge;

        public Builder() {
            edge = new Edge();
        }

        public Builder relation(String relation) {
            this.edge.relation = relation;
            return this;
        }

        public Builder properties(Map<String, String> p) {
            this.edge.setProperties(p);
            return this;
        }

        public Edge build() {
            return this.edge;
        }
    }
}
