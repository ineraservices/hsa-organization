package se.inera.hsa.lettuce.commands;

import io.lettuce.core.RedisFuture;
import io.lettuce.core.dynamic.Commands;
import io.lettuce.core.dynamic.annotation.Command;
import io.lettuce.core.dynamic.annotation.CommandNaming;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;
import java.util.Map;

import static io.lettuce.core.dynamic.annotation.CommandNaming.Strategy.DOT;

/**
 * Internal,
 * Do not use this interface, use @Link{RedisHSAWriteCommands}, or another implementing class.
 */
interface RedisTransactionSafeCommands extends Commands {
    @Command("SET :key :value")
    RedisFuture<String> set(
            @Param("key") String key,
            @Param("value") String value);

    @Command("JSON.SET :key . :json NX")
    RedisFuture<String> jsonSetIfNotExists(
            @Param("key") String key,
            @Param("json") String json);

    @Command("JSON.DEL :key .")
    RedisFuture<Long> jsonDel(
            @Param("key") String key);

    @Command("JSON.SET :key . :json XX")
    RedisFuture<String> jsonSetIfExists(
            @Param("key") String key,
            @Param("json") String json);

    @Command("FT.ADD :index :id :score FIELDS :fields")
    RedisFuture<String> ftAdd(
            @Param("index") String index,
            @Param("id") String id,
            @Param("score") String score,
            @Param("fields") Map<String, String> fields);

    @CommandNaming(strategy = DOT)
    RedisFuture<String> ftAdd(
            String index,
            String id,
            String score,
            String replace,
            String partial,
            String fieldsLabel,
            Map<String, String> fields);

    @CommandNaming(strategy = DOT)
    RedisFuture<Long> ftDel(String index, String id, String deleteDocument);

    @CommandNaming(strategy = DOT)
    RedisFuture<List<Object>> graphQuery(String name, String query);
}
