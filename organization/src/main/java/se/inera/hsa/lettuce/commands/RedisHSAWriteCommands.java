package se.inera.hsa.lettuce.commands;

import io.lettuce.core.dynamic.RedisCommandFactory;

import java.util.Map;

import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.*;

public class RedisHSAWriteCommands {

    private final RedisTransactionSafeCommands redisTransactionSafeCommandsDelegate;
    private final RedisJSONCommands redisJSONCommandsDelegate;
    private final RedisSearchCommands redisSearchCommandsDelegate;

    public RedisHSAWriteCommands(RedisCommandFactory factory) {
        redisTransactionSafeCommandsDelegate = factory.getCommands(RedisTransactionSafeCommands.class);
        redisJSONCommandsDelegate = factory.getCommands(RedisJSONCommands.class);
        redisSearchCommandsDelegate = factory.getCommands(RedisSearchCommands.class);
    }

    public void set(String key, String value) {
        redisTransactionSafeCommandsDelegate.set(key, value);
    }

    public void jsonSetIfExists(String key, String json) {
        redisTransactionSafeCommandsDelegate.jsonSetIfExists(ORGANIZATION_JSON_ID_PREFIX + key, json);
    }

    public void jsonSetIfNotExists(String key, String json) {
        redisTransactionSafeCommandsDelegate.jsonSetIfNotExists(ORGANIZATION_JSON_ID_PREFIX + key, json);
    }

    public void ftAdd(String id, String index, String prefix, String score, Map<String, String> fields) {
        redisTransactionSafeCommandsDelegate.ftAdd(index, prefix + id, score, fields);
    }

    public void ftUpdate(String id, String index, String prefix, String score, Map<String, String> fields) {
        redisTransactionSafeCommandsDelegate.ftAdd(
                index,
                prefix + id,
                score,
                INDEX_REPLACE_DOCUMENT,
                INDEX_PARTIAL,
                INDEX_FIELDS_LABEL,
                fields);
    }

    public boolean noIndexExists(String docId, String indexName, String prefix) {
        Map<String, String> index = redisSearchCommandsDelegate.ftGet(indexName, prefix + docId);
        return (index.get("type") == null);
    }

    public void ftDel(String docId, String index, String prefix) {
        redisTransactionSafeCommandsDelegate.ftDel(index, prefix + docId, "DD");
    }

    public boolean noJsonExist(String key) {
        String jsonBlob = redisJSONCommandsDelegate.jsonGet(ORGANIZATION_JSON_ID_PREFIX + key);
        return !jsonBlob.contains("type");
    }

    public void graphQuery(String query) {
        redisTransactionSafeCommandsDelegate.graphQuery(GRAPH_NAME, query);
    }

    public void jsonDel(String id) {
        redisTransactionSafeCommandsDelegate.jsonDel(ORGANIZATION_JSON_ID_PREFIX + id);
    }

    public void updateNode(String uuid, String name, String relation, String parentTargetId, String parentSourceId) {
        String updateNodeQuery = String.format("MATCH (node {id:'%s'}) SET node.name = '%s', node.pid = '%s'", uuid, name, parentTargetId);
        graphQuery(updateNodeQuery);
        String deleteRelationQuery = String.format("MATCH (parent {id:'%s'})-[relation:%s]->(child {id:'%s'}) DELETE relation", parentSourceId, relation, uuid);
        graphQuery(deleteRelationQuery);
        String addRelationQuery = String.format("MATCH (parent {id:'%s'}), (child {id:'%s'}) CREATE (parent)-[:%s]->(child)", parentTargetId, uuid, relation);
        graphQuery(addRelationQuery);
    }
}
