package se.inera.hsa.lettuce.commands;

import io.lettuce.core.dynamic.Commands;
import io.lettuce.core.dynamic.annotation.Command;
import io.lettuce.core.dynamic.annotation.CommandNaming;
import io.lettuce.core.dynamic.annotation.Param;

import static io.lettuce.core.dynamic.annotation.CommandNaming.Strategy.DOT;

/**
 * @author kdaham
 * Command interface to reJSON
 * @see http://rejson.io/commands/
 */
public interface RedisJSONCommands extends Commands {

    @CommandNaming(strategy = DOT)
    String jsonGet(String key, String NOESCAPE, String path);

    @Command("JSON.GET :key NOESCAPE .")
    String jsonGet(
            @Param("key") String key);

    @CommandNaming(strategy = DOT)
    String jsonSet(String key, String path, String json);
}