package se.inera.hsa.lettuce.redisgraph.model;

class NotUniqueResultException extends RuntimeException {
    NotUniqueResultException(String message) {
        super(message);
    }
}
