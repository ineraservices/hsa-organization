package se.inera.hsa.lettuce.redisgraph.model;

import java.util.List;

public class Record {

    private final List<String> headers;
    private final List<String> values;

    Record(List<String> header, List<String> values) {
        this.headers = header;
        this.values = values;
    }

    public String getValue(int index) {
        return this.values.get(index);
    }

    public String getValue(String header) {
        return getValue(this.headers.indexOf(header));
    }
}
