package se.inera.hsa.lettuce.commands;

import io.lettuce.core.dynamic.Commands;
import io.lettuce.core.dynamic.annotation.Command;
import io.lettuce.core.dynamic.annotation.CommandNaming;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;
import java.util.Map;

import static io.lettuce.core.dynamic.annotation.CommandNaming.Strategy.DOT;

/**
 * Command interface to redis search
 *
 * @see <a href="https://oss.redislabs.com/redisearch/Commands/">RedisSearch Commands</a>
 */
public interface RedisSearchCommands extends Commands {

    @CommandNaming(strategy = DOT)
    String ftAdd(String index, String id, String score, String replace, String partial, String fieldsLabel, Map<String, String> fields);

    @CommandNaming(strategy = DOT)
    String ftAdd(String index, String id, String score, String replace, String fieldsLabel, Map<String, String> fields);

    @CommandNaming(strategy = DOT)
    String ftAdd(String index, String id, String score, String fieldsLabel, Map<String, String> fields);

    @CommandNaming(strategy = DOT)
    void ftDel(String index, String id, String deleteDocument);

    @Command("FT.SEARCH :index :query NOCONTENT LIMIT :first :num")
    List<Object> ftSearch(
            @Param("index") String index,
            @Param("query") String query,
            @Param("first") String first,
            @Param("num") String num
    );

    @Command("FT.GET :index :docId")
    Map<String, String> ftGet(
            @Param("index") String index,
            @Param("docId") String docId);
}
