package se.inera.hsa.lettuce.redisgraph.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Node {
    private Optional<String> alias = Optional.ofNullable(null);
    private Optional<String> label = Optional.ofNullable(null);
    private Map<String, String> properties = new HashMap<>();

    private enum Param {

        TYPE(".type"),
        ID(".id"),
        NAME(".name"),
        PARENT_ID(".parentId"),
        PARENT_TYPE(".parentType"),
        UPWARDS_RELATION("upwards_relation"),
        LDAP_TYPE(".ldap_type");

        Param(String value) {
            this.value = value;
        }

        final String value;
    }

    public Optional<String> getAlias() {
        return this.alias;
    }

    public Optional<String> getAliasTypeParam() {return Optional.ofNullable(this.alias.get() + Param.TYPE.value);}
    public Optional<String> getAliasIdParam() {return Optional.ofNullable(this.alias.get() + Param.ID.value);}
    public Optional<String> getAliasNameParam() {return Optional.ofNullable(this.alias.get() + Param.NAME.value);}
    public Optional<String> getAliasParentIdParam() {return Optional.ofNullable(this.alias.get() + Param.PARENT_ID.value);}
    public Optional<String> getAliasParentTypeParam() {return Optional.ofNullable(this.alias.get() + Param.PARENT_TYPE.value);}
    public Optional<String> getAliasUpwardsRelationParam() {return Optional.ofNullable(this.alias.get() + Param.UPWARDS_RELATION.value);}
    public Optional<String> getAliasLdapTypeParam() {return Optional.ofNullable(this.alias.get() + Param.LDAP_TYPE.value);}

    public void setAlias(String alias) {
        this.alias = Optional.ofNullable(alias);
    }

    public Optional<String> getLabel() {
        return this.label;
    }
    public void setLabel(String label) {
        this.label = Optional.ofNullable(label);
    }

    public Map<String, String> getProperties() {
        return this.properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = (Map)Optional.ofNullable(properties).orElse(new HashMap());
    }


    public Node() {}

    public String toString() {
        String res = "(";
        if (this.alias.isPresent()) {
            res = res + this.alias.get();
        }

        if(this.label.isPresent()) {
            res = res + ":" + this.label.get();
            if (!this.properties.isEmpty()) {
                String p = this.properties.entrySet().stream().map((entry) -> {
                    return entry.getKey() + ":" + this.quoteString(entry.getValue());
                }).collect(Collectors.joining(","));
                res = res + "{" + p + "}";
            }
        }
        return res + ")";
    }

    public static Builder start() {
        return new Builder();
    }

    private String quoteString(String val) {
        if (val.isEmpty()) {
            return "''";
        } else {
            if (val.charAt(0) != '\'') {
                val = '\'' + val;
            }

            if (val.charAt(val.length() - 1) != '\'') {
                val = val + '\'';
            }

            return val;
        }
    }

    public static class Builder {

        private Node node;

        public Builder() {
            node = new Node();
        }

        public Builder alias(String alias) {
            this.node.setAlias(alias);
            return this;
        }

        public Builder label(String label) {
            this.node.setLabel(label);
            return this;
        }

        public Builder properties(Map<String, String> p) {
            this.node.setProperties(p);
            return this;
        }

        public Node build() {
            return this.node;
        }
    }
}
