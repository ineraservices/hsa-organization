package se.inera.hsa.clients;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@ManagedBean
public class CodeSystemClient {

    @Inject
    @ConfigProperty(name = "codesystem.uri")
    private String codeSystemUri;

    public boolean validate(String oid, String code) {
        return ClientBuilder.newClient()
            .target(codeSystemUri)
            .path(oid).path("codes").path(code)
            .request(MediaType.APPLICATION_JSON)
            .get().getStatus() == Response.Status.OK.getStatusCode();
    }
}
