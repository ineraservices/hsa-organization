package se.inera.hsa.org.service.version_0_3;

import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.ForbiddenException;
import se.inera.hsa.org.model.version_0_3.Country;
import se.inera.hsa.org.model.version_0_3.CountryPersistence;
import se.inera.hsa.org.model.version_0_3.CountryResponse;
import se.inera.hsa.org.validation.version_0_3.PatchModification;
import se.inera.hsa.jsonb.JsonbUtil;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.NotFoundException;
import java.util.Collections;
import java.util.List;

@RequestScoped
public class CountryProvider implements Provider<Country> {

    @Inject
    HSARedisReadAPI hsaRedisAPI;

    @Override
    public void persist(@NotNull String id, @NotNull Country entity) {
        throw new ForbiddenException("Not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public String merge(@NotNull String id, @NotNull Country entity) {
        throw new ForbiddenException("Not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public String move(@NotNull String oldPath, String newPath) {
        throw new ForbiddenException("Not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public void remove(@NotNull String id) {
        throw new ForbiddenException("Not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public @NotNull List<CountryResponse> findImmediateSubordinates(@NotNull String path, @NotNull String parentType,
                                                                    @NotNull String subordinatesType) {
        return Collections.singletonList(find(path));
    }

    @Override
    public CountryResponse find(@NotNull String requestPath) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisAPI.toCountryResponse(JsonbUtil.jsonb().fromJson(hsaRedisAPI.findById(id), CountryPersistence.class));
    }
    
    @Override
    public String patch(@NotNull String path, @NotNull List<PatchModification> mods) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }
}
