package se.inera.hsa.org.model.version_0_3;

import javax.validation.Valid;

public class OrganizationPersistence extends Persistence {

    @Valid
    public Organization organization;

}
