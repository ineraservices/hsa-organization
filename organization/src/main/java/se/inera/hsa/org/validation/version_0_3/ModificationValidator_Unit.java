package se.inera.hsa.org.validation.version_0_3;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ModificationValidator_Unit implements ConstraintValidator<ValidModifier_Unit, UnitModification> {


    @Override
    public void initialize(ValidModifier_Unit constraintAnnotation) {
    }

    @Override
    public boolean isValid(UnitModification modification, ConstraintValidatorContext context) {
        if (modification.modificationType.equalsIgnoreCase("merge")) {
            return modification.unit != null && modification.newPath == null;
        } else {
            return (modification.unit == null) && (modification.newPath != null);
        }
    }
}
