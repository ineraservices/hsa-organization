package se.inera.hsa.org.drools;

public class ValidationError {
    private final String rulesetName;
    private ValidationCode errorCode;
    private String className;
    private String attributeName;
    private String errorMessage;

    public ValidationError(String rulesetName, ValidationCode errorCode, String className, String attributeName, String errorMessage) {
        this.rulesetName = rulesetName;
        this.errorCode = errorCode;
        this.className = className;
        this.attributeName = attributeName;
        this.errorMessage = errorMessage;
    }

    public ValidationCode getErrorCode() {
        return errorCode;
    }

    public String getClassName() {
        return className;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "ValidationError{" +
                "rulesetName='" + rulesetName + '\'' +
                ", errorCode=" + errorCode +
                ", className='" + className + '\'' +
                ", attributeName='" + attributeName + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
