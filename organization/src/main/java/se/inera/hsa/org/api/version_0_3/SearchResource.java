package se.inera.hsa.org.api.version_0_3;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.opentracing.Traced;

import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.model.version_0_3.SearchResult;
import se.inera.hsa.org.service.version_0_3.SearchProvider;
import se.inera.hsa.log.service.version_0_3.AuditLogger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.ZonedDateTime;

@Tag(name = "Search")
@Stateless
@Path(ApiVersion.VERSION+"/search")
@Traced
public class SearchResource {

    @Inject
    SearchProvider provider;

    @Inject
    AuditLogger auditLogger;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("simple/{freeText:.*}")
    public SearchResult getAllSubordinatesSimple(@PathParam("freeText") String freeText, @QueryParam("node") final String node) {
        // Redis search, any character of ,.<>{}[]"':;!@#$%^&*()-+=~ will break the text into terms. Thus, we've to escape them here
        System.out.println(" free text " + freeText);
        if (node != null) {
            auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.FREE_TEXT_SEARCH,
                    "subordinates of " + node + " based on free text: " + freeText);
            return provider.performSimpleSearchAndFilterByRootNodeSubordinates(node, freeText);
        } else {
            auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.FREE_TEXT_SEARCH, freeText);
            return provider.performSimpleSearch(freeText) ;
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("free-text/{freeText:.*}")
    public SearchResult getAllSubordinates(@PathParam("freeText") String freeText, @QueryParam("node") final String node) {
        if (node != null) {
            auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.FREE_TEXT_SEARCH,
                    "subordinates of " + node + " based on free text: " + freeText);
            return provider.performSearchAndFilterByRootNodeSubordinates(node, freeText);
        } else {
            auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.FREE_TEXT_SEARCH, freeText);
            return provider.performSearch(freeText);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("one/parent-path/{parentPath:.*}")
    public SearchResult getOneLevel(@PathParam("parentPath") String parentPath) {
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.ONE_SEARCH, parentPath);
        return provider.getChildren(parentPath);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("sub/parent-path/{parentPath:.*}")
    public SearchResult getSubTree(@PathParam("parentPath") String parentPath) {
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.SUB_SEARCH, parentPath);
        return provider.getSubTree(parentPath);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("get-path/{path:.*}")
    public SearchResult getPath(@PathParam("path") String path) {
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_PATH, path);
        return provider.getPath(path);
    }
}
