package se.inera.hsa.org.data.version_0_5.model;

/**
 * Java representation of a complete database entry.
 */
public class EntityResult {

    private String internal_uuid;
    private NodeType node_type;
    private String node_name;
    private String json_object;
    //metadata också, på nåt sätt?

    public EntityResult(){

    }

    public EntityResult(String internal_uuid, String node_type, String node_name, String json_object){
        this.internal_uuid=internal_uuid;
        this.node_name=node_name;
        this.json_object=json_object;
        this.node_type = convertNodeType(node_type);
    }

    public EntityResult(String internal_uuid, NodeType node_type, String node_name, String json_object){
        this.internal_uuid=internal_uuid;
        this.node_name=node_name;
        this.json_object=json_object;
        this.node_type = node_type;
    }

    public NodeType convertNodeType(String node_type) {
        if(node_type.equals(NodeType.COUNTRY.getTypeName()))
            return NodeType.COUNTRY;
        else if(node_type.equals(NodeType.COUNTY.getTypeName()))
            return NodeType.COUNTY;
        else if(node_type.equals(NodeType.EMPLOYEE.getTypeName()))
            return NodeType.EMPLOYEE;
        else if(node_type.equals(NodeType.FUNCTION.getTypeName()))
            return NodeType.FUNCTION;
        else if(node_type.equals(NodeType.ORGANIZATION.getTypeName()))
            return NodeType.ORGANIZATION;
        else if(node_type.equals(NodeType.UNIT.getTypeName()))
            return NodeType.UNIT;
        else if(node_type.equals(NodeType.TOP.getTypeName()))
            return NodeType.TOP;

        throw new IllegalArgumentException(node_type + " is not an accepted NodeType");
    }

    public String getInternal_uuid() {
        return internal_uuid;
    }

    public void setInternal_uuid(String internal_uuid) {
        this.internal_uuid = internal_uuid;
    }

    public NodeType getNode_type() {
        return node_type;
    }

    public void setNode_type(NodeType node_type) {
        this.node_type = node_type;
    }

    public String getNode_name() {
        return node_name;
    }

    public void setNode_name(String node_name) {
        this.node_name = node_name;
    }

    public String getJson_object() {
        return json_object;
    }

    public void setJson_object(String json_object) {
        this.json_object = json_object;
    }
}
