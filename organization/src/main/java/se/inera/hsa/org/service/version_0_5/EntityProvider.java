package se.inera.hsa.org.service.version_0_5;

import static se.inera.hsa.org.api.ApiVersion.VERSION_0_5;
import static se.inera.hsa.org.model.version_0_5.EntityType.*;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import se.inera.hsa.GsonHelper;
import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.PatchException;
import se.inera.hsa.jsonb.JsonbUtil;
import se.inera.hsa.log.service.version_0_3.AuditLogger.eventTypes;
import se.inera.hsa.org.data.version_0_5.OrganizationRepository;
import se.inera.hsa.org.data.version_0_5.model.EntityResult;
import se.inera.hsa.org.data.version_0_5.model.NodeType;
import se.inera.hsa.org.drools.ValidationEngine;
import se.inera.hsa.org.model.unions.MetaData;
import se.inera.hsa.org.model.version_0_5.*;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.UriBuilder;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Syftet med EntityProvider är att skapa ett gränssnitt mot Data Access Layer som sköter om:
 *  - Konvertering mellan JSON-format och Java-objekt
 *  - Indatakontroll och upprätthållande av tvingande verksamhetsregler
 *  - Konstruktion av retur-objekt
 * Varje Resource-klass i API-lagret har en egen instans av EntityProvider parametriserad mot sin egen objekttyp.
 */
public class EntityProvider<T extends Entity> {

    public static final String GRAPH_ROOT_ID = "3ebda4ad-79fc-4c43-ac0c-089d3ba93501";
    public static final String ROOT_PATH = "countries/Sverige";
    public static final String DEFAULT_MODIFIER_PATH = "countries/Sverige/organizations/Värmlands län/employees/admin";

    /**
     * ValidationEngine är klassen som används för att köra reglerna för indatavalidering när ett nytt objekt ska
     * läggas till eller av andra skäl behöver valideras.
     */
    @Inject
    private ValidationEngine<T> validationEngine;

    //Borde den här vara static?
    @Inject
    private OrganizationRepository organizationRepository;

    /**
     * Gson är ett bibliotek för att serialisera och deserialisera JSON-objekt till och från Java. Den tar hand om de
     * flesta klasser som behövs, men ibland behöver man lägga till specialgjorda deserializers som är fallet nedan för
     * ZonedDateTime.
     */
    private GsonBuilder gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, GsonHelper.ZDT_DESERIALIZER);

    public EntityResponse getEntityWithPath(String path) {
        String id = organizationRepository.getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        return getEntity(id);
    }

    @SuppressWarnings("all")
    /**
     * Eftersom parametrisering måste ske vid deklaration krävs det en switch-sats som skapar rätt objekttyp i runtime,
     * annars skalas parametriseringen bort pga Java type erasure. På det här sättet slipper vi skapa olika provider-
     * klasser för alla olika objekttyper.
     */
    public EntityResponse getEntity(String id) {

        EntityResult result = organizationRepository.findById(id);
        NodeType nodeType = result.getNode_type();
        Type entityType = initType(nodeType);
        String json = result.getJson_object();

        switch (nodeType){
            case TOP: throw new IllegalArgumentException("TOP nodes cannot be deserialized.");
            case EMPLOYEE:
                EntityPersistence<Employee> toReturnEmployee = gson.create().fromJson(json, entityType);
                return new EntityResponse<>(toReturnEmployee, convertMetadata(toReturnEmployee), id);
            case COUNTY:
                EntityPersistence<County> toReturnCounty = gson.create().fromJson(json, entityType);
                return new EntityResponse<>(toReturnCounty, convertMetadata(toReturnCounty), id);
            case COUNTRY:
                EntityPersistence<Country> toReturnCountry = gson.create().fromJson(json, entityType);
                return new EntityResponse<>(toReturnCountry, convertMetadata(toReturnCountry), id);
            case ORGANIZATION:
                EntityPersistence<Organization> toReturnOrganization = gson.create().fromJson(json, entityType);
                return new EntityResponse<>(toReturnOrganization, convertMetadata(toReturnOrganization), id);
            case FUNCTION:
                EntityPersistence<Function> toReturnFunction = gson.create().fromJson(json, entityType);
                return new EntityResponse<>(toReturnFunction, convertMetadata(toReturnFunction), id);
            case UNIT:
                EntityPersistence<Unit> toReturnUnit = gson.create().fromJson(json, entityType);
                return new EntityResponse<>(toReturnUnit, convertMetadata(toReturnUnit), id);
        }
        throw new IllegalArgumentException("Unknown node type: "+nodeType.getTypeName());
    }

    /**
     * Den här metoden skapar ett Type-objekt som behövs för att JSON-deserialiseringen ska kunna fungera för olika
     * objekttyper utan att återupprepa en massa kod. Type-objektet skickas in till JSON-parsaren så att den vet vilka
     * attribut som ska matchas till vilket.
     */
    @SuppressWarnings("all")
    private Type initType(NodeType nodeType){
        switch (nodeType){
            case TOP: throw new IllegalArgumentException("TOP nodes cannot be deserialized.");
            case COUNTRY: return new TypeToken<EntityPersistence<Country>>() {}.getType();
            case COUNTY: return new TypeToken<EntityPersistence<County>>() {}.getType();
            case ORGANIZATION: return new TypeToken<EntityPersistence<Organization>>() {}.getType();
            case FUNCTION: return new TypeToken<EntityPersistence<Function>>() {}.getType();
            case EMPLOYEE: return new TypeToken<EntityPersistence<Employee>>() {}.getType();
            case UNIT: return new TypeToken<EntityPersistence<Unit>>() {}.getType();
        }
        throw new IllegalArgumentException("Unknown node type");
    }

    /**
     * Populerar fälten för metadata i responsobjektet. Just nu finns inget stöd för att spåra skapare eller vem som
     * senast ändrade i ett objekt, istället fylls det fältet av ett defaultvärde som är samma för alla, en konstant
     * som heter DEFAULT_MODIFIER_PATH.
     *
     * Det finns troligen mycket potential att optimera den här metoden för att förbättra prestanda.
     */
    private MetaData convertMetadata(EntityPersistence entity) {
        MetaData metaData = new MetaData(entity.createTimestamp,
                entity.uuid,
                DEFAULT_MODIFIER_PATH,
                organizationRepository.getNumImmediateSubordinates(entity.uuid),
                organizationRepository.getNumAllSubordinates(entity.uuid),
                organizationRepository.getPathFromId(entity.uuid));
        if (entity.modifyTimestamp != null && entity.modifierUUID != null) {
            metaData.modifierPath = DEFAULT_MODIFIER_PATH;
            metaData.modifierUUID = entity.modifierUUID;
            metaData.modifyTimestamp = entity.modifyTimestamp;
        }
        return metaData;
    }

    public void removeEntity(String id) {
        organizationRepository.removeEntity(id);
    }

    public void removeEntityWithPath(String path) {
        String id = organizationRepository.getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");
        removeEntity(id);
    }

    /** !!!!!
     * Denna metod är inte färdigimplementerad eller testad, och anropas inte i API-lagret.
     * Det utreds om dess funktionalitet ska ingå i PATCH-anropet.
     */
    public String moveEntity(String oldPath, String newPath) {
        //Check if oldPath is and entity
        String id = organizationRepository.getIdFromPath(oldPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + oldPath + "\"");

        //TODO: Check following steps if move is OK

        //      Check if newparent exist
     // get parentId by allowed types
        String newParentPath = newPath.substring(0, newPath.lastIndexOf("/units"));
        String newParentUuid = organizationRepository.getIdFromPath(newParentPath);
        if (newParentUuid != null) {
            throw new BadRequestException("parent object does not exist", ErrorCodes.NO_SUCH_OBJECT);
        }
        //      Check if can move entity under new Parent (that it is the right type, ex. no units under employee or other similiar stuff)
        if ( !isAllowedEdge(getType(getParentTypeFromPath(newParentPath)), getType(getTypeName(newPath)))) {
            throw new BadRequestException("Child/Parent type is wrong", ErrorCodes.NOT_SUPPORTED);
        }
        //      check that nothing exist in newPath, nothing is named same way in new place
        String someUuid = organizationRepository.getIdFromPath(newPath);
        if (someUuid == null) {
            throw new BadRequestException("Some entity already exist at path \"" + newPath + "\"", ErrorCodes.ENTRY_ALREADY_EXISTS);
        }

        organizationRepository.move(id, newParentUuid);
        return organizationRepository.getPathFromId(id);
    }

    /**
     * Metod för att lägga till objekt i databasen.
     *
     * Just nu sker validering i respektive Resource-klass vilket inte är riktigt bra. Det bör refaktoreras till att
     * istället ske här i EntityProvider, men då behöver man arbeta lite med att göra anropet mer generiskt. Den rad
     * som är bortkommenterad är ett exempel på hur detta anrop kan komma att se ut, men behöver implementeras och
     * testas på riktigt.
     */
    public void addEntity(String parentPath, T entity) {

        //T validatedEntity = validationEngine.validate((T)entity, eventTypes.ADD, parentPath, null);
        EntityPersistence<T> entityPersistence = new EntityPersistence<>();

        entityPersistence.creatorUUID = "not_yet_implemented";
        entityPersistence.createTimestamp = ZonedDateTime.now();
        entityPersistence.uuid = UUID.randomUUID().toString();
        entityPersistence.data = entity;

        String childPath = UriBuilder.fromPath(parentPath).path(entity.type).path(entity.name).build().getPath();

        if ( !isAllowedEdge(getType(getTypeName(parentPath)), getType(getTypeName(childPath)))) {
            throw new BadRequestException("Child/Parent type is wrong", ErrorCodes.NOT_SUPPORTED);
        }
        organizationRepository.addEntity(parentPath, childPath, entityPersistence.uuid, entity.name, entity.type, JsonbUtil.jsonb().toJson(entityPersistence));
    }

    /**
     * Denna metod används för att kontrollera om en viss objekttyp får placeras som barn till en annan objekttyp i
     * trädstrukturen i databasen.
     *
     * Följande logiska regler implementeras:
     * - Top nodes can only have Country nodes as children
     * - Country nodes can only have County or Organization nodes as children
     * - County nodes can only have Organization nodes as children
     * - Organization and Unit nodes can only have Unit, Function or Employee nodes as children
     */
    public static boolean isAllowedEdge(EntityType parentType, EntityType childType) {
        if(parentType.equals(TOP) && childType.equals(COUNTRY))
            return true;

        if (parentType.equals(COUNTRY)
                && (childType.equals(COUNTY) || childType.equals(ORGANIZATION) ))
            return true;

        if (parentType.equals(COUNTY) && childType.equals(ORGANIZATION) )
            return true;

        if (parentType.equals(ORGANIZATION) || (parentType.equals(UNIT))
                && (childType.equals(UNIT) || childType.equals(FUNCTION) || childType.equals(EMPLOYEE)))
            return true;

        return false;
    }
    /**
     * Används för att parsa fram objekttyp för ett objekt utifrån dess path. Splittar pathen till en array utifrån "/"-
     * tecknen och hämtar innehållet från den näst sista, vilket korresponderar till objekttypen i LDAP-pathen.
     *
     * Exempel på resultat:
     * - countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/employees/test Person -> employees
     * - countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/function/test function -> function
     * - countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/test Unit -> units
     */
    public static String getTypeName(String path) {
        String[] split = path.split("/");
        if (split.length >= 2 ) {
            return split[split.length-2];
        }
        return "";
    }

    /**
     * Används för att parsa fram objekttyp för ett objekts parent-objekt utifrån dess path. Splittar pathen till en
     * array utifrån "/"-tecknen och hämtar innehållet från den plats som korresponderar till förälderns objekttyp i
     * LDAP-pathen.
     *
     * Exempel på resultat:
     * - countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/employees/test Person -> organizations
     * - countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/test/function/test function -> units
     * - countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest -> counties
     * - countries/Sverige/organizations/Nordic MedTest -> countries
    */
    private static String getParentTypeFromPath(String path) {
        String[] split = path.split("/");
        if (split.length >= 4 ) {
            return getTypeName(split[split.length-4]);
        }else
            throw new IllegalArgumentException("Could not parse parent type from "+path);
    }

    public static EntityType getType(Entity entity){

        if(entity.type == null)
            throw new IllegalArgumentException("type cannot be null");

        if(entity.type.toLowerCase().equals(COUNTRY.getNameLabel()))
            return COUNTRY;
        else if(entity.type.toLowerCase().equals(COUNTY.getNameLabel()))
            return COUNTY;
        else if(entity.type.toLowerCase().equals(ORGANIZATION.getNameLabel()))
            return ORGANIZATION;
        else if(entity.type.toLowerCase().equals(FUNCTION.getNameLabel()))
            return FUNCTION;
        else if(entity.type.toLowerCase().equals(UNIT.getNameLabel()))
            return UNIT;
        else if(entity.type.toLowerCase().equals(EMPLOYEE.getNameLabel()))
            return EMPLOYEE;
        else if(entity.type.toLowerCase().equals(TOP.getNameLabel()))
            return TOP;
        else
            throw new IllegalArgumentException(entity.type+" is not a valid EntityType");
    }

    /**
     * Den här metoden kan ta emot antingen ett typnamn i singular vilket är vad som anges i Entity, ex "unit", eller
     * så kan den ta emot i plural vilket sker när typen parsas från LDAP-pathen, ex "units". Oavsett så ska det
     * matchas till rätt EntityType.
     */
    public static EntityType getType(String type){

        if(type == null)
            throw new IllegalArgumentException("type cannot be null");

        if(type.toLowerCase().equals(COUNTRY.getNameLabel()) || type.toLowerCase().equals(COUNTRY.getPathLabel()))
            return COUNTRY;
        else if(type.toLowerCase().equals(COUNTY.getNameLabel()) || type.toLowerCase().equals(COUNTY.getPathLabel()))
            return COUNTY;
        else if(type.toLowerCase().equals(ORGANIZATION.getNameLabel()) || type.toLowerCase().equals(ORGANIZATION.getPathLabel()))
            return ORGANIZATION;
        else if(type.toLowerCase().equals(FUNCTION.getNameLabel()) || type.toLowerCase().equals(FUNCTION.getPathLabel()))
            return FUNCTION;
        else if(type.toLowerCase().equals(UNIT.getNameLabel()) || type.toLowerCase().equals(UNIT.getPathLabel()))
            return UNIT;
        else if(type.toLowerCase().equals(EMPLOYEE.getNameLabel()) || type.toLowerCase().equals(EMPLOYEE.getPathLabel()))
            return EMPLOYEE;
        else if(type.toLowerCase().equals(TOP.getNameLabel()) || type.toLowerCase().equals(TOP.getPathLabel()))
            return TOP;
        else
            throw new IllegalArgumentException(type+" is not a valid EntityType");
    }

    /**
     * Patch-metoden tar emot en lista på modifikationer som ska göras för ett objekt och utför sedan de operationer
     * som krävs för att göra alla ändringar. Detta ger ett mer granulärt sätt att ändra i databasen än att helt sonika
     * ta bort och ersätta ett helt JSON-objekt med ett nytt. Det ger också bättre förutsättningar till att skriva en
     * läsbar audit-log.
     *
     * TEPPO! Kanske kan du beskriva dina två TODO-kommentarer
     */
    @SuppressWarnings("unchecked")
    public String patch(String path, List<Modification> mods) {
        String id = organizationRepository.getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        List<String> attributesInModificationList = mods.stream().map( Modification::getAttributeName).distinct().collect(Collectors.toList());

        EntityResult result = organizationRepository.findById(id);
        NodeType nodeType = result.getNode_type();
        Type entityType = initType(nodeType);
        String json = result.getJson_object();
        EntityPersistence<? extends Entity> entityPersistence = gson.create().fromJson(json, entityType);

        Entity entity = entityPersistence.data;

        ModificationUtil patcher = new ModificationUtil();

        try {
            patcher.handleModification(entity, mods);
        } catch (PatchException e) {
            throw new BadRequestException("Error modifying entity: "+e.getMessage(), ErrorCodes.OBJECT_CLASS_VIOLATION);
        }

        T validatedEntity = validationEngine.validate(VERSION_0_5,(T)entity, getEventType(nodeType), path, attributesInModificationList);

        Map<String, String> jsonAttributes = new HashMap<>();

        attributesInModificationList.forEach(attr -> jsonAttributes.put("$.data."+attr, getJsonAttribute(validatedEntity, attr)));
        jsonAttributes.put("$.modifierUUID", "not_yet_implemented");
        jsonAttributes.put("$.modifyTimestamp", ZonedDateTime.now().toString());
        organizationRepository.patch(id, jsonAttributes);

        return organizationRepository.getPathFromId(id);
    }

    eventTypes getEventType(NodeType nodeType) {
        switch (nodeType){
        case TOP: throw new IllegalArgumentException("TOP nodes cannot be deserialized.");
        case EMPLOYEE:
            return eventTypes.MODIFY_EMPLOYEE;
        case COUNTY:
            return eventTypes.MODIFY_COUNTY;
        case COUNTRY:
            return eventTypes.MODIFY_COUNTRY;
        case ORGANIZATION:
            return eventTypes.MODIFY_ORGANIZATION;
        case FUNCTION:
            return eventTypes.MODIFY_FUNCTION;
        case UNIT:
            return eventTypes.MODIFY_UNIT;
        }
        throw new IllegalArgumentException("Unknown node type: "+nodeType.getTypeName());
    }

    public String getJsonAttribute(Object object, String attributeName) {
        try {
            Field field = object.getClass().getField(attributeName);
            Object attribute = field.get(object);
            if ( attribute != null) {
                if ( field.getType().equals(String.class)) {
                    return attribute.toString();
                } else if ( field.getType().equals(Boolean.TYPE) ) {
                    return "CAST('"+attribute.toString()+"' as BIT)";
                } else {
                    return JsonbUtil.jsonb().toJson(attribute);
                }
            }
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
