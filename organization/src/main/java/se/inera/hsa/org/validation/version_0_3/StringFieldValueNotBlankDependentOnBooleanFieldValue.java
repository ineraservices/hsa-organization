package se.inera.hsa.org.validation.version_0_3;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Validerar att ett strängfält {@code stringFieldName} inte är tomt om ett booleanfält {@code dependBooleanFieldName}
 * har ett värde  {@code dependBooleanFieldValueExpected}
 **/
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = StringFieldValueNotBlankDependentOnBooleanFieldValueValidator.class)
@Documented
public @interface StringFieldValueNotBlankDependentOnBooleanFieldValue {

    String stringFieldName();
    String dependBooleanFieldName();
    String dependBooleanFieldValueExpected();

    String message() default "{StringFieldValueNotBlankDependentOnBooleanFieldValue.message}";
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        StringFieldValueNotBlankDependentOnBooleanFieldValue[] value();
    }

}