package se.inera.hsa.org.drools;

public class ValidationWarning {
    private final String rulesetName;
    private ValidationCode warningCode;
    private String className;
    private String attributeName;
    private String warningMessage;

    public ValidationWarning(String rulesetName, ValidationCode warningCode, String className, String attributeName, String warningMessage) {
        this.rulesetName = rulesetName;
        this.warningCode = warningCode;
        this.className = className;
        this.attributeName = attributeName;
        this.warningMessage = warningMessage;
    }

    public ValidationCode getWarningCode() {
        return warningCode;
    }

    public String getClassName() {
        return className;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    @Override
    public String toString() {
        return "ValidationWarning{" +
                "rulesetName='" + rulesetName + '\'' +
                ", warningCode=" + warningCode +
                ", className='" + className + '\'' +
                ", attributeName='" + attributeName + '\'' +
                ", warningMessage='" + warningMessage + '\'' +
                '}';
    }
}
