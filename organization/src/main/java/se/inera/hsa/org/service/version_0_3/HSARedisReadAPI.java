package se.inera.hsa.org.service.version_0_3;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.InternalServerErrorException;

import io.lettuce.core.RedisFuture;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.dynamic.RedisCommandFactory;
import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.jsonb.JsonbUtil;
import se.inera.hsa.lettuce.RedisClientUtil;
import se.inera.hsa.lettuce.commands.RedisJSONCommands;
import se.inera.hsa.lettuce.commands.RedisSearchCommands;
import se.inera.hsa.log.service.TechnicalLogger;
import se.inera.hsa.org.model.unions.MetaData;
import se.inera.hsa.org.model.unions.UnitPersistence;
import se.inera.hsa.org.model.unions.UnitResponse;
import se.inera.hsa.org.model.version_0_3.CountryPersistence;
import se.inera.hsa.org.model.version_0_3.CountryResponse;
import se.inera.hsa.org.model.version_0_3.CountyPersistence;
import se.inera.hsa.org.model.version_0_3.CountyResponse;
import se.inera.hsa.org.model.version_0_3.EmployeePersistence;
import se.inera.hsa.org.model.version_0_3.EmployeeResponse;
import se.inera.hsa.org.model.version_0_3.FunctionPersistence;
import se.inera.hsa.org.model.version_0_3.FunctionResponse;
import se.inera.hsa.org.model.version_0_3.OrganizationPersistence;
import se.inera.hsa.org.model.version_0_3.OrganizationResponse;
import se.inera.hsa.org.model.version_0_3.QueryResult;

public class HSARedisReadAPI {

    public static String INDEX_NAME = "index:country:se";

    public static String GRAPH_ROOT_ID = "3ebda4ad-79fc-4c43-ac0c-089d3ba93501";

    public static String INDEX_SIMPLE_SEARCH_NAME = "index:simpleSearch:country:se";

    public static String GRAPH_NAME = "graph:country:se";

    public static final String COUNTRY_LABEL = "Country";
    public static final String COUNTY_LABEL = "County";
    public static final String FUNCTION_LABEL = "Function";
    public static final String EMPLOYEE_LABEL = "Employee";
    public static final String ORGANIZATION_LABEL = "Organization";
    public static final String UNIT_LABEL = "Unit";
    public static final String COUNTIES_RELATION = "HAS_COUNTIES";
    public static final String ORGANIZATIONS_RELATION = "HAS_ORGANIZATIONS";
    public static final String UNITS_RELATION = "HAS_UNITS";
    public static final String FUNCTIONS_RELATION = "HAS_FUNCTIONS";
    public static final String EMPLOYEES_RELATION = "HAS_EMPLOYEES";

    public static final String ORGANIZATION_JSON_ID_PREFIX = "organization:id:";
    public static final String SIMPLE_SEARCH_JSON_ID_PREFIX = "simpleSearch:index:id:";
    private static final String REJSON_ROOT = ".";
    private static final String NOESCAPE = "NOESCAPE";
    public static final String INDEX_SCORE = "1.0";
    private static final String INDEX_NO_CONTENT = "NOCONTENT";
    public static final String INDEX_FIELDS_LABEL = "FIELDS";
    public static final String ORGANIZATION_INDEX_ID_PREFIX = "organization:index:id:";
    private static final String INDEX_DELETE_DOCUMENT = "DD";
    public static final String INDEX_PARTIAL = "PARTIAL";
    public static final String INDEX_REPLACE_DOCUMENT = "REPLACE";

    @Inject
    TechnicalLogger LOG;

    private RedisJSONCommands rejson;

    private OrganizationGraph graph;

    private RedisSearchCommands redisearch;

    private StatefulRedisConnection<String, String> connection;

    @PostConstruct
    public void start() {
        System.out.println("HSARedisReadAPI PostConstruct");
        try {
            connection = RedisClientUtil.getClient().connect();
            RedisCommandFactory factory = new RedisCommandFactory(connection);
            rejson = factory.getCommands(RedisJSONCommands.class);
            graph = new OrganizationGraph(factory, GRAPH_ROOT_ID, GRAPH_NAME);
            redisearch = factory.getCommands(RedisSearchCommands.class);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
            LOG.error("Can't start OrganizationProvider!", ex);
        }
    }

    @PreDestroy
    public void stop() {
        System.out.println("HSARedisReadAPI PreDestroy");
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * Use HSARedisWriteAPI instead
     *
     */
    @Deprecated
    void delete(String id) {
        // no subordinates
        int numAllSubordinates = graph.getNumAllSubordinates(id);
        if (numAllSubordinates > 0) {
            throw new BadRequestException("resource has " + numAllSubordinates + " subordinates, removal not allowed", ErrorCodes.NOT_ALLOWED_ON_NON_LEAF);
        }

        // transactional
        // remove graph node
        graph.delete(id);

        try {
            redisearch.ftDel(INDEX_NAME, ORGANIZATION_INDEX_ID_PREFIX + id, INDEX_DELETE_DOCUMENT);
        } catch (Exception e) {
            throw new InternalServerErrorException("failed to remove index post for key " + id);
        }

        // remove json blob and index post
        try {
            RedisAsyncCommands<String, String> deleteCommands = RedisClientUtil.getClient().connect().async();
            RedisFuture<Long> noOfDeletedKeys = deleteCommands.del(ORGANIZATION_JSON_ID_PREFIX + id);
            int res = noOfDeletedKeys.get().intValue();
            if (res != 1) {
                throw new InternalServerErrorException("failed to remove json or index object for key " + id);
            }
        } catch (Exception e) {
            throw new InternalServerErrorException("failed to remove json object for key " + id);
        }

    }

    private String replaceDocumentIndex(String index, String prefix, Map<String, String> fields) {
        return redisearch.ftAdd(index, prefix + fields.get("uuid"), INDEX_SCORE, INDEX_REPLACE_DOCUMENT, INDEX_FIELDS_LABEL, fields);
    }

    private String updateDocumentIndex(String index, String prefix, Map<String, String> fields) {
        return redisearch.ftAdd(index, prefix + fields.get("uuid"), INDEX_SCORE, INDEX_REPLACE_DOCUMENT, INDEX_PARTIAL, INDEX_FIELDS_LABEL, fields);
    }

    List<String> getAllSubordinates(String id) {
        List<String> allSubordinates = new ArrayList<>();
        List<String> allSubordinatesIds = graph.getAllSubordinatesIds(graph.getImmediateSubordinatesIds(id), new ArrayList<>());

        allSubordinatesIds.forEach(subordinateId -> allSubordinates.add(
                rejson.jsonGet(ORGANIZATION_JSON_ID_PREFIX + subordinateId, NOESCAPE, REJSON_ROOT)));

        return allSubordinates;
    }

    // update Json
    /**
     * Use HSARedisWriteAPI instead
     *
     */
    @Deprecated
    void merge(String jsonBlob, Map<String, String> indexFields, Map<String, String> simpleIndexFields) {
        // do in block
        rejson.jsonSet(ORGANIZATION_JSON_ID_PREFIX + indexFields.get("uuid"), REJSON_ROOT, jsonBlob);
        replaceDocumentIndex(INDEX_NAME, ORGANIZATION_INDEX_ID_PREFIX, indexFields);
        replaceDocumentIndex(INDEX_SIMPLE_SEARCH_NAME, SIMPLE_SEARCH_JSON_ID_PREFIX, simpleIndexFields);
        // end block
    }

    /**
     * Use HSARedisWriteAPI instead
     *
     */
    @Deprecated
    void move(String jsonBlob, Map<String, String> indexFields, Map<String, String> simpleIndexFields, String newParentId, String relation, String oldParentId) {
        // do in block
        rejson.jsonSet(ORGANIZATION_JSON_ID_PREFIX + indexFields.get("uuid"), REJSON_ROOT, jsonBlob);
        graph.updateNode(indexFields.get("uuid"), indexFields.get("name"), relation, newParentId, oldParentId);
        updateDocumentIndex(INDEX_NAME, ORGANIZATION_INDEX_ID_PREFIX, indexFields);
        updateDocumentIndex(INDEX_SIMPLE_SEARCH_NAME, SIMPLE_SEARCH_JSON_ID_PREFIX, simpleIndexFields);
        // end block
    }

    String findById(String id) {
        System.out.println("findById 1" + id);

        String entityString = null;
        try {
            entityString = rejson.jsonGet(ORGANIZATION_JSON_ID_PREFIX + id, NOESCAPE, REJSON_ROOT);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace(System.out);
        }
        System.out.println("findById 2 id = " + id + " entity string = " + entityString);

        JsonObject entityObject;

        try {
            entityObject = Json.createReader(new StringReader(entityString)).readObject();
        } catch (Exception e) {
            return null;
        }

        return entityObject.toString();
    }


    private QueryResult search(String indexPrefix, String indexName, String query) {
        String numberOfHits;
        List<Object> matchingIds;
        QueryResult queryResult = new QueryResult();
        Integer limit = 200;
        try {
            matchingIds = redisearch.ftSearch(indexName, query, "0", limit.toString());
            numberOfHits = matchingIds.remove(0).toString();
        } catch (Exception e) {
            return queryResult;
        }
        List<String> matchingIdsList = matchingIds.stream().filter(Objects::nonNull)
                .map(Objects::toString).map(index -> index.replace(indexPrefix, "")).collect(Collectors.toList());
        queryResult.totalNumberOfHits = Integer.parseInt(numberOfHits);
        queryResult.result = matchingIdsList;
        queryResult.limit = limit;
        return queryResult;
    }

    QueryResult searchIndex(String query) {
        return search(ORGANIZATION_INDEX_ID_PREFIX, INDEX_NAME, query);
    }

    QueryResult simpleSearchIndex(String query) {
        return search(SIMPLE_SEARCH_JSON_ID_PREFIX, INDEX_SIMPLE_SEARCH_NAME, query);
        }

    CountryResponse toCountryResponse(CountryPersistence countryPersistence) {
        CountryResponse countryResponse = JsonbUtil.jsonb().fromJson(
                JsonbUtil.jsonb().toJson(countryPersistence.country), CountryResponse.class);
        countryResponse.uuid = countryPersistence.uuid;
        MetaData metaData = new MetaData(countryPersistence.createTimestamp, countryPersistence.uuid,
                "countries/Sverige/organizations/Värmlands län/employees/admin", graph.getNumImmediateSubordinates(countryPersistence.uuid),
                graph.getNumAllSubordinates(countryPersistence.uuid),
                graph.getPathFromId(countryPersistence.uuid));
        if (countryPersistence.modifyTimestamp != null && countryPersistence.modifierUUID != null) {
            metaData.modifierPath = "countries/Sverige/organizations/Värmlands län/employees/admin";
            metaData.modifierUUID = countryPersistence.modifierUUID;
            metaData.modifyTimestamp = countryPersistence.modifyTimestamp;
        }
        countryResponse.metaData = metaData;
        return countryResponse;
    }

    CountyResponse toCountyResponse(CountyPersistence countyPersistence) {
        CountyResponse countyResponse = JsonbUtil.jsonb().fromJson(JsonbUtil.jsonb().toJson(countyPersistence.county),
                CountyResponse.class);
        countyResponse.uuid = countyPersistence.uuid;
        MetaData metaData = new MetaData(countyPersistence.createTimestamp, countyPersistence.uuid,
                "countries/Sverige/organizations/Värmlands län/employees/admin", graph.getNumImmediateSubordinates(countyPersistence.uuid),
                graph.getNumAllSubordinates(countyPersistence.uuid),
                graph.getPathFromId(countyPersistence.uuid));
        if (countyPersistence.modifyTimestamp != null && countyPersistence.modifierUUID != null) {
            metaData.modifierPath = "countries/Sverige/organizations/Värmlands län/employees/admin";
            metaData.modifierUUID = countyPersistence.modifierUUID;
            metaData.modifyTimestamp = countyPersistence.modifyTimestamp;
        }
        countyResponse.metaData = metaData;
        return countyResponse;
    }

    OrganizationResponse toOrganizationResponse(OrganizationPersistence organizationPersistence) {
        OrganizationResponse organizationResponse = JsonbUtil.jsonb()
                .fromJson(JsonbUtil.jsonb().toJson(organizationPersistence.organization), OrganizationResponse.class);
        organizationResponse.uuid = organizationPersistence.uuid;
        MetaData metaData = new MetaData(organizationPersistence.createTimestamp, organizationPersistence.creatorUUID,
                "countries/Sverige/organizations/Värmlands län/employees/admin", graph.getNumImmediateSubordinates(organizationPersistence.uuid),
                graph.getNumAllSubordinates(organizationPersistence.uuid),
                graph.getPathFromId(organizationPersistence.uuid));
        if (organizationPersistence.modifyTimestamp != null && organizationPersistence.modifierUUID != null) {
            metaData.modifierPath = "countries/Sverige/organizations/Värmlands län/employees/admin";
            metaData.modifierUUID = organizationPersistence.modifierUUID;
            metaData.modifyTimestamp = organizationPersistence.modifyTimestamp;
        }
        organizationResponse.metaData = metaData;
        return organizationResponse;
    }

    UnitResponse toUnitResponse(UnitPersistence unitPersistence) {
        UnitResponse unitResponse = null;

        try {
            unitResponse = JsonbUtil.jsonb()
                    .fromJson(JsonbUtil.jsonb().toJson(unitPersistence.unit), UnitResponse.class);
            unitResponse.uuid = unitPersistence.uuid;
            MetaData metaData = new MetaData(unitPersistence.createTimestamp, unitPersistence.creatorUUID,
                    "countries/Sverige/organizations/Värmlands län/employees/admin", graph.getNumImmediateSubordinates(unitPersistence.uuid),
                    graph.getNumAllSubordinates(unitPersistence.uuid),
                    graph.getPathFromId(unitPersistence.uuid));
            if (unitPersistence.modifyTimestamp != null && unitPersistence.modifierUUID != null) {
                metaData.modifierPath = "countries/Sverige/organizations/Värmlands län/employees/admin";
                metaData.modifierUUID = unitPersistence.modifierUUID;
                metaData.modifyTimestamp = unitPersistence.modifyTimestamp;
            }
            unitResponse.metaData = metaData;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unitResponse;
    }

    FunctionResponse toFunctionResponse(FunctionPersistence functionPersistence) {
        FunctionResponse functionResponse = JsonbUtil.jsonb().fromJson(JsonbUtil.jsonb().toJson(functionPersistence.function),
                FunctionResponse.class);
        functionResponse.uuid = functionPersistence.uuid;
        MetaData metaData = new MetaData(functionPersistence.createTimestamp, functionPersistence.uuid,
                "countries/Sverige/organizations/Värmlands län/employees/admin", graph.getNumImmediateSubordinates(functionPersistence.uuid),
                graph.getNumAllSubordinates(functionPersistence.uuid),
                graph.getPathFromId(functionPersistence.uuid));
        if (functionPersistence.modifyTimestamp != null && functionPersistence.modifierUUID != null) {
            metaData.modifierPath = "countries/Sverige/organizations/Värmlands län/employees/admin";
            metaData.modifierUUID = functionPersistence.modifierUUID;
            metaData.modifyTimestamp = functionPersistence.modifyTimestamp;
        }
        functionResponse.metaData = metaData;
        return functionResponse;
    }

    public EmployeeResponse toEmployeeResponse(EmployeePersistence employeePersistence) {
        EmployeeResponse employeeResponse = JsonbUtil.jsonb().fromJson(JsonbUtil.jsonb().toJson(employeePersistence.employee),
                EmployeeResponse.class);
        employeeResponse.uuid = employeePersistence.uuid;
        MetaData metaData = new MetaData(
                employeePersistence.createTimestamp,
                employeePersistence.uuid,
                "countries/Sverige/organizations/Värmlands län/employees/admin",
                graph.getNumImmediateSubordinates(employeePersistence.uuid),
                graph.getNumAllSubordinates(employeePersistence.uuid),
                graph.getPathFromId(employeePersistence.uuid));
        if (employeePersistence.modifyTimestamp != null && employeePersistence.modifierUUID != null) {
            metaData.modifierPath = "countries/Sverige/organizations/Värmlands län/employees/admin";
            metaData.modifierUUID = employeePersistence.modifierUUID;
            metaData.modifyTimestamp = employeePersistence.modifyTimestamp;
        }
        employeeResponse.metaData = metaData;
        return employeeResponse;
    }

    boolean equalsIgnoreCase(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equalsIgnoreCase(str2);
    }

    OrganizationGraph getGraph() {
        return graph;
    }
}
