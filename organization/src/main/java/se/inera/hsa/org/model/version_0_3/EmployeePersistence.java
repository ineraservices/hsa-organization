package se.inera.hsa.org.model.version_0_3;

import javax.validation.Valid;

public class EmployeePersistence extends Persistence {
    @Valid
    public Employee employee;
}
