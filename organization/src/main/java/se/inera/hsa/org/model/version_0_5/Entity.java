package se.inera.hsa.org.model.version_0_5;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import se.inera.hsa.org.model.version_0_3.Indexed;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Core class for all entities. Name and type and implements serializable is all they have in common at this level.
 */
public abstract class Entity implements Serializable {

    private static final long serialVersionUID = 5953205515659374058L;

    @NotBlank
    @Schema(
            required = true
    )
    public String name;

    @NotBlank
    @Schema(
            required = true
    )
    public String type;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Field field : this.getClass().getFields()) {
            try {
                sb.append(field.getName()).append(" - ").append(field.get(this));
            } catch (IllegalArgumentException | IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}