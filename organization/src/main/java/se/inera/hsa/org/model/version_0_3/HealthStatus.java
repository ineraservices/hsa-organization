package se.inera.hsa.org.model.version_0_3;

public class HealthStatus {

    private static final String VALUE_UP = "UP";

    public String status;

    public HealthStatus() {
        status = VALUE_UP;
    }
}
