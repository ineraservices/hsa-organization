package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import se.inera.hsa.org.validation.version_0_3.StringFieldValueNotBlankDependentOnBooleanFieldValue;

import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.ws.rs.DefaultValue;
import java.io.Serializable;
import java.util.List;

@StringFieldValueNotBlankDependentOnBooleanFieldValue(stringFieldName = "orgNo", dependBooleanFieldName = "hsaHealthCareProvider", dependBooleanFieldValueExpected = "true")
public class Organization implements Serializable {

    public static final long serialVersionUID = 5953205515659374058L;

    @NotBlank
    @Indexed
    @Schema(required = true)
    public String name;

    @NotBlank
    @Pattern(regexp = "Organization")
    @Indexed
    @Schema(
            required = true,
            pattern = "Organization"
    )
    public String type;

    @NotBlank
    public String hsaHpt;

    @NotBlank
    @Schema(
            required = true
    )
    @Indexed
    public String hsaIdentity;

    @NotBlank
    @Indexed
    public String mail; //(rfc822Mailbox);

    @NotBlank
    @Indexed
    @Schema(
            required = true
    )
    public String orgNo;

    @NotNull
    @Valid
    @Indexed
    public PostalAddress postalAddress;

    @NotEmpty
    @Indexed
    public List<String> telephoneNumber;
    
    /* should these be used?
    String alternativeName;
    String approvedHpt;
    String HsaIdPrefix;
    String HsaIdCounter;
    String contentResponsibleEmail;
    Place place;
    */

    //Single value attributes


    //From HSA-SCHEMA 5
    public String archivingTimePerson;
    public String c; // (countryName);
    @Indexed
    public String countyCode;
    @Indexed
    public String description;
    public String displayOption;
    public String endDate;
    public GeographicalCoordinates geographicalCoordinates;

    @Size(max = 12)
    public String hsaSweref99Latitude;
    @Size(max = 12)
    public String hsaSweref99Longitude;
    public String hsaGlnCode;
    public String indoorRouteDescription;
    public String hsaAdminComment;
    public String hsaAltText;
    public String hsaDestinationIndicator;
    @Indexed
    public String hsaDirectoryContact;
    public String hsaHealthCareArea;
    public String hsaHealthCareUnitManager;
    public String hsaIdCounter;
    public String hsaIdPrefix;
    public String hsaJpegLogotype;
    public String hsaResponsibleHealthCareProvider;
    public String hsaSwitchboardNumber;
    public String hsaVisitingRuleReferral;
    public String hsaVisitingRules;
    public String hsaVpwInformation1;
    public String hsaVpwInformation2;
    public String hsaVpwInformation3;
    public String hsaVpwInformation4;
    public String hsaVpwWebPage;

    public String jpegPhoto;
    public String localityName; // (l);
    public String labeledURI;
    @Indexed
    public String municipalityCode;
    @Indexed
    public String postalCode;
    public String route;
    @Indexed
    public String smsTelephoneNumber;
    public String startDate;
    public String street; //(streetAddress);

    //Multivalue attributes
    @Indexed
    public List<String> businessClassificationCode;
    public List<String> careType;
    public List<Hours> dropInHours;
    @Indexed
    public List<String> facsimileTelephoneNumber;
    public List<String> financingOrganization;
    @Indexed
    public List<String> hsaBusinessType;
    public List<String> hsaHealthCareUnitMember;
    @Indexed
    public List<String> hsaSyncId;
    @Indexed
    public List<String> hsaTelephoneNumber;
    @Indexed
    public List<String> hsaTextTelephoneNumber;
    public List<String> hsaVisitingRuleAge;
    public List<String> hsaVpwNeighbouringObject;
    public List<String> management;
    public List<String> mobile; //(mobileTelephoneNumber);
    public List<String> ouShort;
    public List<String> seeAlso;
    public List<Hours> surgeryHours;
    public List<Hours> telephoneHours;
    public List<String> unitPrescriptionCode;
    public List<Hours> visitingHours;

    @Indexed
    @Schema(
            defaultValue = "false"
    )
    @DefaultValue("false")
    public boolean hsaHealthCareProvider;
}
