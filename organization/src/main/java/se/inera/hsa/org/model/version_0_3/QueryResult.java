package se.inera.hsa.org.model.version_0_3;

import java.util.List;

public class QueryResult {
    public Integer limit;
    public Integer totalNumberOfHits;
    public List<String> result;

}
