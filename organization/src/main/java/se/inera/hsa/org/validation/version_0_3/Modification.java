package se.inera.hsa.org.validation.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public abstract class Modification  {

    @Schema(
            required = true,
            pattern = "MERGE|MOVE"
    )
    @NotNull
    @Pattern(regexp = "merge|move", flags = Pattern.Flag.CASE_INSENSITIVE, message = "merge or move only")
    public String modificationType;

    public String newPath;

}
