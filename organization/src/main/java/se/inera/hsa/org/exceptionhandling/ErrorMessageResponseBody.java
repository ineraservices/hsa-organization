package se.inera.hsa.org.exceptionhandling;

import javax.json.bind.JsonbBuilder;

public class ErrorMessageResponseBody {
    public String title;
    public String userFriendlyMessage;
    public String exceptionReference;

    public ErrorMessageResponseBody(String title, String userFriendlyMessage, String exceptionReference) {
        this.title = title;
        this.userFriendlyMessage = userFriendlyMessage;
        this.exceptionReference = exceptionReference;
    }

    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }
}
