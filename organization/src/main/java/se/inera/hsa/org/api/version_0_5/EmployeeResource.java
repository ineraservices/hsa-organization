package se.inera.hsa.org.api.version_0_5;


import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Encoding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.opentracing.Traced;

import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.model.version_0_5.Employee;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.model.version_0_5.ModificationList;
import se.inera.hsa.org.service.version_0_5.EntityProvider;
import se.inera.hsa.log.service.version_0_3.AuditLogger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import static se.inera.hsa.org.api.ApiVersion.VERSION_0_5;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

@Tag(name = "Employee")
@Stateless
@Path(ApiVersion.VERSION_0_5)
@Traced
public class EmployeeResource {
    @Context
    UriInfo uriInfo;

//    @Inject
//    EmployeeProvider employeeProvider;

    @Inject
    EntityProvider<Employee> entityProvider;

    @Inject
    AuditLogger auditLogger;

    @Operation(
            summary = "Get Employee",
            description = "Get Employee by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "employeeName", description = "Employee name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns EntityResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = EntityResponse.class,
                                    description = "EntityResponse object"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/employees/{employeeName}")
    public @Valid EntityResponse getEmployee1(@PathParam("countryName") String countryName,
                                                @PathParam("countyName") String countyName,
                                                @PathParam("organizationName") String organizationName,
                                                @PathParam("units") String units,
                                                @PathParam("employeeName") String employeeName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName);
        Arrays.asList(units.split("/")).forEach(builder::path);
        builder.path("employees").path(employeeName);
        String requestPath = builder.build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_EMPLOYEE, requestPath);
        return entityProvider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Get Employee",
            description = "Get Employee by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "employeeName", description = "Employee name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns EntityResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = EntityResponse.class,
                                    description = "EntityResponse object"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/employees/{employeeName}")
    public @Valid EntityResponse getEmployee4(@PathParam("countryName") String countryName,
                                                @PathParam("countyName") String countyName,
                                                @PathParam("organizationName") String organizationName,
                                                @PathParam("employeeName") String employeeName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName)
                .path("employees").path(employeeName);
        String requestPath = builder.build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_EMPLOYEE, requestPath);
        return entityProvider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Get Employee",
            description = "Get Employee by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "employeeName", description = "Employee name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns EntityResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = EntityResponse.class,
                                    description = "EntityResponse object"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/employees/{employeeName}")
    public @Valid EntityResponse getEmployee6(@PathParam("countryName") String countryName,
                                                @PathParam("organizationName") String organizationName,
                                                @PathParam("employeeName") String employeeName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName)
                .path("employees").path(employeeName);
        String requestPath = builder.build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_EMPLOYEE, requestPath);
        return entityProvider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Get Employee",
            description = "Get Employee by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "employeeName", description = "Employee name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns EntityResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = EntityResponse.class,
                                    description = "EntityResponse object"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/employees/{employeeName}")
    public @Valid EntityResponse getEmployee7(@PathParam("countryName") String countryName,
                                                @PathParam("organizationName") String organizationName,
                                                @PathParam("units") String units,
                                                @PathParam("employeeName") String employeeName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName);
        Arrays.asList(units.split("/")).forEach(builder::path);
        builder.path("employees").path(employeeName);
        String requestPath = builder.build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_EMPLOYEE, requestPath);
        return entityProvider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Add Employee",
            description = "Add Employee under existing Organization."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @RequestBody(
            required = true,
            name = "Employee",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            name = "Employee",
                            description = "Employee object",
                            implementation = Employee.class
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Employee",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    }
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/employees")
    public Response createEmployee1(@PathParam("countryName") String countryName,
                                    @PathParam("countyName") String countyName,
                                    @PathParam("organizationName") String organizationName,
                                    @Valid Employee employee) {
        String parentPath = toPath(countryName, countyName, organizationName);
        return persist(parentPath, employee);
    }

    @Operation(
            summary = "Add Employee",
            description = "Add Employee under existing Unit."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @RequestBody(
            required = true,
            name = "Employee",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            name = "Employee",
                            description = "Employee object",
                            implementation = Employee.class
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Employee",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    }
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/employees")
    public Response createEmployee2(@PathParam("countryName") String countryName,
                                    @PathParam("countyName") String countyName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("units") String units,
                                    @Valid Employee employee) {
        String parentPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")));
        return persist(parentPath, employee);
    }

    @Operation(
            summary = "Add Employee",
            description = "Add Employee under existing Organization."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @RequestBody(
            required = true,
            name = "Employee",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            name = "Employee",
                            description = "Employee object",
                            implementation = Employee.class
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Employee",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    }
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/employees")
    public Response createEmployee3(@PathParam("countryName") String countryName,
                                    @PathParam("organizationName") String organizationName,
                                    @Valid Employee employee) {
        String parentPath = toPath(countryName, organizationName);
        return persist(parentPath, employee);
    }

    @Operation(
            summary = "Add Employee",
            description = "Add Employee under existing Unit."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @RequestBody(
            required = true,
            name = "Employee",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            name = "Employee",
                            description = "Employee object",
                            implementation = Employee.class
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Employee",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    }
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/employees")
    public Response createEmployee4(@PathParam("countryName") String countryName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("units") String units,
                                    @Valid Employee employee) {
        String parentPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")));
        return persist(parentPath, employee);
    }

    private Response persist(String parentPath, Employee employee) {
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.ADD_EMPLOYEE, expectedChildPath(parentPath, employee.name));
        entityProvider.addEntity(parentPath, employee);
        return Response.created(uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath("employees").path(employee.name).build())).build();
    }

//    @Operation(
//            summary = "Update Employee",
//            description = "Update Employee, either by a MERGE request i.e updating attributes except name,<br/>" +
//                    "or a MOVE request i.e change parent and/or change name of Employee"
//    )
//    @Parameter(name = "countryName", description = "Country name")
//    @Parameter(name = "organizationName", description = "Organization name")
//    @Parameter(name = "employeeName", description = "Employee name")
//    @RequestBody(
//            required = true,
//            name = "EmployeeModification",
//            content = @Content(
//                    mediaType = "application/json",
//                    encoding = @Encoding(
//                            name = "UTF-8"
//                    ),
//                    schema = @Schema(
//                            implementation = EmployeeModification.class,
//                            description = "EmployeeModification object"
//                    )
//            )
//    )
//    @APIResponses({
//            @APIResponse(
//                    responseCode = "204",
//                    description = "Request successful",
//                    headers = {
//                            @Header(
//                                    name = "Location",
//                                    description = "Path to updated Employee",
//                                    schema = @Schema(
//                                            type = SchemaType.STRING
//                                    )
//                            )
//                    }
//            ),
//            @APIResponse(
//                    responseCode = "404",
//                    description = "object not found for specified path"
//            )
//    }
//    )
//    @PUT
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("countries/{countryName}/organizations/{organizationName}/employees/{employeeName}")
//    public Response updateEmployee1(@PathParam("countryName") String countryName,
//                                    @PathParam("organizationName") String organizationName,
//                                    @PathParam("employeeName") String employeeName,
//                                    @Valid EmployeeModification employeeModification) {
//        String requestPath = toPathF(countryName, organizationName, employeeName);
//        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_EMPLOYEE, requestPath);
//        String employeePath = modifyEmployee(requestPath, employeeModification);
//        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(employeePath).build()).build();
//    }
//
//    @Operation(
//            summary = "Update Employee",
//            description = "Update Employee, either by a MERGE request i.e updating attributes except name,<br/>" +
//                    "or a MOVE request i.e change parent and/or change name of Employee"
//    )
//    @Parameter(name = "countryName", description = "Country name")
//    @Parameter(name = "countyName", description = "County name")
//    @Parameter(name = "organizationName", description = "Organization name")
//    @Parameter(name = "employeeName", description = "Employee name")
//    @RequestBody(
//            required = true,
//            name = "EmployeeModification",
//            content = @Content(
//                    mediaType = "application/json",
//                    encoding = @Encoding(
//                            name = "UTF-8"
//                    ),
//                    schema = @Schema(
//                            implementation = EmployeeModification.class,
//                            description = "EmployeeModification object")
//            )
//    )
//    @APIResponses({
//            @APIResponse(
//                    responseCode = "204",
//                    description = "Request successful",
//                    headers = {
//                            @Header(
//                                    name = "Location",
//                                    description = "Path to updated Employee",
//                                    schema = @Schema(
//                                            type = SchemaType.STRING
//                                    )
//                            )
//                    }
//            ),
//            @APIResponse(
//                    responseCode = "404",
//                    description = "object not found for specified path"
//            )
//    }
//    )
//    @PUT
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/employees/{employeeName}")
//    public Response updateEmployee2(@PathParam("countryName") String countryName,
//                                    @PathParam("countyName") String countyName,
//                                    @PathParam("organizationName") String organizationName,
//                                    @PathParam("employeeName") String employeeName,
//                                    @Valid EmployeeModification employeeModification) {
//        String requestPath = toPath(countryName, countyName, organizationName, employeeName);
//        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_EMPLOYEE, requestPath);
//        String employeePath = modifyEmployee(requestPath, employeeModification);
//        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(employeePath).build()).build();
//    }
//
//    @Operation(
//            summary = "Update Employee",
//            description = "Update Employee, either by a MERGE request i.e updating attributes except name,<br/>" +
//                    "or a MOVE request i.e change parent and/or change name of Employee"
//    )
//    @Parameter(name = "countryName", description = "Country name")
//    @Parameter(name = "organizationName", description = "Organization name")
//    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
//    @Parameter(name = "employeeName", description = "Employee name")
//    @RequestBody(
//            required = true,
//            name = "EmployeeModification",
//            content = @Content(
//                    mediaType = "application/json",
//                    encoding = @Encoding(
//                            name = "UTF-8"
//                    ),
//                    schema = @Schema(
//                            implementation = EmployeeModification.class,
//                            description = "EmployeeModification object")
//            )
//    )
//    @APIResponses({
//            @APIResponse(
//                    responseCode = "204",
//                    description = "Request successful",
//                    headers = {
//                            @Header(
//                                    name = "Location",
//                                    description = "Path to updated Employee",
//                                    schema = @Schema(
//                                            type = SchemaType.STRING
//                                    )
//                            )
//                    }
//            ),
//            @APIResponse(
//                    responseCode = "404",
//                    description = "object not found for specified path"
//            )
//    }
//    )
//    @PUT
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/employees/{employeeName}")
//    public Response updateEmployee3(@PathParam("countryName") String countryName,
//                                    @PathParam("organizationName") String organizationName,
//                                    @PathParam("units") String units,
//                                    @PathParam("employeeName") String employeeName,
//                                    @Valid EmployeeModification employeeModification) {
//        String requestPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")), employeeName);
//        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_EMPLOYEE, requestPath);
//        String employeePath = modifyEmployee(requestPath, employeeModification);
//        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(employeePath).build()).build();
//    }
//
//    @Operation(
//            summary = "Update Employee",
//            description = "Update Employee, either by a MERGE request i.e updating attributes except name,<br/>" +
//                    "or a MOVE request i.e change parent and/or change name of Employee"
//    )
//    @Parameter(name = "countryName", description = "Country name")
//    @Parameter(name = "countyName", description = "County name")
//    @Parameter(name = "organizationName", description = "Organization name")
//    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
//    @Parameter(name = "employeeName", description = "Employee name")
//    @RequestBody(
//            required = true,
//            name = "EmployeeModification",
//            content = @Content(
//                    mediaType = "application/json",
//                    encoding = @Encoding(
//                            name = "UTF-8"
//                    ),
//                    schema = @Schema(
//                            implementation = EmployeeModification.class,
//                            description = "EmployeeModification object"
//                    )
//            )
//    )
//    @APIResponses({
//            @APIResponse(
//                    responseCode = "204",
//                    description = "Request successful",
//                    headers = {
//                            @Header(
//                                    name = "Location",
//                                    description = "Path to updated Employee",
//                                    schema = @Schema(
//                                            type = SchemaType.STRING
//                                    )
//                            )
//                    }
//            ),
//            @APIResponse(
//                    responseCode = "404",
//                    description = "object not found for specified path"
//            )
//    }
//    )
//    @PUT
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/employees/{employeeName}")
//    public Response updateEmployee4(@PathParam("countryName") String countryName,
//                                    @PathParam("countyName") String countyName,
//                                    @PathParam("organizationName") String organizationName,
//                                    @PathParam("units") String units,
//                                    @PathParam("employeeName") String employeeName,
//                                    @Valid EmployeeModification employeeModification) {
//        String requestPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")), employeeName);
//        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_EMPLOYEE, requestPath);
//        String employeePath = modifyEmployee(requestPath, employeeModification);
//        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(employeePath).build()).build();
//    }
    @Operation(
            summary = "Update Employee",
            description = "Update Employee, by a list of attribute modifications<br/>"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "employeeName", description = "Employee name")
    @RequestBody(
            required = true,
            name = "ModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = ModificationList.class,
                            description = "ModificationList object"
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to updated Employee",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Something went wrong during patch, see actual error message."
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/employees/{employeeName}")
    public Response updateEmployeePatch1(@PathParam("countryName") String countryName,
                                         @PathParam("organizationName") String organizationName,
                                         @PathParam("employeeName") String employeeName,
                                         ModificationList modifications) {
        String requestPath = toPathF(countryName, organizationName, employeeName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_EMPLOYEE, requestPath);
        String employeePath = entityProvider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION_0_5).path(employeePath).build()).build();
    }

    @Operation(
            summary = "Update Employee",
            description = "Update Employee, by a list of attribute modifications<br/>"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "employeeName", description = "Employee name")
    @RequestBody(
            required = true,
            name = "ModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = ModificationList.class,
                            description = "ModificationList object")
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to updated Employee",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Something went wrong during patch, see actual error message."
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/employees/{employeeName}")
    public Response updateEmployeePatch2(@PathParam("countryName") String countryName,
                                         @PathParam("countyName") String countyName,
                                         @PathParam("organizationName") String organizationName,
                                         @PathParam("employeeName") String employeeName,
                                         ModificationList modifications) {
        String requestPath = toPath(countryName, countyName, organizationName, employeeName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_EMPLOYEE, requestPath);
        String employeePath = entityProvider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION_0_5).path(employeePath).build()).build();
    }

    @Operation(
            summary = "Update Employee",
            description = "Update Employee, by a list of attribute modifications<br/>"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "employeeName", description = "Employee name")
    @RequestBody(
            required = true,
            name = "ModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = ModificationList.class,
                            description = "ModificationList object")
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to updated Employee",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Something went wrong during patch, see actual error message."
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/employees/{employeeName}")
    public Response updateEmployeePatch3(@PathParam("countryName") String countryName,
                                         @PathParam("organizationName") String organizationName,
                                         @PathParam("units") String units,
                                         @PathParam("employeeName") String employeeName,
                                         ModificationList modifications) {
        String requestPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")), employeeName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_EMPLOYEE, requestPath);
        String employeePath = entityProvider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION_0_5).path(employeePath).build()).build();
    }

    @Operation(
            summary = "Update Employee",
            description = "Update Employee, by a list of attribute modifications<br/>"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "employeeName", description = "Employee name")
    @RequestBody(
            required = true,
            name = "ModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = ModificationList.class,
                            description = "ModificationList object"
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to updated Employee",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Something went wrong during patch, see actual error message."
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/employees/{employeeName}")
    public Response updateEmployeePatch4(@PathParam("countryName") String countryName,
                                         @PathParam("countyName") String countyName,
                                         @PathParam("organizationName") String organizationName,
                                         @PathParam("units") String units,
                                         @PathParam("employeeName") String employeeName,
                                         ModificationList modifications) {
        String requestPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")), employeeName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_EMPLOYEE, requestPath);
        String employeePath = entityProvider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION_0_5).path(employeePath).build()).build();
    }

    @Operation(
            summary = "Delete Employee",
            description = "Delete Employee by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "employeeName", description = "Employee name")
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @DELETE
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/employees/{employeeName}")
    public Response deleteEmployee1(@PathParam("countryName") String countryName,
                                    @PathParam("countyName") String countyName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("employeeName") String employeeName) {
        String requestPath = toPath(countryName, countyName, organizationName, employeeName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_EMPLOYEE, requestPath);
        entityProvider.removeEntityWithPath(requestPath);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Delete Employee",
            description = "Delete Employee by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "employeeName", description = "Employee name")
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @DELETE
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/employees/{employeeName}")
    public Response deleteEmployee2(@PathParam("countryName") String countryName,
                                    @PathParam("countyName") String countyName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("units") String units,
                                    @PathParam("employeeName") String employeeName) {
        String requestPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")), employeeName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_EMPLOYEE, requestPath);
        entityProvider.removeEntityWithPath(requestPath);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Delete Employee",
            description = "Delete Employee by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "employeeName", description = "Employee name")
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @DELETE
    @Path("countries/{countryName}/organizations/{organizationName}/employees/{employeeName}")
    public Response deleteEmployee3(@PathParam("countryName") String countryName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("employeeName") String employeeName) {
        String requestPath = toPathF(countryName, organizationName, employeeName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_EMPLOYEE, requestPath);
        entityProvider.removeEntityWithPath(requestPath);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Delete Employee",
            description = "Delete Employee by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "employeeName", description = "Employee name")
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @DELETE
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/employees/{employeeName}")
    public Response deleteEmployee4(@PathParam("countryName") String countryName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("units") String units,
                                    @PathParam("employeeName") String employeeName) {
        String requestPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")), employeeName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_EMPLOYEE, requestPath);
        entityProvider.removeEntityWithPath(requestPath);
        return Response.noContent().build();
    }

//    private String modifyEmployee(String path, EmployeeModification employeeModification) {
//        if (employeeModification.modificationType.equalsIgnoreCase(ModificationTypes.MOVE.name())) {
//            return employeeProvider.move(path, employeeModification.newPath);
//        } else {
//            return employeeProvider.merge(path, employeeModification.employee);
//        }
//    }

    private String toPath(String countryName, String countyName, String organizationName, List<String> units) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName);
        units.forEach(builder::path);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String organizationName, List<String> units) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName);
        units.forEach(builder::path);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String countyName, String organizationName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName);
        return builder.build().getPath();
    }

    private String toPathF(String countryName, String organizationName, String employeeName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName)
                .path("organizations").path(organizationName).path("employees")
                .path(employeeName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String organizationName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String countyName, String organizationName, String employeeName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName).path("employees").path(employeeName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String countyName, String organizationName, List<String> units, String employeeName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName);
        units.forEach(builder::path);
        builder.path("employees").path(employeeName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String organizationName, List<String> units, String employeeName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName);
        units.forEach(builder::path);
        builder.path("employees").path(employeeName);
        return builder.build().getPath();
    }

    private String expectedChildPath(String parentPath, String childName) {
        return UriBuilder.fromPath(parentPath).path("employees/").path(childName).build().getPath();
    }
}

