package se.inera.hsa.org.drools;

/**
 * Enums that are used in Drools rule files.
 */
public enum ValidationCode {
    // error codes
    MANDATORY_ATTRIBUTE_NOT_SET,
    CODE_SYSTEM_VALIDATION,
    ILLEGAL_SYNTAX,

    // warning codes
    ATTRIBUTE_HAS_BEEN_REMOVED,

    // Info codes
    INFORMATION;
}
