package se.inera.hsa.org.api.version_0_5;


import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Encoding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.opentracing.Traced;

import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.model.version_0_5.Function;
import se.inera.hsa.org.model.version_0_5.ModificationList;
import se.inera.hsa.org.service.version_0_5.EntityProvider;
import se.inera.hsa.log.service.version_0_3.AuditLogger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

@Tag(name = "Function")
@Stateless
@Path(ApiVersion.VERSION_0_5)
@Traced
public class FunctionResource {

    private final String V3_0 = "v0.3/";

    @Context
    UriInfo uriInfo;

    @Inject
    EntityProvider<Function> entityProvider;

    @Inject
    AuditLogger auditLogger;

    @Operation(
            summary = "Get Function",
            description = "Get Function by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "functionName", description = "Function name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns EntityResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = EntityResponse.class,
                                    description = "EntityResponse object"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/functions/{functionName}")
    public @Valid EntityResponse getFunction1(@PathParam("countryName") String countryName,
                                                @PathParam("countyName") String countyName,
                                                @PathParam("organizationName") String organizationName,
                                                @PathParam("units") String units,
                                                @PathParam("functionName") String functionName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName);
        Arrays.asList(units.split("/")).forEach(builder::path);
        builder.path("functions").path(functionName);
        String requestPath = builder.build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_FUNCTION, requestPath);
        return entityProvider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Get Function",
            description = "Get Function by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "functionName", description = "Function name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns EntityResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = EntityResponse.class,
                                    description = "EntityResponse object"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/functions/{functionName}")
    public @Valid EntityResponse getFunction4(@PathParam("countryName") String countryName,
                                                @PathParam("countyName") String countyName,
                                                @PathParam("organizationName") String organizationName,
                                                @PathParam("functionName") String functionName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName)
                .path("functions").path(functionName);
        String requestPath = builder.build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_FUNCTION, requestPath);
        return entityProvider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Get Function",
            description = "Get Function by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "functionName", description = "Function name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns EntityResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = EntityResponse.class,
                                    description = "EntityResponse object"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/functions/{functionName}")
    public @Valid EntityResponse getFunction6(@PathParam("countryName") String countryName,
                                                @PathParam("organizationName") String organizationName,
                                                @PathParam("functionName") String functionName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName)
                .path("functions").path(functionName);
        String requestPath = builder.build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_FUNCTION, requestPath);
        return entityProvider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Get Function",
            description = "Get Function by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "functionName", description = "Function name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns EntityResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = EntityResponse.class,
                                    description = "EntityResponse object"
                            ),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/functions/{functionName}")
    public @Valid EntityResponse getFunction7(@PathParam("countryName") String countryName,
                                                @PathParam("organizationName") String organizationName,
                                                @PathParam("units") String units,
                                                @PathParam("functionName") String functionName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName);
        Arrays.asList(units.split("/")).forEach(builder::path);
        builder.path("functions").path(functionName);
        String requestPath = builder.build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_FUNCTION, requestPath);
        return entityProvider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Add Function",
            description = "Add Function under existing Organization."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @RequestBody(
            required = true,
            name = "Function",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            name = "Function",
                            description = "Function object",
                            implementation = Function.class
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Function",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    }
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/functions")
    public Response createFunction1(@PathParam("countryName") String countryName,
                                    @PathParam("countyName") String countyName,
                                    @PathParam("organizationName") String organizationName,
                                    @Valid Function function) {
        String parentPath = toPath(countryName, countyName, organizationName);
        return persist(parentPath, function);
    }

    @Operation(
            summary = "Add Function",
            description = "Add Function under existing Unit."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @RequestBody(
            required = true,
            name = "Function",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            name = "Function",
                            description = "Function object",
                            implementation = Function.class
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Function",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    }
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/functions")
    public Response createFunction2(@PathParam("countryName") String countryName,
                                    @PathParam("countyName") String countyName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("units") String units,
                                    @Valid Function function) {
        String parentPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")));
        return persist(parentPath, function);
    }

    @Operation(
            summary = "Add Function",
            description = "Add Function under existing Organization."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @RequestBody(
            required = true,
            name = "Function",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            name = "Function",
                            description = "Function object",
                            implementation = Function.class
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Function",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    }
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/functions")
    public Response createFunction3(@PathParam("countryName") String countryName,
                                    @PathParam("organizationName") String organizationName,
                                    @Valid Function function) {
        String parentPath = toPath(countryName, organizationName);
        return persist(parentPath, function);
    }

    @Operation(
            summary = "Add Function",
            description = "Add Function under existing Unit."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @RequestBody(
            required = true,
            name = "Function",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            name = "Function",
                            description = "Function object",
                            implementation = Function.class
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Function",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    }
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/functions")
    public Response createFunction4(@PathParam("countryName") String countryName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("units") String units,
                                    @Valid Function function) {
        String parentPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")));
        return persist(parentPath, function);
    }

    private Response persist(String parentPath, Function function) {
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.ADD_FUNCTION, expectedChildPath(parentPath, function.name));
        entityProvider.addEntity(parentPath, function);
        return Response.created(uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath("functions").path(function.name).build())).build();
    }


    @Operation(
            summary = "Update Function",
            description = "Update a Function, by a list of attribute modifications<br/>"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "functionName", description = "Function name")
    @RequestBody(
            required = true,
            name = "ModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = ModificationList.class,
                            description = "ModificationList object"
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to updated Function",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Something went wrong during patch, see actual error message."
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/functions/{functionName}")
    public Response updateFunctionPatch1(@PathParam("countryName") String countryName,
                                         @PathParam("organizationName") String organizationName,
                                         @PathParam("functionName") String functionName,
                                         ModificationList modifications) {
        String requestPath = toPath(countryName, organizationName, functionName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_FUNCTION, requestPath);
        String functionPath = entityProvider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath(functionPath).build())).build();
    }

    @Operation(
            summary = "Update Function",
            description = "Update a Function, by a list of attribute modifications<br/>"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "functionName", description = "Function name")
    @RequestBody(
            required = true,
            name = "ModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = ModificationList.class,
                            description = "ModificationList object")
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to updated Function",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Something went wrong during patch, see actual error message."
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/functions/{functionName}")
    public Response updateFunctionPatch2(@PathParam("countryName") String countryName,
                                         @PathParam("countyName") String countyName,
                                         @PathParam("organizationName") String organizationName,
                                         @PathParam("functionName") String functionName,
                                         ModificationList modifications) {
        String requestPath = toPath(countryName, countyName, organizationName, functionName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_FUNCTION, requestPath);
        String functionPath = entityProvider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath(functionPath).build())).build();
    }

    @Operation(
            summary = "Update Function",
            description = "Update a Function, by a list of attribute modifications<br/>"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "functionName", description = "Function name")
    @RequestBody(
            required = true,
            name = "ModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = ModificationList.class,
                            description = "ModificationList object")
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to updated Function",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Something went wrong during patch, see actual error message."
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/functions/{functionName}")
    public Response updateFunctionPatch3(@PathParam("countryName") String countryName,
                                         @PathParam("organizationName") String organizationName,
                                         @PathParam("units") String units,
                                         @PathParam("functionName") String functionName,
                                         ModificationList modifications) {
        String requestPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")), functionName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_FUNCTION, requestPath);
        String functionPath = entityProvider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath(functionPath).build())).build();
    }

    @Operation(
            summary = "Update Function",
            description = "Update a Function, by a list of attribute modifications<br/>"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "functionName", description = "Function name")
    @RequestBody(
            required = true,
            name = "ModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = ModificationList.class,
                            description = "ModificationList object"
                    )
            )
    )
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to updated Function",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    }
            ),
            @APIResponse(
                    responseCode = "400",
                    description = "Something went wrong during patch, see actual error message."
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/functions/{functionName}")
    public Response updateFunctionPatch4(@PathParam("countryName") String countryName,
                                         @PathParam("countyName") String countyName,
                                         @PathParam("organizationName") String organizationName,
                                         @PathParam("units") String units,
                                         @PathParam("functionName") String functionName,
                                         ModificationList modifications) {
        String requestPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")), functionName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_FUNCTION, requestPath);
        String functionPath = entityProvider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath(functionPath).build())).build();
    }

    @Operation(
            summary = "Delete Function",
            description = "Delete Function by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "functionName", description = "Function name")
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @DELETE
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/functions/{functionName}")
    public Response deleteFunction1(@PathParam("countryName") String countryName,
                                    @PathParam("countyName") String countyName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("functionName") String functionName) {
        String requestPath = toPath(countryName, countyName, organizationName, functionName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_FUNCTION, requestPath);
        entityProvider.removeEntityWithPath(requestPath);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Delete Function",
            description = "Delete Function by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "functionName", description = "Function name")
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @DELETE
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/functions/{functionName}")
    public Response deleteFunction2(@PathParam("countryName") String countryName,
                                    @PathParam("countyName") String countyName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("units") String units,
                                    @PathParam("functionName") String functionName) {
        String requestPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")), functionName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_FUNCTION, requestPath);
        entityProvider.removeEntityWithPath(requestPath);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Delete Function",
            description = "Delete Function by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "functionName", description = "Function name")
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @DELETE
    @Path("countries/{countryName}/organizations/{organizationName}/functions/{functionName}")
    public Response deleteFunction3(@PathParam("countryName") String countryName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("functionName") String functionName) {
        String requestPath = toPathF(countryName, organizationName, functionName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_FUNCTION, requestPath);
        entityProvider.removeEntityWithPath(requestPath);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Delete Function",
            description = "Delete Function by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @Parameter(name = "functionName", description = "Function name")
    @APIResponses({
            @APIResponse(
                    responseCode = "204",
                    description = "Request successful"
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found for specified path"
            )
    }
    )
    @DELETE
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/functions/{functionName}")
    public Response deleteFunction4(@PathParam("countryName") String countryName,
                                    @PathParam("organizationName") String organizationName,
                                    @PathParam("units") String units,
                                    @PathParam("functionName") String functionName) {
        String requestPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")), functionName);
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_FUNCTION, requestPath);
        entityProvider.removeEntityWithPath(requestPath);
        return Response.noContent().build();
    }

//    private String modifyFunction(String path, FunctionModification functionModification) {
//        if (functionModification.modificationType.equalsIgnoreCase(ModificationTypes.MOVE.name())) {
//            return entityProvider.move(path, functionModification.newPath);
//        } else {
//            return entityProvider.merge(path, functionModification.function);
//        }
//    }

    private String toPath(String countryName, String countyName, String organizationName, List<String> units) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName);
        units.forEach(builder::path);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String organizationName, List<String> units) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName);
        units.forEach(builder::path);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String countyName, String organizationName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName);
        return builder.build().getPath();
    }

    private String toPathF(String countryName, String organizationName, String functionName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName)
                .path("organizations").path(organizationName).path("functions")
                .path(functionName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String organizationName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String countyName, String organizationName, String functionName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName).path("functions").path(functionName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String countyName, String organizationName, List<String> units, String functionName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("counties")
                .path(countyName).path("organizations").path(organizationName);
        units.forEach(builder::path);
        builder.path("functions").path(functionName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String organizationName, List<String> units, String functionName) {
        UriBuilder builder = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName);
        units.forEach(builder::path);
        builder.path("functions").path(functionName);
        return builder.build().getPath();
    }

    private String expectedChildPath(String parentPath, String childName) {
        return UriBuilder.fromPath(parentPath).path("functions/").path(childName).build().getPath();
    }
}
