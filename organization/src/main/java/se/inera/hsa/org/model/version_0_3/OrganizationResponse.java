package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import se.inera.hsa.org.model.unions.MetaData;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class OrganizationResponse extends Organization {

    @Schema(
            required = true,
            readOnly = true
    )
    @NotBlank
    public String uuid;

    @Schema(
            required = true,
            readOnly = true
    )
    @NotNull
    public MetaData metaData;


}
