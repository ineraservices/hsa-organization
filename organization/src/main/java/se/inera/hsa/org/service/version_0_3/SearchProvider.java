package se.inera.hsa.org.service.version_0_3;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import se.inera.hsa.exception.NotFoundException;
import se.inera.hsa.jsonb.JsonbUtil;
import se.inera.hsa.org.model.unions.UnitPersistence;
import se.inera.hsa.org.model.unions.UnitResponse;
import se.inera.hsa.org.model.version_0_3.CountryPersistence;
import se.inera.hsa.org.model.version_0_3.CountryResponse;
import se.inera.hsa.org.model.version_0_3.CountyPersistence;
import se.inera.hsa.org.model.version_0_3.CountyResponse;
import se.inera.hsa.org.model.version_0_3.EmployeePersistence;
import se.inera.hsa.org.model.version_0_3.EmployeeResponse;
import se.inera.hsa.org.model.version_0_3.FunctionPersistence;
import se.inera.hsa.org.model.version_0_3.FunctionResponse;
import se.inera.hsa.org.model.version_0_3.OrganizationPersistence;
import se.inera.hsa.org.model.version_0_3.OrganizationResponse;
import se.inera.hsa.org.model.version_0_3.QueryResult;
import se.inera.hsa.org.model.version_0_3.SearchResult;

@RequestScoped
public class SearchProvider {

    @Inject
    HSARedisReadAPI hsaRedisAPI;

    public SearchResult performSearch(String freeText) {
        return processIds(hsaRedisAPI.searchIndex(freeText));
    }

    public SearchResult performSimpleSearch(String freeText) {
        return processIds(hsaRedisAPI.simpleSearchIndex(freeText));
    }


    public SearchResult getChildren(String parentPath) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(parentPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + parentPath + "\"");
        List<String> result = hsaRedisAPI.getGraph().getImmediateSubordinatesIds(id).stream().map(hsaRedisAPI::findById).collect(Collectors.toList());
        return processIds(result, result.size(), result.size());
    }

    public SearchResult getSubTree(String parentPath) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(parentPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + parentPath + "\"");

        List<String> persistedObjects = new ArrayList<>();
        persistedObjects.add(hsaRedisAPI.findById(id));
        persistedObjects.addAll(hsaRedisAPI.getAllSubordinates(id));
        return processIds(persistedObjects, persistedObjects.size(), persistedObjects.size());
    }

    public SearchResult performSearchAndFilterByRootNodeSubordinates(String rootNodePath, String freeText) {
        String rootNodeId = hsaRedisAPI.getGraph().getIdFromPath(rootNodePath);
        if (rootNodeId == null) {
            throw new NotFoundException("No node found for path " + rootNodePath);
        }
        List<String> subIds = hsaRedisAPI.getGraph().getAllSubordinatesIds(hsaRedisAPI.getGraph().getImmediateSubordinatesIds(rootNodeId), new ArrayList<>());
        QueryResult queryResult = hsaRedisAPI.searchIndex(freeText);
        List<String> filteredIds = queryResult
                .result
                .stream()
                .filter(Objects::nonNull)
                .filter(subIds::contains)
                .collect(Collectors.toList());
        filteredIds.forEach(System.out::println);
        List<String> filteredResults = filteredIds.stream().map(hsaRedisAPI::findById).collect(Collectors.toList());
        return processIds(filteredResults, queryResult.totalNumberOfHits, queryResult.limit);
    }

    public SearchResult performSimpleSearchAndFilterByRootNodeSubordinates(String rootNodePath, String freeText) {
        System.out.println("FreeText: " + freeText);
        String rootNodeId = hsaRedisAPI.getGraph().getIdFromPath(rootNodePath);
        if (rootNodeId == null) {
            throw new NotFoundException("No node found for path " + rootNodePath);
        }
        List<String> subIds = hsaRedisAPI.getGraph().getAllSubordinatesIds(hsaRedisAPI.getGraph().getImmediateSubordinatesIds(rootNodeId), new ArrayList<>());
        QueryResult queryResult = hsaRedisAPI.simpleSearchIndex(freeText);
        List<String> filteredIds = queryResult
                .result
                .stream()
                .filter(Objects::nonNull)
                .filter(subIds::contains)
                .collect(Collectors.toList());
        return processIds(filteredIds.stream().map(hsaRedisAPI::findById).collect(Collectors.toList()), queryResult.totalNumberOfHits, queryResult.limit);
    }

    private SearchResult processIds(QueryResult queryResult) {
        List<String> results = queryResult.result.stream().filter(Objects::nonNull).map(hsaRedisAPI::findById).collect(Collectors.toList());
        return processIds(results, queryResult.totalNumberOfHits, queryResult.limit);
    }

    private SearchResult processIds(List<String> persistedStrings, Integer totalNumberOfHits, Integer limit) {

        List<CountryResponse> countries = new ArrayList<>();
        List<CountyResponse> counties = new ArrayList<>();
        List<OrganizationResponse> organizations = new ArrayList<>();
        List<UnitResponse> units = new ArrayList<>();
        List<FunctionResponse> functions = new ArrayList<>();
        List<EmployeeResponse> employees = new ArrayList<>();

        persistedStrings.forEach(persistedString -> {
            JsonReader parser = Json.createReader(new StringReader(persistedString));
            JsonObject object = parser.readObject();
            if (object.keySet().contains("country")) {
                countries.add(hsaRedisAPI.toCountryResponse(JsonbUtil.jsonb().fromJson(persistedString, CountryPersistence.class)));
            } else if (object.keySet().contains("county")) {
                counties.add(hsaRedisAPI.toCountyResponse(JsonbUtil.jsonb().fromJson(persistedString, CountyPersistence.class)));
            } else if (object.keySet().contains("organization")) {
                organizations.add(hsaRedisAPI.toOrganizationResponse(JsonbUtil.jsonb().fromJson(persistedString, OrganizationPersistence.class)));
            } else if (object.keySet().contains("unit")) {
                units.add(hsaRedisAPI.toUnitResponse(JsonbUtil.jsonb().fromJson(persistedString, UnitPersistence.class)));
            } else if (object.keySet().contains("function")) {
                functions.add(hsaRedisAPI.toFunctionResponse(JsonbUtil.jsonb().fromJson(persistedString, FunctionPersistence.class)));
            } else if (object.keySet().contains("employee")) {
                employees.add(hsaRedisAPI.toEmployeeResponse(JsonbUtil.jsonb().fromJson(persistedString, EmployeePersistence.class)));
            }
        });

        SearchResult result = new SearchResult();
        result.countries = countries;
        result.counties = counties;
        result.organizations = organizations;
        result.units = units;
        result.functions = functions;
        result.employees = employees;

        result.limit = limit;
        result.totalHits = totalNumberOfHits;

        return result;
    }

    public SearchResult getPath(String path) {
        String id = hsaRedisAPI.getGraph().getIdFromPathIgnoreTypes(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        List<String> persistedObjects = new ArrayList<>();
        persistedObjects.add(hsaRedisAPI.findById(id));
        return processIds(persistedObjects, persistedObjects.size(), persistedObjects.size());
    }
}
