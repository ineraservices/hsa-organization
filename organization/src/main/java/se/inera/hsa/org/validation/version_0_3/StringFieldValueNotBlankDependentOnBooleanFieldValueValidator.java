package se.inera.hsa.org.validation.version_0_3;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

/**
 * Implementation av {@link StringFieldValueNotBlankDependentOnBooleanFieldValue}.
 **/
public class StringFieldValueNotBlankDependentOnBooleanFieldValueValidator
        implements ConstraintValidator<StringFieldValueNotBlankDependentOnBooleanFieldValue, Object> {

    private String fieldName;
    private String dependBooleanFieldName;
    private Boolean dependBooleanFieldValueExpected;

    @Override
    public void initialize(StringFieldValueNotBlankDependentOnBooleanFieldValue annotation) {
        fieldName = annotation.stringFieldName();
        dependBooleanFieldName = annotation.dependBooleanFieldName();
        dependBooleanFieldValueExpected = Boolean.parseBoolean(annotation.dependBooleanFieldValueExpected());
    }

    @Override
    public boolean isValid(Object objectToBeValidated, ConstraintValidatorContext constraintValidatorContext) {

        if (objectToBeValidated == null) {
            return true;
        }

        String fieldValue = getStringValue(objectToBeValidated, fieldName);
        boolean dependBooleanFieldValue = getBooleanValue(objectToBeValidated, dependBooleanFieldName);

        if ((fieldValue == null || fieldValue.trim().length() <= 0) && dependBooleanFieldValue == dependBooleanFieldValueExpected) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(constraintValidatorContext.getDefaultConstraintMessageTemplate())
                    .addPropertyNode(this.dependBooleanFieldName)
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

    private String getStringValue(Object objectToBeValidated, String fieldName) {
        Field field;
        String value;
        Class c = objectToBeValidated.getClass();
        try {
            field = c.getField(fieldName);
            value = (String) field.get(objectToBeValidated);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
            if (objectToBeValidated.getClass().getSuperclass() != null) {
                value = getStringValue(objectToBeValidated.getClass().getSuperclass(), fieldName);
            } else {
                throw new RuntimeException(e);
            }
        }
        return value;
    }

    private boolean getBooleanValue(Object objectToBeValidated, String fieldName) {
        Field field;
        boolean value;
        Class c = objectToBeValidated.getClass();
        try {
            field = c.getField(fieldName);
            value = (boolean) field.get(objectToBeValidated);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
            if (objectToBeValidated.getClass().getSuperclass() != null) {
                value = getBooleanValue(objectToBeValidated.getClass().getSuperclass(), fieldName);
            } else {
                throw new RuntimeException(e);
            }
        }
        return value;
    }

}
