package se.inera.hsa.org.drools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The class is populated from Drools-rule files and then the result is evaluated in {@link ValidationEngine}.
 */
public class DroolsValidationResult {
    private final String rulesetName;
    private final String className;
    private Map<String, List<ValidationError>> errors;
    private Map<String, List<ValidationWarning>> warnings;
    private Map<String, List<ValidationInformation>> informations;

    public DroolsValidationResult(String rulesetName, String ObjectName) {
        this.rulesetName = rulesetName;
        className = ObjectName;
        errors = new HashMap<>();
        warnings = new HashMap<>();
        informations = new HashMap<>();
    }

    public void addError(ValidationCode errorCode, String attributeName, String errorMessage) {
        errors.computeIfAbsent(attributeName, list -> new ArrayList<>()).add(new ValidationError(rulesetName, errorCode, className, attributeName, errorMessage));
    }

    public void addWarning(ValidationCode warningCode, String attributeName, String warningMessage) {
        warnings.computeIfAbsent(attributeName, list -> new ArrayList<>()).add(new ValidationWarning(rulesetName, warningCode, className, attributeName, warningMessage));
    }

    public void addInfo(ValidationCode informationCode, String attributeName, String message) {
        informations.computeIfAbsent(attributeName, list -> new ArrayList<>()).add(new ValidationInformation(rulesetName, informationCode, className, attributeName, message));
    }

    public List<ValidationError> getErrors(String attributeName) {
        return errors.getOrDefault(attributeName, Collections.emptyList());
    }

    public List<ValidationWarning> getWarnings(String attributeName) {
        return warnings.getOrDefault(attributeName, Collections.emptyList());
    }

    public List<ValidationInformation> getInformations(String attributeName) {
        return informations.getOrDefault(attributeName, Collections.emptyList());
    }

    public List<ValidationError> getAllErrors() {
        return errors.values().stream().flatMap(List::stream).collect(Collectors.toList());
    }

    public String getRulesetName() {
        return rulesetName;
    }

}
