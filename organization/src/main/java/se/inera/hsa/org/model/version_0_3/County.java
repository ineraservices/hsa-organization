package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

public class County implements Serializable {

    public static final long serialVersionUID = 5074967657402238248L;

    @NotBlank
    @Schema(
            required = true
    )
    public String name;

    @NotBlank
    @Pattern(regexp = "County")
    @Schema(
            required = true,
            pattern = "County"
    )
    public String type;

    //From HSA-Schema 5
    public String description;

    public String hsaIdCounter;
    public String hsaIdPrefix;
}
