package se.inera.hsa.org.service.version_0_5;

import static java.lang.String.format;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.PatchException;
import se.inera.hsa.org.model.version_0_3.GeographicalCoordinates;
import se.inera.hsa.org.model.version_0_3.Hours;
import se.inera.hsa.org.model.version_0_3.PostalAddress;
import se.inera.hsa.org.model.version_0_5.Modification;

public class ModificationUtil {
    private Gson gson= new GsonBuilder().create();

    public void handleModification(Object object, List<Modification> mods) throws PatchException {
        for (Modification modification : mods) {
            if (modification.getAttributeName().equalsIgnoreCase("name") ) {
                throw new BadRequestException("name only modifiable through move", ErrorCodes.NOT_ALLOWED_ON_RDN);
            } else if (modification.getAttributeName().equalsIgnoreCase("hsaIdentity") ) {
                throw new BadRequestException("Not allowed to modify hsaIdentity", ErrorCodes.CONSTRAINT_VIOLATION);
            }
            try {
                Field field = object.getClass().getField(modification.getAttributeName());
                String key = field.getName(); // attribute name
                if ( modification.getAttributeName().equalsIgnoreCase(key) ) {
                    if ( modification.getModificationType().equalsIgnoreCase("DELETE") ) {
                        handleDeleteAttributeValue(object, modification, field);
                    } else if ( modification.getModificationType().equalsIgnoreCase("ADD") ) {
                        handleAddAttributeValue(object, modification, field);
                    } else {
                        throw new BadRequestException(format("ModificationType %s not equal to ADD or DELETE", modification.getModificationType()));
                    }
                }
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
                throw new PatchException(e);
            }
        }
    }

    private void handleAddAttributeValue(Object object, Modification modification, Field field)
            throws IllegalAccessException {
        // lägg till attributet
        Object newValue = null;

        if ( field.getType().equals(List.class)) {
            addListValue(object, modification, field);
        } else {
            if ( field.getType().equals(String.class)) {
                newValue = modification.getAttributeValue();
            } else if ( field.getType().equals(PostalAddress.class)) {
                newValue = mapToPostalAddress(modification.getAttributeValue());
            } else if ( field.getType().equals(GeographicalCoordinates.class)) {
                newValue = mapToGeoGraphicalCoordinates(modification.getAttributeValue());
            } else if ( field.getType().equals(Boolean.TYPE)) {
                newValue = (Boolean) modification.getAttributeValue();
            }
            if ( newValue == null) {
                throw new BadRequestException(format("Did you forgot add the attributeValue for attribute %s", field.getName()));
            }

            Object oldValues = field.get(object);
            if ( oldValues == null || ( oldValues instanceof Boolean )) {
                field.set(object, newValue);
            } else {
                throw new BadRequestException(format("Cannot add another value to a single value attribute %s", field.getName()), ErrorCodes.CONSTRAINT_VIOLATION);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void addListValue(Object object, Modification modification, Field field)
            throws IllegalAccessException {
        List<Object> oldValues = (List<Object>) field.get(object);
        if ( oldValues == null ) {
            oldValues = new ArrayList<>();
        }
        List<?> newValues = null;
        if ( modification.getAttributeName().toLowerCase().endsWith("hours")) {
           newValues = mapToHoursList(modification.getAttributeValue());
        } else {
            newValues = mapToStringList(modification.getAttributeValue());
        }
        if ( newValues == null ) {
            throw new BadRequestException(format("Did you forgot add the attributeValue for attribute %s", field.getName()));
        }
        for (Object aNewValue : newValues) {
            boolean contains = oldValues.contains(aNewValue);
            if ( !contains ) {
                oldValues.add(aNewValue);

            } else {
                throw new BadRequestException(format("Cannot add a value already existing to an attribute %s", field.getName()), ErrorCodes.CONSTRAINT_VIOLATION);
            }
        }
        field.set(object, oldValues);
    }

    private void handleDeleteAttributeValue(Object object, Modification modification, Field field)
            throws IllegalAccessException {
        // Ta bort attributet
        if ( modification.getAttributeValue() == null ) {
            removeAttribute(object, field);
        } else {
//            ta bort specifikt värde
            if ( field.getType().equals(List.class)) {
                handleDeleteListValue(object, modification, field);
            } else {
                Object valueToBeDeleted = getValueToBeDeleted(modification, field);

                Object oldValue = field.get(object);
                if ( oldValue == null ) {
                    throw new BadRequestException(format("No such attribute or value: %s", field.getName()), ErrorCodes.NO_SUCH_ATTRIBUTE);
                }
                if ( oldValue.equals(valueToBeDeleted)) {
                    removeAttribute(object, field);
                } else {
                    throw new BadRequestException(format("No such attribute or value:%s=%s", valueToBeDeleted, field.getName()), ErrorCodes.NO_SUCH_ATTRIBUTE);
                }
            }
        }
    }

    private void removeAttribute(Object object, Field field) throws IllegalAccessException {
        if ( field.getType().equals(Boolean.TYPE)) {
            field.set(object, false);
        } else {
            field.set(object, null);
        }
    }

    private Object getValueToBeDeleted(Modification modification, Field field) {
        Object valueToBeDeleted = null;
        if ( field.getType().equals(String.class)) {
            valueToBeDeleted = modification.getAttributeValue();
        }
        else if ( field.getType().equals(PostalAddress.class)) {
            valueToBeDeleted = mapToPostalAddress(modification.getAttributeValue());
        } else if ( field.getType().equals(GeographicalCoordinates.class)) {
            valueToBeDeleted = mapToGeoGraphicalCoordinates(modification.getAttributeValue());
        }  else if ( field.getType().equals(Boolean.TYPE)) {
            valueToBeDeleted = (Boolean) modification.getAttributeValue();
        }
        return valueToBeDeleted;
    }

    private Object mapToGeoGraphicalCoordinates(Object attributeValue) {
        return gson.fromJson(gson.toJson(attributeValue), new TypeToken<GeographicalCoordinates>() {}.getType());
    }

    private Object mapToPostalAddress(Object attributeValue) {
        return gson.fromJson(gson.toJson(attributeValue), new TypeToken<PostalAddress>() {}.getType());
    }

    private void handleDeleteListValue(Object object, Modification modification, Field field)
            throws IllegalAccessException {
        @SuppressWarnings("unchecked")
        List<Object> oldValues = (List<Object>) field.get(object);
        if ( oldValues == null ) {
            //  Error remove a specific value from an empty list
            throw new BadRequestException(format("No such attribute or value: %s", field.getName()), ErrorCodes.NO_SUCH_ATTRIBUTE);
        }

        List<?> toBeDeletedValues =null;
        if ( modification.getAttributeName().toLowerCase().endsWith("hours")) {
            toBeDeletedValues = mapToHoursList(modification.getAttributeValue());
        } else {
            toBeDeletedValues = mapToStringList(modification.getAttributeValue());
        }
        for (Object aValueToBeDeleted : toBeDeletedValues) {
            deleteOneValue(oldValues, aValueToBeDeleted, modification.getAttributeName());
        }
        if (oldValues.isEmpty()) {
            oldValues = null;
        }

        field.set(object, oldValues);
    }

    private List<Hours> mapToHoursList(Object attributeValue) {
        return gson.fromJson(gson.toJson(attributeValue), new TypeToken<List<Hours>>() {}.getType());
    }
    private List<String> mapToStringList(Object attributeValue) {
        return gson.fromJson(gson.toJson(attributeValue), new TypeToken<List<String>>() {}.getType());
    }

    private void deleteOneValue(List<Object> oldValues, Object object, String attributeName) {
        boolean removed = oldValues.remove(object);
        if ( !removed ) {
            //  Error didn't contain the specific value
            throw new BadRequestException(format("No such attribute or value:%s=%s", object, attributeName), ErrorCodes.NO_SUCH_ATTRIBUTE);
        }
    }
}
