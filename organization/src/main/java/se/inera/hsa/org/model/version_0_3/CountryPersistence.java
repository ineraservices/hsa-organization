package se.inera.hsa.org.model.version_0_3;

import javax.validation.Valid;

public class CountryPersistence extends Persistence{
    @Valid
    public Country country;
}
