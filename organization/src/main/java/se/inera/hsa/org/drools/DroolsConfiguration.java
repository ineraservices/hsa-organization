package se.inera.hsa.org.drools;

import static se.inera.hsa.org.api.ApiVersion.toFolderString;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.KieRepository;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;

import se.inera.hsa.org.api.ApiVersion;

/**
 * This class is responsible for initializing Drools, by loading the different rule-files for different versions.
 */
public class DroolsConfiguration {

    private DroolsConfiguration() {
        // empty for now
    }

    public static final String CURRENT_RULES = "se/inera/hsa/org/rules/current";
    public static final String FORMER_RULES = "se/inera/hsa/org/rules/former";
    public static final String V03_CURRENT_RULES = "se/inera/hsa/org/rules/v03/current";
    public static final String V03_FORMER_RULES = "se/inera/hsa/org/rules/v03/former";

    private static Hashtable<String, KieContainer> containers;

    /**
     * creates a KieFileSystem that contains the given files.
     */
    private static KieFileSystem getKieFileSystem(List<URL> fileNames) {
        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        for (URL fileName : fileNames) {
            kieFileSystem.write(ResourceFactory.newUrlResource(fileName));
        }
        return kieFileSystem;
    }

    /**
     * Initializes a KieRepository.
     */
    private static void getKieRepository() {
        KieServices kieServices = KieServices.Factory.get();
        final KieRepository kieRepository = kieServices.getRepository();
        kieRepository.addKieModule(kieRepository::getDefaultReleaseId);
    }

    /**
     * Initialize Drools KieContainer with given ruleFile URLs.
     */
    private static KieContainer initContainer(List<URL> formerRuleFiles) {

        getKieRepository();
        KieFileSystem kieFileSystem = getKieFileSystem(formerRuleFiles);

        KieServices kieServices = KieServices.Factory.get();
        KieBuilder kb = kieServices.newKieBuilder(kieFileSystem);
        kb.buildAll();
        KieModule kieModule = kb.getKieModule();

        return kieServices.newKieContainer(kieModule.getReleaseId());
    }

    /**
     * This method is responsible for returning all files that are located under a given package as a List of URLs.
     */
    private static List<URL> getFilenamesForDirnameFromCP(String directoryName)  {
        List<URL> filenames = new ArrayList<>();

        try {
            URL url = Thread.currentThread().getContextClassLoader().getResource(directoryName);
            if (url != null) {
                String path = url.getPath();
                int indexOf = path.indexOf('!');
                if (url.getProtocol().equals("file")) {
                    filenames = handleFileDirectory(url);
                } else if (indexOf >= 0) {
                    filenames = handleWarDirectory(directoryName, path, indexOf);
                }
            }
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
        return filenames;
    }

    /**
     * This method adds all files that under given directory inside the application war-file.
     * @return
     */
    private static List<URL> handleWarDirectory(String directoryName,String path, int indexOf)
            throws IOException {
        List<URL> filenames = new ArrayList<>();
        String dirname = "WEB-INF/classes/" +directoryName + "/";
        String jarPath = path.substring(5, indexOf);
        try (JarFile jar = new JarFile(URLDecoder.decode(jarPath, StandardCharsets.UTF_8.name()))) {
            Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                String name = entry.getName();
                if (name.startsWith(dirname) && !dirname.equals(name)) {
                    URL resource = Thread.currentThread().getContextClassLoader().getResource(name);
                    filenames.add(resource);
                }
            }
        }
        return filenames;
    }

    /**
     * This method returns all files that are under the given URL.
     */
    private static List<URL> handleFileDirectory(URL url) throws URISyntaxException {
        List<URL> filenames = new ArrayList<>();
        File file = Paths.get(url.toURI()).toFile();
        if (file != null) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File filename : files) {
                    try {
                        filenames.add(filename.toURI().toURL());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return filenames;
    }

    /**
     * This method returns a KieSession (drools session) that uses the active rules.
     */
    public static KieSession getKieSessionCurrent(String version) {
        try {
            KieContainer kieContainer = containers.get(toFolderString(version)+"_current");
            if ( kieContainer != null) {
                //TODO: Borde nog ändras till newStatelessKieSession istället
                return kieContainer.newKieSession();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method returns a KieSession (drools session) that uses the older rules.
     */
    public static KieSession getKieSessionFormer(String version) {
        try {
            KieContainer kieContainer = containers.get(toFolderString(version)+"_former");
            if ( kieContainer != null) {
              //TODO: Borde nog ändras till newStatelessKieSession istället
                return kieContainer.newKieSession();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method is responsible for initializing drools. It loads rule files from the package 'se/inera/hsa/org/rules/[aVersion]/current'
     * Where aVersion is a member of the versionDirectories Array parameter.
     */
    public static void initDroolsConfiguration(String... versionDirectories) {
        System.out.println("## DROOLS INIT BEGIN ##");
        containers = new Hashtable<>();
        for (String aVersion : versionDirectories) {
            aVersion=ApiVersion.toFolderString(aVersion);
            System.out.println(aVersion);
            String currentRulesPath = String.format("se/inera/hsa/org/rules/%s/current", aVersion);
            String formerRulesPath = String.format("se/inera/hsa/org/rules/%s/former", aVersion);
            System.out.println(currentRulesPath);
            System.out.println(formerRulesPath);
            List<URL> currentRuleFiles = getFilenamesForDirnameFromCP(currentRulesPath);
            System.out.println("# CURRENT");
            currentRuleFiles.forEach(System.out::println);
            List<URL> formerRuleFiles = getFilenamesForDirnameFromCP(formerRulesPath);
            System.out.println("# FORMER");
            formerRuleFiles.forEach(System.out::println);

            KieContainer currentContainer = initContainer(currentRuleFiles);
            containers.put(aVersion+"_current", currentContainer);
            KieContainer formerContainer = initContainer(formerRuleFiles);
            containers.put(aVersion+"_former", formerContainer);
        }

        System.out.println("### DROOLS INIT END ###");
    }
}
