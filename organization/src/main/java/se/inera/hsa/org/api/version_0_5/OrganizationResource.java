package se.inera.hsa.org.api.version_0_5;

import static se.inera.hsa.org.api.ApiVersion.VERSION_0_5;

import java.time.ZonedDateTime;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Encoding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.opentracing.Traced;

import se.inera.hsa.log.service.version_0_3.AuditLogger;
import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.model.version_0_3.OrganizationResponse;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.model.version_0_5.ModificationList;
import se.inera.hsa.org.model.version_0_5.Organization;
import se.inera.hsa.org.service.version_0_5.EntityProvider;
import se.inera.hsa.org.validation.version_0_3.PatchModificationList;

/**
 * @author kdaham, Tewelle, magnusg
 */
@Tag(name = "Organization")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Path(VERSION_0_5)
@Traced
public class OrganizationResource {

    @Context
    UriInfo uriInfo;

    @Inject
    EntityProvider<Organization> provider;

    @Inject
    AuditLogger auditLogger;

    @Operation(
            summary = "Get Organization",
            description = "Get Organization by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns OrganizationResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = OrganizationResponse.class),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}")
    public EntityResponse getOrganization1(@PathParam("countryName") String countryName,
                                                                  @PathParam("countyName") String countyName,
                                                                  @PathParam("organizationName") String organizationName) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("counties").path(countyName)
                .path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_ORGANIZATION, requestPath);
        return provider.getEntityWithPath(requestPath);
    }

    @Operation(
            summary = "Get Organization",
            description = "Get Organization by path.")
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns OrganizationResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = OrganizationResponse.class),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Path("countries/{countryName}/organizations/{organizationName}")
    public EntityResponse getOrganizationUnderCountry(@PathParam("countryName") String countryName,
                                                                   @PathParam("organizationName") String organizationName) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_ORGANIZATION, requestPath);
        return provider.getEntityWithPath(requestPath);
    }

    @Operation(hidden = true,
            description = "Update Organization."
            )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}")
    public Response updateOrganizationPatch1(@PathParam("countryName") String countryName,
            @PathParam("organizationName") String organizationName,
            ModificationList modifications) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_ORGANIZATION, requestPath);
        String organizationPath = provider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION_0_5).path(organizationPath).build()).build();
    }

    @Operation(hidden = true,
            description = "Update Organization")
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}")
    public Response updateOrganizationPatch2(@PathParam("countryName") String countryName,
            @PathParam("countyName") String countyName,
            @PathParam("organizationName") String organizationName,
            ModificationList modifications) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("counties").path(countyName)
                .path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_ORGANIZATION, requestPath);
        String organizationPath = provider.patch(requestPath, modifications.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION_0_5).path(organizationPath).build()).build();
    }
//
//    private String modifyOrganization(String path, OrganizationModification organizationModification) {
//        if (organizationModification.modificationType.equalsIgnoreCase(ModificationTypes.MOVE.name())) {
//            return provider.move(path, organizationModification.newPath);
//        } else {
//            return provider.merge(path, organizationModification.organization);
//        }
//    }

}
