package se.inera.hsa.org.model.unions;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UnitResponse extends Unit {

    @Schema(
         required = true,
         readOnly = true
    )
    @NotBlank
    public String uuid;

    @Schema(

            required = true,
            readOnly = true
    )
    @NotNull
    public MetaData metaData;

}
