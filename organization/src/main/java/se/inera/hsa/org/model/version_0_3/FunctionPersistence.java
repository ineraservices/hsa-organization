package se.inera.hsa.org.model.version_0_3;

import javax.validation.Valid;

public class FunctionPersistence extends Persistence {

    @Valid
    public Function function;

}
