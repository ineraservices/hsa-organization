package se.inera.hsa.org.validation.version_0_3;

import java.util.List;

public class PatchModificationList {
    private List<PatchModification> modifications;

    public List<PatchModification> getModifications() {
        return modifications;
    }

    public void setModifications(List<PatchModification> modifications) {
        this.modifications = modifications;
    }
    
    
}
