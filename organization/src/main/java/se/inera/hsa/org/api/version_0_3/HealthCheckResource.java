package se.inera.hsa.org.api.version_0_3;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.opentracing.Traced;
import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.model.version_0_3.HealthStatus;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Tag(name = "Health")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Path(ApiVersion.VERSION)
@Traced
public class HealthCheckResource {

    @GET
    @Path("health")
    public HealthStatus health() {
        return new HealthStatus();
    }
}
