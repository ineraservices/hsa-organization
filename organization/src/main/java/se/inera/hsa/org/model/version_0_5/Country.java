package se.inera.hsa.org.model.version_0_5;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class Country extends Entity {

    @NotBlank
    @Pattern(regexp = "[A-Z]{2}")
    @Schema(
            required = true
    )
    public String isoCode;
}
