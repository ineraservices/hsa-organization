package se.inera.hsa.org.validation.version_0_3;

import se.inera.hsa.org.model.version_0_3.County;

import javax.validation.Valid;
import java.io.Serializable;

@ValidModifier_County
public class CountyModification extends Modification implements Serializable {

    private static final long serialVersionUID = 7952072974190694542L;

    @Valid
    public County county;

}
