package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import se.inera.hsa.org.model.version_0_5.Entity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Obligatoriska attribut:
 *     namn (cn)\n
 *     HSA-id (hsaIdentity)\n
 *
 * derived
 *     OBS! Följande attribut ska skapas per automatik i REST API:et (ska alltså inte ingå i skapande-begäran):
 *         createdBy
 *         createTimestamp
 *         modifiersName
 *         modifyTimestamp
 *         numSubordinates
 *         numAllSubordinates.
 */
public class Function implements Serializable {

    private static final long serialVersionUID = -3149239444045289829L;

    @NotBlank
    @Indexed
    @Schema(
            required = true
    )
    public String name;

    @NotBlank
    @Pattern(regexp = "Function")
    @Indexed
    @Schema(
            required = true,
            pattern = "Function"
    )
    public String type;

    @NotBlank
    @Indexed
    @Schema(
            required = true
    )
    public String hsaIdentity;

    //// Envärdes attribut
    public String c;  // (countryName)

    @Indexed
    public String countyCode;

    @Indexed
    public String description;
    public String displayOption;

    public String endDate;

    public GeographicalCoordinates geographicalCoordinates;

    @Size(max = 12)
    public String hsaSweref99Latitude;
    @Size(max = 12)
    public String hsaSweref99Longitude;
    public String hsaGlnCode;
    public String indoorRouteDescription;
    public String hsaAdminComment;
    public String hsaAltText;
    public String hsaDestinationIndicator;
    public String hsaHealthCareArea;
    public String hsaJpegLogotype;
    public String hsaSwitchboardNumber;
    public String hsaVisitingRuleReferral;
    public String hsaVisitingRules;
    public String hsaVpwInformation1;
    public String hsaVpwInformation2;
    public String hsaVpwInformation3;
    public String hsaVpwInformation4;
    public String hsaVpwWebPage;

    public String jpegPhoto;

    @Indexed
    public String localityName;  //(l)
    public String labeledURI;

    @Indexed
    public String mail;  // (rfc822Mailbox)
    @Indexed
    public String municipalityCode;
    @Indexed
    public String orgNo;
    public String ou;  // (organizationalUnitName)
    @Indexed
    public PostalAddress postalAddress;
    @Indexed
    public String postalCode;

    public String route;
    @Indexed
    public String smsTelephoneNumber;
    public String startDate;
    public String street;  // (streetAddress)

    //Flervärdes attribut
    @Indexed
    public List<String> businessClassificationCode;

    public List<String> careType;

    public List<Hours> dropInHours;
    @Indexed
    public List<String> facsimileTelephoneNumber;

    public List<String> financingOrganization;

    @Indexed
    public List<String> hsaBusinessType;

    @Indexed
    public List<String> hsaSyncId;

    @Indexed
    public List<String> hsaTelephoneNumber;

    @Indexed
    public List<String> hsaTextTelephoneNumber;

    public List<String> hsaVisitingRuleAge;

    public List<String> hsaVpwNeighbouringObject;

    public List<String> management;

    public List<String> mobile;  //(mobileTelephoneNumber)

    public List<String> seeAlso;

    public List<Hours> surgeryHours;

    public List<Hours> telephoneHours;

    @Indexed
    public List<String> telephoneNumber;

    public List<String> unitPrescriptionCode;

    public List<Hours> visitingHours;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Function \n");
        for(Field field : this.getClass().getDeclaredFields()) {
            Object object;
            try {
                object = field.get(this);
                if ( object != null ) {
                    sb.append("\t"+field.getName() + ": " + object).append("\n");
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return sb.toString();
    }


}
