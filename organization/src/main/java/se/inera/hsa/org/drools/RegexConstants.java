package se.inera.hsa.org.drools;

/**
 * This class is meant to be used to gather regular expressions which can be used in Drools rule files.
 */
public class RegexConstants {
    public static final String VALID_MAIL = "\\b[\\w\\.\\-]+@[a-zA-Z0-9\\.\\-]+\\.[a-zA-Z]{2,4}\\b";
}
