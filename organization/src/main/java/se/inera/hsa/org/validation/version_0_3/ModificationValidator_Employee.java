package se.inera.hsa.org.validation.version_0_3;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ModificationValidator_Employee implements ConstraintValidator<ValidModifier_Employee, EmployeeModification> {


    @Override
    public void initialize(ValidModifier_Employee constraintAnnotation) {
    }

    @Override
    public boolean isValid(EmployeeModification modification, ConstraintValidatorContext context) {
        if (modification.modificationType.equalsIgnoreCase("merge")) {
            return modification.employee != null && modification.newPath == null;
        } else {
            return (modification.employee == null) && (modification.newPath != null);
        }
    }
}
