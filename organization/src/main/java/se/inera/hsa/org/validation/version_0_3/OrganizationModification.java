package se.inera.hsa.org.validation.version_0_3;

import se.inera.hsa.org.model.version_0_3.Organization;

import javax.validation.Valid;
import java.io.Serializable;

@ValidModifier_Organization
public class OrganizationModification extends Modification implements Serializable {

    private static final long serialVersionUID = 3091679861275015572L;

    @Valid
    public Organization organization;

}
