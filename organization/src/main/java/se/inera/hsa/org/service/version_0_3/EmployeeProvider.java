package se.inera.hsa.org.service.version_0_3;

import org.eclipse.microprofile.metrics.annotation.Metered;
import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.PatchException;
import se.inera.hsa.org.model.version_0_3.Employee;
import se.inera.hsa.org.model.version_0_3.EmployeePersistence;
import se.inera.hsa.org.model.version_0_3.EmployeeResponse;
import se.inera.hsa.org.validation.version_0_3.PatchModification;
import se.inera.hsa.jsonb.JsonbUtil;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.NotFoundException;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.EMPLOYEES_RELATION;
import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.EMPLOYEE_LABEL;


@RequestScoped
public class EmployeeProvider implements Provider<Employee> {

    @Inject
    @ConfigProperty(name = "codesystem.uri")
    String codeSystemURI;

    @Inject
    HSARedisReadAPI hsaRedisReadAPI;

    @Inject
    HSARedisWriteAPI hsaRedisWriteAPI;

    private static String COUNTY_CODES_OID = "1.2.752.129.2.2.1.18";
    private static String MUNICIPALITY_CODES_OID = "1.2.752.129.2.2.1.17";

    private List<String> allowedParentTypes = Arrays.asList("Organization", "Unit");

    @Metered
    @Override
    public void persist(@NotNull String parentPath, @NotNull Employee employee) throws BadRequestException {
        EmployeePersistence employeePersistence = new EmployeePersistence();

        // add metadata
        employeePersistence.creatorUUID = "not_yet_implemented";
        employeePersistence.createTimestamp = ZonedDateTime.now();
        employeePersistence.uuid = UUID.randomUUID().toString();

        // add unit
        employeePersistence.employee = employee;

        // fields map
        Map<String, String> indexFields = mapAttributes(employeePersistence);
        Map<String, String> simpleSearchIndexFields = null;
        try {

            simpleSearchIndexFields = mapSimpleSearchAttributes(parentPath, employeePersistence);
        } catch (Exception e) {
            e.printStackTrace();
        }

        hsaRedisWriteAPI.persist(
                parentPath,
                employee.name,
                allowedParentTypes,
                JsonbUtil.jsonb().toJson(employeePersistence),
                EMPLOYEES_RELATION,
                EMPLOYEE_LABEL,
                indexFields,
                simpleSearchIndexFields);
    }
    
    @Override
    public String patch(@NotNull String path, @NotNull List<PatchModification> mods) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");
     
        String findById = hsaRedisReadAPI.findById(id);
        EmployeePersistence storedEmployee = JsonbUtil.jsonb().fromJson(findById, EmployeePersistence.class);
        
        PatchModificationUtil patcher = new PatchModificationUtil();
        
        try {
            //Mandatory attributes which cannot be changed in handlePatchModification
            patcher.handlePatchModification(storedEmployee.employee, mods, "name", "hsaIdentity");
        } catch (PatchException e) {
            throw new BadRequestException("Error modifying employee: "+e.getMessage(), ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        //TODO: Check for that mandatory attributes are still valid.
        if( (storedEmployee.employee.sn == null || storedEmployee.employee.sn.trim().isEmpty())) {
            throw new BadRequestException("Mandatory attribute sn from object class person is missing", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        if( (storedEmployee.employee.fullName == null || storedEmployee.employee.fullName.trim().isEmpty())) {
            throw new BadRequestException("Mandatory attribute fullName from object class person is missing", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        
        storedEmployee.modifierUUID = "not_yet_implemented";
        storedEmployee.modifyTimestamp = ZonedDateTime.now();
        hsaRedisReadAPI.merge(JsonbUtil.jsonb().toJson(storedEmployee), mapAttributes(storedEmployee),
                mapSimpleSearchAttributes(path.replace("/employees/" + storedEmployee.employee.name, ""), storedEmployee));
        return hsaRedisReadAPI.getGraph().getPathFromId(id);
    }

    @Override
    public String merge(@NotNull String path, @NotNull Employee employee) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        EmployeePersistence employeePersistence = JsonbUtil.jsonb().fromJson(hsaRedisReadAPI.findById(id), EmployeePersistence.class);
        // name is  not allowed to change through merge
        if (!employeePersistence.employee.name.equalsIgnoreCase(employee.name)) {
            throw new BadRequestException("name only modifiable through move", ErrorCodes.NOT_ALLOWED_ON_RDN);
        }
        // hsaIdentity is  not allowed to change
        if (!employeePersistence.employee.hsaIdentity.equalsIgnoreCase(employee.hsaIdentity)) {
            throw new BadRequestException("hsaIdentity not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }

        employeePersistence.modifierUUID = "not_yet_implemented";
        employeePersistence.modifyTimestamp = ZonedDateTime.now();
        employeePersistence.employee = employee;
        hsaRedisReadAPI.merge(JsonbUtil.jsonb().toJson(employeePersistence), mapAttributes(employeePersistence),
                mapSimpleSearchAttributes(path.replace("/employees/" + employeePersistence.employee.name, ""), employeePersistence));
        return hsaRedisReadAPI.getGraph().getPathFromId(id);
    }

    @Override
    public String move(@NotNull String path, String newPath) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        assertMoveOk(newPath);
        EmployeePersistence employeePersistence = JsonbUtil.jsonb().fromJson(hsaRedisReadAPI.findById(id), EmployeePersistence.class);
        employeePersistence.employee.name = newPath.substring(newPath.lastIndexOf("/") + 1);
        employeePersistence.modifierUUID = UUID.randomUUID().toString();
        employeePersistence.modifyTimestamp = ZonedDateTime.now();
        String parentPath = newPath.substring(0, newPath.lastIndexOf("/employees"));
        String newParentId = hsaRedisReadAPI.getGraph().getIdFromPath(parentPath);
        String oldPath = hsaRedisReadAPI.getGraph().getPathFromId(id);
        String oldParentId = hsaRedisReadAPI.getGraph().getIdFromPath(oldPath.substring(0, oldPath.lastIndexOf("/employees")));
        Map<String, String> indexFields = new HashMap<>();
        indexFields.put("uuid", employeePersistence.uuid);
        indexFields.put("name", employeePersistence.employee.name);
        indexFields.put("modifierUUID", employeePersistence.modifierUUID);
        indexFields.put("modifyTimestamp", employeePersistence.modifyTimestamp.toString());
        hsaRedisReadAPI.move(JsonbUtil.jsonb().toJson(employeePersistence), indexFields, mapSimpleSearchAttributes(parentPath,
                employeePersistence),newParentId, HSARedisReadAPI.EMPLOYEES_RELATION, oldParentId);
        return hsaRedisReadAPI.getGraph().getPathFromId(id);
    }

    @Override
    public void remove(@NotNull String requestPath) {
        // anything additional need to be done when deleting unit?
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(requestPath);
        if (id == null) {
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        }
        hsaRedisWriteAPI.delete(requestPath);
    }

    @Override
    public List<EmployeeResponse> findImmediateSubordinates(@NotNull String requestPath, @NotNull String parentType,
                                                            @NotNull String relationType) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");

        return hsaRedisReadAPI.getGraph().getImmediateSubordinatesIdsOfCertainType(id, parentType, relationType).stream()
                .map(hsaRedisReadAPI::findById)
                .map(persistedString -> JsonbUtil.jsonb().fromJson(persistedString, EmployeePersistence.class))
                .map(hsaRedisReadAPI::toEmployeeResponse)
                .collect(Collectors.toList());
    }

    @Override
    public @NotNull EmployeeResponse find(@NotNull String requestPath) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisReadAPI.toEmployeeResponse(JsonbUtil.jsonb().fromJson(hsaRedisReadAPI.findById(id), EmployeePersistence.class));
    }

    private void assertMoveOk(String newPath) {

        // get parentId by allowed types
        String newParentPath = newPath.substring(0, newPath.lastIndexOf("/employees"));

        // verify parentType is ok and that there exists no other object with the same path as requested
        if (hsaRedisReadAPI.getGraph().getType(hsaRedisReadAPI.getGraph().getIdFromPath(newParentPath), allowedParentTypes) == null) {
            throw new BadRequestException("parent object does not exist or if of wrong type (not one of " +
                    String.join(",", allowedParentTypes) + ")", ErrorCodes.NO_SUCH_OBJECT);
        }
        if (hsaRedisReadAPI.getGraph().getIdFromPath(newPath) != null) {
            throw new BadRequestException("entry already exists", ErrorCodes.ENTRY_ALREADY_EXISTS);
        }
    }

    private Map<String, String> mapAttributes(EmployeePersistence employeePersistence) {
        Map<String, String> indexFields = IndexAttributeMapper.mapAttributes(employeePersistence);
        indexFields.putAll(IndexAttributeMapper.mapAttributes(employeePersistence.employee));
        return indexFields;
    }

    private Map<String, String> mapSimpleSearchAttributes(String parentPath, EmployeePersistence employeePersistence) {

        // fields map
        Map<String, String> simpleSearchIndexFields = new HashMap<>();

        // mandatory attributes, space sparated key-words list
        String searchKeyWords = "";
        if (notEmpty(employeePersistence.employee.name)) {
            searchKeyWords += " " + employeePersistence.employee.name;
        }

        if (notEmpty(employeePersistence.employee.givenName)) {
            searchKeyWords += " " + employeePersistence.employee.givenName;
        }

        if (notEmpty(employeePersistence.employee.sn)) {
            searchKeyWords += " " + employeePersistence.employee.sn;
        }

        if (notEmpty(employeePersistence.employee.title)) {
            searchKeyWords += " " + employeePersistence.employee.hsaIdentity;
        }

        if (notEmpty(employeePersistence.employee.hsaIdentity)) {
            searchKeyWords += " " + employeePersistence.employee.hsaIdentity;
        }

        if (notEmpty(parentPath)) {
            searchKeyWords += " " + parentPath.replace("/", " ")
                    .replace("countries", "")
                    .replace("counties", "")
                    .replace("organizations", "")
                    .replace("units", "")
                    .replace("functions", "")
                    .replace("employees", "");
        }

        simpleSearchIndexFields.put("words", escapeSpecialCharacters(searchKeyWords));
        simpleSearchIndexFields.put("type", employeePersistence.employee.type);

        return simpleSearchIndexFields;
    }

    private static boolean notEmpty( final String s ) {
        return s != null && !s.trim().isEmpty();
    }

    private String escapeSpecialCharacters(String inputText) {
        // Redis search, any character of ,.<>{}[]"':;!@#$%^&*()-+=~ will break the text into terms. Thus, we've to escape them here
        return inputText.replaceAll("([-.+@<>{$%}(=;:)])", "\\\\$0");
    }
}
