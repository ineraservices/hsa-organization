package se.inera.hsa.org.model.version_0_3;

import javax.validation.Valid;

public class CountyPersistence extends Persistence {

    @Valid
    public County county;

}
