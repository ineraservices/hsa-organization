package se.inera.hsa.org.validation.version_0_3;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {ModificationValidator_Country.class})
public @interface ValidModifier_Country {

    String message() default "move modification requires newPath but not country, " +
            "merge modification requires country but not newPath";

    Class<? extends Payload>[] payload() default {};

    Class<?>[] groups() default {};

}
