package se.inera.hsa.org.api.version_0_5;

import java.time.ZonedDateTime;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Encoding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import se.inera.hsa.log.service.version_0_3.AuditLogger;
import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.model.version_0_3.CountyResponse;
import se.inera.hsa.org.model.version_0_5.County;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.service.version_0_5.EntityProvider;

/**
 * @author magnusg
 */
@Tag(name = "County")
@Stateless
@Path(ApiVersion.VERSION_0_5)
public class CountyResource {

    @Context
    UriInfo uriInfo;

    @Inject
    EntityProvider<County> provider;

    @Inject
    AuditLogger auditLogger;

    @Operation(
            summary = "Get County",
            description = "Get County by path"
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns CountyResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = CountyResponse.class),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Path("countries/{countryName}/counties/{countyName}")
    @Produces(MediaType.APPLICATION_JSON)
    public EntityResponse getCounty(@PathParam("countryName") String countryName, @PathParam("countyName") String countyName) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("counties").path(countyName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_COUNTY, requestPath);
        return provider.getEntityWithPath(requestPath);
    }
}
