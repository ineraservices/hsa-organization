package se.inera.hsa.org.model.version_0_5;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

import javax.validation.constraints.Email;
import javax.ws.rs.DefaultValue;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import se.inera.hsa.org.model.version_0_3.GeographicalCoordinates;
import se.inera.hsa.org.model.version_0_3.Hours;
import se.inera.hsa.org.model.version_0_3.Indexed;
import se.inera.hsa.org.model.version_0_3.PostalAddress;
import se.inera.hsa.org.validation.version_0_3.CodeSystem;

public class Unit extends Entity {

    // Single value attrs
    public String c; //(countryName);

    @Indexed
    @Schema(
            description = "Any non-null value validated against code system with OID 1.2.752.129.2.2.1.18"
    )
    @CodeSystem(oid = "1.2.752.129.2.2.1.18")
    public String countyCode;

    @Indexed
    public String description;
    public String displayOption;

    @Indexed
    @Schema(
            required = true,
            description = "Mandatory attribute"
    )
    public String hsaIdentity;

    public String endDate;
    /**
     * Geografiska koordinater (enligt RT90) som anger enhetens fysiska placering.
     * Exempel på en koordinat i Stockholm (Djurgårdsbron) är: "X:6581164,Y:1630250"
     */
    //@JsonbTypeDeserializer(GeographicalCoordinatesDeserializer.class)
    public GeographicalCoordinates geographicalCoordinates;
    /**
     * Geografiska koordinater enligt SWEREF 99 TM som anger enhetens fysiska placering.
     * Exempel på en koordinat i Linköping (E4-bron över Stångån) är: N:6477155,E:536352
     */
    //@Size(max = 12)
    public String hsaSweref99Latitude;
    //@Size(max = 12)
    public String hsaSweref99Longitude;
    public String hsaAdminComment;
    public String hsaAltText;
    public String hsaDestinationIndicator;
    @Indexed
    public String hsaDirectoryContact;
    public String hsaGlnCode;
    public String hsaHealthCareArea;
    public String hsaHealthCareUnitManager;
    public String hsaJpegLogotype;
    public String hsaResponsibleHealthCareProvider;
    public String hsaSwitchboardNumber;
    public String hsaVisitingRuleReferral;
    public String hsaVisitingRules;
    public String hsaVpwInformation1;
    public String hsaVpwInformation2;
    public String hsaVpwInformation3;
    public String hsaVpwInformation4;
    public String hsaVpwWebPage;
    public String indoorRouteDescription;
    public String jpegPhoto;
    public String labeledURI;
    @Indexed
    public String localityName; // (l);

    @Indexed
    @Email
    public String mail;// (rfc822Mailbox);

    @Indexed
    @Schema(
            description = "Any non-null value validated against code system with OID 1.2.752.129.2.2.1.17"
    )
    @CodeSystem(oid = "1.2.752.129.2.2.1.17")
    public String municipalityCode;

    @Indexed
    public String orgNo;

    @Indexed
    public PostalAddress postalAddress;
    @Indexed
    public String postalCode;

    public String route;
    @Indexed
    public String smsTelephoneNumber;
    public String startDate;
    public String street; // (streetAddress);

    //MultivalueAttributes
    @Indexed
    public List<String> businessClassificationCode;
    public List<String> careType;
    public List<Hours> dropInHours;
    @Indexed
    public List<String> facsimileTelephoneNumber;
    public List<String> financingOrganization;
    @Indexed
    public List<String> hsaBusinessType;
    public List<String> hsaHealthCareUnitMember;
    @Indexed
    public List<String> hsaSyncId;
    @Indexed
    public List<String> hsaTelephoneNumber;
    @Indexed
    public List<String> hsaTextTelephoneNumber;
    public List<String> hsaVisitingRuleAge;
    public List<String> hsaVpwNeighbouringObject;
    public List<String> management;
    public List<String> mobile; //(mobileTelephoneNumber);
    public List<String> ouShort; // alternative name/s
    public List<String> seeAlso;
    public List<Hours> surgeryHours;
    public List<Hours> telephoneHours;
    @Indexed
    public List<String> telephoneNumber;
    public List<String> unitPrescriptionCode;
    public List<Hours> visitingHours;

    @Indexed
    @Schema(
            defaultValue = "false",
            description = "if true then organizationNumber (orgNo) is required"
    )
    @DefaultValue("false")
    public boolean hsaHealthCareProvider;

    @Schema(
            defaultValue = "false"
    )
    @DefaultValue("false")
    public boolean hsaHealthCareUnit;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Unit\n");
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                Object object = field.get(this);
                if ( object != null) {
                    sb.append("\t").append(field.getName()).append(": ").append(object.toString()).append("\n");
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
