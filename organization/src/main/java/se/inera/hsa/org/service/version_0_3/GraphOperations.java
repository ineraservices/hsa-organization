package se.inera.hsa.org.service.version_0_3;

import java.util.List;

public interface GraphOperations {

    void addRelation(String parentId,
                     String parentType,
                     String relationType,
                     String childType,
                     String childId,
                     String childName);

    void updateNode(String id,
                    String name,
                    String relation,
                    String newParentId,
                    String oldParentId);

    void delete(String id);

    List<String> getImmediateSubordinatesIds(String id);

    List<String> getImmediateSubordinatesIdsOfCertainType(String id, String parentType, String relationType);

    boolean nodeHasSubordinates(String id);

    String getType(String id, List<String> parentTypes);

    String getPathFromId(String id);

    String getIdFromPath(String path);
}
