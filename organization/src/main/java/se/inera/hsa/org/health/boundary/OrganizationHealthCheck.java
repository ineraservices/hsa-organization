package se.inera.hsa.org.health.boundary;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author kdaham
 */
@Health
@ApplicationScoped
public class OrganizationHealthCheck implements HealthCheck {

    // Check if we have a redis connection
    public HealthCheckResponse call() {
        return HealthCheckResponse.named("organization").up().build();
    }
}