package se.inera.hsa.org.drools;

public class ValidationInformation {
    private final String rulesetName;
    private ValidationCode infoCode;
    private String className;
    private String attributeName;
    private String infoMessage;

    public ValidationInformation(String rulesetName, ValidationCode infoCode, String className, String attributeName, String infoMessage) {
        this.rulesetName = rulesetName;
        this.infoCode = infoCode;
        this.className = className;
        this.attributeName = attributeName;
        this.infoMessage = infoMessage;
    }

    public ValidationCode getInfoCode() {
        return infoCode;
    }

    public String getClassName() {
        return className;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getInfoMessage() {
        return infoMessage;
    }

    @Override
    public String toString() {
        return "ValidationInformation{" +
                "rulesetName='" + rulesetName + '\'' +
                ", infoCode=" + infoCode +
                ", className='" + className + '\'' +
                ", attributeName='" + attributeName + '\'' +
                ", infoMessage='" + infoMessage + '\'' +
                '}';
    }
}
