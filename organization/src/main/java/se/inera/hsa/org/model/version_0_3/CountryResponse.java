package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import se.inera.hsa.org.model.unions.MetaData;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CountryResponse extends Country {

    @Schema(
            required = true,
            readOnly = true
    )
    @NotBlank
    public String uuid;

    @Schema(
            required = true,
            readOnly = true
    )
    @NotNull
    public MetaData metaData;


    @Override
    public String toString() {
        return "CountryResponse{" +
            "uuid='" + uuid + '\'' +
            ", metaData=" + metaData +
            ", isoCode='" + isoCode + '\'' +
            ", name='" + name + '\'' +
            ", type='" + type + '\'' +
            '}';
    }
}
