package se.inera.hsa.org.data.version_0_5.model;

/**
 * NodeType is basically a Data Access Layer mirroring of the EntityType enum. It could be a good idea to refactor this
 * class into oblivion and just use EntityType, or something even more generic.
 */
public enum NodeType {

    //Dessa kommer snart ändras
    TOP("Top"),
    COUNTRY("Country"),
    COUNTY("County"),
    ORGANIZATION("Organization"),
    FUNCTION("Function"),
    EMPLOYEE("Employee"),
    UNIT("Unit");

    private final String typeName;

    private NodeType(String typeName){
        this.typeName=typeName;
    }

    public String getTypeName(){
        return typeName;
    }
}
