package se.inera.hsa.org.drools.log;

import org.kie.api.event.rule.*;
import org.kie.api.runtime.rule.Match;

public class HSADebugAgendaEventListener
        implements AgendaEventListener {



    public HSADebugAgendaEventListener() {

    }

    public void matchCreated(MatchCreatedEvent event) {
        log("MatchCreatedEvent", event.toString());
    }

    public void matchCancelled(MatchCancelledEvent event) {
        log("MatchCancelledEvent", event.toString());
    }

    public void beforeMatchFired(BeforeMatchFiredEvent event) {
        log("BeforeMatchFiredEvent", event.toString());
    }

    public void afterMatchFired(AfterMatchFiredEvent event) {
        log("Rule was run", event.getMatch());
    }

    public void agendaGroupPopped(AgendaGroupPoppedEvent event) {
        log(event.toString());
    }

    public void agendaGroupPushed(AgendaGroupPushedEvent event) {
        log(event.toString());
    }

    public void beforeRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
        log(event.toString());
    }

    public void afterRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
        log(event.toString());
    }

    public void beforeRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event) {
        log(event.toString());
    }

    public void afterRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event) {
        log(event.toString());
    }

    private void log(String message) {
        System.out.println(message);
    }

    private void log(String message, String event) {
        System.out.println(message + " event: " + event);
    }

    private void log(String message, Match match) {
        System.out.println("----------------------------------");
        System.out.println(message);
        System.out.println("RULE: \"" + match.getRule().getName() + "\"");
        System.out.println("    package: " + match.getRule().getPackageName());
        System.out.println("    meta data: " + match.getRule());
        System.out.println("----------------------------------");
    }
}
