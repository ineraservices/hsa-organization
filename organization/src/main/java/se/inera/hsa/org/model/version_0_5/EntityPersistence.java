package se.inera.hsa.org.model.version_0_5;

import se.inera.hsa.org.model.version_0_3.Indexed;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * Wrapper class for entities to be saved, allows for metadata to be part of the persistence object without actually
 * being part of the core class (which can then be reused for response-objects etc). Parameterized to County, Unit etc.
 * @param <T>
 */
public class EntityPersistence<T extends Entity> {

    @Indexed
    @NotBlank
    public String uuid;

    @Indexed
    @NotBlank
    public String creatorUUID;

    @Indexed
    @NotNull
    public ZonedDateTime createTimestamp;

    @Indexed
    public String modifierUUID;
    @Indexed
    public ZonedDateTime modifyTimestamp;

    public T data;
}
