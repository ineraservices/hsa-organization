package se.inera.hsa.org.validation.version_0_3;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ModificationValidator_Country implements ConstraintValidator<ValidModifier_Country, CountryModification> {
    @Override
    public void initialize(ValidModifier_Country constraintAnnotation) {
    }

    @Override
    public boolean isValid(CountryModification modification, ConstraintValidatorContext context) {
        return true;
    }
}
