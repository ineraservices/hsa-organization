package se.inera.hsa.org.validation.version_0_3;


import java.util.HashMap;
import java.util.Map;

public class ValidationResult<T> {


    private Map<String, String> info2 = new HashMap<>();

    private Map<String, String> info1 = new HashMap<>();

    private String systemExceptionMessage;

    public T unit;

    ValidationResult(T validatedObj) {
        this.unit = validatedObj;
    }

    public Map<String, String> getInfo2() {
        return info2;
    }

    public Map<String, String> getInfo1() {
        return info1;
    }

    public String getInternalServerErrorMessage() {
        return systemExceptionMessage;
    }

    public boolean validationHasCausedInternalServerError() {
        return systemExceptionMessage != null;
    }

    public void addInfo1(String property, String message) {
        info1.computeIfPresent(property, (k, old) -> old + "\n" + message);
        info1.putIfAbsent(property, message);
    }

    public void addInfo2(String property, String message) {
        info2.computeIfPresent(property, (k, old) -> old + "\n" + message);
        info2.putIfAbsent(property, message);
    }

    public T getValidatedObject() {
        return unit;
    }
}
