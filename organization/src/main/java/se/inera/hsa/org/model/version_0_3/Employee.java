package se.inera.hsa.org.model.version_0_3;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

public class Employee implements Serializable {

	private static final long serialVersionUID = -4351601723042232172L;

	//obligatoriska
	@Indexed
	@NotEmpty
	public String type;
	@Indexed
	@NotEmpty
	public String fullName;
	@Indexed
	@NotEmpty
	public String hsaIdentity;
	@Indexed
	@NotEmpty
	public String sn;
	@Indexed
	@NotEmpty
	public String name;
	
	@Indexed
	public String personalIdentityNumber;
	@Indexed
	public String middleName;
	@Indexed
	public String userPrincipalName;
	@Indexed
	public String hsaPassportBirthDate;
	@Indexed
	public String personalPrescriptionCode;
	@Indexed
	public String hsaPassportValidThru;
	@Indexed
	public String hospIdentityNumber;
	public String hsaAdminComment;
	@Indexed
	public String hsaPassportNumber;
	public String hsaEmigratedPerson;
	@Indexed
	public String hsaProtectedPerson;
	@Indexed
	public String endDate;
	@Indexed
	public String startDate;
	public String hsaVerifiedIdentityNumber;
	@Indexed
	public String hsaDestinationIndicator;
	public String hsaAltText;
	@Indexed
	public String description;
	@Indexed
	public String street;
	public String jpegPhoto;
	public String hsaManagerCode;
	@Indexed
	public String mail;
	@Indexed
	public String initials;
	public PostalAddress postalAddress;
	@Indexed
	public String nickName;
	@Indexed
	public String givenName;
	@Indexed
	public String title;
	@Indexed
	public String hsaSwitchboardNumber;
	@Indexed
	public List<String> serialNumber;
	@Indexed
	public List<String> telephoneNumber;
	@Indexed
	public List<String> hsaSosNursePrescriptionRight;
	@Indexed
	public List<String> validNotBefore;
	@Indexed
	public List<String> validNotAfter;
	@Indexed
	public List<String> hsaGroupPrescriptionCode;
	@Indexed
	public List<String> userCertificate;
	@Indexed
	public List<String> hsaSystemRole;
	@Indexed
	public List<String> hsaSyncId;
	@Indexed
	public List<String> cardNumber;
	@Indexed
	public List<String> hsaSosTitleCodeSpeciality;
	@Indexed
	public List<String> hsaTitle;
	@Indexed
	public List<String> hsaMifareSerialNumber;
	@Indexed
	public List<String> mobile;
	@Indexed
	public List<String> seeAlso;
	public List<Hours> telephoneHours;
	@Indexed
	public List<String> hsaTextTelephoneNumber;
	@Indexed
	public List<String> hsaTelephoneNumber;
	@Indexed
	public List<String> occupationalCode;
	@Indexed
	public List<String> paTitleCode;
}
