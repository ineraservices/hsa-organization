package se.inera.hsa.org.model.unions;

import se.inera.hsa.org.model.version_0_3.Persistence;

import javax.validation.Valid;

public class UnitPersistence extends Persistence {
    @Valid
    public Unit unit;

}
