package se.inera.hsa.org.validation.version_0_3;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CodeSystemValidator.class)
@Target(ElementType.FIELD)
public @interface CodeSystem {

    @NotNull
    String oid() default "";

    String message() default "{provided code not valid for specified code system}";

    Class<? extends Payload>[] payload() default {};

    Class<?>[] groups() default {};
}
