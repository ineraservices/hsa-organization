package se.inera.hsa.org.api;

import java.io.Serializable;

/**
 *
 */
public class ApiVersion implements Serializable {
    public static final String VERSION = "/v0.3";
    public static final String VERSION_0_5 = "/v0.5";

    public static final String toFolderString(String version) {
        return version.replace(".", "").replace("/", "");
    }
}
