package se.inera.hsa.org.service.version_0_3;

import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.ForbiddenException;
import se.inera.hsa.org.model.version_0_3.County;
import se.inera.hsa.org.model.version_0_3.CountyPersistence;
import se.inera.hsa.org.model.version_0_3.CountyResponse;
import se.inera.hsa.org.validation.version_0_3.PatchModification;
import se.inera.hsa.jsonb.JsonbUtil;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
public class CountyProvider implements Provider<County> {

    @Inject
    HSARedisReadAPI hsaRedisAPI;

    @Override
    public void persist(@NotNull String requestPath, @NotNull County entity) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public String merge(@NotNull String id, @NotNull County entity) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public String move(@NotNull String oldPath, String newPath) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }


    @Override
    public void remove(@NotNull String requestPath) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public @NotNull List<CountyResponse> findImmediateSubordinates(@NotNull String requestPath, @NotNull String parentType,
                                                                   @NotNull String relationType) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisAPI.getGraph().getImmediateSubordinatesIdsOfCertainType(id, parentType, relationType).stream()
                .map(hsaRedisAPI::findById)
                .map(persistedString -> JsonbUtil.jsonb().fromJson(persistedString, CountyPersistence.class))
                .map(hsaRedisAPI::toCountyResponse)
                .collect(Collectors.toList());
    }

    @Override
    public CountyResponse find(@NotNull String requestPath) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisAPI.toCountyResponse(JsonbUtil.jsonb().fromJson(hsaRedisAPI.findById(id), CountyPersistence.class));
    }

    @Override
    public String patch(@NotNull String path, @NotNull List<PatchModification> mods) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }
}
