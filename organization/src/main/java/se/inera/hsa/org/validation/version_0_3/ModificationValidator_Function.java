package se.inera.hsa.org.validation.version_0_3;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ModificationValidator_Function implements ConstraintValidator<ValidModifier_Function, FunctionModification> {


    @Override
    public void initialize(ValidModifier_Function constraintAnnotation) {
    }

    @Override
    public boolean isValid(FunctionModification modification, ConstraintValidatorContext context) {
        if (modification.modificationType.equalsIgnoreCase("merge")) {
            return modification.function != null && modification.newPath == null;
        } else {
            return (modification.function == null) && (modification.newPath != null);
        }
    }
}
