package se.inera.hsa.org.api.version_0_3;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Encoding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.opentracing.Traced;

import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.model.version_0_3.OrganizationResponse;
import se.inera.hsa.org.service.version_0_3.OrganizationProvider;
import se.inera.hsa.org.validation.version_0_3.ModificationTypes;
import se.inera.hsa.org.validation.version_0_3.OrganizationModification;
import se.inera.hsa.org.validation.version_0_3.PatchModificationList;
import se.inera.hsa.log.service.version_0_3.AuditLogger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import static se.inera.hsa.org.api.ApiVersion.VERSION;

import java.time.ZonedDateTime;

/**
 * @author kdaham, Tewelle, magnusg
 */
@Tag(name = "Organization")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Path(ApiVersion.VERSION)
@Traced
public class OrganizationResource {

    @Context
    UriInfo uriInfo;

    @Inject
    OrganizationProvider provider;

    @Inject
    AuditLogger auditLogger;

    @Operation(
            summary = "Get Organization",
            description = "Get Organization by path."
    )
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "countyName", description = "County name")
    @Parameter(name = "organizationName", description = "Organization name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns OrganizationResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = OrganizationResponse.class),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}")
    public @Valid OrganizationResponse getOrganization1(@PathParam("countryName") String countryName,
                                                                  @PathParam("countyName") String countyName,
                                                                  @PathParam("organizationName") String organizationName) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("counties").path(countyName)
                .path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_ORGANIZATION, requestPath);
        return provider.find(requestPath);
    }

    @Operation(
            summary = "Get Organization",
            description = "Get Organization by path.")
    @Parameter(name = "countryName", description = "Country name")
    @Parameter(name = "organizationName", description = "Organization name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns OrganizationResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = OrganizationResponse.class),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Path("countries/{countryName}/organizations/{organizationName}")
    public @Valid OrganizationResponse getOrganizationUnderCountry(@PathParam("countryName") String countryName,
                                                                   @PathParam("organizationName") String organizationName) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_ORGANIZATION, requestPath);
        return provider.find(requestPath);
    }

    @Operation(hidden = true,
            description = "Update Organization."
    )
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}")
    public Response updateOrganization1(@PathParam("countryName") String countryName,
                                              @PathParam("organizationName") String organizationName,
                                              @Valid OrganizationModification organizationModification) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_ORGANIZATION, requestPath);
        String organizationPath = modifyOrganization(requestPath, organizationModification);
        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(organizationPath).build()).build();
    }

    @Operation(hidden = true,
            description = "Update Organization")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}")
    public Response updateOrganization2(@PathParam("countryName") String countryName,
                                             @PathParam("countyName") String countyName,
                                             @PathParam("organizationName") String organizationName,
                                             @Valid OrganizationModification organizationModification) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("counties").path(countyName)
                .path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_ORGANIZATION, requestPath);
        String organizationPath = modifyOrganization(requestPath, organizationModification);
        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(organizationPath).build()).build();
    }
    @Operation(hidden = true,
            description = "Update Organization."
            )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}")
    public Response updateOrganizationPatch1(@PathParam("countryName") String countryName,
            @PathParam("organizationName") String organizationName,
            PatchModificationList patchModification) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_ORGANIZATION, requestPath);
        String organizationPath = provider.patch(requestPath, patchModification.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(organizationPath).build()).build();
    }
    
    @Operation(hidden = true,
            description = "Update Organization")
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}")
    public Response updateOrganizationPatch2(@PathParam("countryName") String countryName,
            @PathParam("countyName") String countyName,
            @PathParam("organizationName") String organizationName,
            PatchModificationList patchModification) {
        String requestPath = UriBuilder.fromPath("countries").path(countryName).path("counties").path(countyName)
                .path("organizations").path(organizationName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_ORGANIZATION, requestPath);
        String organizationPath = provider.patch(requestPath, patchModification.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(organizationPath).build()).build();
    }

    private String modifyOrganization(String path, OrganizationModification organizationModification) {
        if (organizationModification.modificationType.equalsIgnoreCase(ModificationTypes.MOVE.name())) {
            return provider.move(path, organizationModification.newPath);
        } else {
            return provider.merge(path, organizationModification.organization);
        }
    }

}
