package se.inera.hsa.org.api.version_0_3;

import static se.inera.hsa.org.api.ApiVersion.VERSION;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Encoding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import se.inera.hsa.clients.CodeSystemClient;
import se.inera.hsa.log.service.version_0_3.AuditLogger;
import se.inera.hsa.log.service.version_0_3.AuditLogger.eventTypes;
import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.drools.ValidationEngine;
import se.inera.hsa.org.model.unions.Unit;
import se.inera.hsa.org.model.unions.UnitResponse;
import se.inera.hsa.org.service.version_0_3.UnitProvider;
import se.inera.hsa.org.validation.version_0_3.ModificationTypes;
import se.inera.hsa.org.validation.version_0_3.PatchModificationList;
import se.inera.hsa.org.validation.version_0_3.UnitModification;


@Tag(name = "Unit")
@Path(ApiVersion.VERSION)
public class UnitResource {

    private static final String COUNTRIES = "countries";
    private static final String COUNTIES = "counties";
    private static final String ORGANIZATIONS = "organizations";
    private static final String UNITS = "units";

    @Inject
    CodeSystemClient codeSystemClient;

    @Inject
    UnitProvider provider;

    @Context
    UriInfo uriInfo;

    @Inject
    AuditLogger auditLogger;
    @Inject
    ValidationEngine<Unit> unitValidation;

    @Operation(
            summary = "Get Unit",
            description = "Get Unit by path."
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "countyName", description = "Name of existing County")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @APIResponse(
            responseCode = "200",
            description = "UnitResponse object containing Unit, internalEntryUUID and generated metadata",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = UnitResponse.class,
                            description = "UnitResponse object"
                    ),
                    encoding = @Encoding(name = "UTF-8")
            )
    )
    @APIResponse(
            responseCode = "404",
            description = "no object found for specified path"
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}")
    public UnitResponse getUnit1(@PathParam("countryName") String countryName,
                                        @PathParam("countyName") String countyName,
                                        @PathParam("organizationName") String organizationName,
                                        @PathParam("units") String units) {
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES).path(countryName).path(COUNTIES)
                .path(countyName).path(ORGANIZATIONS).path(organizationName);
        Arrays.asList(units.split("/")).forEach(builder::path);
        String requestPath = builder.build().getPath();
        return provider.find(requestPath);
    }

    @Operation(
            summary = "Get Unit",
            description = "Get Unit by path."
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc")
    @APIResponse(
            responseCode = "200",
            description = "UnitResponse object containing Unit, internalEntryUUID and generated metadata",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = UnitResponse.class,
                            description = "UnitResponse object"),
                    encoding = @Encoding(name = "UTF-8")
            )
    )
    @APIResponse(
            responseCode = "404",
            description = "no object found for specified path"
    )
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}")
    public UnitResponse getUnit2(@PathParam("countryName") String countryName,
                                 @PathParam("organizationName") String organizationName,
                                 @PathParam("units") String units) {
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES).path(countryName).path(ORGANIZATIONS).path(organizationName);
        Arrays.asList(units.split("/")).forEach(builder::path);
        String requestPath = builder.build().getPath();
        return provider.find(requestPath);
    }

    @Operation(
            summary = "Add Unit",
            description = "Add Unit under existing Organization."
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "countyName", description = "Name of existing County")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @RequestBody(
            required = true,
            name = "Unit",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = Unit.class,
                            description = "Unit"
                    )
            )
    )
            @APIResponse(
                    responseCode = "201",
                    description = "Request successful, unit created",
                    headers = {
                            @Header(
                                    name = "Location",
                                    description = "Path to created Unit",
                                    schema = @Schema(
                                            type = SchemaType.STRING
                                    )
                            )
                    },
                    content = @Content(
                            mediaType = "application/json",
                            encoding = @Encoding(name = "UTF-8")
                    )
            )
            @APIResponse(
                    responseCode = "400",
                    description = "object already exists for specified path"
            )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/units")
    public Response createUnit1(@PathParam("countryName") String countryName,
                                @PathParam("countyName") String countyName,
                                @PathParam("organizationName") String organizationName,
                                Unit unit) {
        URI uri = uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath(UNITS).path(getUnitName(unit)).build());
        Unit validatedUnit = getVerifiedUnit(unit, eventTypes.ADD_UNIT, uri.getPath());

        //getValidatedObject(alteredUnit, AuditLogger.eventTypes.ADD_UNIT, uriInfo.getPath())
        doPersist(toPath(countryName, countyName, organizationName),validatedUnit);

        return Response.created(uri).build();
    }

    private String getUnitName(Unit unit) {
        return unit.name != null ? unit.name : "null";
    }

    @Operation(
            summary = "Add Unit",
            description = "Add Unit under existing Organization."
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "countyName", description = "Name of existing County")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.<br/>" +
            "All units except the leafmost unit must exist prior to POST")
    @RequestBody(
            required = true,
            name = "Unit",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = Unit.class,
                            description = "Unit"
                    )
            )
    )
    @APIResponse(
            responseCode = "201",
            description = "Request successful, unit created",
            headers = {
                    @Header(
                            name = "Location",
                            description = "Path to created Unit",
                            schema = @Schema(
                                    type = SchemaType.STRING
                            )
                    )
            },
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(name = "UTF-8")
            )
    )
    @APIResponse(
            responseCode = "400",
            description = "object already exists for specified path"
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}/units")
    public Response createUnit2(@PathParam("countryName") String countryName,
                                @PathParam("countyName") String countyName,
                                @PathParam("organizationName") String organizationName,
                                @PathParam("units") String units,
                                Unit unit) {
        URI uri = uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath(UNITS).path(getUnitName(unit)).build());
        Unit validatedUnit = getVerifiedUnit(unit, eventTypes.ADD_UNIT, uri.getPath() );

        //getValidatedObject(validatedUnit, AuditLogger.eventTypes.ADD_UNIT, uriInfo.getPath())

        doPersist(toPath(countryName, countyName, organizationName,
                Arrays.asList(units.split("/"))),
                validatedUnit);

        return Response.created(uri).build();
    }

    @Operation(
            summary = "Add Unit",
            description = "Add Unit under existing Organization."
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @RequestBody(
            required = true,
            name = "Unit",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = Unit.class,
                            description = "Unit"
                    )
            )
    )
    @APIResponse(
            responseCode = "201",
            description = "Request successful, unit created",
            headers = {
                    @Header(
                            name = "Location",
                            description = "Path to created Unit",
                            schema = @Schema(
                                    type = SchemaType.STRING
                            )
                    )
            }
    )
    @APIResponse(
            responseCode = "400",
            description = "object already exists for specified path"
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/units")
    public Response createUnit3(@PathParam("countryName") String countryName,
                                @PathParam("organizationName") String organizationName,
                                Unit unit) {
        URI uri = uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath(UNITS).path(getUnitName(unit)).build());

        Unit validatedUnit = getVerifiedUnit(unit, eventTypes.ADD_UNIT, uri.getPath());
//        getValidatedObject(unit, AuditLogger.eventTypes.ADD_UNIT, uriInfo.getPath())
        doPersist(toPath(countryName, organizationName), validatedUnit);

        return Response.created(uri).build();

    }

    @Operation(
            summary = "Add Unit",
            description = "Add Unit under existing Unit."
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.<br/>" +
            "All units except the leafmost unit must exist prior to POST")
    @RequestBody(
            required = true,
            name = "Unit",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = Unit.class,
                            description = "Unit"
                    )
            )
    )
    @APIResponse(
            responseCode = "201",
            description = "Request successful, unit created",
            headers = {
                    @Header(
                            name = "Location",
                            description = "Path to created Unit",
                            schema = @Schema(
                                    type = SchemaType.STRING
                            )
                    )
            },
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(name = "UTF-8")
            )
    )
    @APIResponse(
            responseCode = "400",
            description = "object already exists for specified path"
    )
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}/units")
    public Response createUnit4(@PathParam("countryName") String countryName,
                                @PathParam("organizationName") String organizationName,
                                @PathParam("units") String units,
                                Unit unit) {
        URI uri = uriInfo.getAbsolutePath().resolve(UriBuilder.fromPath(UNITS).path(getUnitName(unit)).build());
        Unit validatedUnit = getVerifiedUnit(unit, eventTypes.ADD_UNIT, uri.getPath());

        doPersist(toPath(countryName, organizationName, Arrays.asList(units.split("/"))), validatedUnit);

        return Response.created(uri).build();
    }

    private Unit getVerifiedUnit(Unit unit, eventTypes eventType, String path) {
//        try {
            return unitValidation.validate(VERSION, unit, eventType, path);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException(e);
//        }
    }

    @Operation(
            summary = "Update Unit",
            description = "Update Unit, either by a MERGE request i.e updating attributes except name,<br/>" +
                    "or a MOVE request i.e change parent and/or change name of Unit"
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "countyName", description = "Name of existing County")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @RequestBody(
            required = true,
            name = "UnitModification",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = UnitModification.class,
                            description = "UnitModification"
                    )
            )
    )
    @APIResponse(
            responseCode = "204",
            description = "Request successful",
            headers = {
                    @Header(
                            name = "Location",
                            description = "Path to updated Unit",
                            schema = @Schema(
                                    type = SchemaType.STRING
                            )
                    )
            },
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(name = "UTF-8")
            )
    )
    @APIResponse(
            responseCode = "404",
            description = "object not found for specified path"
    )
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}")
    public Response updateUnit1(@PathParam("countryName") String countryName,
                                @PathParam("countyName") String countyName,
                                @PathParam("organizationName") String organizationName,
                                @PathParam("units") String units,
                                UnitModification unitModification) {
        String requestPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")));
        auditLogger.log("EMP-1000000001", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_UNIT, requestPath);
        String unitPath = modifyUnit(requestPath, unitModification);
        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(unitPath).build()).build();
    }

    @Operation(
            summary = "Update Unit",
            description = "Update Unit, either by a MERGE request i.e updating attributes except name,<br/>" +
                    "or a MOVE request i.e change parent and/or change name of Unit"
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @RequestBody(
            required = true,
            name = "UnitModification",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                    ),
                    schema = @Schema(
                            implementation = Unit.class,
                            description = "UnitModification"
                    )
            )
    )
    @APIResponse(
            responseCode = "204",
            description = "Request successful",
            headers = {
                    @Header(
                            name = "Location",
                            description = "Path to updated Unit",
                            required = true,
                            schema = @Schema(
                                    type = SchemaType.STRING
                            )
                    )
            },
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(name = "UTF-8")
            )
    )
    @APIResponse(
            responseCode = "404",
            description = "object not found for specified path"
    )
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}")
    public Response updateUnit2(@PathParam("countryName") String countryName,
                                @PathParam("organizationName") String organizationName,
                                @PathParam("units") String units,
                                UnitModification unitModification) {
        String requestPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")));
        auditLogger.log("EMP-1000000001", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_UNIT, requestPath);
        String unitPath = modifyUnit(requestPath, unitModification);
        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(unitPath).build()).build();
    }
    @Operation(
            summary = "Update Unit",
            description = "Update Unit, by a list of attribute modifications<br/>"
            )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "countyName", description = "Name of existing County")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @RequestBody(
            required = true,
            name = "PatchModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                            ),
                    schema = @Schema(
                            implementation = PatchModificationList.class,
                            description = "PatchModificationList"
                            )
                    )
            )
    @APIResponse(
            responseCode = "204",
            description = "Request successful",
            headers = {
                    @Header(
                            name = "Location",
                            description = "Path to updated Unit",
                            schema = @Schema(
                                    type = SchemaType.STRING
                                    )
                            )
            },
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(name = "UTF-8")
                    )
                )
        @APIResponse(
                responseCode = "400",
                description = "Something went wrong during patch, see actual error message."
                )
    @APIResponse(
            responseCode = "404",
            description = "object not found for specified path"
            )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}")
    public Response updateUnitPatch1(@PathParam("countryName") String countryName,
            @PathParam("countyName") String countyName,
            @PathParam("organizationName") String organizationName,
            @PathParam("units") String units,
            PatchModificationList patchModification) {
        String requestPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")));
        auditLogger.log("EMP-1000000001", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_UNIT, requestPath);
        String unitPath = provider.patch(requestPath, patchModification.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(unitPath).build()).build();
    }

    @Operation(
            summary = "Update Unit",
            description = "Update Unit, by a list of attribute modifications<br/>"
            )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @RequestBody(
            required = true,
            name = "PatchModificationList",
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(
                            name = "UTF-8"
                            ),
                    schema = @Schema(
                            implementation = PatchModificationList.class,
                            description = "PatchModificationList"
                            )
                    )
            )
    @APIResponse(
            responseCode = "204",
            description = "Request successful",
            headers = {
                    @Header(
                            name = "Location",
                            description = "Path to updated Unit",
                            required = true,
                            schema = @Schema(
                                    type = SchemaType.STRING
                                    )
                            )
            },
            content = @Content(
                    mediaType = "application/json",
                    encoding = @Encoding(name = "UTF-8")
                    )
                )
        @APIResponse(
                responseCode = "400",
                description = "Something went wrong during patch, see actual error message."
                )
    @APIResponse(
            responseCode = "404",
            description = "object not found for specified path"
            )
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}")
    public Response updateUnitPatch2(@PathParam("countryName") String countryName,
            @PathParam("organizationName") String organizationName,
            @PathParam("units") String units,
            PatchModificationList patchModification) {
        String requestPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")));
        auditLogger.log("EMP-1000000001", ZonedDateTime.now(), AuditLogger.eventTypes.MODIFY_UNIT, requestPath);
        String unitPath = provider.patch(requestPath, patchModification.getModifications());
        return Response.noContent().location(UriBuilder.fromPath(VERSION).path(unitPath).build()).build();
    }

    @Operation(
            summary = "Delete Unit",
            description = "Delete Unit."
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "countyName", description = "Name of existing County")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @APIResponse(
            responseCode = "204",
            description = "Request successful"
    )
    @APIResponse(
            responseCode = "404",
            description = "object not found for specified path"
    )
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/counties/{countyName}/organizations/{organizationName}/{units:.*}")
    public Response deleteUnit1(@PathParam("countryName") String countryName,
                                @PathParam("countyName") String countyName,
                                @PathParam("organizationName") String organizationName,
                                @PathParam(UNITS) String units) {
        String requestPath = toPath(countryName, countyName, organizationName, Arrays.asList(units.split("/")));
        auditLogger.log("EMP-1000000001", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_UNIT, requestPath);
        provider.remove(requestPath);
        return Response.noContent().build();
    }

    @Operation(
            summary = "Delete Unit",
            description = "Delete Unit."
    )
    @Parameter(name = "countryName", description = "Always Sverige")
    @Parameter(name = "organizationName", description = "Name of existing Organization")
    @Parameter(name = "units", description = "units#name pairs on the form units/name_1/units/name_2 etc.")
    @APIResponse(
            responseCode = "204",
            description = "Request successful"
    )
    @APIResponse(
            responseCode = "404",
            description = "object not found for specified path"
    )
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("countries/{countryName}/organizations/{organizationName}/{units:.*}")
    public Response deleteUnit2(@PathParam("countryName") String countryName,
                                @PathParam("organizationName") String organizationName,
                                @PathParam("units") String units) {
        String requestPath = toPath(countryName, organizationName, Arrays.asList(units.split("/")));
        auditLogger.log("EMP-1000000001", ZonedDateTime.now(), AuditLogger.eventTypes.DELETE_UNIT, requestPath);
        provider.remove(requestPath);
        return Response.noContent().build();
    }

    private void doPersist(String parentPath, Unit unit) {
        provider.persist(parentPath, unit);
    }

    private String modifyUnit(String path, UnitModification unitModification) {
        if (unitModification.modificationType.equalsIgnoreCase(ModificationTypes.MOVE.name())) {
            return provider.move(path, unitModification.newPath);
        } else {
            Unit validatedUnit = getVerifiedUnit(unitModification.unit, eventTypes.ADD_UNIT, path);
            return provider.merge(path, validatedUnit);
        }
    }

    private String toPath(String countryName, String countyName, String organizationName, List<String> units) {
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES).path(countryName).path(COUNTIES)
                .path(countyName).path(ORGANIZATIONS).path(organizationName);
        units.forEach(builder::path);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String organizationName, List<String> units) {
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES).path(countryName).path(ORGANIZATIONS).path(organizationName);
        units.forEach(builder::path);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String countyName, String organizationName) {
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES).path(countryName).path(COUNTIES)
                .path(countyName).path(ORGANIZATIONS).path(organizationName);
        return builder.build().getPath();
    }

    private String toPath(String countryName, String organizationName) {
        UriBuilder builder = UriBuilder.fromPath(COUNTRIES).path(countryName).path(ORGANIZATIONS).path(organizationName);
        return builder.build().getPath();
    }

}
