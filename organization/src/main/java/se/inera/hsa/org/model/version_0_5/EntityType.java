package se.inera.hsa.org.model.version_0_5;

/**
 * EntityType is used for cleaner comparisons, instead of using just Strings. The pathlabel is the string found in the
 * LDAP-paths that represent the EntityType. Namelabel is on the other hand used in the Entity object itself.
 */
public enum EntityType {

    TOP("top", ""),
    COUNTRY("country", "countries"),
    COUNTY("county", "counties"),
    ORGANIZATION("organization", "organizations"),
    UNIT("unit", "units"),
    FUNCTION("function", "functions"),
    EMPLOYEE("employee", "employees");

    private String nameLabel;
    private String pathLabel;

    private EntityType(String nameLabel, String pathLabel){
        this.nameLabel=nameLabel;
        this.pathLabel=pathLabel;
    }

    public String getNameLabel(){
        return this.nameLabel;
    }

    public String getPathLabel(){
        return this.pathLabel;
    }

}
