package se.inera.hsa.org.validation.version_0_3;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import se.inera.hsa.org.model.version_0_3.GeographicalCoordinates;
import se.inera.hsa.org.model.version_0_3.Hours;
import se.inera.hsa.org.model.version_0_3.PostalAddress;

@Schema(
        description = "Depending on what the attribute type of the attribute is only one of the *value attributes is to be used."
)
public class PatchModification  {
    public static final String ADD = "ADD";
    public static final String DELETE = "DELETE";

    @Schema(
            required = true,
            pattern = "add|delete",
            description = "Must be ADD or DELETE"
    )
    @NotBlank
    @Pattern(regexp = "add|delete", flags = Pattern.Flag.CASE_INSENSITIVE, message = "add or delete only")
    private String modificationType;

    @Schema(
            required = true,
            description = "Name of the attribute"
    )
    private String attributeName;

    @Schema(
            description = "If the attribute type is a String then this field is to be used when modifying it."
    )
    private String stringValue;

    @Schema(
            description = "If the attribute type is a List of strings then this field is to be used when modifying it."
    )
    private List<String> stringValues;

    @Schema(
            description = "If the attribute typeis a List of Hours then this field is to be used when modifying it."
    )
    private List<Hours> hoursValues;

    @Schema(
            description = "If the attribute type is a List of Hours then this field is to be used when modifying it."
    )
    private PostalAddress postalAddressValue;

    @Schema(
            description = "If the attribute type is a Boolean then this field is to be used when modifying it."
    )
    private Boolean booleanValue;

    @Schema(
            description = "If the attribute type is a GeographicalCoordinate then this field is to be used when modifying it."
    )
    private GeographicalCoordinates geographicalCoordinatesValue;


    public PatchModification() {
        //Empty constructor needs to exist
    }

    public PatchModification(String modificationType, String attrName, String attrValue) {
        this.modificationType = modificationType;
        this.attributeName=attrName;
        this.stringValue = attrValue;
    }

    public PatchModification(String modificationType, String attrName) {
        this.modificationType = modificationType;
        this.attributeName=attrName;
    }
    public PatchModification(String modificationType, String attrName, boolean value) {
        this.modificationType = modificationType;
        this.attributeName=attrName;
        this.booleanValue=value;
    }

    public String getModificationType() {
        return modificationType;
    }

    public void setModificationType(String modificationType) {
        this.modificationType = modificationType;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public List<String> getStringValues() {
        return stringValues;
    }

    public void setStringValues(List<String> stringValues) {
        this.stringValues = stringValues;
    }

    public List<Hours> getHoursValues() {
        return hoursValues;
    }

    public void setHoursValues(List<Hours> hoursValues) {
        this.hoursValues = hoursValues;
    }

    public PostalAddress getPostalAddressValue() {
        return postalAddressValue;
    }

    public void setPostalAddressValue(PostalAddress postalAddressValue) {
        this.postalAddressValue = postalAddressValue;
    }

    public GeographicalCoordinates getGeographicalCoordinatesValue() {
        return geographicalCoordinatesValue;
    }

    public void setGeographicalCoordinatesValue(GeographicalCoordinates geographicalCoordinatesValue) {
        this.geographicalCoordinatesValue = geographicalCoordinatesValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }
}
