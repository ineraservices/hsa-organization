package se.inera.hsa.org.validation.version_0_3;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class SoftValidation<T> {

    @Inject
    private Validator validator;

    public ValidationResult<T> softValidate(T requestObj, Object oldObj, Object currentObj) {
        Map<String, List<ConstraintViolation>> oldViolations = getViolationsForObject(oldObj);
        Map<String, List<ConstraintViolation>> currentViolations = getViolationsForObject(currentObj);

        ValidationResult<T> result = new ValidationResult<>(requestObj);

        getInvalidProperties(getDeclaredFieldNames(requestObj), getDeclaredFieldNames(oldObj), getDeclaredFieldNames(currentObj), oldViolations, currentViolations)
            .forEach(property -> {
                clearProperty(property, requestObj);
                result.addInfo2(property, getViolationMessagesForProperty(property, currentViolations, oldViolations));
            });

        getRemovedProperties(getDeclaredFieldNames(currentObj), getDeclaredFieldNames(oldObj))
            .filter(property -> !violatesSchema(property, oldViolations))
            .forEach(property -> result.addInfo1(property, "Property has been removed in the current HSA schema"));

        getCurrentSchemaViolations(currentViolations, oldViolations.keySet(), getDeclaredFieldNames(oldObj))
            .forEach(property -> result.addInfo1(property, getViolationMessagesForProperty(property, currentViolations, oldViolations)));

        return result;
    }

    /**
     * Returns properties that are invalid according to both schemas.
     */
    private Stream<String> getInvalidProperties(List<String> allProperties, List<String> oldProperties, List<String> currentProperties, Map<String, List<ConstraintViolation>> oldViolations, Map<String, List<ConstraintViolation>> currentViolations) {
        return allProperties.stream()
            .filter(f -> !currentProperties.contains(f) || violatesSchema(f, currentViolations))
            .filter(f -> !oldProperties.contains(f) || violatesSchema(f, oldViolations));
    }

    /**
     * Returns properties that have been removed in the current schema, but exists in the old.
     */
    private Stream<String> getRemovedProperties(List<String> current, List<String> old) {
        return old.stream().filter(p -> !current.contains(p));
    }

    /**
     * Returns a Stream of properties that violates the current schema, but passes (and exists in) the old schema.
     */
    private Stream<String> getCurrentSchemaViolations(Map<String, List<ConstraintViolation>> currentViolations, Set<String> oldViolatingProperties, List<String> oldProperties) {
        return currentViolations.keySet().stream()
            .filter(oldProperties::contains)
            .filter(property -> !oldViolatingProperties.contains(property));
    }

    private String getViolationMessagesForProperty(String p, Map<String, List<ConstraintViolation>> currentViolations, Map<String, List<ConstraintViolation>> oldViolations) {
        return currentViolations.getOrDefault(p, oldViolations.get(p)).stream()
            .map(ConstraintViolation::getMessage)
            .reduce("", (acc, c) -> acc + c);
    }

    private List<String> getDeclaredFieldNames(Object obj) {
        return Arrays.stream(obj.getClass().getDeclaredFields())
            .map(Field::getName)
            .collect(toList());
    }
    private boolean violatesSchema(String field, Map<String, List<ConstraintViolation>> violations) {
        return violations.containsKey(field);
    }

    private void clearProperty(String property, Object obj) {
        try {
            Field field = obj.getClass().getDeclaredField(property);
            field.setAccessible(true);
            field.set(obj, null);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private Map<String, List<ConstraintViolation>> getViolationsForObject(Object obj) {
        return validator.validate(obj).stream()
            .collect(groupingBy(v -> v.getPropertyPath().toString()));
    }
}
