package se.inera.hsa.org.service.version_0_3;

import javax.validation.constraints.NotNull;

import se.inera.hsa.org.validation.version_0_3.PatchModification;

import java.util.List;

public interface Provider<T> {

    void persist(@NotNull String parentPath, @NotNull T entity);

    String merge(@NotNull String path, @NotNull T  entity);

    String move(@NotNull String path, String newPath);

    void remove(@NotNull String path);

    List<? extends T> findImmediateSubordinates(@NotNull String path, @NotNull String parentType, @NotNull String subordinatesType);

    T find(@NotNull String path);
    
    String patch(@NotNull String path, @NotNull List<PatchModification> mods);
}
