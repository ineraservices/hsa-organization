package se.inera.hsa.org.service.version_0_3;

import static java.lang.String.format;

import java.lang.reflect.Field;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.PatchException;
import se.inera.hsa.org.model.version_0_3.Employee;
import se.inera.hsa.org.model.version_0_3.Function;
import se.inera.hsa.org.model.version_0_3.GeographicalCoordinates;
import se.inera.hsa.org.model.version_0_3.Organization;
import se.inera.hsa.org.model.version_0_3.PostalAddress;
import se.inera.hsa.org.model.version_0_5.Unit;
import se.inera.hsa.org.validation.version_0_3.PatchModification;

public class PatchModificationUtil {

    public void handlePatchModification(Object object, List<PatchModification> mods, String... attributes) throws PatchException {
        //checkInputType(object);
        List<String> nonModifiableAttributes = Arrays.asList(attributes).stream().map(String::toLowerCase).collect(Collectors.toList());

        for (PatchModification patchModification : mods) {
            isAttributeModifiable(nonModifiableAttributes, patchModification);
            try {
                Field field = object.getClass().getField(patchModification.getAttributeName());
                String key = field.getName(); // attribute name
                if ( patchModification.getAttributeName().equalsIgnoreCase(key) ) {
                    if ( patchModification.getModificationType().equalsIgnoreCase("DELETE") ) {
                        handleDeleteAttributeValue(object, patchModification, field);
                    } else if ( patchModification.getModificationType().equalsIgnoreCase("ADD") ) {
                        handleAddAttributeValue(object, patchModification, field);
                    } else {
                        throw new BadRequestException(format("ModificationType %s not equal to ADD or DELETE", patchModification.getModificationType()));
                    }
                }
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
                e.printStackTrace();
                throw new PatchException(e);
            }
        }
    }

    private void isAttributeModifiable(List<String> nonModifiableAttributes, PatchModification patchModification) {
        if (patchModification.getAttributeName().equalsIgnoreCase("name") ) {
            throw new BadRequestException("name only modifiable through move", ErrorCodes.NOT_ALLOWED_ON_RDN);
        } else if(nonModifiableAttributes.contains(patchModification.getAttributeName().toLowerCase()) ) {
            throw new BadRequestException(patchModification.getAttributeName()+" not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
    }

    private void checkInputType(Object object) {
        if ( !(object instanceof Function ||
             object instanceof Employee ||
             object instanceof Unit ||
             object instanceof Organization)) {
            throw new InvalidParameterException("Not valid input");
        }
    }

    @SuppressWarnings("unchecked")
    private void handleAddAttributeValue(Object object, PatchModification patchModification, Field field)
            throws PatchException, IllegalArgumentException, IllegalAccessException {
        // lägg till attributet
        Object newValue = null;

        if ( field.getType().equals(List.class)) {
            addListValue(object, patchModification, field);
        } else {
            String type = "";
            if ( field.getType().equals(String.class)) {
                newValue = patchModification.getStringValue();
                type = "stringValue";
            } else if ( field.getType().equals(PostalAddress.class)) {
                type = "postalAddressValue";
                newValue = patchModification.getPostalAddressValue();
            } else if ( field.getType().equals(GeographicalCoordinates.class)) {
                type = "geographicalCoordinatesValue";
                newValue = patchModification.getGeographicalCoordinatesValue();
            } else if ( field.getType().equals(Boolean.TYPE)) {
                type = "booleanValue";
                newValue = patchModification.getBooleanValue();
            }
            if ( newValue == null) {
                throw new BadRequestException(format("Did you use the wrong attribute type for this attribute? Attribute %s expects %s", field.getName(), type));
            }

            Object oldValues = field.get(object);
            if ( oldValues == null || ( oldValues instanceof Boolean )) {
                field.set(object, newValue);
            } else {
                throw new BadRequestException(format("Cannot add another value to a single value attribute %s", field.getName()), ErrorCodes.CONSTRAINT_VIOLATION);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void addListValue(Object object, PatchModification patchModification, Field field)
            throws PatchException, IllegalArgumentException, IllegalAccessException {
        List<Object> oldValues = (List<Object>) field.get(object);
        if ( oldValues == null ) {
            oldValues = new ArrayList<>();
        }
        String type = "";
        List<?> newValues = null;
        if ( patchModification.getAttributeName().toLowerCase().endsWith("hours")) {
           newValues = patchModification.getHoursValues();
           type = "hoursValues";
        } else {
            type = "stringValues";
            newValues = patchModification.getStringValues();
        }
        if ( newValues == null ) {
            throw new BadRequestException(format("Did you use the wrong attribute type for this attribute? Attribute %s expects %s", field.getName(), type));
        }
        for (Object aNewValue : newValues) {
            boolean contains = oldValues.contains(aNewValue);
            if ( !contains ) {
                oldValues.add(aNewValue);

            } else {
//                throw new PatchException("Cannot add a value already existing to an attribute");
                throw new BadRequestException(format("Cannot add a value already existing to an attribute %s", field.getName()), ErrorCodes.CONSTRAINT_VIOLATION);
            }
        }
        field.set(object, oldValues);
    }

    private void handleDeleteAttributeValue(Object object, PatchModification patchModification, Field field)
            throws PatchException, IllegalArgumentException, IllegalAccessException {
        // Ta bort attributet
        if ( patchModification.getGeographicalCoordinatesValue() == null &&
             patchModification.getPostalAddressValue() == null &&
             patchModification.getStringValue() == null &&
             patchModification.getStringValues() == null &&
             patchModification.getBooleanValue() == null &&
             patchModification.getHoursValues() == null) {
            if ( field.getType().equals(Boolean.TYPE)) {
                field.set(object, false);
            } else {
                field.set(object, null);
            }
        } else {
            Object valueToBeDeleted = null;
//            ta bort specifikt värde
            if ( field.getType().equals(List.class)) {
                handleDeleteListValue(object, patchModification, field);
            } else {
                if ( field.getType().equals(String.class)) {
                    valueToBeDeleted = patchModification.getStringValue();
                } else if ( field.getType().equals(PostalAddress.class)) {
                    valueToBeDeleted = patchModification.getPostalAddressValue();
                } else if ( field.getType().equals(GeographicalCoordinates.class)) {
                    valueToBeDeleted = patchModification.getGeographicalCoordinatesValue();
                }  else if ( field.getType().equals(Boolean.TYPE)) {
                    valueToBeDeleted = patchModification.getBooleanValue();
                }

                Object oldValue = field.get(object);
                if ( oldValue == null ) {
//                    throw new PatchException("Cannot remove attribute which has no value");
                    throw new BadRequestException(format("No such attribute or value: %s", field.getName()), ErrorCodes.NO_SUCH_ATTRIBUTE);
                }
                if ( oldValue.equals(valueToBeDeleted)) {
                    if ( field.getType().equals(Boolean.TYPE)) {
                        field.set(object, false);
                    } else {
                        field.set(object, null);
                    }
                } else {
//                    throw new PatchException("Cannot remove attribute value that does not exist on attribute");
                    throw new BadRequestException(format("No such attribute or value:%s=%s", valueToBeDeleted, field.getName()), ErrorCodes.NO_SUCH_ATTRIBUTE);
                }
            }
        }
    }

    private void handleDeleteListValue(Object object, PatchModification patchModification, Field field)
            throws PatchException, IllegalArgumentException, IllegalAccessException {
        @SuppressWarnings("unchecked")
        List<Object> oldValues = (List<Object>) field.get(object);
        if ( oldValues == null ) {
            //  Error remove a specific value from an empty list
            throw new BadRequestException(format("No such attribute or value: %s", field.getName()), ErrorCodes.NO_SUCH_ATTRIBUTE);
//            throw new PatchException("no attribute value exist");
        }

        List<?> toBeDeletedValues =null;
        if ( patchModification.getAttributeName().toLowerCase().endsWith("hours")) {
            toBeDeletedValues = patchModification.getHoursValues();
        } else {
            toBeDeletedValues = patchModification.getStringValues();
        }
        for (Object aValueToBeDeleted : toBeDeletedValues) {
            deleteOneValue(oldValues, aValueToBeDeleted, patchModification.getAttributeName());
        }
        if (oldValues.isEmpty()) {
            oldValues = null;
        }

        field.set(object, oldValues);
    }

    private void deleteOneValue(List<Object> oldValues, Object object, String attributeName) throws PatchException {
        boolean removed = oldValues.remove(object);
        if ( !removed ) {
            //  Error didn't contain the specific value
//            throw new PatchException("attribute value '"+object+"' does not exist");
            throw new BadRequestException(format("No such attribute or value:%s=%s", object, attributeName), ErrorCodes.NO_SUCH_ATTRIBUTE);
        }
    }
}
