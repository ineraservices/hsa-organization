package se.inera.hsa.org.validation.version_0_3;

import se.inera.hsa.org.model.version_0_3.Country;

import javax.validation.Valid;
import java.io.Serializable;

@ValidModifier_Country
public class CountryModification extends Modification implements Serializable {

    private static final long serialVersionUID = -3059749683961306088L;

    @Valid
    public Country country;

}
