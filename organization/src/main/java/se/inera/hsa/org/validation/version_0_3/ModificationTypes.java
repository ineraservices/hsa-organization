package se.inera.hsa.org.validation.version_0_3;

public enum ModificationTypes {
    MERGE,
    MOVE
}