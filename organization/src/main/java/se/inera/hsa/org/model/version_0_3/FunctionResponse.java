package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import se.inera.hsa.org.model.unions.MetaData;

import java.lang.reflect.Field;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class FunctionResponse extends Function {

    @Schema(
            required = true,
            readOnly = true
    )
    @NotBlank
    public String uuid;

    @Schema(
            required = true,
            readOnly = true)
    @NotNull
    public MetaData metaData;


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Field field : this.getClass().getFields()) {
            try {
                if ( field.get(this) != null ) {
                    sb.append(field.getName()).append(" - ").append(field.get(this)).append("\n");
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
