package se.inera.hsa.org.model.version_0_3;

import java.io.Serializable;
import java.util.List;

//import se.inera.hsa.org.model.unions.UnitResponse;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import se.inera.hsa.org.model.unions.UnitResponse;

public class SearchResult implements Serializable {

    public static final long serialVersionUID = 7613420175293995689L;

    public @NotBlank Integer totalHits;

    public @NotBlank Integer limit;

    public @Valid List<CountryResponse> countries;

    public @Valid List<CountyResponse> counties;

    public @Valid List<OrganizationResponse> organizations;

    public @Valid List<UnitResponse> units;

    public @Valid List<FunctionResponse> functions;

    public @Valid List<EmployeeResponse> employees;

    public SearchResult(){}

}
