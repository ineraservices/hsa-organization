package se.inera.hsa.org.model.version_0_5;

public class County extends Entity {
    //From HSA-Schema 5
    public String description;

    public String hsaIdCounter;
    public String hsaIdPrefix;
}
