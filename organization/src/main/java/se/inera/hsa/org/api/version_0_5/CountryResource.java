package se.inera.hsa.org.api.version_0_5;

import java.time.ZonedDateTime;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Encoding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import se.inera.hsa.log.service.version_0_3.AuditLogger;
import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.model.version_0_5.Country;
import se.inera.hsa.org.model.version_0_5.EntityResponse;
import se.inera.hsa.org.service.version_0_5.EntityProvider;
@Tag(name = "Country")
@Stateless
@Path(ApiVersion.VERSION_0_5)
public class CountryResource {
    private static final Logger log = LogManager.getLogger(CountryResource.class);

    @Context
    UriInfo uriInfo;

    @Inject
    EntityProvider<Country> provider;

    @Inject
    AuditLogger auditLogger;

    @Operation(
            summary = "Get Country",
            description = "Get Country by path (Sverige)."
    )
    @Parameter(name = "countryName", description = "Country name")
    @APIResponses({
            @APIResponse(
                    responseCode = "200",
                    description = "returns CountryResponse object",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = EntityResponse.class),
                            encoding = @Encoding(name = "UTF-8")
                    )
            ),
            @APIResponse(
                    responseCode = "404",
                    description = "object not found"
            )
    }
    )
    @GET
    @Path("countries/{countryName}")
    @Produces(MediaType.APPLICATION_JSON)
    public EntityResponse getCountry(@PathParam("countryName") String countryName) {
        log.info(ApiVersion.VERSION_0_5+" log4j funkar!");
        log.debug(ApiVersion.VERSION_0_5+" log4j funkar!");
        log.error(ApiVersion.VERSION_0_5+" log4j funkar!");
        log.warn(ApiVersion.VERSION_0_5+" log4j funkar!");
        String requestPath = UriBuilder.fromPath("countries").path(countryName).build().getPath();
        auditLogger.log("ADMIN_UUID", ZonedDateTime.now(), AuditLogger.eventTypes.GET_COUNTRY, "countries");
        return provider.getEntityWithPath(requestPath);
    }
}
