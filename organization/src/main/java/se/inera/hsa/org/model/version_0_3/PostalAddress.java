package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author kdaham
 */
public class PostalAddress implements Serializable {

    @Schema(
            required = true,
            minItems = 1
    )
    public List<String> addressLine;

    @Override
    public String toString() {
        return String.join(" ", addressLine);
    }



    public List<String> getAddressLine() {
        return addressLine;
    }



    public void setAddressLine(List<String> addressLine) {
        this.addressLine = addressLine;
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((addressLine == null) ? 0 : addressLine.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PostalAddress other = (PostalAddress) obj;
        if (addressLine == null) {
            if (other.addressLine != null)
                return false;
        } else if (!addressLine.equals(other.addressLine))
            return false;
        return true;
    }



}
