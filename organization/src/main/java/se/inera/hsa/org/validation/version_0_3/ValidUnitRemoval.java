package se.inera.hsa.org.validation.version_0_3;

import se.inera.hsa.org.model.version_0_3.DeletableUnit;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidUnitRemoval implements ConstraintValidator<UnitRemovalValidator, DeletableUnit> {

    @Override
    public void initialize(UnitRemovalValidator constraintAnnotation) {
    }

    @Override
    public boolean isValid(DeletableUnit value, ConstraintValidatorContext context) {
        return !value.hsaHealthCareProvider && !value.hsaHealthCareUnit;
    }
}
