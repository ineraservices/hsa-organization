package se.inera.hsa.org.service.version_0_3;

import se.inera.hsa.org.model.version_0_3.Indexed;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IndexAttributeMapper {
    static public Map<String, String> mapAttributes(Object persistence) {
        Map<String, String> indexFields = new HashMap<>();
        System.out.println("Indexed starting!");
        List<String> unEscapedFields = Arrays.asList("uuid", "creatorUUID", "hsaSyncId");
        for(Field field :persistence.getClass().getDeclaredFields()) {
            indexFields.putAll(mapAttributes(persistence, unEscapedFields, field));
        }
        for(Field field :persistence.getClass().getSuperclass().getDeclaredFields()) {
            indexFields.putAll(mapAttributes(persistence, unEscapedFields, field));
        }
        System.out.println("Indexed done!");
        return indexFields;
    }

    private static Map<String, String> mapAttributes(Object functionPersistence, List<String> unEscapedFields, Field field) {
        Map<String, String> indexFields = new HashMap<>();
        try {
            field.setAccessible(true);
            if (field.isAnnotationPresent(Indexed.class)) {
                String key = field.getName();
                Object value = field.get(functionPersistence);
                if (key != null && value != null) {
                    if (unEscapedFields.contains(key)) {
                        indexFields.put(key, String.join(" ", value.toString()));
                    } else {
                        indexFields.put(key, escapeSpecialCharacters(String.join(" ", value.toString())));
                    }
                    System.out.println("indexing : " + key + ":" + value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return indexFields;
    }

    private static String escapeSpecialCharacters(String inputText) {
        // Redis search, any character of ,.<>{}[]"':;!@#$%^&*()-+=~ will break the text into terms. Thus, we've to escape them here
        return inputText.replaceAll("([-.+@<>{$%}(=;:)])", "\\\\$0");
    }
}