package se.inera.hsa.org.model.version_0_5;

import java.util.List;

public class ModificationList {
    private List<Modification> modifications;

    private String newPath;




    public String getNewPath() {
        return newPath;
    }

    public void setNewPath(String newPath) {
        this.newPath = newPath;
    }

    public List<Modification> getModifications() {
        return modifications;
    }

    public void setModifications(List<Modification> modifications) {
        this.modifications = modifications;
    }


}
