package se.inera.hsa.org.validation.version_0_3;

import se.inera.hsa.org.model.version_0_3.Employee;

import javax.validation.Valid;
import java.io.Serializable;

@ValidModifier_Employee
public class EmployeeModification extends Modification implements Serializable {

    private static final long serialVersionUID = 7634531704622177148L;

    @Valid
    public Employee employee;

}
