package se.inera.hsa.org.mappers;

import org.modelmapper.ModelMapper;
import se.inera.hsa.org.model.unions.Unit;

public class UnitMapper {
    private static ModelMapper modelMapper = new ModelMapper();

    static {
        modelMapper.getConfiguration().setFieldMatchingEnabled(true);
    }

    public static se.inera.hsa.org.model.version_0_3.Unit mapToOldVersion(Unit unit) {
        return modelMapper.map(unit, se.inera.hsa.org.model.version_0_3.Unit.class);
    }

    public static se.inera.hsa.org.model.version_0_4.Unit mapToCurrentVersion(Unit unit) {
        return modelMapper.map(unit, se.inera.hsa.org.model.version_0_4.Unit.class);
    }

    public static Unit mapToUnion(se.inera.hsa.org.model.version_0_3.Unit unit) {
        return modelMapper.map(unit, se.inera.hsa.org.model.unions.Unit.class);
    }
}
