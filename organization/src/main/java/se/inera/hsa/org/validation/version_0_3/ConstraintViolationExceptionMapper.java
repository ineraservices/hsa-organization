package se.inera.hsa.org.validation.version_0_3;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException ex) {
        Map<String, String> errors = new HashMap<>();
        for (ConstraintViolation<?> constraintViolation : ex
                .getConstraintViolations()) {
            errors.put(constraintViolation.getPropertyPath().toString()
                            .substring(constraintViolation.getPropertyPath().toString().lastIndexOf(".")+1),
                    constraintViolation.getMessage());
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(errors)
                .build();
    }
}
