package se.inera.hsa.org.validation.version_0_3;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ModificationValidator_Organization implements ConstraintValidator<ValidModifier_Organization, OrganizationModification> {


    @Override
    public void initialize(ValidModifier_Organization constraintAnnotation) {
    }

    @Override
    public boolean isValid(OrganizationModification modification, ConstraintValidatorContext context) {
        return true;
    }
}
