package se.inera.hsa.org.model.version_0_5;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(
        description = "Depending on what the attribute type of the attribute is only one of the *value attributes is to be used."
)
public class Modification  {
    public static final String ADD = "ADD";
    public static final String DELETE = "DELETE";

    @Schema(
            required = true,
            pattern = "add|delete",
            description = "Must be ADD or DELETE"
    )
    @NotBlank
    @Pattern(regexp = "add|delete", flags = Pattern.Flag.CASE_INSENSITIVE, message = "add or delete only")
    private String modificationType;

    @Schema(
            required = true,
            description = "Name of the attribute"
    )
    private String attributeName;

    @Schema(
            description = "The attribute value. Must match the attribute type in the model. E.g. if attribute type is list the attributeValue must be a list. If attribute type is postal Address the attributeValue must be a postalAddress object, etc."
    )
    private Object attributeValue;


    public Modification() {
        //Empty constructor needs to exist
    }

    public Modification(String modificationType, String attributeName, Object attributeValue) {
        this.modificationType=modificationType;
        this.attributeName=attributeName;
        this.attributeValue=attributeValue;
    }

    public String getModificationType() {
        return modificationType;
    }

    public void setModificationType(String modificationType) {
        this.modificationType = modificationType;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public Object getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(Object attributeValue) {
        this.attributeValue = attributeValue;
    }

    @Override
    public String toString() {
        return "Modification ["+modificationType + " " + attributeName
                + "=" + attributeValue + "]";
    }


}
