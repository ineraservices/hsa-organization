package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

public class Hours {
    @Schema(
            required = true
    )
    public TimeSpan timeSpan;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((timeSpan == null) ? 0 : timeSpan.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Hours other = (Hours) obj;
        if (timeSpan == null) {
            if (other.timeSpan != null)
                return false;
        } else if (!timeSpan.equals(other.timeSpan))
            return false;
        return true;
    }
    
    
}
