package se.inera.hsa.org.service.version_0_3;


import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.UNITS_RELATION;
import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.UNIT_LABEL;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.metrics.annotation.Metered;

import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.PatchException;
import se.inera.hsa.jsonb.JsonbUtil;
import se.inera.hsa.org.model.unions.Unit;
import se.inera.hsa.org.model.unions.UnitPersistence;
import se.inera.hsa.org.model.unions.UnitResponse;
import se.inera.hsa.org.model.version_0_3.DeletableUnit;
import se.inera.hsa.org.validation.version_0_3.PatchModification;


@RequestScoped
public class UnitProvider implements Provider<Unit> {

    @Inject
    @ConfigProperty(name = "codesystem.uri")
    String codeSystemURI;

    @Inject
    private Validator validator;

    @Inject
    HSARedisReadAPI hsaRedisAPI;

    @Inject
    HSARedisWriteAPI hsaRedisWriteAPI;

    private List<String> allowedParentTypes = Arrays.asList("Organization", "Unit");
    private static String COUNTY_CODES_OID = "1.2.752.129.2.2.1.18";
    private static String MUNICIPALITY_CODES_OID = "1.2.752.129.2.2.1.17";


    @Metered
    @Override
    public void persist(@NotNull String parentPath, @NotNull Unit unit) throws BadRequestException {

        UnitPersistence unitPersistence = new UnitPersistence();

        // add metadata
        unitPersistence.creatorUUID = "not_yet_implemented";
        unitPersistence.createTimestamp = ZonedDateTime.now();
        unitPersistence.uuid = UUID.randomUUID().toString();

        // add unit
        unitPersistence.unit = unit;

        // map index attributes
        Map<String, String> indexFields = mapAttributes(unitPersistence);
        Map<String, String> simpleSearchIndexFields = null;
        try {

            simpleSearchIndexFields = mapSimpleSearchAttributes(parentPath, unitPersistence);
        } catch (Exception e) {
            e.printStackTrace();
        }
        hsaRedisWriteAPI.persist(
                parentPath,
                unit.name,
                allowedParentTypes,
                JsonbUtil.jsonb().toJson(unitPersistence),
                UNITS_RELATION,
                UNIT_LABEL,
                indexFields,
                simpleSearchIndexFields);
    }

    @Override
    public String patch(@NotNull String path, @NotNull List<PatchModification> mods) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        String findById = hsaRedisAPI.findById(id);
        UnitPersistence storedUnit = JsonbUtil.jsonb().fromJson(findById, UnitPersistence.class);

        PatchModificationUtil patcher = new PatchModificationUtil();

        try {
            patcher.handlePatchModification(storedUnit.unit, mods, "name", "hsaIdentity");
        } catch (PatchException e) {
            throw new BadRequestException("Error modifying unit: "+e.getMessage(), ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        if( storedUnit.unit.hsaHealthCareProvider  && (storedUnit.unit.orgNo == null || storedUnit.unit.orgNo.trim().isEmpty()) ) {
            throw new BadRequestException("Can't remove orgNo when hsaHealtCareProvider", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }

        storedUnit.modifierUUID = "not_yet_implemented";
        storedUnit.modifyTimestamp = ZonedDateTime.now();
        Map<String, String> indexFields = mapAttributes(storedUnit);
        hsaRedisAPI.merge(JsonbUtil.jsonb().toJson(storedUnit), mapAttributes(storedUnit),
                mapSimpleSearchAttributes(path.replace("/units/" + storedUnit.unit.name, ""), storedUnit));
        return hsaRedisAPI.getGraph().getPathFromId(id);
    }

    @Override
    public String merge(@NotNull String path, @NotNull Unit unit) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        UnitPersistence storedUnit = JsonbUtil.jsonb().fromJson(hsaRedisAPI.findById(id), UnitPersistence.class);
        // name is  not allowed to change through merge
        if (!storedUnit.unit.name.equalsIgnoreCase(unit.name)) {
            throw new BadRequestException("name only modifiable through move", ErrorCodes.NOT_ALLOWED_ON_RDN);
        }
        // hsaIdentity is not allowed to change
        if (!storedUnit.unit.hsaIdentity.equalsIgnoreCase(unit.hsaIdentity)) {
            throw new BadRequestException("hsaIdentity not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        storedUnit.modifierUUID = "not_yet_implemented";
        storedUnit.modifyTimestamp = ZonedDateTime.now();
        storedUnit.unit = unit;
        hsaRedisAPI.merge(JsonbUtil.jsonb().toJson(storedUnit), mapAttributes(storedUnit),
                mapSimpleSearchAttributes(path.replace("/units/" + unit.name, ""), storedUnit));
        return hsaRedisAPI.getGraph().getPathFromId(id);
    }

    @Override
    public String move(String path, String newPath) {
        path = escapeSpecialCharacters(path);
        newPath = escapeSpecialCharacters(newPath);
        String id = hsaRedisAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        assertMoveOk(newPath);
        UnitPersistence unitPersistence = JsonbUtil.jsonb().fromJson(hsaRedisAPI.findById(id), UnitPersistence.class);
        unitPersistence.unit.name = newPath.substring(newPath.lastIndexOf("/") + 1);
        unitPersistence.modifierUUID = UUID.randomUUID().toString();
        unitPersistence.modifyTimestamp = ZonedDateTime.now();
        String parentPath = newPath.substring(0, newPath.lastIndexOf("/units"));
        String newParentId = hsaRedisAPI.getGraph().getIdFromPath(parentPath);
        String oldPath = hsaRedisAPI.getGraph().getPathFromId(id);
        String oldParentId = hsaRedisAPI.getGraph().getIdFromPath(oldPath.substring(0, oldPath.lastIndexOf("/units")));
        Map<String, String> indexFields = new HashMap<>();
        indexFields.put("uuid", unitPersistence.uuid);
        indexFields.put("name", escapeSpecialCharacters(unitPersistence.unit.name));
        indexFields.put("modifierUUID", unitPersistence.modifierUUID);
        indexFields.put("modifyTimestamp", unitPersistence.modifyTimestamp.toString());
        hsaRedisAPI.move(JsonbUtil.jsonb().toJson(unitPersistence), indexFields,
                mapSimpleSearchAttributes(parentPath, unitPersistence),
                newParentId, UNITS_RELATION, oldParentId);
        return hsaRedisAPI.getGraph().getPathFromId(id);
    }

    @Override
    public void remove(@NotNull String requestPath) {
        requestPath = escapeSpecialCharacters(requestPath);
        String id = hsaRedisAPI.getGraph().getIdFromPath(requestPath);
        if (id == null) {
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        }
        // anything additional need to be done when deleting unit?
        // validate deletion
        UnitPersistence unitPersistence = JsonbUtil.jsonb().fromJson(hsaRedisAPI.findById(id), UnitPersistence.class);
        DeletableUnit deletableUnit = JsonbUtil.jsonb().fromJson(JsonbUtil.jsonb().toJson(unitPersistence.unit), DeletableUnit.class);
        final Set<ConstraintViolation<DeletableUnit>> validationResult = validator.validate(deletableUnit);
        if (!validationResult.isEmpty()) {
            throw new BadRequestException(validationResult.iterator().next().getMessage(), ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        hsaRedisWriteAPI.delete(requestPath);
    }

    @Override
    public List<UnitResponse> findImmediateSubordinates(@NotNull String requestPath, @NotNull String parentType,
                                                        @NotNull String relationType) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisAPI.getGraph().getImmediateSubordinatesIdsOfCertainType(id, parentType, relationType).stream()
                .map(hsaRedisAPI::findById)
                .map(i -> JsonbUtil.jsonb().fromJson(i, UnitPersistence.class))
                .map(hsaRedisAPI::toUnitResponse).collect(Collectors.toList());
    }

    @Override
    public UnitResponse find(@NotNull String requestPath) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisAPI.toUnitResponse(JsonbUtil.jsonb().fromJson(hsaRedisAPI.findById(id), UnitPersistence.class));
    }

    private void assertMoveOk(String newPath) {

        // get parentId by allowed types
        String newParentPath = newPath.substring(0, newPath.lastIndexOf("/units"));

        // verify parentType is ok and that there exists no other object with the same path as requested
        if (hsaRedisAPI.getGraph().getType(hsaRedisAPI.getGraph().getIdFromPath(newParentPath), allowedParentTypes) == null) {
            throw new BadRequestException("parent object does not exist or if of wrong type (not one of " +
                    String.join(",", allowedParentTypes) + ")", ErrorCodes.NO_SUCH_OBJECT);
        }
        if (hsaRedisAPI.getGraph().getIdFromPath(newPath) != null) {
            throw new BadRequestException("entry already exists", ErrorCodes.ENTRY_ALREADY_EXISTS);
        }

    }

    private Map<String, String> mapAttributes(UnitPersistence unitPersistence) {
        Map<String, String> mappedIndexes = IndexAttributeMapper.mapAttributes(unitPersistence);
        mappedIndexes.putAll(IndexAttributeMapper.mapAttributes(unitPersistence.unit));
        return mappedIndexes;
    }

    private Map<String, String> mapSimpleSearchAttributes(String parentPath, UnitPersistence unitPersistence) {

        // fields map
        Map<String, String> simpleSearchIndexFields = new HashMap<>();

        // mandatory attributes, space sparated key-words list
        String searchKeyWords = "";
        if (unitPersistence.unit.name != null && !unitPersistence.unit.name.isEmpty()) {
            searchKeyWords += " " + unitPersistence.unit.name;
        }
        if (unitPersistence.unit.localityName != null && !unitPersistence.unit.localityName.isEmpty()) {
            searchKeyWords += " " + unitPersistence.unit.localityName;
        }

        if (unitPersistence.unit.hsaIdentity != null && !unitPersistence.unit.hsaIdentity.isEmpty()) {
            searchKeyWords += " " + unitPersistence.unit.hsaIdentity;
        }

        if (unitPersistence.unit.c != null && !unitPersistence.unit.c.isEmpty()) {
            searchKeyWords += " " + unitPersistence.unit.c;
        }
        if (unitPersistence.unit.c != null && !unitPersistence.unit.c.isEmpty()) {
            searchKeyWords += " " + unitPersistence.unit.c;
        }

        if (parentPath != null && !parentPath.isEmpty()) {
            searchKeyWords += " " + parentPath.replace("/", " ")
                    .replace("countries", "")
                    .replace("counties", "")
                    .replace("organizations", "")
                    .replace("units", "")
                    .replace("functions", "")
                    .replace("employees", "");
        }
        if (unitPersistence.unit.postalAddress != null &&
                unitPersistence.unit.postalAddress.addressLine != null &&
                !unitPersistence.unit.postalAddress.addressLine.isEmpty()) {
            searchKeyWords += " " + String.join(" ", unitPersistence.unit.postalAddress.addressLine);
        }

        if (unitPersistence.unit.countyCode != null && !unitPersistence.unit.countyCode.isEmpty()) {

            WebTarget codeSystemTarget_CountyCode = ClientBuilder.newClient().target(codeSystemURI)
                    .path(COUNTY_CODES_OID).path("codes").path(unitPersistence.unit.countyCode);

            Builder request = codeSystemTarget_CountyCode.request();

            request.accept(MediaType.APPLICATION_JSON_TYPE);

            Response response = request.buildGet().invoke();

            JsonObject countyCodeJsonObject;
            if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
                response.bufferEntity();
                countyCodeJsonObject = response.readEntity(JsonObject.class);
                String countyName = countyCodeJsonObject.get("name").toString();
                searchKeyWords += " " + countyName;
            }

        }

        if (unitPersistence.unit.municipalityCode != null && !unitPersistence.unit.municipalityCode.isEmpty()) {

            WebTarget codeSystemTarget_MunicipalityCode = ClientBuilder.newClient().target(codeSystemURI).path(MUNICIPALITY_CODES_OID)
                    .path("codes").path(unitPersistence.unit.municipalityCode);

            Builder request = codeSystemTarget_MunicipalityCode.request();

            request.accept(MediaType.APPLICATION_JSON_TYPE);

            Response response = request.buildGet().invoke();

            JsonObject countyCodeJsonObject;
            if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
                response.bufferEntity();
                countyCodeJsonObject = response.readEntity(JsonObject.class);
                String municipalityName = countyCodeJsonObject.get("name").toString();
                searchKeyWords += " " + municipalityName;
            }
        }
        simpleSearchIndexFields.put("words", escapeSpecialCharacters(searchKeyWords));
        simpleSearchIndexFields.put("type", unitPersistence.unit.type);

        return simpleSearchIndexFields;
    }

    public String escapeSpecialCharacters(String inputText) {
        // Redis search, any character of ,.<>{}[]"':;!@#$%^&*()-+=~ will break the text into terms. Thus, we've to escape them here
        return inputText.replaceAll("([-.+@<>{$%}(=;:)])", "\\\\$0");
    }
}
