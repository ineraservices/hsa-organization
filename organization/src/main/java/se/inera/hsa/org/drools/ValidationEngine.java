package se.inera.hsa.org.drools;

import static java.util.stream.Collectors.toList;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.logging.log4j.Level;
import org.kie.api.runtime.KieSession;

import se.inera.hsa.clients.CodeSystemClient;
import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.Error;
import se.inera.hsa.log.service.version_0_3.AuditLogger;
import se.inera.hsa.log.service.version_0_3.AuditLogger.eventTypes;
import se.inera.hsa.log.service.version_0_3.SoftValidationLogger;
import se.inera.hsa.org.drools.log.HSADebugAgendaEventListener;
import se.inera.hsa.org.drools.log.HSADebugRuleRuntimeEventListener;

/**
 * This class is reponsible for validating the object T against both active and older Rules.
 */
//TODO Maybe refactor to ValidationEngine<T, Version> so that no need of having Version as parameter to validate methods.?
public class ValidationEngine<T> {


    /**
     * Will contain validation result when using the current-rules.
     */
    private DroolsValidationResult currentRulesValidationResult;

    /**
     * Will contain validation result when using the former-rules.
     */
    private DroolsValidationResult olderRulesValidationResult;

    /**
     * CodeSystemClient is used in drools rule files to validate codes which are in CodeSystems.
     */
    private CodeSystemClient codeSystemClient;

    @Inject
    SoftValidationLogger softValidationLogger;

    /**
     * This method is used when validating "complete objects", for example in ADD-operations.
     */
    public T validate(String version, T object, AuditLogger.eventTypes eventType, String path) {
        return validate(version, object, eventType, path, Collections.emptyList());
    }

    /**
     * This method is used when validating an object. if the given list of attributes are empty then all attributes will be processed.
     * otherwise only the given attributes are validated, like for example in PATCH-operations.
     */
    public T validate(String version, T object, AuditLogger.eventTypes eventType, String path, List<String> attributes) {
        KieSession kieSession1 = DroolsConfiguration.getKieSessionCurrent(version);
        KieSession kieSession2 = DroolsConfiguration.getKieSessionFormer(version);
        // add object(s) to be validated to session
        kieSession1.insert(object);
        kieSession2.insert(object);

        // add result variable
        currentRulesValidationResult = new DroolsValidationResult("HSA Schema 5.0 rev. B", object.getClass().getName());
        olderRulesValidationResult = new DroolsValidationResult("Earlier rule set", object.getClass().getName());

        kieSession1.setGlobal("droolsValidationResult", currentRulesValidationResult);
        kieSession2.setGlobal("droolsValidationResult", olderRulesValidationResult);

        // debug listeners for logging
        kieSession1.addEventListener( new HSADebugAgendaEventListener() );
        kieSession1.addEventListener( new HSADebugRuleRuntimeEventListener() );
        kieSession2.addEventListener( new HSADebugAgendaEventListener() );
        kieSession2.addEventListener( new HSADebugRuleRuntimeEventListener() );

        // add services
        kieSession1.setGlobal("codeSystemClient", codeSystemClient);
        kieSession2.setGlobal("codeSystemClient", codeSystemClient);

        System.out.println("VALIDATE NOW: "+object.getClass());
        // fire rules
        fireAllRules(kieSession1);
        fireAllRules(kieSession2);
        System.out.println("EVALUATE RESULTS");
        // evalute results from rules and change object to be correct
        T result = evaluateRuleResult(object, eventType, path, attributes);
        System.out.println("POST VALIDATION" + result.getClass());
        kieSession1.dispose();
        kieSession2.dispose();

        return result;
    }

    /**
     * Method is responsible to evaluate the result. This method is described in more detail in confluence.
     * At the time of writing comment: https://inera.atlassian.net/wiki/spaces/HSANG/pages/13075617/Validering
     */
    private T evaluateRuleResult(T object, eventTypes eventType, String path, List<String> attributes) {
        List<String> attributesToProcess = getFieldNames(object);
        if ( attributes != null && !attributes.isEmpty()) {
            attributesToProcess = attributes;
        }

        System.out.println(currentRulesValidationResult.getAllErrors());
        System.out.println(olderRulesValidationResult.getAllErrors());
        boolean errorHasOccurred = false;
        // Process all attributes before throwing exception so that a complete log is created showin all errors and warnings.
        for (String attributeName : attributesToProcess) {
            List<ValidationError> errors = currentRulesValidationResult.getErrors(attributeName);
            List<ValidationError> errorsDeprecated = olderRulesValidationResult.getErrors(attributeName);

            List<ValidationWarning> warnings = currentRulesValidationResult.getWarnings(attributeName);
            List<ValidationWarning> warningsDeprecated = olderRulesValidationResult.getWarnings(attributeName);
            List<ValidationInformation> infoDeprecated = currentRulesValidationResult.getInformations(attributeName);

            //fel i antingen nya "schemat" och i gamla" schemat ELLER fel i nya schemat och finns inte info gamla schemat
            if ( (!errors.isEmpty() && !errorsDeprecated.isEmpty()) || ( !errors.isEmpty() && infoDeprecated.isEmpty()) ) {
                softValidationLogger.log(Level.ERROR, "EMP-1000000001", eventType, path, Map.of(attributeName, getErrorString(errors, errorsDeprecated)));
                errorHasOccurred=true;
            }

            // rensa attribut ifall varningar i båda gamla o nya schemat eller ifall varningar i nya schemat och inget info om attributet i gamla
            if ( (!warnings.isEmpty() && !warningsDeprecated.isEmpty()) || (!warnings.isEmpty() && infoDeprecated.isEmpty()) ) {
                clearProperty(attributeName, object);
                softValidationLogger.log(Level.WARN, "EMP-1000000001", eventType, path, Map.of(attributeName, getWarningString(warnings, warningsDeprecated)));
            }

            //TODO LOG other warnings?
        }

        if ( errorHasOccurred) {
            throw new BadRequestException("Validation error", ErrorCodes.OBJECT_CLASS_VIOLATION, generateErrors(currentRulesValidationResult, olderRulesValidationResult));
        }

        return object;
    }

    /**
     * Generate error array which is used in Json report.
     */
    private Error[] generateErrors(DroolsValidationResult currentRules,DroolsValidationResult olderRules) {
        List<Error> errors = new ArrayList<>();

        currentRules.getAllErrors().forEach(e -> errors.add(new Error(e.getErrorCode().toString(), currentRules.getRulesetName() + " -  " + e.getAttributeName(), e.getErrorMessage())));

        olderRules.getAllErrors().forEach(e -> errors.add(new Error(e.getErrorCode().toString(), olderRules.getRulesetName() + " - " + e.getAttributeName(), e.getErrorMessage())));
        return errors.toArray(new Error[errors.size()]);
    }

    /**
     * Generate warning log message
     */
    private String getWarningString(List<ValidationWarning> warnings, List<ValidationWarning> warningsDeprecated) {
        StringBuilder sb = new StringBuilder();
        String warningStringCurrent = warnings.stream().map( ValidationWarning::getWarningMessage).collect(Collectors.joining(","));
        String warningStringOld = warningsDeprecated.stream().map( ValidationWarning::getWarningMessage).collect(Collectors.joining(","));
        if ( warningStringCurrent != null && !warningStringCurrent.isBlank()) {
            sb.append(" Warnings '").append(currentRulesValidationResult.getRulesetName()).append("' ").append(warningStringCurrent).append("\n");
        }
        if ( warningStringOld != null && !warningStringOld.isBlank()) {
            sb.append(" Warnings '").append(olderRulesValidationResult.getRulesetName()).append("' ").append(warningStringOld).append("\n");
        }

        return sb.toString().trim();
    }

    /**
     * Generate error log message
     */
    private String getErrorString(List<ValidationError> errors, List<ValidationError> errorsDeprecated) {
        StringBuilder sb = new StringBuilder();
        String errorStringCurrent = errors.stream().map( ValidationError::getErrorMessage).collect(Collectors.joining(","));
        String errorStringOld = errorsDeprecated.stream().map( ValidationError::getErrorMessage).collect(Collectors.joining(","));
        if ( errorStringCurrent != null && !errorStringCurrent.isBlank()) {
            sb.append(" Errors '").append(currentRulesValidationResult.getRulesetName()).append("' ").append(errorStringCurrent).append("\n");
        }
        if ( errorStringOld != null && !errorStringOld.isBlank()) {
            sb.append(" Errors '").append(olderRulesValidationResult.getRulesetName()).append("' ").append(errorStringOld).append("\n");
        }

        return sb.toString().trim();
    }

    /**
     * run the rules and return result
     */
    private void fireAllRules(KieSession session) {
        session.fireAllRules();
        session.dispose();
    }


    /**
     * Get all modifiable attributes for object.
     */
    private List<String> getFieldNames(Object obj) {
        return Arrays.stream(obj.getClass().getFields())
            .filter( f -> (f.getModifiers()& Modifier.FINAL) != Modifier.FINAL )
            .map(Field::getName)
            .collect(toList());
    }

    /**
     * Clear the specified attribute in object
     */
    private void clearProperty(String property, Object object) {
        try {
            Field field = object.getClass().getField(property);
            field.setAccessible(true);
            if ( field.getType().equals(Boolean.TYPE)) {
                field.set(object, false);
            } else {
                field.set(object, null);
            }
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
