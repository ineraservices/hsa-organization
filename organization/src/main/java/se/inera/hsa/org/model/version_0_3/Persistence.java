package se.inera.hsa.org.model.version_0_3;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;

public class Persistence implements Serializable {

    private static final long serialVersionUID = -3945674397610691761L;

    @Indexed
    @NotBlank
    public String uuid;

    @Indexed
    @NotBlank
    public String creatorUUID;

    @Indexed
    @NotNull
    public ZonedDateTime createTimestamp;

    @Indexed
    public String modifierUUID;
    @Indexed
    public ZonedDateTime modifyTimestamp;

}
