package se.inera.hsa.org.data.version_0_5.model;

/**
 * NOT YET IN USE
 * This class is a representation of a node rather than the full database entry. This can be used to populate a list
 * or a tree for browsing, instead of returning the complete information for each object from the database this only
 * returns the essentials for displaying the entry and calling the GET service for that object if needed.
 */
public class NodeResult {

    private String uuid;
    private NodeType node_type;
    private String node_name;
    private String numberOfSubordinates;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public NodeType getNode_type() {
        return node_type;
    }

    public void setNode_type(NodeType node_type) {
        this.node_type = node_type;
    }

    public String getNode_name() {
        return node_name;
    }

    public void setNode_name(String node_name) {
        this.node_name = node_name;
    }

    public String getNumberOfSubordinates() {
        return numberOfSubordinates;
    }

    public void setNumberOfSubordinates(String numberOfSubordinates) {
        this.numberOfSubordinates = numberOfSubordinates;
    }
}
