package se.inera.hsa.org.api;

import static se.inera.hsa.org.drools.DroolsConfiguration.CURRENT_RULES;
import static se.inera.hsa.org.drools.DroolsConfiguration.FORMER_RULES;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.servlet.ServletContext;

import se.inera.hsa.org.drools.DroolsConfiguration;

@ApplicationScoped
public class StartupListener {

    public void init(@Observes
            @Initialized(ApplicationScoped.class) ServletContext context) {
        // Perform action during application's startup
//        DroolsConfiguration.initDroolsConfiguration(CURRENT_RULES, FORMER_RULES);
        DroolsConfiguration.initDroolsConfiguration(ApiVersion.VERSION, ApiVersion.VERSION_0_5);
    }

    public void destroy(@Observes
            @Destroyed(ApplicationScoped.class) ServletContext context) {
        // Perform action during application's shutdown
    }
}
