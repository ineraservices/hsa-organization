package se.inera.hsa.org.service.version_0_3;

import io.lettuce.core.dynamic.RedisCommandFactory;
import se.inera.hsa.lettuce.commands.RedisGraphCommands;
import se.inera.hsa.lettuce.redisgraph.model.GraphQueryResult;

import javax.ws.rs.InternalServerErrorException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.lang.String.format;
import static java.util.Arrays.asList;

public class OrganizationGraph implements GraphOperations {
    private final String graphRootId;
    private final String graphName;

    private static final String
            ROOT_PATH = "countries/Sverige",
            ID_KEY = "id",
            COUNTRY_LABEL = "Country",
            COUNTRIES_PATH_LABEL = "Countries",
            COUNTY_LABEL = "County",
            COUNTIES_PATH_LABEL = "Counties",
            FUNCTION_LABEL = "Function",
            FUNCTIONS_PATH_LABEL = "Functions",
            ORGANIZATION_LABEL = "Organization",
            ORGANIZATIONS_PATH_LABEL = "Organizations",
            UNIT_LABEL = "Unit",
            UNITS_PATH_LABEL = "Units",
            COUNTIES_RELATION = "HAS_COUNTIES",
            ORGANIZATIONS_RELATION = "HAS_ORGANIZATIONS",
            UNITS_RELATION = "HAS_UNITS",
            FUNCTIONS_RELATION = "HAS_FUNCTIONS",
            EMPLOYEE_PATH_LABEL = "Employees",
            EMPLOYEE_LABEL = "Employee",
            EMPLOYEE_RELATION = "HAS_EMPLOYEES";

    private RedisGraphCommands regraph;

    public OrganizationGraph(RedisCommandFactory redisCommandFactory, String graphRootId, String graphName) {
        this.regraph = redisCommandFactory.getCommands(RedisGraphCommands.class);
        this.graphRootId = graphRootId;
        this.graphName = graphName;
    }

    public int getNumAllSubordinates(String id) {
        return getAllSubordinatesIds(getImmediateSubordinatesIds(id), new ArrayList<>()).size();
    }

    List<String> getAllSubordinatesIds(List<String> inIds, List<String> resIds) {

        for (String id : inIds) {
            resIds.add(id);
            getAllSubordinatesIds(getImmediateSubordinatesIds(id), resIds);
        }
        return resIds;
    }
    
    public List<String> getImmediateSubordinatesIds(String id) {

        String nodeQuery = String.format("MATCH (node) where node.id='%s' return node.type, node.rel", id);

        GraphQueryResult nodeQueryResult = new GraphQueryResult(regraph.graphQuery(graphName, nodeQuery));

        String type = nodeQueryResult.getUniqueRecord().getValue("node.type");
        
        String immediateSubordinatesQuery = String.format("MATCH (parent:%s{id:'%s'})-[]->(child) RETURN child.id", type, id);

        GraphQueryResult subordinatesResult = new GraphQueryResult(regraph.graphQuery(graphName, immediateSubordinatesQuery));

        List<String> ids = new ArrayList<>();

        subordinatesResult.getRecords().forEach(i -> ids.add(i.getValue("child.id")));

        return ids;
    }

    public int getNumImmediateSubordinates(String id) {
        return getImmediateSubordinatesIds(id).size();
    }

    public void delete(String id) throws InternalServerErrorException {
        List<Object> graphDeleteResult = regraph.graphQuery(graphName, format("MATCH(node) WHERE node.id='%s' DELETE node", id));
        if (graphDeleteResult != null && !graphDeleteResult.isEmpty()) {
            throw new InternalServerErrorException("failed to remove graph node for key " + id);
        }
    }

    public void updateNode(String id, String name, String relation, String newParentId, String oldParentId) {
        String updateNodeQuery = String.format("MATCH (node {id:'%s'}) SET node.name = '%s', node.pid = '%s'", id, name, newParentId);
        regraph.graphQuery(graphName, updateNodeQuery);
        String deleteRelationQuery = String.format("MATCH (parent {id:'%s'})-[relation:%s]->(child {id:'%s'}) DELETE relation", oldParentId, relation, id);
        regraph.graphQuery(graphName, deleteRelationQuery);
        String addRelationQuery = String.format("MATCH (parent {id:'%s'}), (child {id:'%s'}) CREATE (parent)-[:%s]->(child)", newParentId, id, relation);
        regraph.graphQuery(graphName, addRelationQuery);
    }

    // adding a relation implies having previously asserted that any possible parent object exist
    public void addRelation(String parentId,
                             String parentType,
                             String relationType,
                             String childType,
                             String childId,
                             String childName) {

        String addQuery = String.format("MATCH (a:%s {id:'%s'}) " +
                        "CREATE (a)-[:%s]->(:%s {" +
                        "id:'%s'," +
                        "name:'%s'," +
                        "type:'%s'," +
                        "pid:'%s'," +
                        "ptype:'%s'," +
                        "rel:'%s'})",
                parentType,
                parentId,
                relationType,
                childType,
                childId,
                childName,
                childType,
                parentId,
                parentType,
                relationType);

        regraph.graphQuery(graphName, addQuery);
    }

    public boolean nodeHasSubordinates(String id) {

        String nodeQuery = String.format("MATCH (node) WHERE  node.id='%s' RETURN node.rel", id);

        GraphQueryResult nodeQueryResult = new GraphQueryResult(regraph.graphQuery(graphName, nodeQuery));

        String relationType = id.equals(graphRootId) ? COUNTIES_RELATION :
                nodeQueryResult.getUniqueRecord().getValue("node.rel").equals(COUNTIES_RELATION) ?
                        ORGANIZATIONS_RELATION : UNITS_RELATION;

        String hasLeavesQuery = String.format("MATCH (parent)-[:%s]->(child) WHERE parent.id='%s' RETURN child.id", relationType, id);

        GraphQueryResult leavesQueryResult = new GraphQueryResult(regraph.graphQuery(graphName, hasLeavesQuery));

        return (leavesQueryResult.getRecords() != null && leavesQueryResult.getRecords().size() > 0);
    }

    private String getPathFromId(String id, String type, String relation, String output) {
        if (id.trim().equals(graphRootId)) {
            String relativePath = output.replaceFirst("/", "");
            return relativePath.isEmpty() ? ROOT_PATH : String.format("%s/" + relativePath, ROOT_PATH);
        }

        String query = String.format("MATCH (parent)-[:%s]->(child:%s {%s:'%s'}) " +
                "RETURN parent.id, parent.type, child.type, child.name, parent.rel", relation, type, ID_KEY, id);

        GraphQueryResult result = new GraphQueryResult(regraph.graphQuery(graphName, query));

        String parentId = result.getUniqueRecord().getValue("parent.id");
        String parentNodeType = result.getUniqueRecord().getValue("parent.type");
        String nodeType = result.getUniqueRecord().getValue("child.type");
        String childName = "/";

        switch (nodeType) {
            case COUNTRY_LABEL:
                childName += COUNTRIES_PATH_LABEL.toLowerCase() + "/" + result.getUniqueRecord().getValue("child.name");
                break;
            case COUNTY_LABEL:
                childName += COUNTIES_PATH_LABEL.toLowerCase() + "/" + result.getUniqueRecord().getValue("child.name");
                break;
            case ORGANIZATION_LABEL:
                childName += ORGANIZATIONS_PATH_LABEL.toLowerCase() + "/" + result.getUniqueRecord().getValue("child.name");
                break;
            case FUNCTION_LABEL:
                childName += FUNCTIONS_PATH_LABEL.toLowerCase() + "/" + result.getUniqueRecord().getValue("child.name");
                break;
            case EMPLOYEE_LABEL:
                childName += EMPLOYEE_PATH_LABEL.toLowerCase() + "/" + result.getUniqueRecord().getValue("child.name");
                break;
            default:
                childName += UNITS_PATH_LABEL.toLowerCase() + "/" + result.getUniqueRecord().getValue("child.name");
                break;
        }

        String upwardsRelation = result.getUniqueRecord().getValue("parent.rel");
        return getPathFromId(parentId, parentNodeType, upwardsRelation, childName + output);
    }

    public String getPathFromId(String id) {
        String query = String.format("MATCH (node) WHERE node.id='%s' RETURN node.type, node.rel", id);
        GraphQueryResult result = new GraphQueryResult(regraph.graphQuery(graphName, query));
        String type = result.getUniqueRecord().getValue("node.type");
        String upwardsRelation = result.getUniqueRecord().getValue("node.rel");

        return getPathFromId(id, type, upwardsRelation, "");
    }

    public List<String> getImmediateSubordinatesIdsOfCertainType(String id, String parentType, String relationType) {

        String subordinatesType;

        switch (relationType) {
            case COUNTIES_RELATION:
                subordinatesType = COUNTY_LABEL;
                break;
            case ORGANIZATIONS_RELATION:
                subordinatesType = ORGANIZATION_LABEL;
                break;
            case UNITS_RELATION:
                subordinatesType = UNIT_LABEL;
                break;
            case EMPLOYEE_RELATION:
                subordinatesType = EMPLOYEE_LABEL;
                break;
            default:
                subordinatesType = FUNCTION_LABEL;
        }

        String immediateSubordinatesQuery = String.format("MATCH (parent:%s{id:'%s'})-[:%s]->(child:%s) RETURN child.id",
                parentType, id, relationType, subordinatesType);

        GraphQueryResult subordinatesResult = new GraphQueryResult(regraph.graphQuery(graphName, immediateSubordinatesQuery));

        List<String> ids = new ArrayList<>();

        if (!subordinatesResult.getRecords().isEmpty()) {
            subordinatesResult.getRecords().forEach(i -> ids.add(i.getValue("child.id")));
        }
        return ids;
    }

    public String getType(String id, List<String> parentTypes) {

        String initialQueryPart = String.format("MATCH (node) WHERE node.id='%s' AND (", id);

        StringBuilder dataQuery = new StringBuilder(initialQueryPart);

        Iterator<String> parentTypesIterator = parentTypes.iterator();

        while (parentTypesIterator.hasNext()) {
            String parentType = parentTypesIterator.next();
            dataQuery.append("node.type='").append(parentType).append("'");
            if (parentTypesIterator.hasNext())
                dataQuery.append(" OR ");
        }

        dataQuery.append(") RETURN node.type");

        GraphQueryResult parentData = new GraphQueryResult(regraph.graphQuery(graphName, dataQuery.toString()));

        if (parentData.getRecords().isEmpty())
            return null;

        return parentData.getUniqueRecord().getValue("node.type");
    }

    public String getIdFromPath(String path) {

        List<String> pathList = asList(path.split("/"));

        if (pathList.size() < 2) return null;

        //Create query backwards cause then you know what kind of relation you are supposed to have between the nodes
        StringBuilder queryBuilder = new StringBuilder(String.format(" RETURN a%s.id", pathList.size() / 2));
        int counter = pathList.size() / 2;
        for (int i = pathList.size() - 1; i >= 0; i -= 2) {
            String name = pathList.get(i);
            String type = pathList.get(i - 1);
            int indexOf = type.indexOf('-');
            type = type.substring(indexOf + 1);
            type = Character.toUpperCase(type.charAt(0)) + type.substring(1);
            String relation;
            switch (type) {
                case COUNTRIES_PATH_LABEL:
                    type = COUNTRY_LABEL;
                    relation = "";
                    break;
                case COUNTIES_PATH_LABEL:
                    relation = COUNTIES_RELATION;
                    type = COUNTY_LABEL;
                    break;
                case ORGANIZATIONS_PATH_LABEL:
                    relation = ORGANIZATIONS_RELATION;
                    type = ORGANIZATION_LABEL;
                    break;
                case UNITS_PATH_LABEL:
                    relation = UNITS_RELATION;
                    type = UNIT_LABEL;
                    break;
                case FUNCTIONS_PATH_LABEL:
                    relation = FUNCTIONS_RELATION;
                    type = FUNCTION_LABEL;
                    break;
                case EMPLOYEE_PATH_LABEL:
                    relation = EMPLOYEE_RELATION;
                    type = EMPLOYEE_LABEL;
                    break;
                default:
                    relation = null;
            }

            queryBuilder.insert(0, String.format("-[:%s]->(a%s:%s {name:'%s'})", relation, counter--, type, name));
        }

        //Remove relation above country node
        queryBuilder.delete(0, queryBuilder.indexOf("->") + 2);
        queryBuilder.insert(0, "MATCH ");

        String id = null;
        GraphQueryResult result;
        try {
            result = new GraphQueryResult(regraph.graphQuery(graphName, queryBuilder.toString()));
            if (result.getRecords() != null && !result.getRecords().isEmpty()) {
                id = result.getUniqueRecord().getValue(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }
    
    public String getIdFromPathIgnoreTypes(String path) {
        String query = getQueryFromPathIgnoreTypes(path);
        if ( query == null ) {
            return null;
        }
        String id = null;
        GraphQueryResult result;
        try {
            result = new GraphQueryResult(regraph.graphQuery(graphName, query));
            if (result.getRecords() != null && !result.getRecords().isEmpty()) {
                id = result.getUniqueRecord().getValue(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return id;
    }
    
    private String getQueryFromPathIgnoreTypes(String path) {
        List<String> pathList = asList(path.split("/"));
        if ( pathList.size() < 2 ) return null;
        
        //Create query backwards cause then you know what kind of relation you are supposed to have between the nodes
        StringBuilder queryBuilder = new StringBuilder("MATCH ");
        String label = "obj";
        int counter=0;
        for (int i = 1; i < pathList.size(); i+=2) {
            String string = pathList.get(i);
            queryBuilder.append("(").append(label).append(counter++).append(" {name:'").append(string).append("'})").append("-[]->");
        }
        queryBuilder.delete(queryBuilder.lastIndexOf("-[]->"), queryBuilder.length());
        queryBuilder.append(" return ").append(label).append(--counter).append(".id");
        return queryBuilder.toString();
    }
}
