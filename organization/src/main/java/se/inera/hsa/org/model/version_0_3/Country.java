package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

public class Country implements Serializable {

    public static final long serialVersionUID = 617713482739655818L;

    @NotBlank
    @Pattern (regexp = "[A-Z]{2}")
    @Schema(
            required = true
    )
    public String isoCode;

    @NotBlank
    @Schema(
            required = true
    )
    public String name;

    @NotBlank
    @Pattern (regexp = "Country")
    @Schema(
            required = true,
            pattern = "Country"
    )
    public String type;

}

