package se.inera.hsa.org.validation.version_0_3;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ModificationValidator_County implements ConstraintValidator<ValidModifier_County, CountyModification> {

    @Override
    public void initialize(ValidModifier_County constraintAnnotation) {
    }

    @Override
    public boolean isValid(CountyModification modification, ConstraintValidatorContext context) {
        return true;
    }
}
