package se.inera.hsa.org.model.version_0_3;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author kdaham
 */
@Schema(
        description = "TimeSpan holds repeating event information,<br/>e.g. weekly opening hours or telephone hours.<br/>" +
                "An example:<br/>" +
                "1-5#08:00#10:00 or 1-5#08:00#10:00#(optional comment)#(optional start date)#(optional end date)<br/>" +
                "with 1 meaning monday and 5 meaning friday corresponds to a TimeSpan with:<br/>" +
                "fromDay=1,<br/>" +
                "toDay=5,<br/>" +
                "fromTime2=08:00,<br/>" +
                "toTime2=10:00,<br/>" +
                "comment=(optional comment),<br/>" +
                "fromDate=(optional start date),<br/>" +
                "toDate=(optional end date)"
)
public class TimeSpan implements Serializable {

    @Schema(
            required = true,
            pattern = "[1-7]{1}"
    )
    public String fromDay;

    @Schema(
            required = true,
            pattern = "[1-7]{1}"
    )
    public String toDay;

    @Schema(
            required = true,
            pattern = "HH:MM"
    )
    public String fromTime2;

    @Schema(
            required = true,
            pattern = "HH:MM"
    )
    public String toTime2;


    public String comment;

    @Schema(
            pattern = "yyyy-mm-dd"
    )
    public String fromDate;

    @Schema(
            pattern = "yyyy-mm-dd"
    )
    public String toDate;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
        result = prime * result + ((fromDay == null) ? 0 : fromDay.hashCode());
        result = prime * result + ((fromTime2 == null) ? 0 : fromTime2.hashCode());
        result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
        result = prime * result + ((toDay == null) ? 0 : toDay.hashCode());
        result = prime * result + ((toTime2 == null) ? 0 : toTime2.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TimeSpan other = (TimeSpan) obj;
        if (comment == null) {
            if (other.comment != null)
                return false;
        } else if (!comment.equals(other.comment))
            return false;
        if (fromDate == null) {
            if (other.fromDate != null)
                return false;
        } else if (!fromDate.equals(other.fromDate))
            return false;
        if (fromDay == null) {
            if (other.fromDay != null)
                return false;
        } else if (!fromDay.equals(other.fromDay))
            return false;
        if (fromTime2 == null) {
            if (other.fromTime2 != null)
                return false;
        } else if (!fromTime2.equals(other.fromTime2))
            return false;
        if (toDate == null) {
            if (other.toDate != null)
                return false;
        } else if (!toDate.equals(other.toDate))
            return false;
        if (toDay == null) {
            if (other.toDay != null)
                return false;
        } else if (!toDay.equals(other.toDay))
            return false;
        if (toTime2 == null) {
            if (other.toTime2 != null)
                return false;
        } else if (!toTime2.equals(other.toTime2))
            return false;
        return true;
    }
}
