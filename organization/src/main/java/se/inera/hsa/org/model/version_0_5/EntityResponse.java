package se.inera.hsa.org.model.version_0_5;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import se.inera.hsa.org.model.unions.MetaData;

/**
 * Wrapper class for the response message. Includes the domain object T (a County, or Unit etc), the uuid and a
 * MetaData object. This makes sure we can reuse the Entity class for both incoming and outgoing data transfers.
 * @param <T>
 */
public class EntityResponse<T extends Entity> {

    @Schema(
            required = true,
            readOnly = true
    )
    private String uuid;
    @Schema(

            required = true,
            readOnly = true
    )
    private MetaData meta;
    private T data;

    public EntityResponse(EntityPersistence<T> entityPersistence, MetaData meta, String uuid){
        this.data=entityPersistence.data;
        this.meta=meta;
        this.uuid=uuid;
    }

    public EntityResponse() {
        //Empty constructor need to exist
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public MetaData getMeta() {
        return meta;
    }

    public void setMeta(MetaData meta) {
        this.meta = meta;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
