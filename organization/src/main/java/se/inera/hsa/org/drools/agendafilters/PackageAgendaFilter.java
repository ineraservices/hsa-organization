package se.inera.hsa.org.drools.agendafilters;

import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.Match;

public class PackageAgendaFilter implements AgendaFilter {

    private final String packageName;

    public PackageAgendaFilter(String packageName) {

        this.packageName = packageName;
    }

    @Override
    public boolean accept(Match match) {
        return match.getRule().getPackageName().equals(packageName);
    }
}
