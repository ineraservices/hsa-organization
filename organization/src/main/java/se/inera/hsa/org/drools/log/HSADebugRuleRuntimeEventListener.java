package se.inera.hsa.org.drools.log;

import org.kie.api.event.rule.*;

public class HSADebugRuleRuntimeEventListener
            implements RuleRuntimeEventListener {

    public HSADebugRuleRuntimeEventListener() {
        // intentionally left blank
    }

    public void objectInserted(ObjectInsertedEvent event) {
        log( event.toString() );
    }

    public void objectUpdated(ObjectUpdatedEvent event) {
        log( event.toString() );
    }

    public void objectDeleted(ObjectDeletedEvent event) {
        log( event.toString() );
    }

    private void log(String message) {
        System.out.println(message);
    }
}
