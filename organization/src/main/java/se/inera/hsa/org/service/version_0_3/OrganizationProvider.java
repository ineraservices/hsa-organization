package se.inera.hsa.org.service.version_0_3;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.ForbiddenException;
import se.inera.hsa.exception.PatchException;
import se.inera.hsa.org.model.version_0_3.Organization;
import se.inera.hsa.org.model.version_0_3.OrganizationPersistence;
import se.inera.hsa.org.model.version_0_3.OrganizationResponse;
import se.inera.hsa.org.validation.version_0_3.PatchModification;
import se.inera.hsa.jsonb.JsonbUtil;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequestScoped
public class OrganizationProvider implements Provider<Organization> {

    @Inject
    @ConfigProperty(name = "codesystem.uri")
    String codeSystemURI;

    @Inject
    HSARedisReadAPI hsaRedisAPI;

    private static String COUNTY_CODES_OID = "1.2.752.129.2.2.1.18";
    private static String MUNICIPALITY_CODES_OID = "1.2.752.129.2.2.1.17";

    @Override
    public OrganizationResponse find(@NotNull String requestPath) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisAPI.toOrganizationResponse(JsonbUtil.jsonb().fromJson(hsaRedisAPI.findById(id), OrganizationPersistence.class));
    }

    @Override
    public void persist(@NotNull String path, @NotNull Organization entity) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public String merge(@NotNull String path, @NotNull Organization organization) {

        String id = hsaRedisAPI.getGraph().getIdFromPath(path);

        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        OrganizationPersistence storedOrganization = JsonbUtil.jsonb().fromJson(hsaRedisAPI.findById(id), OrganizationPersistence.class);
        // name is  not allowed to change through merge
        if (!storedOrganization.organization.name.equalsIgnoreCase(organization.name)) {
            throw new BadRequestException("name only modifiable through move", ErrorCodes.NOT_ALLOWED_ON_RDN);
        }
        // hsaIdentity is not allowed to change
        if (!storedOrganization.organization.hsaIdentity.equalsIgnoreCase(organization.hsaIdentity)) {
            throw new BadRequestException("hsaIdentity not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        
        //TODO change this later to be user specific this is for ldap-sync-users
        // hsaHpt is not allowed to change // mandatory attribute
        if (!hsaRedisAPI.equalsIgnoreCase(storedOrganization.organization.hsaHpt, organization.hsaHpt)) {
            throw new BadRequestException("hsaHpt not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        
        // hsaIdPrefix is not allowed to change //optionalAttribute
        if (!hsaRedisAPI.equalsIgnoreCase(storedOrganization.organization.hsaIdPrefix, organization.hsaIdPrefix)) {
            throw new BadRequestException("hsaIdPrefix not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        // hsaIdCounter is not allowed to change // optional Attribute
        if (!hsaRedisAPI.equalsIgnoreCase(storedOrganization.organization.hsaIdCounter, organization.hsaIdCounter)) {
            throw new BadRequestException("hsaIdCounter not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        
        storedOrganization.modifierUUID = "not_yet_implemented";
        storedOrganization.modifyTimestamp = ZonedDateTime.now();
        storedOrganization.organization = organization;
        hsaRedisAPI.merge(JsonbUtil.jsonb().toJson(storedOrganization), mapAttributes(storedOrganization),
                mapSimpleSearchAttributes(path.replace("/organizations/" + storedOrganization.organization.name, ""), storedOrganization));
        return hsaRedisAPI.getGraph().getPathFromId(id);
    }

    @Override
    public String move(@NotNull String path, String newPath) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }


    @Override
    public void remove(@NotNull String path) {
        throw new ForbiddenException("not allowed", ErrorCodes.NOT_SUPPORTED);
    }

    @Override
    public @NotNull List<OrganizationResponse> findImmediateSubordinates(@NotNull String requestPath,
                                                                         @NotNull String parentType,
                                                                         @NotNull String relationType) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisAPI.getGraph().getImmediateSubordinatesIdsOfCertainType(id, parentType, relationType).stream()
                .map(hsaRedisAPI::findById)
                .map(i -> JsonbUtil.jsonb().fromJson(i, OrganizationPersistence.class))
                .map(hsaRedisAPI::toOrganizationResponse).collect(Collectors.toList());
    }

    private Map<String, String> mapAttributes(OrganizationPersistence organizationPersistence) {
        Map<String, String> mappedAttributes = IndexAttributeMapper.mapAttributes(organizationPersistence);
        mappedAttributes.putAll(IndexAttributeMapper.mapAttributes(organizationPersistence.organization));
        return mappedAttributes;
    }

    private Map<String, String> mapSimpleSearchAttributes(String parentPath, OrganizationPersistence organizationPersistence) {

        // fields map
        Map<String, String> simpleSearchIndexFields = new HashMap<>();

        // mandatory attributes, space sparated key-words list
        String searchKeyWords = "";
        if (organizationPersistence.organization.name != null && !organizationPersistence.organization.name.isEmpty()) {
            searchKeyWords += " " + organizationPersistence.organization.name;
        }
        if (organizationPersistence.organization.localityName != null && !organizationPersistence.organization.localityName.isEmpty()) {
            searchKeyWords += " " + organizationPersistence.organization.localityName;
        }

        if (organizationPersistence.organization.hsaIdentity != null && !organizationPersistence.organization.hsaIdentity.isEmpty()) {
            searchKeyWords += " " + organizationPersistence.organization.hsaIdentity;
        }

        if (organizationPersistence.organization.c != null && !organizationPersistence.organization.c.isEmpty()) {
            searchKeyWords += " " + organizationPersistence.organization.c;
        }
        if (organizationPersistence.organization.c != null && !organizationPersistence.organization.c.isEmpty()) {
            searchKeyWords += " " + organizationPersistence.organization.c;
        }

        if (parentPath != null && !parentPath.isEmpty()) {
            searchKeyWords += " " + parentPath.replace("/", " ")
                    .replace("countries", "")
                    .replace("counties", "")
                    .replace("organizations", "")
                    .replace("units", "")
                    .replace("functions", "")
                    .replace("employees", "");
        }
        if (organizationPersistence.organization.postalAddress != null &&
                organizationPersistence.organization.postalAddress.addressLine != null &&
                !organizationPersistence.organization.postalAddress.addressLine.isEmpty()) {
            searchKeyWords += " " + String.join(" ", organizationPersistence.organization.postalAddress.addressLine);
        }

        if (organizationPersistence.organization.countyCode != null && !organizationPersistence.organization.countyCode.isEmpty()) {

            WebTarget codeSystemTarget_CountyCode = ClientBuilder.newClient().target(codeSystemURI)
                    .path(COUNTY_CODES_OID).path("codes").path(organizationPersistence.organization.countyCode);

            Invocation.Builder request = codeSystemTarget_CountyCode.request();

            request.accept(MediaType.APPLICATION_JSON_TYPE);

            Response response = request.buildGet().invoke();

            JsonObject countyCodeJsonObject;
            if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
                response.bufferEntity();
                countyCodeJsonObject = response.readEntity(JsonObject.class);
                String countyName = countyCodeJsonObject.get("name").toString();
                searchKeyWords += " " + countyName;
            }

        }

        if (organizationPersistence.organization.municipalityCode != null && !organizationPersistence.organization.municipalityCode.isEmpty()) {

            WebTarget codeSystemTarget_MunicipalityCode = ClientBuilder.newClient().target(codeSystemURI).path(MUNICIPALITY_CODES_OID)
                    .path("codes").path(organizationPersistence.organization.municipalityCode);

            Invocation.Builder request = codeSystemTarget_MunicipalityCode.request();

            request.accept(MediaType.APPLICATION_JSON_TYPE);

            Response response = request.buildGet().invoke();

            JsonObject countyCodeJsonObject;
            if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
                response.bufferEntity();
                countyCodeJsonObject = response.readEntity(JsonObject.class);
                String municipalityName = countyCodeJsonObject.get("name").toString();
                searchKeyWords += " " + municipalityName;
            }
        }
        simpleSearchIndexFields.put("words", escapeSpecialCharacters(searchKeyWords));
        simpleSearchIndexFields.put("type", organizationPersistence.organization.type);

        return simpleSearchIndexFields;
    }

    private String escapeSpecialCharacters(String inputText) {
        // Redis search, any character of ,.<>{}[]"':;!@#$%^&*()-+=~ will break the text into terms. Thus, we've to escape them here
        return inputText.replaceAll("([-.+@<>{$%}(=;:)])", "\\\\$0");
    }

    @Override
    public String patch(@NotNull String path, @NotNull List<PatchModification> mods) {
        String id = hsaRedisAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");
     
        String findById = hsaRedisAPI.findById(id);
        OrganizationPersistence storedOrganization = JsonbUtil.jsonb().fromJson(findById, OrganizationPersistence.class);
        
        PatchModificationUtil patcher = new PatchModificationUtil();
        
        try {
            //Mandatory attributes is handled in handlePatchModification
            patcher.handlePatchModification(storedOrganization.organization, mods, "name", "hsaIdentity", "hsaHpt", "hsaIdPrefix","hsaIdCounter");
        } catch (PatchException e) {
            throw new BadRequestException("Error modifying organization: "+e.getMessage(), ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        
      //TODO: Check for that mandatory attributes are still valid.
        if( (storedOrganization.organization.mail == null || storedOrganization.organization.mail.trim().isEmpty())) {
            throw new BadRequestException("Mandatory attribute mail from object class person is missing", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        if( (storedOrganization.organization.orgNo == null || storedOrganization.organization.orgNo.trim().isEmpty())) {
            throw new BadRequestException("Mandatory attribute orgNo from object class person is missing", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        if( (storedOrganization.organization.telephoneNumber == null || storedOrganization.organization.telephoneNumber.isEmpty())) {
            throw new BadRequestException("Mandatory attribute telephoneNumber from object class person is missing", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        if( storedOrganization.organization.postalAddress == null ) {
            throw new BadRequestException("Mandatory attribute postalAddress from object class person is missing", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        
        
        storedOrganization.modifierUUID = "not_yet_implemented";
        storedOrganization.modifyTimestamp = ZonedDateTime.now();
        Map<String, String> indexFields = mapAttributes(storedOrganization);
        hsaRedisAPI.merge(JsonbUtil.jsonb().toJson(storedOrganization), indexFields,
                mapSimpleSearchAttributes(path.replace("/organizations/" + storedOrganization.organization.name, ""), storedOrganization));
        return hsaRedisAPI.getGraph().getPathFromId(id);
    }
   
}





