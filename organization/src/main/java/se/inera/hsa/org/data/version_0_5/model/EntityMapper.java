package se.inera.hsa.org.data.version_0_5.model;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class maps a ResultSet to an actual EntityResult object. It is bound to the JDBI-object in the
 * OrganizationRepository instance.
 */
public class EntityMapper implements RowMapper<EntityResult> {

    @Override
    public EntityResult map(ResultSet rs, StatementContext ctx) throws SQLException {
        EntityResult entityResult = new EntityResult();
        entityResult.setInternal_uuid(rs.getString("internal_entry_uuid"));
        entityResult.setNode_type(convertNodeType(rs.getString("node_type")));
        entityResult.setNode_name(rs.getString("node_name"));
        entityResult.setJson_object(rs.getString("json_object"));
        System.out.println(entityResult.getJson_object());
        return entityResult;
    }

    private NodeType convertNodeType(String node_type) {
        if(node_type.equals(NodeType.COUNTRY.getTypeName()))
            return NodeType.COUNTRY;
        else if(node_type.equals(NodeType.COUNTY.getTypeName()))
            return NodeType.COUNTY;
        else if(node_type.equals(NodeType.EMPLOYEE.getTypeName()))
            return NodeType.EMPLOYEE;
        else if(node_type.equals(NodeType.FUNCTION.getTypeName()))
            return NodeType.FUNCTION;
        else if(node_type.equals(NodeType.ORGANIZATION.getTypeName()))
            return NodeType.ORGANIZATION;
        else if(node_type.equals(NodeType.UNIT.getTypeName()))
            return NodeType.UNIT;
        else if(node_type.equals(NodeType.TOP.getTypeName()))
            return NodeType.TOP;

        throw new IllegalArgumentException(node_type + " is not an accepted NodeType");
    }

    @Override
    public RowMapper<EntityResult> specialize(ResultSet rs, StatementContext ctx) throws SQLException {
        return this;
    }
}
