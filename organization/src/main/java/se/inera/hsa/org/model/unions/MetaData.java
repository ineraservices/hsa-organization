package se.inera.hsa.org.model.unions;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;

/**
 * MetaData contains information not stored in the actual domain object, but is instead either computed from a state
 * in the database or stored in the database entry outside of the JSON-object. MetaData is delivered as part of the
 * response object for the GET services.
 */
public class MetaData {

    public MetaData() { }

    @Schema(
            type = SchemaType.OBJECT,
            required = true
    )
    @NotBlank
    public ZonedDateTime createTimestamp;

    @Schema(
            required = true
    )
    @NotBlank
    public String creatorUUID;

    @Schema(
            required = true
    )
    @NotBlank
    public String creatorPath;

    @Schema(
            required = true
    )
    @NotBlank
    public int numSubordinates;

    @Schema(
            required = true
    )
    @NotBlank
    public int numAllSubordinates;

    @Schema(
            required = true
    )
    @NotBlank
    public String path;

    public ZonedDateTime modifyTimestamp;

    public String modifierPath;

    public String modifierUUID;
    
    public MetaData(ZonedDateTime createTimestamp,
                    String creatorUUID,
                    String creatorPath,
                    int numSubordinates,
                    int numAllSubordinates,
                    String path) {
        this.createTimestamp = createTimestamp;
        this.creatorUUID = creatorUUID;
        this.creatorPath = creatorPath;
        this.numSubordinates = numSubordinates;
        this.numAllSubordinates = numAllSubordinates;
        this.path = path;
    }
}
