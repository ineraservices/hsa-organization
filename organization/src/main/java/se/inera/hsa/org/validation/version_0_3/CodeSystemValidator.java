package se.inera.hsa.org.validation.version_0_3;

import se.inera.hsa.clients.CodeSystemClient;

import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotNull;

public class CodeSystemValidator implements ConstraintValidator<CodeSystem, String> {

    @Inject
    private CodeSystemClient codeSystemClient;

    private @NotNull String oid;

    @Override
    public void initialize(CodeSystem constraintAnnotation) {
        oid = constraintAnnotation.oid();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || codeSystemClient.validate(oid, value);
    }
}
