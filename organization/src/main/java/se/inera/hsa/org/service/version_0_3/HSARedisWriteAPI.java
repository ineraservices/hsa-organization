package se.inera.hsa.org.service.version_0_3;

import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.dynamic.RedisCommandFactory;
import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.lettuce.RedisClientUtil;
import se.inera.hsa.lettuce.commands.RedisHSAWriteCommands;
import se.inera.hsa.lettuce.commands.RedisHSATransactionCommands;
import se.inera.hsa.lettuce.commands.RedisJSONCommands;
import se.inera.hsa.lettuce.commands.RedisSearchCommands;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.UriBuilder;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.lang.String.format;
import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.*;

public class HSARedisWriteAPI {

    private StatefulRedisConnection<String, String> connection;
    private RedisHSAWriteCommands redisCommands;
    private RedisHSATransactionCommands transactionCommands;
    private RedisJSONCommands redisJSONCommands;
    private RedisSearchCommands redisearch;
    private OrganizationGraph graph;

    @PostConstruct
    public void start() {
        start("hsa-redis");
    }

    public void start(String redisHost) {
        System.out.println("HSARedisWriteAPI PostConstruct");
        try {
            connection = RedisClientUtil.getClient(redisHost).connect();
            RedisCommandFactory factory = new RedisCommandFactory(connection);
            transactionCommands = new RedisHSATransactionCommands(connection.sync());
            redisCommands = new RedisHSAWriteCommands(factory);
            redisJSONCommands = factory.getCommands(RedisJSONCommands.class);
            redisearch = factory.getCommands(RedisSearchCommands.class);
            graph = new OrganizationGraph(factory, GRAPH_ROOT_ID, GRAPH_NAME);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    @PreDestroy
    public void stop() {
        System.out.println("HSARedisWriteAPI PreDestroy");
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * Save a node to the database
     */
    public void persist(String parentPath,
                        String name,
                        List<String> allowedParentTypes,
                        String jsonBlob,
                        String relationType,
                        String childType,
                        Map<String, String> indexFields,
                        Map<String, String> simpleSearchIndexFields) {

        String path = UriBuilder.fromPath(parentPath).path(childType.toLowerCase() + "s").path(name).build().getPath();

        // optimistically locks redis for the node we are adding. If this key is changed
        // during the transaction it will not do the transaction.
        transactionCommands.watch(path);
        System.out.println("PERSIST: watching path : " + path);

        // We have identified some things that must be checked so that the transaction will be able to run
        // 1. Does the resource already exist in the graph?
        if ((graph.getIdFromPath(path)) != null) {
            transactionCommands.unwatch();
            String message = "ERROR: PERSIST: resource already exists for path \"" + path + "\"";
            System.out.println(message);
            throw new BadRequestException(message, ErrorCodes.ENTRY_ALREADY_EXISTS);
        }

        // 2. Does the correct parent type exist in the graph?
        String parentId = graph.getIdFromPath(parentPath);
        if (parentId == null) {
            transactionCommands.unwatch();
            String message = "ERROR: PERSIST: no resource found for parent path \"" + parentPath + "\"";
            System.out.println(message);
            throw new NotFoundException(message);
        }
        String parentType = graph.getType(parentId, allowedParentTypes);
        if (parentType == null) {
            transactionCommands.unwatch();
            String message = "ERROR: PERSIST: Parent must exist and be of type " + allowedParentTypes.toString();
            System.out.println(message);
            throw new BadRequestException(message);
        }

        // 3. check that json-blob does't exist in redisjson
        String persistedJsonBlob = redisJSONCommands.jsonGet(ORGANIZATION_JSON_ID_PREFIX + indexFields.get("uuid"));
        if (persistedJsonBlob != null) {
            transactionCommands.unwatch();
            String message = "ERROR: PERSIST: json blob already exists " + persistedJsonBlob;
            System.out.println(message);
            throw new BadRequestException(message);
        }

        // 4. check that "index name" exists (will cause error otherwise) in redisSearch and that there is not
        // already an index for the document
        Map<String, String> index = redisearch.ftGet(INDEX_NAME, indexFields.get("uuid"));
        if (index != null) {
            transactionCommands.unwatch();
            String message = "ERROR: PERSIST: Index already exists " + index.keySet().toString();
            System.out.println(message);
            throw new BadRequestException(message);
        }

        // starts transaction
        transactionCommands.multi();

        // writes over the watched key. This will make other transactions trying
        // to change the database at the same time fail. (only one will succeed)
        redisCommands.set(path, UUID.randomUUID().toString());
        addRelation(
                parentId,
                parentType,
                relationType,
                childType,
                indexFields.get("uuid"),
                name);
        
        redisCommands.jsonSetIfNotExists(
                indexFields.get("uuid"),
                jsonBlob);
        redisCommands.ftAdd(
                indexFields.get("uuid"),
                INDEX_NAME,
                ORGANIZATION_INDEX_ID_PREFIX,
                HSARedisReadAPI.INDEX_SCORE,
                indexFields);
        redisCommands.ftAdd(
                indexFields.get("uuid"),
                INDEX_SIMPLE_SEARCH_NAME,
                SIMPLE_SEARCH_JSON_ID_PREFIX,
                HSARedisReadAPI.INDEX_SCORE,
                simpleSearchIndexFields);

        // executes transaction
        transactionCommands.exec();
        transactionCommands.unwatch();
        System.out.println("PERSIST: DONE!");
    }

    public void delete(String path) {
        transactionCommands.watch(path);
        System.out.println("DELETE: watching path : " + path);

        String id = graph.getIdFromPath(path);
        if (id == null) {
            String message = "ERROR: DELETE: no resource found for path \"" + path + "\"";
            System.out.println(message);
            transactionCommands.unwatch();
            throw new NotFoundException(message);
        }
        // We have identified some things that must be checked so that the transaction will be able to run
        // Observe: Checking if we are ALLOWED to remove it (some objects are not allowed to be deleted) should not
        // be done in this layer, but higher up.

        // 1. does it exist in graph and is it a leaf, only leafs are allowed to be deleted
        int numAllSubordinates = graph.getNumAllSubordinates(id);
        if (numAllSubordinates > 0) {
            String message = "ERROR: DELETE: resource with id: " + id + " has " + numAllSubordinates + " subordinates, removal not allowed, it can't be deleted. ";
            System.out.println(message);
            transactionCommands.unwatch();
            throw new BadRequestException(message, ErrorCodes.NOT_ALLOWED_ON_NON_LEAF);
        }

        // 2. does it exist in redisjson
        if (redisCommands.noJsonExist(id)) {
            transactionCommands.unwatch();
            String message = "ERROR: DELETE: This object has an illegal state! It can't be deleted. Missing jsonblob for id: " + id;
            System.out.println(message);
            throw new IllegalStateException(message);
        }

        // 3. does it exist in any index
        if (redisCommands.noIndexExists(id, INDEX_NAME, ORGANIZATION_INDEX_ID_PREFIX) ||
                redisCommands.noIndexExists(id, INDEX_SIMPLE_SEARCH_NAME, SIMPLE_SEARCH_JSON_ID_PREFIX)) {
            transactionCommands.unwatch();
            String message = "ERROR: DELETE: This object has an illegal state! It can't be deleted. Missing index for id: " + id;
            System.out.println(message);
            throw new IllegalStateException(message);
        }

        transactionCommands.multi();

        redisCommands.set(path, UUID.randomUUID().toString());

        String query = format("MATCH(node) WHERE node.id='%s' DELETE node", id);
        redisCommands.graphQuery(query);
        redisCommands.jsonDel(id);
        redisCommands.ftDel(id, INDEX_NAME, ORGANIZATION_INDEX_ID_PREFIX);
        redisCommands.ftDel(id, INDEX_SIMPLE_SEARCH_NAME, SIMPLE_SEARCH_JSON_ID_PREFIX);


        transactionCommands.exec();
        transactionCommands.unwatch();
        System.out.println("DELETE: DONE!");
    }

    // adding a relation implies having previously asserted that any possible parent object exist
    private void addRelation(String parentId,
                             String parentType,
                             String relationType,
                             String childType,
                             String childId,
                             String childName) {

        String addQuery = String.format("MATCH (a:%s {id:'%s'}) " +
                        "CREATE (a)-[:%s]->(:%s {" +
                        "id:'%s'," +
                        "name:'%s'," +
                        "type:'%s'," +
                        "pid:'%s'," +
                        "ptype:'%s'," +
                        "rel:'%s'})",
                parentType,
                parentId,
                relationType,
                childType,
                childId,
                childName,
                childType,
                parentId,
                parentType,
                relationType);

        redisCommands.graphQuery(addQuery);
    }
}
