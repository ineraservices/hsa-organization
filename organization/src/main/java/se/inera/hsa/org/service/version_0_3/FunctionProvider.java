package se.inera.hsa.org.service.version_0_3;

import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.FUNCTIONS_RELATION;
import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.FUNCTION_LABEL;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.metrics.annotation.Metered;

import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.exception.PatchException;
import se.inera.hsa.org.model.version_0_3.Function;
import se.inera.hsa.org.model.version_0_3.FunctionPersistence;
import se.inera.hsa.org.model.version_0_3.FunctionResponse;
import se.inera.hsa.org.validation.version_0_3.PatchModification;
import se.inera.hsa.jsonb.JsonbUtil;


@RequestScoped
public class FunctionProvider implements Provider<Function> {

    @Inject
    @ConfigProperty(name = "codesystem.uri")
    String codeSystemURI;

    @Inject
    HSARedisReadAPI hsaRedisReadAPI;

    @Inject
    HSARedisWriteAPI hsaRedisWriteAPI;

    private static String COUNTY_CODES_OID = "1.2.752.129.2.2.1.18";
    private static String MUNICIPALITY_CODES_OID = "1.2.752.129.2.2.1.17";

    private List<String> allowedParentTypes = Arrays.asList("Organization", "Unit");

    @Metered
    @Override
    public void persist(@NotNull String parentPath, @NotNull Function function) throws BadRequestException {
        FunctionPersistence functionPersistence = new FunctionPersistence();

        // add metadata
        functionPersistence.creatorUUID = "not_yet_implemented";
        functionPersistence.createTimestamp = ZonedDateTime.now();
        functionPersistence.uuid = UUID.randomUUID().toString();

        // add unit
        functionPersistence.function = function;

        // fields map
        Map<String, String> indexFields = mapAttributes(functionPersistence);
        Map<String, String> simpleSearchIndexFields = null;
        try {
            simpleSearchIndexFields = mapSimpleSearchAttributes(parentPath, functionPersistence);
        } catch (Exception e) {
            e.printStackTrace();
        }

        hsaRedisWriteAPI.persist(
                parentPath,
                function.name,
                allowedParentTypes,
                JsonbUtil.jsonb().toJson(functionPersistence),
                FUNCTIONS_RELATION,
                FUNCTION_LABEL,
                indexFields,
                simpleSearchIndexFields);
    }


    @Override
    public String patch(@NotNull String path, @NotNull List<PatchModification> mods) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        String findById = hsaRedisReadAPI.findById(id);
        FunctionPersistence storedFunction = JsonbUtil.jsonb().fromJson(findById, FunctionPersistence.class);

        PatchModificationUtil patcher = new PatchModificationUtil();

        System.out.println("DSDASDASDAS");
        mods.forEach(m -> m.getAttributeName());
        try {
            //Mandatory attributes is handled in handlePatchModification
            patcher.handlePatchModification(storedFunction.function, mods, "name", "hsaIdentity", "type");
        } catch (PatchException e) {
            throw new BadRequestException("Error modifying function: "+e.getMessage(), ErrorCodes.OBJECT_CLASS_VIOLATION);
        }

        storedFunction.modifierUUID = "not_yet_implemented";
        storedFunction.modifyTimestamp = ZonedDateTime.now();
        Map<String, String> indexFields = mapAttributes(storedFunction);
        hsaRedisReadAPI.merge(JsonbUtil.jsonb().toJson(storedFunction), indexFields,
                mapSimpleSearchAttributes(path.replace("/functions/" + storedFunction.function.name, ""), storedFunction));
        return hsaRedisReadAPI.getGraph().getPathFromId(id);
    }


    @Override
    public String merge(@NotNull String path, @NotNull Function function) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        FunctionPersistence storedFunction = JsonbUtil.jsonb().fromJson(hsaRedisReadAPI.findById(id), FunctionPersistence.class);
        // name is  not allowed to change through merge
        if (!storedFunction.function.name.equalsIgnoreCase(function.name)) {
            throw new BadRequestException("name only modifiable through move", ErrorCodes.NOT_ALLOWED_ON_RDN);
        }
        // hsaIdentity is  not allowed to change
        if (!storedFunction.function.hsaIdentity.equalsIgnoreCase(function.hsaIdentity)) {
            throw new BadRequestException("hsaIdentity not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }
        // hsaIdentity is  not allowed to change
        if (!storedFunction.function.type.equalsIgnoreCase(function.type)) {
            throw new BadRequestException("type not modifiable", ErrorCodes.OBJECT_CLASS_VIOLATION);
        }

        storedFunction.modifierUUID = "not_yet_implemented";
        storedFunction.modifyTimestamp = ZonedDateTime.now();
        storedFunction.function = function;
        Map<String, String> indexFields = mapAttributes(storedFunction);
        hsaRedisReadAPI.merge(JsonbUtil.jsonb().toJson(storedFunction), indexFields,
                mapSimpleSearchAttributes(path.replace("/functions/" + storedFunction.function.name, ""), storedFunction));
        return hsaRedisReadAPI.getGraph().getPathFromId(id);
    }

    @Override
    public String move(@NotNull String path, String newPath) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(path);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + path + "\"");

        assertMoveOk(newPath);
        FunctionPersistence functionPersistence = JsonbUtil.jsonb().fromJson(hsaRedisReadAPI.findById(id), FunctionPersistence.class);
        functionPersistence.function.name = newPath.substring(newPath.lastIndexOf("/") + 1);
        functionPersistence.modifierUUID = UUID.randomUUID().toString();
        functionPersistence.modifyTimestamp = ZonedDateTime.now();
        String parentPath = newPath.substring(0, newPath.lastIndexOf("/functions"));
        String newParentId = hsaRedisReadAPI.getGraph().getIdFromPath(parentPath);
        String oldPath = hsaRedisReadAPI.getGraph().getPathFromId(id);
        String oldParentId = hsaRedisReadAPI.getGraph().getIdFromPath(oldPath.substring(0, oldPath.lastIndexOf("/functions")));
        Map<String, String> indexFields = new HashMap<>();
        indexFields.put("uuid", functionPersistence.uuid);
        indexFields.put("name", functionPersistence.function.name);
        indexFields.put("modifierUUID", functionPersistence.modifierUUID);
        indexFields.put("modifyTimestamp", functionPersistence.modifyTimestamp.toString());
        hsaRedisReadAPI.move(JsonbUtil.jsonb().toJson(functionPersistence), indexFields, mapSimpleSearchAttributes(parentPath,
                functionPersistence),newParentId, HSARedisReadAPI.FUNCTIONS_RELATION, oldParentId);
        return hsaRedisReadAPI.getGraph().getPathFromId(id);
    }

    @Override
    public void remove(@NotNull String requestPath) {
        // anything additional need to be done when deleting unit?
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(requestPath);
        if (id == null) {
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        }
        hsaRedisWriteAPI.delete(requestPath);
    }

    @Override
    public List<FunctionResponse> findImmediateSubordinates(@NotNull String requestPath, @NotNull String parentType,
                                                            @NotNull String relationType) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");

        return hsaRedisReadAPI.getGraph().getImmediateSubordinatesIdsOfCertainType(id, parentType, relationType).stream()
                .map(hsaRedisReadAPI::findById)
                .map(persistedString -> JsonbUtil.jsonb().fromJson(persistedString, FunctionPersistence.class))
                .map(hsaRedisReadAPI::toFunctionResponse)
                .collect(Collectors.toList());
    }

    @Override
    public @NotNull FunctionResponse find(@NotNull String requestPath) {
        String id = hsaRedisReadAPI.getGraph().getIdFromPath(requestPath);
        if (id == null)
            throw new NotFoundException("no resource found for path \"" + requestPath + "\"");
        return hsaRedisReadAPI.toFunctionResponse(JsonbUtil.jsonb().fromJson(hsaRedisReadAPI.findById(id), FunctionPersistence.class));
    }

    private void assertMoveOk(String newPath) {

        // get parentId by allowed types
        String newParentPath = newPath.substring(0, newPath.lastIndexOf("/functions"));

        // verify parentType is ok and that there exists no other object with the same path as requested
        if (hsaRedisReadAPI.getGraph().getType(hsaRedisReadAPI.getGraph().getIdFromPath(newParentPath), allowedParentTypes) == null) {
            throw new BadRequestException("parent object does not exist or if of wrong type (not one of " +
                    String.join(",", allowedParentTypes) + ")", ErrorCodes.NO_SUCH_OBJECT);
        }
        if (hsaRedisReadAPI.getGraph().getIdFromPath(newPath) != null) {
            throw new BadRequestException("entry already exists", ErrorCodes.ENTRY_ALREADY_EXISTS);
        }
    }

    private Map<String, String> mapAttributes(FunctionPersistence functionPersistence) {
        Map<String, String> indexMappings = IndexAttributeMapper.mapAttributes(functionPersistence);
        indexMappings.putAll(IndexAttributeMapper.mapAttributes(functionPersistence.function));
        return indexMappings;
    }

    private Map<String, String> mapSimpleSearchAttributes(String parentPath, FunctionPersistence functionPersistence) {

        // fields map
        Map<String, String> simpleSearchIndexFields = new HashMap<>();

        // mandatory attributes, space sparated key-words list
        String searchKeyWords = "";
        if (functionPersistence.function.name != null && !functionPersistence.function.name.isEmpty()) {
            searchKeyWords += " " + functionPersistence.function.name;
        }
        if (functionPersistence.function.localityName != null && !functionPersistence.function.localityName.isEmpty()) {
            searchKeyWords += " " + functionPersistence.function.localityName;
        }

        if (functionPersistence.function.hsaIdentity != null && !functionPersistence.function.hsaIdentity.isEmpty()) {
            searchKeyWords += " " + functionPersistence.function.hsaIdentity;
        }

        if (functionPersistence.function.c != null && !functionPersistence.function.c.isEmpty()) {
            searchKeyWords += " " + functionPersistence.function.c;
        }
        if (functionPersistence.function.c != null && !functionPersistence.function.c.isEmpty()) {
            searchKeyWords += " " + functionPersistence.function.c;
        }

        if (parentPath != null && !parentPath.isEmpty()) {
            searchKeyWords += " " + parentPath.replace("/", " ")
                    .replace("countries", "")
                    .replace("counties", "")
                    .replace("organizations", "")
                    .replace("units", "")
                    .replace("functions", "")
                    .replace("employees", "");
        }
        if (functionPersistence.function.postalAddress != null &&
                functionPersistence.function.postalAddress.addressLine != null &&
                !functionPersistence.function.postalAddress.addressLine.isEmpty()) {
            searchKeyWords += " " + String.join(" ", functionPersistence.function.postalAddress.addressLine);
        }

        if (functionPersistence.function.countyCode != null && !functionPersistence.function.countyCode.isEmpty()) {

            WebTarget codeSystemTarget_CountyCode = ClientBuilder.newClient().target(codeSystemURI)
                    .path(COUNTY_CODES_OID).path("codes").path(functionPersistence.function.countyCode);

            Builder request = codeSystemTarget_CountyCode.request();

            request.accept(MediaType.APPLICATION_JSON_TYPE);

            Response response = request.buildGet().invoke();

            JsonObject countyCodeJsonObject;
            if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
                response.bufferEntity();
                countyCodeJsonObject = response.readEntity(JsonObject.class);
                String countyName = countyCodeJsonObject.get("name").toString();
                searchKeyWords += " " + countyName;
            }
        }

        if (functionPersistence.function.municipalityCode != null && !functionPersistence.function.municipalityCode.isEmpty()) {

            WebTarget codeSystemTarget_MunicipalityCode = ClientBuilder.newClient().target(codeSystemURI).path(MUNICIPALITY_CODES_OID)
                    .path("codes").path(functionPersistence.function.municipalityCode);

            Builder request = codeSystemTarget_MunicipalityCode.request();

            request.accept(MediaType.APPLICATION_JSON_TYPE);

            Response response = request.buildGet().invoke();

            JsonObject countyCodeJsonObject;
            if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
                response.bufferEntity();
                countyCodeJsonObject = response.readEntity(JsonObject.class);
                String municipalityName = countyCodeJsonObject.get("name").toString();
                searchKeyWords += " " + municipalityName;
            }
        }

        simpleSearchIndexFields.put("words", escapeSpecialCharacters(searchKeyWords));
        simpleSearchIndexFields.put("type", functionPersistence.function.type);

        return simpleSearchIndexFields;
    }

    public String escapeSpecialCharacters(String inputText) {
        // Redis search, any character of ,.<>{}[]"':;!@#$%^&*()-+=~ will break the text into terms. Thus, we've to escape them here
        return inputText.replaceAll("([-.+@<>{$%}(=;:)])", "\\\\$0");
    }
}
