package se.inera.hsa.org.validation.version_0_3;


import java.io.Serializable;

import javax.validation.Valid;

import se.inera.hsa.org.model.unions.Unit;

@ValidModifier_Unit
public class UnitModification extends Modification implements Serializable {

    private static final long serialVersionUID = 1378691177398573339L;

    @Valid
    public Unit unit;

}
