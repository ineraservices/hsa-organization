package se.inera.hsa;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class GsonHelper {

    public static final JsonDeserializer<ZonedDateTime> ZDT_DESERIALIZER = (json, typeOfT, context) -> getZonedDateTime(json);

    /**
     * This method is needed for telling Gson how to deserialize ZonedDateTime-objects. It is added to the Gson-object
     * in runtime, and Gson will then use it whenever it needs to deserialize an object of this type.
     */
    public static ZonedDateTime getZonedDateTime(JsonElement json) {
        JsonPrimitive jsonPrimitive = json.getAsJsonPrimitive();
        try {

            // if provided as String - '2011-12-03T10:15:30+01:00[Europe/Paris]'
            if (jsonPrimitive.isString()) {
                return ZonedDateTime.parse(jsonPrimitive.getAsString(), DateTimeFormatter.ISO_ZONED_DATE_TIME);
            }

            // if provided as Long
            if (jsonPrimitive.isNumber()) {
                return ZonedDateTime.ofInstant(Instant.ofEpochMilli(jsonPrimitive.getAsLong()), ZoneId.systemDefault());
            }

        } catch (RuntimeException e) {
            throw new JsonParseException("Unable to parse ZonedDateTime", e);
        }
        throw new JsonParseException("Unable to parse ZonedDateTime");
    }
}
