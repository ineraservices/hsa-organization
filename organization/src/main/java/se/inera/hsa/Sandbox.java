package se.inera.hsa;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import se.inera.hsa.org.model.version_0_5.Employee;
import se.inera.hsa.org.model.version_0_5.EntityPersistence;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.lang.String.format;
import static java.util.Arrays.asList;

public class Sandbox {

    public static void main(String[] args) {

        String path = "countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/Nordic Unit";
        System.out.println(getParentType(path));
        System.out.println(getTypeName(path));
    }

    public static String getParentType(String path) {
        //countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/employees/test Person -> organizations
        //countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/test/function/test function -> units
        //countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest -> counties
        //countries/Sverige/organizations/Nordic MedTest -> countries
        String[] split = path.split("/");
        if (split.length >= 4 ) {
            return split[split.length-4];
        }
        return "";
    }
    /*
    countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/employees/test Person -> employees
    countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/function/test function -> function
    countries/Sverige/counties/Värmlands län/organizations/Nordic MedTest/units/test Unit -> units
     */
    public static String getTypeName(String path) {
        String[] split = path.split("/");
        if (split.length >= 2 ) {
            return split[split.length-2];
        }
        return "";
    }

    private static String getSqlQuery(String path) {
        List<String> pathList = asList(path.split("/"));
        int counterP = 1;

        StringBuilder andBuilder = new StringBuilder();
        StringBuilder fromBuilder = new StringBuilder("FROM");
        StringBuilder matchBuilder = new StringBuilder("MATCH(");
        for (int i = 1; i < pathList.size(); i += 2) {
            String name = pathList.get(i);
            fromBuilder.append(" organization_node node").append(counterP).append(", organization_edge as has").append(counterP).append(",");
            matchBuilder.append("node").append(counterP).append("-(has").append(counterP).append(")->");
            andBuilder.append(" AND node").append(counterP).append(".node_name = '").append(name).append("'");
            counterP++;
        }

        int finalNodeNumber = counterP-1;
        fromBuilder.delete(fromBuilder.indexOf(format(", organization_edge as has%s", finalNodeNumber)), fromBuilder.length());
        matchBuilder.delete(matchBuilder.indexOf(format("-(has%s)->",finalNodeNumber)), matchBuilder.length());
        matchBuilder.append(")");

        if (pathList.size() == 2) {
            matchBuilder.setLength(0);
            andBuilder.delete(0, 5);
        }
        StringBuilder queryBuilder = new StringBuilder(format("SELECT node%s.internal_entry_uuid ", finalNodeNumber));
        queryBuilder.append(fromBuilder).append(" ").append("WHERE ").append(matchBuilder).append(" ").append(andBuilder);
        return queryBuilder.toString();
    }
}
