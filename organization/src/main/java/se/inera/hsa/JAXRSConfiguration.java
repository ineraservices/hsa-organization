package se.inera.hsa;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.servers.Server;
import org.eclipse.microprofile.openapi.annotations.servers.ServerVariable;

import se.inera.hsa.org.api.ApiVersion;
import se.inera.hsa.org.api.version_0_3.CountryResource;
import se.inera.hsa.org.api.version_0_3.CountyResource;
import se.inera.hsa.org.api.version_0_3.EmployeeResource;
import se.inera.hsa.org.api.version_0_3.OrganizationResource;
import se.inera.hsa.org.api.version_0_3.UnitResource;

/**
 * @author kdaham
 */
@OpenAPIDefinition(
        info = @Info(
                version = "0.3",
                title = "HSA 2.0 Organization API",
                description = "This api provides CRUD functionality for " +
                        "the Organization microservice wich is part of the bundle of components and services constituting<br/>" +
                        "the HSA 2.0 application. Each object is accessible under it's unique path and this path is a 1:1 match<br/>" +
                        "to the DN's used in the API's predecessor, the HSA LDAP directory",
                license = @License(
                        name = "Apache License Version 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html"
                )
        ),
        servers = {
                @Server(url = "http://{ORG_URL}:{ORG_PORT}/organization",
                        variables = {
                                @ServerVariable(
                                        name = "ORG_URL",
                                        defaultValue = "localhost"
                                ),
                                @ServerVariable(
                                        name = "ORG_PORT",
                                        defaultValue = "9080"
                                )
                        }),
                @Server(url = "https://{ORG_URL}:{ORG_PORT}/organization",
                        variables = {
                                @ServerVariable(
                                        name = "ORG_URL",
                                        defaultValue = "organization-dhsa.app-test1.ind-ocp.sth.basefarm.net"
                                ),
                                @ServerVariable(
                                        name = "ORG_PORT",
                                        defaultValue = "443"
                                )
                        })
        }

)
@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {
}
