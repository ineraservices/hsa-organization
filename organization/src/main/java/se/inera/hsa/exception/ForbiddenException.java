package se.inera.hsa.exception;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author tesi
 */
public class ForbiddenException extends ClientErrorException {

	public ForbiddenException(String message) {
        super(Response
        		.status(Response.Status.FORBIDDEN.getStatusCode(), message)
                .entity(new ErrorResponse(message))
                .type(MediaType.APPLICATION_JSON).build());
    }
	
	public ForbiddenException(String message, int code) {
        super(Response
                .status(Response.Status.FORBIDDEN.getStatusCode(), message)
                .entity(new ErrorResponse(code, 
                                          message))
                .type(MediaType.APPLICATION_JSON).build());
    }
	
	public ForbiddenException(String message, ErrorCodes code) {
        super(Response
                .status(Response.Status.FORBIDDEN.getStatusCode(), message)
                .entity(new ErrorResponse(code.getCode(), 
                        message))
                .type(MediaType.APPLICATION_JSON).build());
    }
}
