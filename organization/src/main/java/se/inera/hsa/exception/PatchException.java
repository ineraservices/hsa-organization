package se.inera.hsa.exception;

public class PatchException extends Exception{

    public PatchException() {
        super();
    }

    public PatchException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

    public PatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public PatchException(String message) {
        super(message);
    }

    public PatchException(Throwable cause) {
        super(cause);
    }
}
