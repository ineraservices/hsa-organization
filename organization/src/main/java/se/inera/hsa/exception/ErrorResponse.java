package se.inera.hsa.exception;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private String code;
    private String message;
    private Error[] errors;

    public ErrorResponse(int code, String message) {
        this.code = Integer.toString(code);
        this.message = message;
    }

    public ErrorResponse(String message) {
        this.message = message;
    }

    public ErrorResponse(int code, String message, Error... errors) {
        super();
        this.code = Integer.toString(code);
        this.message = message;
        this.errors = errors;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Error[] getErrors() {
        return errors;
    }

}