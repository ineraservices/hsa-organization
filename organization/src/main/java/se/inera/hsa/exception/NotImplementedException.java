package se.inera.hsa.exception;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author kdaham
 */
public class NotImplementedException extends ServerErrorException {

	public NotImplementedException(String message) {
        super(Response
        		.status(Response.Status.NOT_IMPLEMENTED.getStatusCode(), message)
                .entity(new ErrorResponse(message))
                .type(MediaType.APPLICATION_JSON).build());
    }
	
	public NotImplementedException(String message, int code) {
	    super(Response
	            .status(Response.Status.NOT_IMPLEMENTED.getStatusCode(), message)
	            .entity(new ErrorResponse(code, 
	                    message))
	            .type(MediaType.APPLICATION_JSON).build());
	}
}
