package se.inera.hsa.exception;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author kdaham
 */
public class UnauthorizedException extends ClientErrorException {

    public UnauthorizedException(String message) {
    	 super(Response
          		.status(Response.Status.UNAUTHORIZED.getStatusCode(), message)
                  .entity(new ErrorResponse(message))
                  .type(MediaType.APPLICATION_JSON).build());
    }
    
    public UnauthorizedException(String message, int code) {
        super(Response
                .status(Response.Status.UNAUTHORIZED.getStatusCode(), message)
                .entity(new ErrorResponse(code, 
                        message))
                .type(MediaType.APPLICATION_JSON).build());
    }
}
