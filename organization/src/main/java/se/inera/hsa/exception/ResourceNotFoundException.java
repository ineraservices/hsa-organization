package se.inera.hsa.exception;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author kdaham
 */
public class ResourceNotFoundException extends ClientErrorException {

    public ResourceNotFoundException(String message) {
    	 super(Response
         		.status(Response.Status.NOT_FOUND.getStatusCode(), message)
                 .entity(new ErrorResponse(message))
                 .type(MediaType.APPLICATION_JSON).build());
    }
    
    public ResourceNotFoundException(String message, int code) {
        super(Response
                .status(Response.Status.NOT_FOUND.getStatusCode(), message)
                .entity(new ErrorResponse(code, 
                        message))
                .type(MediaType.APPLICATION_JSON).build());
    }
}
