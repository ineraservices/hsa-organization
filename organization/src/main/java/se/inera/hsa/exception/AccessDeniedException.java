package se.inera.hsa.exception;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author kdaham
 */
public class AccessDeniedException extends ClientErrorException {

    public AccessDeniedException(String message) {
    	super(Response
         		.status(Response.Status.FORBIDDEN.getStatusCode(), message)
                 .entity(new ErrorResponse(message))
                 .type(MediaType.APPLICATION_JSON).build());
    }
    
    public AccessDeniedException(String message, int code) {
        super(Response
                .status(Response.Status.FORBIDDEN.getStatusCode(), message)
                .entity(new ErrorResponse(code, 
                        message))
                .type(MediaType.APPLICATION_JSON).build());
    }
}
