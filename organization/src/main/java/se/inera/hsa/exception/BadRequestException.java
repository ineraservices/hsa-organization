package se.inera.hsa.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class BadRequestException extends WebApplicationException {

    public BadRequestException(String message) {
    	 super(Response
         		.status(Response.Status.BAD_REQUEST.getStatusCode(), message)
                 .entity(new ErrorResponse(message))
                 .type(MediaType.APPLICATION_JSON).build());
    }

    public BadRequestException(String message, int code) {
        super(Response
                .status(Response.Status.BAD_REQUEST.getStatusCode(), message)
                .entity(new ErrorResponse(code,
                        message))
                .type(MediaType.APPLICATION_JSON).build());
    }

    public BadRequestException(String message, ErrorCodes code) {
        super(Response
                .status(Response.Status.BAD_REQUEST.getStatusCode(), message)
                .entity(new ErrorResponse(code.getCode(),
                        message))
                .type(MediaType.APPLICATION_JSON).build());
    }

    public BadRequestException(String message, ErrorCodes code, Error[] errors) {
        super(Response
                .status(Response.Status.BAD_REQUEST.getStatusCode(), message)
                .entity(new ErrorResponse(code.getCode(), message, errors))
                .type(MediaType.APPLICATION_JSON).build());
    }
}
