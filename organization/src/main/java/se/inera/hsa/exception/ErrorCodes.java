package se.inera.hsa.exception;

public enum ErrorCodes {
    NO_SUCH_ATTRIBUTE(16),
    CONSTRAINT_VIOLATION (19),
    NO_SUCH_OBJECT(32),
    OBJECT_CLASS_VIOLATION(65),
    NOT_ALLOWED_ON_NON_LEAF(66),
    NOT_ALLOWED_ON_RDN(67),
    ENTRY_ALREADY_EXISTS(68),
    NOT_SUPPORTED(92);
    
    private int code;
    
    private ErrorCodes(int code) {
        this.code=code;
    }
    
    public int getCode() {
        return code;
    }
}
