package se.inera.hsa.exception;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author kdaham
 */
public class PermissionDeniedException extends ServerErrorException {

	private static final int STATUS_CODE_550 = 550;

	// vad är skillnaden mellan detta och AccessDeniedException?
	// 550 är inget standard http felkod - https://www.restapitutorial.com/httpstatuscodes.html#
    public PermissionDeniedException(String message) {
    	super(Response
    			.status(STATUS_CODE_550, message)
    			.entity(new ErrorResponse(message))
    			.type(MediaType.APPLICATION_JSON).build());
    }
    
    public PermissionDeniedException(String message, int code) {
        super(Response
                .status(STATUS_CODE_550, message)
                .entity(new ErrorResponse(code, 
                        message))
                .type(MediaType.APPLICATION_JSON).build());
    }
}
