package se.inera.hsa.org.validation.version_0_3;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StringFieldValueNotBlankDependentOnBooleanFieldValueValidatorTest {

    private Validator validator;

    @BeforeAll
    void setup() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    void isValidIfNameNotBlankAndHasNameTrue() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = "Magnus";
        testClass.hasName = true;

        assertValid(testClass);
    }

    @Test
    void isValidIfNameNotBlankAndHasNameFalse() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = "Magnus";
        testClass.hasName = false;

        assertValid(testClass);
    }

    @Test
    void isValidIfNameBlankAndHasNameFalse() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = " ";
        testClass.hasName = false;

        assertValid(testClass);
    }

    @Test
    void isValidIfNameEmptyAndHasNameFalse() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = "";
        testClass.hasName = false;

        assertValid(testClass);
    }

    @Test
    void isValidIfNameNullAndHasNameFalse() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = null;
        testClass.hasName = false;

        assertValid(testClass);
    }

    @Test
    void isInvalidIfNameBlankAndHasNameTrue() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = " ";
        testClass.hasName = true;

        assertInvalid(testClass);
    }

    @Test
    void isInvalidIfNameEmptyAndHasNameTrue() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = "";
        testClass.hasName = true;

        assertInvalid(testClass);
    }

    @Test
    void isInvalidIfNameNullAndHasNameTrue() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = null;
        testClass.hasName = true;

        assertInvalid(testClass);
    }

    @Test
    void isInvalidIfNameNullAndHasNameFalse() {
        AnnotationTestClassFalse testClass = new AnnotationTestClassFalse();
        testClass.name = null;
        testClass.hasName = false;

        assertInvalid(testClass);
    }

    @Test
    void isInvalidIfNameBlankAndHasNameFalse() {
        AnnotationTestClassFalse testClass = new AnnotationTestClassFalse();
        testClass.name = " ";
        testClass.hasName = false;
        assertInvalid(testClass);
    }

    @Test
    void isMessageCorrect() {
        AnnotationTestClassTrue testClass = new AnnotationTestClassTrue();
        testClass.name = "";
        testClass.hasName = true;
        Set<ConstraintViolation<AnnotationTestClassTrue>> violations = validator.validate(testClass);

        assertEquals(1, violations.size());
        assertEquals("String field \"name\" is not allowed to be blank if boolean field \"hasName\" is \"true\"", violations.iterator().next().getMessage());
    }

    private void assertValid(Object testClass) {
        System.out.println(validator.validate(testClass));
        assertTrue(validator.validate(testClass).isEmpty());
    }

    private void assertInvalid(Object testClass) {
        assertFalse(validator.validate(testClass).isEmpty());
    }
}