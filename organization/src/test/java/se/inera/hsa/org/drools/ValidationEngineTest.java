package se.inera.hsa.org.drools;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import se.inera.hsa.log.service.version_0_3.AuditLogger.eventTypes;
import se.inera.hsa.log.service.version_0_3.SoftValidationLogger;
import se.inera.hsa.org.model.version_0_5.Unit;

public class ValidationEngineTest {

    public static final String TEST_CURRENT_RULES = "se/inera/hsa/org/test-rules/current";
    public static final String TEST_OLD_RULES = "se/inera/hsa/org/test-rules/former";


    @Mock
    SoftValidationLogger softValidationLogger;

    @InjectMocks
    private ValidationEngine<Unit> unitValidationEngine;
//    @Inject
//    CodeSystemClient codeSystemClient;

    @BeforeAll
    public static void setup() {
        DroolsConfiguration.initDroolsConfiguration("test");
    }

    @BeforeEach
    public void init() {
        unitValidationEngine = new ValidationEngine<>();
        MockitoAnnotations.initMocks(this);
        Mockito.doAnswer( (answer) -> {
            Object arg0 = answer.getArgument(0);
            Object arg1 = answer.getArgument(1);
            Object arg2 = answer.getArgument(2);
            Object arg3 = answer.getArgument(3);
            Object arg4 = answer.getArgument(4);
            System.out.println(arg0 + " "+arg1 + " "+arg2 + " " +arg3 + " " + arg4);
            return null;
        }).when(softValidationLogger).log(any(), any(), any(), any(), any());
    }

    @Test
    public void validateTest() {
        Unit unit = new Unit();
        unit.description = "kalle";

        assertThrows(RuntimeException.class, () -> {
            Unit unit1 = unitValidationEngine.validate("test", unit, eventTypes.ADD_UNIT, "");
          });
    }


    @Test
    public void validate() {
        Unit unit = new Unit();
        unit.hsaIdentity="TEST";
        unit.name="name";
        unit.hsaHealthCareProvider = true;

        assertThrows(RuntimeException.class, () -> {
            Unit unit1 = unitValidationEngine.validate("test",unit, eventTypes.ADD_UNIT, "");
        });
    }


    @Test
    public void validateOK() {
        Unit unit = new Unit();
        unit.hsaIdentity="TEST";
        unit.name="name";

        assertDoesNotThrow(() -> unitValidationEngine.validate("test",unit, eventTypes.ADD_UNIT, ""));
    }

    @Test
    public void validateOKWhenOldRuleOKAndNewRuleFail() {
        Unit unit = new Unit();
        unit.hsaIdentity="TEST";
        unit.name="name";
        unit.hsaDestinationIndicator = "123";

        assertDoesNotThrow(() -> unitValidationEngine.validate("test",unit, eventTypes.ADD_UNIT, ""));
    }

    @Test
    public void removeIllegalNonMandatoryAttributes() {
        Unit unit = new Unit();
        unit.hsaIdentity="TEST";
        unit.name="name";
        unit.mail = "X";

        Unit validatedUnit = unitValidationEngine.validate("test",unit, eventTypes.ADD_UNIT, "foo");
        assertNull("Mail should be null (removed)",validatedUnit.mail);
    }

}
