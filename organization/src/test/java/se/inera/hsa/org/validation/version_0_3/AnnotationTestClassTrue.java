package se.inera.hsa.org.validation.version_0_3;

@StringFieldValueNotBlankDependentOnBooleanFieldValue(
        stringFieldName = "name",
        dependBooleanFieldName = "hasName",
        dependBooleanFieldValueExpected = "true"
)
public class AnnotationTestClassTrue {
    public String name;
    public boolean hasName;
}
