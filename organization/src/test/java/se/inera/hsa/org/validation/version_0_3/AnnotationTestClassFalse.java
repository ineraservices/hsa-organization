package se.inera.hsa.org.validation.version_0_3;

@StringFieldValueNotBlankDependentOnBooleanFieldValue(
        stringFieldName = "name",
        dependBooleanFieldName = "hasName",
        dependBooleanFieldValueExpected = "false"
)
public class AnnotationTestClassFalse {
    public String name;
    public boolean hasName;
}
