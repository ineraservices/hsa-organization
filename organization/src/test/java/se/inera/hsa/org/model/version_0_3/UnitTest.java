package se.inera.hsa.org.model.version_0_3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.inera.hsa.clients.CodeSystemClient;
import se.inera.hsa.org.model.unions.MetaData;
import se.inera.hsa.org.model.unions.UnitResponse;
import se.inera.hsa.org.validation.version_0_3.CodeSystemValidator;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorFactory;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Field;
import java.time.ZonedDateTime;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class UnitTest {

    private Validator validator;

    private CodeSystemClient codeSystemClient = mock(CodeSystemClient.class);

    @BeforeEach
    void setup() {
        when(codeSystemClient.validate(anyString(), anyString())).thenReturn(true);
        validator = Validation.byDefaultProvider().configure()
            .constraintValidatorFactory(new CustomConstraintValidatorFactory()).buildValidatorFactory().getValidator();
    }

    @Test
    void testHsaHealthCareProviderValidConstraints() {
        Unit unit = getUnit();
        unit.orgNo = "ORGNO-" + Math.abs(new Random().nextInt());
        unit.hsaHealthCareProvider = true;
        assertTrue(validator.validate(unit).isEmpty());
    }

    @Test
    void testHsaHealthCareProviderInvalidConstraints() {
        Unit unit = getUnit();
        assertTrue(validator.validate(unit).isEmpty());
        unit.hsaHealthCareProvider = true;
        assertFalse(validator.validate(unit).isEmpty());
    }

    @Test
    void testMandatoryAttributesNotNull() {
        se.inera.hsa.org.model.unions.Unit unit = new se.inera.hsa.org.model.unions.Unit();
        unit.hsaIdentity = null;
        unit.name = null;
        assertFalse(validator.validate(unit).isEmpty());
    }

    @Test
    void testMandatoryAttributesNotBlank() {
        se.inera.hsa.org.model.unions.Unit unit = getUnitUnion();
        assertTrue(validator.validate(unit).isEmpty());
        unit.hsaIdentity = "";
        unit.name = "";
        assertFalse(validator.validate(unit).isEmpty());
        unit.hsaIdentity = " ";
        unit.name = " ";
        assertFalse(validator.validate(unit).isEmpty());
    }

    @Test
    void testValidateUnitType() {
        se.inera.hsa.org.model.unions.Unit unit = getUnitUnion();
        assertTrue(validator.validate(unit).isEmpty());
        unit.type = "";
        assertFalse(validator.validate(unit).isEmpty());
    }

    @Test
    void testValidUnitResponse() {
        assertTrue(validator.validate(getUnitResponse()).isEmpty());
    }

    @Test
    void testValidDeletableUnit() {
        Jsonb builder = JsonbBuilder.create();
        DeletableUnit deletableUnit = builder.fromJson(builder.toJson(getUnit()), DeletableUnit.class);
        assertTrue(validator.validate(deletableUnit).isEmpty());
        deletableUnit.hsaHealthCareProvider = true;
        assertFalse(validator.validate(deletableUnit).isEmpty());
    }

    @Test
    void testCodeSystemValidation() {
        Unit unit = getUnit();
        when(codeSystemClient.validate("1.2.752.129.2.2.1.18", "15")).thenReturn(true);
        when(codeSystemClient.validate("1.2.752.129.2.2.1.18", "999")).thenReturn(false);
        assertTrue(validator.validate(unit).isEmpty());
        unit.countyCode = "15";
        assertTrue(validator.validate(unit).isEmpty());
        unit.countyCode = "999";
        assertFalse(validator.validate(unit).isEmpty());
    }

    public static Unit getUnit() {
        Unit unit = new Unit();
        unit.name = "Unit_" + Math.abs(new Random().nextInt());
        unit.type = "Unit";
        unit.hsaIdentity = "HSA_ID_" + Math.abs(new Random().nextInt());
        return unit;
    }

    static se.inera.hsa.org.model.unions.Unit getUnitUnion() {
        se.inera.hsa.org.model.unions.Unit unit = new se.inera.hsa.org.model.unions.Unit();
        unit.name = "Unit_" + Math.abs(new Random().nextInt());
        unit.type = "Unit";
        unit.hsaIdentity = "HSA_ID_" + Math.abs(new Random().nextInt());
        return unit;
    }

    private UnitResponse getUnitResponse() {
        UnitResponse unit = new UnitResponse();
        unit.name = "Unit_" + Math.abs(new Random().nextInt());
        unit.type = "Unit";
        unit.hsaIdentity = "HSA_ID_" + Math.abs(new Random().nextInt());
        unit.metaData = new MetaData(ZonedDateTime.now(),
                UUID.randomUUID().toString(), "path/to/creator", 0, 0,
                "countries/Sverige/counties/A/organizations/B/units/C");
        unit.uuid = UUID.randomUUID().toString();
        return unit;
    }

    class CustomConstraintValidatorFactory implements ConstraintValidatorFactory {

        private ValidatorFactory validator = Validation.buildDefaultValidatorFactory();
        @Override
        public <T extends ConstraintValidator<?, ?>> T getInstance(Class<T> key) {
            if (key == CodeSystemValidator.class) {
                CodeSystemValidator codeSystemValidator = new CodeSystemValidator();
                try {
                    Field clientField = CodeSystemValidator.class.getDeclaredField("codeSystemClient");
                    clientField.setAccessible(true);
                    clientField.set(codeSystemValidator, codeSystemClient);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                //noinspection unchecked
                return (T) codeSystemValidator;
            } else {
                return validator.getConstraintValidatorFactory().getInstance(key);
            }
        }

        @Override
        public void releaseInstance(ConstraintValidator<?, ?> instance) {

        }
    }
}













