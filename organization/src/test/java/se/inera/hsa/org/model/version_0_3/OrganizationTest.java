package se.inera.hsa.org.model.version_0_3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrganizationTest {

    private Validator validator;

    @BeforeEach
    void setup() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    void testHsaHealthCareProviderValidConstraints() {
        Organization organization = getOrganization("hsaHPT", "mail@mail.com", "postalAddress Line", "+468123456789");
        organization.orgNo = "ORGNO-" + Integer.toString(Math.abs(new Random().nextInt()));
        organization.hsaHealthCareProvider = true;
        Set<ConstraintViolation<Organization>> validate = validator.validate(organization);
        StringBuilder sb = new StringBuilder();
        validate.forEach(a -> sb.append(a.getPropertyPath()).append(" ").append(a.getMessage()).append("\n").toString());
        assertTrue(validate.isEmpty(), sb.toString());
    }

    @Test
    void testHsaHealthCareProviderInvalidConstraints() {
        Organization organization = getOrganization("hsaHPT", "mail@mail.com", "postalAddress Line", "+468123456789");
        organization.hsaHealthCareProvider = true;
        assertFalse(validator.validate(organization).isEmpty());
    }

    @Test
    void testMandatoryAttributesNotNull() {
        Organization organization = getOrganization("hsaHPT", "mail@mail.com", "postalAddress Line", "+468123456789");
        organization.hsaIdentity = null;
        organization.name = null;
        assertFalse(validator.validate(organization).isEmpty());
    }

    @Test
    void testMandatoryAttributesNotBlank() {
        Organization organization = getOrganization("hsaHPT", "mail@mail.com", "postalAddress Line", "+468123456789");
        organization.hsaIdentity = "";
        organization.name = "";
        assertFalse(validator.validate(organization).isEmpty());
        organization.hsaIdentity = " ";
        organization.name = " ";
        assertFalse(validator.validate(organization).isEmpty());
    }

    @Test
    void testValidateOrganizationType() {
        Organization organization = getOrganization("hsaHPT", "mail@mail.com", "postalAddress Line", "+468123456789");
        organization.type = "";
        assertFalse(validator.validate(organization).isEmpty());
    }
    
    @Test
    void testValidateEmptyMail() {
        Organization organization = getOrganization();
        organization.type = "";
        organization.hsaHpt = "asd";
        organization.postalAddress = new PostalAddress();
        organization.postalAddress.addressLine = new ArrayList<>();
        organization.postalAddress.addressLine.add("addressLine");
        assertFalse(validator.validate(organization).isEmpty());
    }
    
    @Test
    void testValidateEmptyHsaHPT() {
        Organization organization = getOrganization();
        organization.type = "";
        organization.mail = "mail@mail.com";
        organization.postalAddress = new PostalAddress();
        organization.postalAddress.addressLine = new ArrayList<>();
        organization.postalAddress.addressLine.add("addressLine");
        assertFalse(validator.validate(organization).isEmpty());
    }
    
    @Test
    void testValidateEmptPostalAddress() {
        Organization organization = getOrganization();
        organization.type = "";
        organization.hsaHpt = "asd";
        organization.mail = "mail@mail.com";
        assertFalse(validator.validate(organization).isEmpty());
    }

    private Organization getOrganization() {
        Organization organization = new Organization();
        organization.name = "Organization_" + Math.abs(new Random().nextInt());
        organization.type = "Organization";
        organization.hsaIdentity = "HSA_ID_" + Integer.toString(Math.abs(new Random().nextInt()));
        return organization;
    }
    
    private Organization getOrganization(String hsaHPT, String mail, String postalAddress, String telephonNumber) {
        Organization organization = getOrganization();
        organization.mail=mail;
        organization.hsaHpt = hsaHPT;
        if ( telephonNumber != null && telephonNumber.length()>0) {
            organization.telephoneNumber = new ArrayList<>();
            organization.telephoneNumber.add(telephonNumber);
        }
        if( postalAddress != null && postalAddress.length() > 0) {
            organization.postalAddress = new PostalAddress();
            organization.postalAddress.addressLine = new ArrayList<>();
            organization.postalAddress.addressLine.add(postalAddress);
        }
        return organization;
    }

}