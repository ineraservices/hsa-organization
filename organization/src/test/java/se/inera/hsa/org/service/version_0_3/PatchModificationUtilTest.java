package se.inera.hsa.org.service.version_0_3;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import se.inera.hsa.org.model.version_0_3.Function;
import se.inera.hsa.org.model.version_0_3.Hours;
import se.inera.hsa.org.model.version_0_3.PostalAddress;
import se.inera.hsa.org.model.version_0_3.TimeSpan;
import se.inera.hsa.org.model.version_0_5.Unit;
import se.inera.hsa.org.validation.version_0_3.PatchModification;

class PatchModificationUtilTest {

    PatchModificationUtil util;
    
    @BeforeEach
    void init() {
        util = new PatchModificationUtil();
    }
    
    @Test
    void removeAllValuesFromMultivalueAttributeFunction() throws Exception {
     // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1", "value2");
//        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "DELETE"));

        util.handlePatchModification(function, mods);
        
        assertNull("Attribute should be null", function.hsaVisitingRuleAge);
    }
    
    @Test
    void removeSingleValueFromMultivalueAttributeFunction() throws Exception {
     // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1", "value2");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "DELETE","value2"));
        
        util.handlePatchModification(function, mods);
        
        List<String> expectedLines = Collections.singletonList("value1");
        
        assertLinesMatch(expectedLines, function.hsaVisitingRuleAge);
    }
    
    @Test
    void removeSingleValueFromSinglevalueAttributeFunction() throws Exception {
     // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("description", "value1");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringModification("description", "DELETE","value1"));
        
        util.handlePatchModification(function, mods);
        
        assertNull(function.description);
    }
    
    @Test
    void removeSingleValueFromSinglevalueAttributeNotExistFunction() throws Exception {
     // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("description", "value1");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringModification("description", "DELETE","value2"));
        
        Exception exception = assertThrows(Exception.class, () -> util.handlePatchModification(function, mods));
    }
    
    @Test
    void removeSingleValueFromSinglevalueAttributeNoValueBeforeFunction() throws Exception {
     // Ta bort list<string> värde (ett värde) OK
        Function function = new Function();
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringModification("description", "DELETE","value2"));
        
        Exception exception = assertThrows(Exception.class, () -> util.handlePatchModification(function, mods));
    }
    
    @Test
    void removeSingleValueFromMultivalueAttributeFunction_null() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "DELETE","value1"));
        
        util.handlePatchModification(function, mods);
        
        
        assertNull("Attribute should be null", function.hsaVisitingRuleAge);
    }
    
    @Test
    void removeSingleValueFromMultivalueAttributeNotExistingFunction() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1", "value2");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "DELETE","value3"));
        
        Exception exception = assertThrows(Exception.class, () -> util.handlePatchModification(function, mods));
    }
    
    @Test
    void removeMultipleValuesFromMultivalueAttributeFunction_null() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1", "value2");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "DELETE","value1", "value2"));
        
        util.handlePatchModification(function, mods);
        
        assertNull("Attribute should be null", function.hsaVisitingRuleAge);
    }
    
    @Test
    void removeMultipleValuesFromMultivalueAttributeFunction() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1", "value2", "value3");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "DELETE","value1", "value2"));
        
        util.handlePatchModification(function, mods);
        
        List<String> expectedLines = Collections.singletonList("value3");
        
        assertLinesMatch(expectedLines, function.hsaVisitingRuleAge);
    }
    
    @Test
    void AddSingleValueAttributeEmptyBeforeFunction() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = new Function();
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringModification("description", "ADD","value1"));
        
        util.handlePatchModification(function, mods);
        
        assertEquals("value1", function.description);
    }
    
    @Test
    void AddSingleValueAttributeValueAlreadyExistFunction() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("description", "value2");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringModification("description", "ADD","value1"));
        
        Exception exception = assertThrows(Exception.class, () -> util.handlePatchModification(function, mods));
    }
    
    @Test
    void AddSingleMultivalueAttributeFunction() throws Exception {
     // Ta bort list<string> värde (ett värde) OK
        Function function = new Function();
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "ADD","value1"));
        
        util.handlePatchModification(function, mods);
        
        List<String> expectedLines = Collections.singletonList("value1");
        
        assertLinesMatch(expectedLines, function.hsaVisitingRuleAge);
    }
    
    @Test
    void AddSingleMultivalueAttributeOtherValuesExistFunction() throws Exception {
     // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "ADD","value2"));
        
        util.handlePatchModification(function, mods);
        
        List<String> expectedLines = Arrays.asList("value1", "value2");
        
        assertLinesMatch(expectedLines, function.hsaVisitingRuleAge);
    }
    
    @Test
    void AddSingleMultivalueAttributeValueAlreadyExistFunction() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "ADD","value1"));
        
        Exception exception = assertThrows(Exception.class, () -> util.handlePatchModification(function, mods));
    }
    
    @Test
    void addMultipleMultivalueAttributeFunction() throws Exception {
     // Ta bort list<string> värde (ett värde) OK
        Function function = new Function();
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "ADD","value1", "value2"));
        
        util.handlePatchModification(function, mods);
        
        List<String> expectedLines = Arrays.asList("value1", "value2");
        
        assertLinesMatch(expectedLines, function.hsaVisitingRuleAge);
    }
    
    @Test
    void addMultipleMultivalueAttributeWhenSomeAlreadyExistFunction() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "ADD","value1", "value2"));
        
        Exception exception = assertThrows(Exception.class, () -> util.handlePatchModification(function, mods));
    }
    
    @Test
    void changeSeveralAttributesFunction() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Function function = createFunction("hsaVisitingRuleAge", "value1", "value2");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "DELETE","value1"));
        mods.add(createPatchStringModification("description", "ADD", "beskrivning"));
        
        util.handlePatchModification(function, mods);
        
        List<String> expectedLines = Arrays.asList("value2");
        
        assertAll("user", 
                () -> assertLinesMatch(expectedLines, function.hsaVisitingRuleAge),
                () -> assertEquals("beskrivning", function.description));
    }
    
    @Test
    void changeSeveralAttributesUnit() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Unit unit = createUnit("hsaVisitingRuleAge", "value1", "value2");
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchStringListModification("hsaVisitingRuleAge", "DELETE","value1"));
        mods.add(createPatchStringModification("description", "ADD", "beskrivning"));
        
        util.handlePatchModification(unit, mods);
        
        List<String> expectedLines = Arrays.asList("value2");
        
        assertAll("user", 
                () -> assertLinesMatch(expectedLines, unit.hsaVisitingRuleAge),
                () -> assertEquals("beskrivning", unit.description));
    }
    
    @Test
    void addPostalAddress() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Unit unit = new Unit();
        
        PostalAddress p = new PostalAddress();
        p.addressLine = new ArrayList<>(Arrays.asList("Testgatan 41", "123 45 Staden"));
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchPostalAddressModification("postalAddress", "ADD",p));
        
        util.handlePatchModification(unit, mods);
        
        
        assertAll("user", 
                () -> assertEquals(p, unit.postalAddress));
    }
    
    @Test
    void addHsaHealthCareUnit() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Unit unit1 = new Unit();
        Unit unit2 = new Unit();
        
        
        util.handlePatchModification(unit1, Arrays.asList(createPatchBooleanModification("hsaHealthCareUnit", "ADD", true)));
        
        
        assertAll("user", 
                () -> assertTrue(unit1.hsaHealthCareUnit),
                () -> assertFalse(unit2.hsaHealthCareUnit)
                );
    }
    @Test
    void deleteHsaHealthCareUnit() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Unit unit1 = new Unit();
        unit1.hsaHealthCareUnit=true;
        Unit unit2 = new Unit();
        unit2.hsaHealthCareUnit=true;
        Unit unit3 = new Unit();
        unit3.hsaHealthCareUnit=true;
        
        
        util.handlePatchModification(unit1, Arrays.asList(createPatchBooleanModification("hsaHealthCareUnit", "DELETE", true)));
        util.handlePatchModification(unit2, Arrays.asList(createPatchBooleanModification("hsaHealthCareUnit", "ADD", false)));
        util.handlePatchModification(unit3, Arrays.asList(createPatchDeleteBooleanModification("hsaHealthCareUnit")));
        
        assertAll("user", 
                () -> assertFalse(unit1.hsaHealthCareUnit),
                () -> assertFalse(unit2.hsaHealthCareUnit),
                () -> assertFalse(unit3.hsaHealthCareUnit)
                );
    }
    
    @Test
    void deleteHsaHealthCareProvidert() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Unit unit1 = new Unit();
        unit1.hsaHealthCareProvider=true;
        Unit unit2 = new Unit();
        unit2.hsaHealthCareProvider=true;
        Unit unit3 = new Unit();
        unit3.hsaHealthCareProvider=true;
        
        
        util.handlePatchModification(unit1, Arrays.asList(createPatchBooleanModification("hsaHealthCareProvider", "DELETE", true)));
        util.handlePatchModification(unit2, Arrays.asList(createPatchBooleanModification("hsaHealthCareProvider", "ADD", false)));
        util.handlePatchModification(unit3, Arrays.asList(createPatchDeleteBooleanModification("hsaHealthCareProvider")));
        
        assertAll("user", 
                () -> assertFalse(unit1.hsaHealthCareProvider),
                () -> assertFalse(unit2.hsaHealthCareProvider),
                () -> assertFalse(unit3.hsaHealthCareProvider)
                );
    }
    
    @Test
    void deletePostalAddress() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Unit unit = new Unit();
        
        PostalAddress postalAddress = new PostalAddress();
        postalAddress.addressLine = new ArrayList<>(Arrays.asList("Testgatan 41", "123 45 Staden"));
        unit.postalAddress = postalAddress;
        
        PostalAddress p = new PostalAddress();
        p.addressLine = new ArrayList<>(Arrays.asList("Testgatan 41", "123 45 Staden"));
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchPostalAddressModification("postalAddress", "DELETE",p));
        
        util.handlePatchModification(unit, mods);
        
        
        assertAll("user", 
                () -> assertNull(unit.postalAddress));
    }
    @Test
    void deleteTelephoneHours() throws Exception {
        // Ta bort list<string> värde (ett värde) OK
        Unit unit = new Unit();
        
        Hours telephoneHours = new Hours();
        TimeSpan timeSpan = new TimeSpan();
        timeSpan.fromDay="1";
        timeSpan.toDay="5";
        timeSpan.fromTime2="08:00";
        timeSpan.toTime2="10:00";
        telephoneHours.timeSpan = timeSpan;
        
        unit.telephoneHours = new ArrayList<>(Arrays.asList(telephoneHours));
        
        Hours telephoneHours1 = new Hours();
        TimeSpan timeSpan1 = new TimeSpan();
        timeSpan1.fromDay="1";
        timeSpan1.toDay="5";
        timeSpan1.fromTime2="08:00";
        timeSpan1.toTime2="10:00";
        telephoneHours1.timeSpan = timeSpan1;
        
        List<PatchModification> mods = new ArrayList<>();
        mods.add(createPatchHoursListModification("telephoneHours", "DELETE",telephoneHours1));
        
        util.handlePatchModification(unit, mods);
        
        
        assertAll("user", 
                () -> assertNull(unit.telephoneHours));
    }
    
    //singlevalue attribut
    //Lägg till värde
    //Lägg till ytterligare värde -> exception
    //Ta bort värde 
    //Ta bort värde som inte finns -> exception
    //Ta bort värde när inte har något värde -> exception
    
    //multivalue
    // lägg till värde 
    // lägg till värde när finns andra värden
    // lägg till flera värden
    // lägg till flera värden när något av värdena finsn redan -> exception
    // lägg till samma värde -> exception
    // Ta bort värde
    // Ta bort Alla värden
    //Ta bort värde som inte finns
    // Ta ett värde när andra värden finns kvar
    
    Function createFunction(String attributeName, String... values) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Function function = new Function();
        Field field = function.getClass().getDeclaredField(attributeName);
        if ( field.getType().equals(List.class)) {
            field.set(function, new ArrayList<>(Arrays.asList(values)));
        } else if ( field.getType().equals(String.class)) {
            field.set(function,values[0]);
        } else {
            
        }
        return function;
     }
    
    Unit createUnit(String attributeName, String... values) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Unit unit = new Unit();
        Field field = unit.getClass().getDeclaredField(attributeName);
        if ( field.getType().equals(List.class)) {
            field.set(unit, new ArrayList<>(Arrays.asList(values)));
        } else if ( field.getType().equals(String.class)) {
            field.set(unit,values[0]);
        } else {
            
        }
        return unit;
    }
    
    PatchModification createPatchPostalAddressModification(String attributeName, String modificationType, PostalAddress value) {
        PatchModification p = new PatchModification();
        
        p.setAttributeName(attributeName);
        p.setModificationType(modificationType);
        p.setPostalAddressValue(value); 
        return p;
    }
    PatchModification createPatchStringModification(String attributeName, String modificationType, String value) {
        PatchModification p = new PatchModification();
        
        p.setAttributeName(attributeName);
        p.setModificationType(modificationType);
        p.setStringValue(value);
        return p;
    }
    
    PatchModification createPatchBooleanModification(String attributeName, String modificationType, boolean value) {
        PatchModification p = new PatchModification();
        
        p.setAttributeName(attributeName);
        p.setModificationType(modificationType);
        p.setBooleanValue(value);
        return p;
    }
    PatchModification createPatchDeleteBooleanModification(String attributeName) {
        PatchModification p = new PatchModification();
        
        p.setAttributeName(attributeName);
        p.setModificationType("DELETE");
        return p;
    }
    
    PatchModification createPatchStringListModification(String attributeName, String modificationType, String... values) {
        PatchModification p = new PatchModification();
        
        p.setAttributeName(attributeName);
        p.setModificationType(modificationType);
        if ( values != null && values.length>0) {
            p.setStringValues(new ArrayList<>(Arrays.asList(values)));
        }
        return p;
    }
    
    PatchModification createPatchHoursListModification(String attributeName, String modificationType, Hours... values) {
        PatchModification p = new PatchModification();
        
        p.setAttributeName(attributeName);
        p.setModificationType(modificationType);
//        if ( values != null && values.length == 1 ) {
//            p.attributValue = values[0];
//        } else if ( values != null && values.length > 1 ) {
        p.setHoursValues(new ArrayList<>(Arrays.asList(values)));
//        }
        return p;
    }
    
}
