package se.inera.hsa.lettuce.redisgraph.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class GraphQueryResultTest {

    @DisplayName("Verify GraphQueryResult with an empty result set")
    @Test
    void given_empty_response() {
        List<Object> resultSet = buildResponse(new ArrayList<>(), new ArrayList<>(), "Query internal execution time: 0.048650 milliseconds");
        GraphQueryResult result = new GraphQueryResult(resultSet);
        assertThat(result.getRecords().size()).isEqualTo(0);
    }

    @DisplayName("Verify GraphQueryResult with keys and values")
    @Test
    void given_keys_and_values() {
        List<String> header =  Arrays.asList("u.key", "u.name");
        List<List<String>> records = new ArrayList<>();
        records.add(Arrays.asList("key2", "VC BRB"));
        records.add(Arrays.asList("NULL", "VC something"));
        records.add(Arrays.asList("key4", "VC something2"));
        records.add(Arrays.asList("key2", "VC BRB"));
        List<Object> resultSet = buildResponse(header, records, "Query internal execution time: 0.048650 milliseconds");
        List<String> expectedKeys = Arrays.asList("u.key", "u.name");

        List<Record> expectedValues = new ArrayList<>();
        expectedValues.add(new Record(header, Arrays.asList("key2", "VC BRB")));
        expectedValues.add(new Record(header, Arrays.asList("NULL", "VC something")));
        expectedValues.add(new Record(header, Arrays.asList("key4", "VC something2")));
        expectedValues.add(new Record(header, Arrays.asList("key2", "VC BRB")));

        GraphQueryResult result = new GraphQueryResult(resultSet);

        assertThat(result.getRecords()).isEqualToComparingFieldByFieldRecursively(expectedValues);
    }

    private List<Object> buildResponse(List<String> header, List<List<String>> records, String statistics) {
        List<Object> response = new ArrayList<>();
        List<List<String>> first = new ArrayList<>();
        first.add(header);
        if(records.size() > 0) {
            first.addAll(records);
        }
        response.add(first);
        List<String> second = new ArrayList<>();
        second.add(statistics);
        response.add(second);
        return response;
    }
}
