package se.inera.hsa.lettuce.redisgraph.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

// LinkedHashMap is used so the query string will be easier to compare against, ordinary hashmap is fine.
public class EdgeTest {

    @Test
    @DisplayName("Test Edge.toString with 2 properties")
    void test_Edge() {
        String expected = "-[:owned_by{code:'se',name:'Sweden'}]->";
        Map<String, String> properties = new HashMap<>();
        properties.put("code", "se");
        properties.put("name", "Sweden");

        Edge e = Edge.start()
                .relation("owned_by").properties(properties)
                .build();

        assertThat(e.toString()).isEqualTo(expected);
    }
}
