package se.inera.hsa.lettuce.redisgraph.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

// LinkedHashMap is used so the query string will be easier to compare against, ordinary hashmap is fine.
public class NodeTest {

    @Test
    @DisplayName("Verify Node.toString with alias and label")
    void test_Node() {
        String expected = "(c:country)";

        Map<String, String> properties = new LinkedHashMap<>();
        Node n = Node.start()
                .alias("c").label("country").properties(properties)
                .build();
        assertThat(n.toString()).isEqualTo(expected);
    }

    @Test
    @DisplayName("Verify Node.toString with no alias and with label plus 4 properties")
    void test_Node_with_properties() {
        String expected = "(:person{name:'John Doe',age:'33',gender:'male',status:'single'})";
        Map<String, String> properties = new LinkedHashMap<>();
        properties.put("name", "John Doe");
        properties.put("age", "33");
        properties.put("gender", "male");
        properties.put("status", "single");
        Node n = Node.start()
                .alias("").label("person").properties(properties)
                .build();
        assertThat(n.toString()).isEqualTo(expected);
    }
}
