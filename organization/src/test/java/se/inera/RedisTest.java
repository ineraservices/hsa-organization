package se.inera;

import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.dynamic.RedisCommandFactory;
import se.inera.hsa.exception.BadRequestException;
import se.inera.hsa.exception.ErrorCodes;
import se.inera.hsa.org.mappers.UnitMapper;
import se.inera.hsa.org.model.unions.MetaData;
import se.inera.hsa.org.model.unions.UnitPersistence;
import se.inera.hsa.org.model.unions.UnitResponse;
import se.inera.hsa.org.model.version_0_3.*;
import se.inera.hsa.org.service.version_0_3.HSARedisReadAPI;
import se.inera.hsa.org.service.version_0_3.IndexAttributeMapper;
import se.inera.hsa.org.service.version_0_3.OrganizationGraph;
import se.inera.hsa.jsonb.JsonbUtil;
import se.inera.hsa.lettuce.RedisClientUtil;
import se.inera.hsa.lettuce.commands.*;

import javax.ws.rs.NotFoundException;
import java.time.ZonedDateTime;
import java.util.*;

import static java.lang.String.format;
import static se.inera.hsa.org.service.version_0_3.HSARedisReadAPI.*;

/**
 * Redis test class, made for testing stuff right towards a local redis.
 * <p>
 * please start redis in docker (or u will not be able to connect)
 * in repo hsa-docker
 * docker run -p 6379:6379 kdaham/hsa-redis
 */
public class RedisTest {
    public static void main(String[] args) {
        StatefulRedisConnection<String, String> connection = null;
        RedisHSAWriteCommands persistCommands;
        RedisHSATransactionCommands redisHSATransactionCommands;
        try {
            connection = RedisClientUtil.getClient("localhost").connect();
            RedisCommandFactory factory = new RedisCommandFactory(connection);
            persistCommands = new RedisHSAWriteCommands(factory);
            redisHSATransactionCommands = new RedisHSATransactionCommands(connection.sync());
            OrganizationGraph graph = new OrganizationGraph(factory, GRAPH_ROOT_ID, GRAPH_NAME);

            UnitPersistence unitPersistence1 = new UnitPersistence();

            // add metadata
            unitPersistence1.creatorUUID = "not_yet_implemented";
            unitPersistence1.createTimestamp = ZonedDateTime.now();
            unitPersistence1.uuid = UUID.randomUUID().toString();

            // add unit 1
            unitPersistence1.unit = UnitMapper.mapToUnion(UnitTest.getUnit());
            String parentPath = "countries/Sverige/counties/Dalarnas län/organizations/Landstinget Dalarna";
            String sourcePath = parentPath + "/units/" + unitPersistence1.unit.name;

            create(graph, redisHSATransactionCommands, persistCommands, parentPath, sourcePath, unitPersistence1);

            //add unit 2
            UnitPersistence unitPersistence2 = new UnitPersistence();
            unitPersistence2.unit = UnitMapper.mapToUnion(UnitTest.getUnit());
            // add metadata
            unitPersistence2.creatorUUID = "not_yet_implemented";
            unitPersistence2.createTimestamp = ZonedDateTime.now();
            unitPersistence2.uuid = UUID.randomUUID().toString();
            unitPersistence2.unit = UnitMapper.mapToUnion(UnitTest.getUnit());
            String targetPath = parentPath + "/units/" + unitPersistence2.unit.name + "/units/" + unitPersistence1.unit.name;
            create(graph, redisHSATransactionCommands, persistCommands, parentPath, targetPath, unitPersistence2);


            Map<String, String> indexFields = new HashMap<>();
            indexFields.put("uuid", unitPersistence1.uuid);
            indexFields.put("name", unitPersistence1.unit.name);
            indexFields.put("modifierUUID", unitPersistence1.modifierUUID);
            unitPersistence1.modifyTimestamp = ZonedDateTime.now();
            indexFields.put("modifyTimestamp", unitPersistence1.modifyTimestamp.toString());
            System.out.println("source path" + sourcePath + " target path " + targetPath);
            move(graph, redisHSATransactionCommands, persistCommands, JsonbUtil.jsonb().toJson(unitPersistence1, UnitPersistence.class), indexFields, UNITS_RELATION, sourcePath, targetPath);

            delete(graph, redisHSATransactionCommands, persistCommands, targetPath);
            delete(graph, redisHSATransactionCommands, persistCommands, sourcePath);

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    private static void move(OrganizationGraph graph, RedisHSATransactionCommands transactionCommands, RedisHSAWriteCommands redisCommands, String jsonBlob, Map<String, String> indexFields, String relation, String sourcePath, String targetPath) {
        transactionCommands.watch(sourcePath);
        transactionCommands.watch(targetPath);

        // We have identified some things that must be checked so that the transaction will be able to run
        String sourceId = graph.getIdFromPath(sourcePath);
        if (sourceId == null) {
            String message = "no resource found for id: " + sourceId + ", path: \"" + sourcePath + "\"";
            System.out.println(message);
            throw new NotFoundException(message);
        }
        String parentTargetPath = targetPath.substring(0, targetPath.lastIndexOf("/units"));
        String parentTargetId = graph.getIdFromPath(parentTargetPath);
        if (parentTargetId == null) {
            String message = "no resource found for parent path \"" + parentTargetPath + "\"";
            System.out.println(message);
            throw new NotFoundException(message);
        }
        String parentSourcePath = sourcePath.substring(0, sourcePath.lastIndexOf("/units"));
        String parentSourceId = graph.getIdFromPath(parentSourcePath);

        assertMoveOk(graph, graph.getIdFromPath(targetPath), parentTargetId);

        transactionCommands.multi();
        // do in block
        redisCommands.set(sourcePath, UUID.randomUUID().toString());
        redisCommands.set(targetPath, UUID.randomUUID().toString());

        redisCommands.jsonSetIfExists(indexFields.get("uuid"), jsonBlob);
        redisCommands.updateNode(indexFields.get("uuid"), indexFields.get("name"), relation, parentTargetId, parentSourceId);
        redisCommands.ftUpdate(indexFields.get("uuid"), INDEX_NAME, ORGANIZATION_INDEX_ID_PREFIX, INDEX_SCORE, indexFields);
        redisCommands.ftUpdate(indexFields.get("uuid"), INDEX_SIMPLE_SEARCH_NAME, SIMPLE_SEARCH_JSON_ID_PREFIX, INDEX_SCORE, indexFields);

        // end block

        transactionCommands.exec();

        System.out.println("DONE! MOVE");
    }

    private static void assertMoveOk(OrganizationGraph graph, String targetPathId, String parentTargetId) {
        List<String> allowedParentTypes = Arrays.asList("Organization", "Unit");
        // verify parentType is ok and that there exists no other object with the same path as requested
        if (graph.getType(parentTargetId, allowedParentTypes) == null) {
            String message = "parent object does not exist or if of wrong type (not one of " +
                    String.join(",", allowedParentTypes) + ")";
            System.out.println(message);
            throw new BadRequestException(message, ErrorCodes.NO_SUCH_OBJECT);
        }
        if (targetPathId != null) {
            String message = "entry already exists + " + graph.getPathFromId(targetPathId);
            System.out.println(message);
            throw new BadRequestException(message, ErrorCodes.ENTRY_ALREADY_EXISTS);
        }
    }


    private static void create(OrganizationGraph graph, RedisHSATransactionCommands transactionCommands, RedisHSAWriteCommands redisCommands, String parentPath, String path, UnitPersistence unitPersistence) {

        // optimistically locks redis for the node we are adding. If this key is changed
        // during the transaction it will not do the transaction.
        transactionCommands.watch(path);

        // We have identified some things that must be checked so that the transaction will be able to run
        // 1. Does the resource already exist in the graph?
        if ((graph.getIdFromPath(path)) != null) {
            transactionCommands.unwatch();
            String message = "resource already exists for path \"" + path + "\"";
            System.out.println(message);
            throw new BadRequestException(message, ErrorCodes.ENTRY_ALREADY_EXISTS);
        }

        // 2. Does the correct parent type exist in the graph?
        String parentId = graph.getIdFromPath(parentPath);
        if (parentId == null) {
            transactionCommands.unwatch();
            String message = "no resource found for parent path \"" + parentPath + "\"";
            System.out.println(message);
            throw new NotFoundException(message);
        }

        List<String> allowedParentTypes = new ArrayList<>();
        allowedParentTypes.add("Organization");
        allowedParentTypes.add("Unit");
        String parentType = graph.getType(parentId, allowedParentTypes);
        if (parentType == null) {
            transactionCommands.unwatch();
            String message = "Parent must exist and be of type " + allowedParentTypes.toString();
            System.out.println(message);
            throw new BadRequestException(message);
        }

        // 3. check that json-blob does't exist in redisjson
        //TODO

        // 4. check that "index name" exists in redisSearch and that there is not
        // already an index for the document
        //TODO

        // starts transaction
        transactionCommands.multi();

        // writes over the watched key. This will make other transactions trying
        // to change the database at the same time fail. (only one will succeed)

        redisCommands.set(path, unitPersistence.uuid);

        addRelation(
                redisCommands,
                parentId,
                parentType,
                "HAS_UNITS",
                "Unit",
                unitPersistence.uuid,
                unitPersistence.unit.name);

        redisCommands.jsonSetIfNotExists(
                unitPersistence.uuid,
                JsonbUtil.jsonb().toJson(unitPersistence.unit));

        redisCommands.ftAdd(
                unitPersistence.uuid,
                INDEX_NAME,
                ORGANIZATION_INDEX_ID_PREFIX,
                HSARedisReadAPI.INDEX_SCORE,
                mapAttributes(unitPersistence));
        try {
            // executes transaction
            transactionCommands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        transactionCommands.unwatch();
        System.out.println("DONE CREATE!");
    }

    static void delete(OrganizationGraph graph, RedisHSATransactionCommands transactionCommands, RedisHSAWriteCommands persistCommands, String path) {

        transactionCommands.watch(path);

        String id = graph.getIdFromPath(path);
        if (id == null) {
            String message = "DELETE : no resource found for id : " + id + " with path \"" + path + "\"";
            System.out.println(message);
            throw new NotFoundException(message);
        }
        // We have identified some things that must be checked so that the transaction will be able to run
        // Observe: Checking if we ALLOWED to remove it (some objects are not allowed to be deleted) should not
        // be done in this layer, but higher up.

        // 1. does it exist in graph and is it a leaf, only leafs are allowed to be deleted
        int numAllSubordinates = graph.getNumAllSubordinates(id);
        if (numAllSubordinates > 0) {
            String message = "DELETE : resource has " + numAllSubordinates + " subordinates, removal not allowed";
            System.out.println(message);
            throw new BadRequestException(message, ErrorCodes.NOT_ALLOWED_ON_NON_LEAF);
        }

        // 2. does it exist in redisjson
        if (persistCommands.noJsonExist(id)) {
            transactionCommands.unwatch();
            throw new IllegalStateException("This object is in a wonky state! missing jsonblob for id: " + id);
        }

        // 3. does it exist in any index
        if (persistCommands.noIndexExists(id, INDEX_NAME, ORGANIZATION_INDEX_ID_PREFIX) ||
                persistCommands.noIndexExists(id, INDEX_SIMPLE_SEARCH_NAME, SIMPLE_SEARCH_JSON_ID_PREFIX)) {
            transactionCommands.unwatch();
            throw new IllegalStateException("This object is in a wonky state! missing index for id: " + id);
        }

        transactionCommands.multi();

        persistCommands.set(path, UUID.randomUUID().toString());

        String query = format("MATCH(node) WHERE node.id='%s' DELETE node", id);
        persistCommands.graphQuery(query);
        persistCommands.jsonDel(id);
        persistCommands.ftDel(id, INDEX_NAME, ORGANIZATION_INDEX_ID_PREFIX);
        //persistCommands.ftDel(id, INDEX_SIMPLE_SEARCH_NAME, SIMPLE_SEARCH_JSON_ID_PREFIX);

        try {
            // executes transaction
            transactionCommands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        transactionCommands.unwatch();
        System.out.println("DONE DELETE!");
    }

    private static void update() {

    }

    private static void addRelation(RedisHSAWriteCommands persistCommands,
                                    String parentId,
                                    String parentType,
                                    String relationType,
                                    String childType,
                                    String childId,
                                    String childName) {

        String addQuery = String.format("MATCH (a:%s {id:'%s'}) " +
                        "CREATE (a)-[:%s]->(:%s {" +
                        "id:'%s'," +
                        "name:'%s'," +
                        "type:'%s'," +
                        "pid:'%s'," +
                        "ptype:'%s'," +
                        "rel:'%s'})",
                parentType,
                parentId,
                relationType,
                childType,
                childId,
                childName,
                childType,
                parentId,
                parentType,
                relationType);

        persistCommands.graphQuery(addQuery);
    }

    private static Map<String, String> mapAttributes(UnitPersistence unitPersistence) {

        // fields map
        Map<String, String> indexFields = IndexAttributeMapper.mapAttributes(unitPersistence);
        indexFields.putAll(IndexAttributeMapper.mapAttributes(unitPersistence.unit));
        return indexFields;
    }

    static UnitResponse toUnitResponse(UnitPersistence unitPersistence, OrganizationGraph graph) {
        UnitResponse unitResponse = null;

        try {
            unitResponse = JsonbUtil.jsonb()
                    .fromJson(JsonbUtil.jsonb().toJson(unitPersistence.unit), UnitResponse.class);
            unitResponse.uuid = unitPersistence.uuid;
            MetaData metaData = new MetaData(unitPersistence.createTimestamp, unitPersistence.creatorUUID,
                    "countries/Sverige/organizations/Värmlands län/employees/admin", graph.getNumImmediateSubordinates(unitPersistence.uuid),
                    graph.getNumAllSubordinates(unitPersistence.uuid),
                    graph.getPathFromId(unitPersistence.uuid));
            if (unitPersistence.modifyTimestamp != null && unitPersistence.modifierUUID != null) {
                metaData.modifierPath = "countries/Sverige/organizations/Värmlands län/employees/admin";
                metaData.modifierUUID = unitPersistence.modifierUUID;
                metaData.modifyTimestamp = unitPersistence.modifyTimestamp;
            }
            unitResponse.metaData = metaData;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unitResponse;
    }
}
