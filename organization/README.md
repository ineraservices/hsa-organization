# Known errors
target test will fail if you have old gradle version. update gradle to at least v. 4.6

# Tips and Tricks

Add some nice bash alias to .bashrc:

    alias docker_stop_all='docker stop $(docker ps -a -q)'
    alias docker_rm_all='docker rm $(docker ps -a -q)'

# Start environment

Start/stop/restart the environment with docker-compose

    docker-compose up -d
    docker-compose stop
    docker-compose restart

# Build

$ gradle build

# Deploy war-file to a running instance 

$ gradle deploy

# Generate API documentation

$ gradle enunciate

The generated documentation can be found under doc/api, open with your fav browser

$ open doc/api/index.html


# Persist a sample org

From file

    curl -H "Content-Type: application/json" http://127.0.0.1:9080/organization/resources/opendata --upload-file SE2321000180-4733.json

Persist organization using PUT method and raw data as json. Use curl or other REST Client    

    curl -X PUT -H "Content-Type: application/json" -d '<ORGANIZATION JSON HERE>' http://127.0.0.1:9080/organization/resources/


# Read the resource

Get all HsaId's

    curl -v http://127.0.0.1:9080/organization/resources/hsaIds

Get organization by HsaId 

    curl -v http://127.0.0.1:9080/organization/resources/byHsaId/{hsaId}

Delete organization by HsaId

    curl -v -X DELETE http://127.0.0.1:9080/organization/resources/byHsaId/{hsaId}

Delete organization by DN

    curl -v -X DELETE http://127.0.0.1:9080/organization/resources/byDn/{dn}

Add county example
curl -X PUT -H "Content-Type: application/json" http://127.0.0.1:9080/organization/resources -d '{"countyCode":"20","countyName":"Dalarnas län","dn":"l=Dalarnas län,c=SE","l":"Dalarnas län","hsaIdentity":"TEST123456789-0001","objectClass":["HSALocalityExtension","locality","top"]}'

## view logs
```
$ docker logs -f hsa-organization
```

# Link to hsa information
https://www.inera.se/kundservice/dokument-och-lankar/tjanster/hsa/


# Working with redisgraph
Getting redisgraph work when adding data has proven a tough task.
The adding of graph data is commented out in OrgProvider since it doesn't quite work as wanted yet, 
but code is checked in as a starting point for coming work.
It has been noticed that in order to make redisgraph accept
queries from org api some initial data must be added using redis-cli:
* GRAPH.QUERY graph:country:se "CREATE (:country {hsaid:'country'})"
* GRAPH.QUERY graph:country:se "MATCH (a:country) WHERE a.hsaid = 'country' CREATE (b:county {hsaid:'county'})-[childto]->(a)"
* GRAPH.QUERY graph:country:se "MATCH (a:county) WHERE a.hsaid = 'county' CREATE (b:organization {hsaid:'organization'})-[childto]->(a)"
* GRAPH.QUERY graph:country:se "MATCH (a:organization) WHERE a.hsaid = 'organization' CREATE (b:unit {hsaid:'unit'})-[childto]->(a)"
* GRAPH.QUERY graph:country:se "MATCH (a:unit) WHERE a.hsaid = 'unit' CREATE (b:unit {hsaid:'subunit'})-[childto]->(a)"
* GRAPH.QUERY graph:country:se "MATCH (a:unit) WHERE a.hsaid = 'unit' CREATE (b:person {hsaid:'person'})-[childto]->(a)"
The structure and content of the data added is by no means final format. Just something used to get started..
